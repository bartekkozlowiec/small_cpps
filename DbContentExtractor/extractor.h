#ifndef EXTRACTOR_H
#define EXTRACTOR_H

#include <QObject>
#include <QStringList>

#include <filesystem>

namespace fs = std::filesystem;

class Extractor : public QObject
{
    Q_OBJECT

    QStringList tokenList;
    QString srcDir, dstDir;

    bool loopCopyDir( const fs::path& );
    bool loopCopyFile( const fs::path& );
    bool tokensPresentIn( const QString& );

signals:
    void extractionSucceeded( QString );
    void extractionFailed( QString );

public slots:
    void setTokens( const QString& );
    void setSrcDir( const QString& dir ) { srcDir = dir; }
    void setDstDir( const QString& dir ) { dstDir = dir; }
    void doDirExtraction();
    void doFileExtraction();
};

#endif // EXTRACTOR_H
