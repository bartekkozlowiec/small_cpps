#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QThread>
#include <QTimer>

class Widget : public QWidget
{
    Q_OBJECT
    QThread extractorThread;

    QVBoxLayout* mainLayout;
    QHBoxLayout* fnameLayout;
    QGridLayout* dirsLayout;

    QLabel* fnameLabel, *statusLabel;
    QLineEdit* fnameLEdit, *srcdirLEdit, *destdirLEdit;
    QPushButton* srcdirButton, *destdirButton, *dirsearchButton, *filesearchButton;
    QFrame* divLine1, *divLine2;

    QTimer* workTimer;

    void createUI();
    void createExtractor();
    void connectUISigsSlots();

private slots:
    void on_srcdirButton_clicked();
    void on_destdirButton_clicked();
    void on_dirNames_changed();
    void on_searchButtons_clicked();
    void updateWorkingStatus();

public slots:
    void on_extractionSucceeded( const QString& );
    void on_extractionFailed( const QString& );

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
};

#endif // WIDGET_H
