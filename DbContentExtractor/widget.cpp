#include "widget.h"
#include "extractor.h"

#include <QFileDialog>

#include <string>

Widget::Widget(QWidget *parent)
    : QWidget(parent),
      mainLayout{new QVBoxLayout(this)},
      fnameLayout{new QHBoxLayout(this)},
      dirsLayout{new QGridLayout(this)},
      fnameLabel{new QLabel("String (or list of strings separated by commas)\nto search for in directory/file names:", this)},
      statusLabel{new QLabel(this)},
      fnameLEdit{new QLineEdit(this)},
      srcdirLEdit{new QLineEdit(this)},
      destdirLEdit{new QLineEdit(this)},
      srcdirButton{new QPushButton("Set source directory to walk through",this)},
      destdirButton{new QPushButton("Set target directory to copy entries to",this)},
      dirsearchButton{new QPushButton("Search && extract subfolders")},
      filesearchButton{new QPushButton("Search && extract files")},
      divLine1{new QFrame},
      divLine2{new QFrame},
      workTimer{new QTimer(this)}
{
    createUI();
    createExtractor();

    connectUISigsSlots();
}

void Widget::createUI()
{
    divLine1->setFrameShape(QFrame::HLine);
    divLine1->setFrameShadow(QFrame::Sunken);
    divLine1->setLineWidth(1);

    divLine2->setFrameShape(QFrame::HLine);
    divLine2->setFrameShadow(QFrame::Sunken);
    divLine2->setLineWidth(1);

    srcdirLEdit->setMinimumWidth(350);
    destdirLEdit->setMinimumWidth(350);

    fnameLayout->addWidget(fnameLabel);
    fnameLayout->addWidget(fnameLEdit);
    dirsLayout->addWidget(srcdirButton,0,0);
    dirsLayout->addWidget(srcdirLEdit,0,1);
    dirsLayout->addWidget(destdirButton,1,0);
    dirsLayout->addWidget(destdirLEdit,1,1);

    mainLayout->addLayout(fnameLayout);
    mainLayout->addWidget(divLine1);
    mainLayout->addLayout(dirsLayout);
    mainLayout->addWidget(divLine2);
    mainLayout->addWidget(dirsearchButton);
    mainLayout->addWidget(filesearchButton);
    mainLayout->addWidget(statusLabel);

    setLayout(mainLayout);
    dirsearchButton->setEnabled(false);
    filesearchButton->setEnabled(false);
}

void Widget::createExtractor()
{
    Extractor* extractor = new Extractor;
    extractor->moveToThread(&extractorThread);

    connect(&extractorThread, &QThread::finished, extractor, &QObject::deleteLater);
    connect(fnameLEdit, &QLineEdit::textChanged, extractor, &Extractor::setTokens);
    connect(srcdirLEdit, &QLineEdit::textChanged, extractor, &Extractor::setSrcDir);
    connect(destdirLEdit, &QLineEdit::textChanged, extractor, &Extractor::setDstDir);
    connect(dirsearchButton, &QPushButton::clicked, extractor, &Extractor::doDirExtraction);
    connect(filesearchButton, &QPushButton::clicked, extractor, &Extractor::doFileExtraction);
    connect(extractor, &Extractor::extractionSucceeded, this, &Widget::on_extractionSucceeded);
    connect(extractor, &Extractor::extractionFailed, this, &Widget::on_extractionFailed);

    extractorThread.start();
}

void Widget::connectUISigsSlots()
{
    connect(srcdirButton, &QPushButton::clicked, this, &Widget::on_srcdirButton_clicked);
    connect(destdirButton, &QPushButton::clicked, this, &Widget::on_destdirButton_clicked);
    connect(srcdirLEdit, &QLineEdit::textChanged, this, &Widget::on_dirNames_changed);
    connect(destdirLEdit, &QLineEdit::textChanged, this, &Widget::on_dirNames_changed);
    connect(dirsearchButton, &QPushButton::clicked, this, &Widget::on_searchButtons_clicked);
    connect(filesearchButton, &QPushButton::clicked, this, &Widget::on_searchButtons_clicked);
    connect(workTimer, &QTimer::timeout, this, &Widget::updateWorkingStatus);
}

void Widget::on_srcdirButton_clicked()
{
    const QString dirName = QFileDialog::getExistingDirectory(this, "Choose a root directory");
    srcdirLEdit->setText(dirName);
}

void Widget::on_destdirButton_clicked()
{
    const QString dirName = QFileDialog::getExistingDirectory(this, "Choose a target directory to save files to");
    destdirLEdit->setText(dirName);
}

void Widget::on_dirNames_changed()
{
    if( !srcdirLEdit->text().isEmpty() && !destdirLEdit->text().isEmpty() ) {
        dirsearchButton->setEnabled(true);
        filesearchButton->setEnabled(true);
    }
    else {
        dirsearchButton->setEnabled(false);
        filesearchButton->setEnabled(false);
    }
}

void Widget::on_searchButtons_clicked()
{
    statusLabel->setStyleSheet("QLabel { color : black; }");
    dirsearchButton->setEnabled(false);
    filesearchButton->setEnabled(false);

    workTimer->start(1000);
}

void Widget::updateWorkingStatus()
{
    static int counter = 0;

    statusLabel->setText("Extracting" + QString{counter % 3, ' '} + ".");
    ++counter;
}

void Widget::on_extractionSucceeded( const QString& msg )
{
    dirsearchButton->setEnabled(true);
    filesearchButton->setEnabled(true);

    statusLabel->setText(msg);
    QTimer::singleShot(2000, statusLabel, &QLabel::clear);

    workTimer->stop();
}

void Widget::on_extractionFailed( const QString& errorMsg )
{
    dirsearchButton->setEnabled(true);
    filesearchButton->setEnabled(true);

    statusLabel->setStyleSheet("QLabel { color : red; }");
    statusLabel->setText(errorMsg);

    workTimer->stop();
}

Widget::~Widget()
{
    extractorThread.quit();
    extractorThread.wait();

    delete divLine2;
    delete divLine1;
}
