#include "extractor.h"

#include <QApplication>
#include <QDebug>

bool Extractor::loopCopyDir( const fs::path& srcDirPath )
{
    bool status = true;
    std::size_t apx = 0;
    // const fs::path destDir{destdirLEdit->text().toStdString()};
    const std::string name = srcDirPath.stem().string();

    std::string addOn = "";
    while( fs::exists(fs::path{dstDir.toStdString() + "/" + name.c_str() + addOn}) )
        addOn = "(" + std::to_string(++apx) + ")";

    try {
        fs::copy(srcDirPath, fs::path{dstDir.toStdString() + "/" + name.c_str() + addOn}, fs::copy_options::recursive);
    } catch( fs::filesystem_error& e ) {
        emit extractionFailed(QString{"Could not copy directory "} + QString::fromStdString(srcDirPath.string()) + " : " + e.what());
        status = false;
    }

    return status;
}

bool Extractor::loopCopyFile( const fs::path& srcFilePath )
{
    bool status = true;
    std::size_t apx = 0;
    const std::string ext = srcFilePath.extension().string();
    const std::string name = srcFilePath.stem().string();

    std::string addOn = "";
    while( fs::exists(fs::path{dstDir.toStdString() + "/" + name.c_str() + addOn + ext.c_str()}) )
        addOn = "(" + std::to_string(++apx) + ")";

    try {
        fs::copy_file(srcFilePath, fs::path{dstDir.toStdString() + "/" + name.c_str() + addOn + ext.c_str()});
    } catch( fs::filesystem_error& e ) {
        emit extractionFailed(QString{"Could not copy file "} + QString::fromStdString(srcFilePath.string()) + " : " + e.what());
        status = false;
    }

    return status;
}

bool Extractor::tokensPresentIn( const QString& path )
{
    bool contained = true;

    for(const auto& token : tokenList)
        if( !path.contains(token) ) {
            contained = false;
            break;
        }

    return contained;
}

void Extractor::setTokens( const QString& tokens )
{
    tokenList = tokens.split(QRegExp("\\W+"), QString::SkipEmptyParts);
}

void Extractor::doDirExtraction()
{
    if( dstDir.contains(srcDir, Qt::CaseInsensitive) ) {
        emit extractionFailed("Destination directory is within source directory. Extraction aborted.");
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    for(const auto& entry : fs::recursive_directory_iterator{srcDir.toStdString()}) {
        if( fs::is_directory(entry.path()) && tokensPresentIn(QString::fromStdString(entry.path().filename().string())) ) {
            const QString entryName = QString::fromStdString(entry.path().filename().string());
            qDebug() << entryName << QString::fromStdString(fs::absolute(entry.path()).string());
            loopCopyDir(entry.path());
        }
    }

    QApplication::restoreOverrideCursor();

    emit extractionSucceeded("Finished extracting subfolders.");
}

void Extractor::doFileExtraction()
{
    if( dstDir.contains(srcDir, Qt::CaseInsensitive) ) {
        emit extractionFailed("Destination directory is within source directory. Extraction aborted.");
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    for(const auto& entry : fs::recursive_directory_iterator{srcDir.toStdString()}) {
        if( fs::is_regular_file(entry.path()) && tokensPresentIn(QString::fromStdString(entry.path().filename().string())) ) {
            const QString entryName = QString::fromStdString(entry.path().filename().string());
            qDebug() << entryName << QString::fromStdString(fs::absolute(entry.path()).string());
            loopCopyFile(entry.path());
        }
    }

    QApplication::restoreOverrideCursor();

    emit extractionSucceeded("Finished extracting files.");
}
