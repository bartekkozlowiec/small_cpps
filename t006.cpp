#include <iostream>

using namespace std;

void  printTables( int (*)[3], int**, int (*)[3], int**, int**, int, int );
int** dynamAlloc ( int, int );
void  passTable( int[] );
void  passPointer( int* );

int main()
{
    const int dim1 = 2;
    const int dim2 = 3;

    // Definicje

    int tab1[dim1][dim2] = { {1,2,3}, {4,5,6} };

    int*  tab2[dim1];
    int (*tab3)[dim2] = new int[dim1][dim2];
    int** tab4;
    int** tab5 = dynamAlloc(dim1,dim2);

    // Inicjalizacje

    for(int i = 0; i < dim1; i++)
    {
        tab2[i] = tab1[i];
        for(int j = 0; j < dim2; j++)
            tab3[i][j] = tab1[i][j];
    }

    tab4 = tab2;
    tab5 = tab2;

    // Wydruk

    printTables(tab1,tab2,tab3,tab4,tab5,dim1,dim2);

    ///////
    cout << endl << "Porownanie rozmiarow argumentow funkcji:" << endl;
    int TT[dim1] = { 1, 2 };
    int TTT[dim2] = { 1, 2, 3 };
    passTable(TT);
    passTable(TTT);
    passPointer(TTT);

    return 0;
}

void printTables( int (*t1)[3], int** t2, int (*t3)[3], int** t4, int** t5, int siz1, int siz2 )
{
    cout << "Tab1:" << endl;
    for(int i = 0; i < siz1; i++)
    {
        for(int j = 0; j < siz2; j++)
            cout << t1[i][j] << " ";
        cout << endl;
    }

    cout << "Tab2:" << endl;
    for(int i = 0; i < siz1; i++)
    {
        for(int j = 0; j < siz2; j++)
            cout << t2[i][j] << " ";
        cout << endl;
    }

    cout << "Tab3:" << endl;
    for(int i = 0; i < siz1; i++)
    {
        for(int j = 0; j < siz2; j++)
            cout << t3[i][j] << " ";
        cout << endl;
    }

    cout << "Tab4:" << endl;
    for(int i = 0; i < siz1; i++)
    {
        for(int j = 0; j < siz2; j++)
            cout << t4[i][j] << " ";
        cout << endl;
    }

    cout << "Tab5:" << endl;
    for(int i = 0; i < siz1; i++)
    {
        for(int j = 0; j < siz2; j++)
            cout << t5[i][j] << " ";
        cout << endl;
    }
}

int** dynamAlloc( int siz1, int siz2 )
{
    int** tablica = new int*[siz1*siz2];
    int*  wiersz  = new int[siz2];

    for(int i = 0; i < siz1; i++)
        tablica[i] = wiersz + i*siz2;

    return tablica;
}

void passTable( int Tab[] )  { cout << sizeof(Tab) << endl; }
void passPointer( int* Ptr ) { cout << sizeof(Ptr) << endl; }
