#include <iostream>

using namespace std;

///////////////////////////// Definicje klas ///////////////////////////////

class A
{
    char znak;

public:
    A( char znak = 'a' ) : znak(znak)
    { }

    A& operator+( A& skladnik )
    {
        znak = char(int(this->znak) + int(skladnik.znak) - int('a'));
        return *this;
    }

    char getc() const { return znak; }
};

////////////////////////////////////////////////////////////////////////////

class B : public A
{
    char drugiZnak;

public:
    B( char znakB = 'b', char znakA = 'b' ) : A(znakA), drugiZnak(znakB)
    { }
};

/********************** Definicje funkcji globalnych **********************/

ostream& operator<<( ostream& ekran, const A& refA )
{
    ekran << refA.getc();
    return ekran;
}

/**************************************************************************/

char operator+( const A& skladA, const char& skladc )
{
    cout << "Operator+, wersja 1: ";
    return (int(skladA.getc()) + int(skladc) - int('a'));
}

/**************************************************************************/

char operator+( const A& skladA, char& skladc )
{
    cout << "Operator+, wersja 2: ";
    return (int(skladA.getc()) + int(skladc) - int('a'));
}

/**************************************************************************/

char operator+( A& skladA, char& skladc )
{
    cout << "Operator+, wersja 3: ";
    return (int(skladA.getc()) + int(skladc) - int('a'));
}

/**************************************************************************/

int main()
{
    char a = 'a', b = 'b';
    char c = a + b;              // == char(int(a) + int(b))
    cout << a << b << c << endl;

    A aa;
    B bb;

    cout << "Dodawanie obiekt + obiekt: " << aa + bb << endl;
    cout << "Dodawanie obiekt + char: " << aa + b << endl;
    cout << "Dodawanie obiekt + literal: " << aa + 'a' << endl;

    return 0;
}
