#include <iostream>
#include <string>

using namespace std;

int main()
{
   char s[] = "hello", t[] = "hello";
   int size = sizeof(t)/sizeof(t[0]) > sizeof(s)/sizeof(s[0]) ?
               sizeof(t)/sizeof(t[0]) : sizeof(s)/sizeof(s[0]);
   bool wyn = true;

   for(int i = 0; i < size; i++)
       if(*(s+i)!=*(t+i))
           wyn = false;

   if(wyn)
       cout << "equal strings!\n";
   cout << "size = " << size << endl;

   string s1 = "Acapulco";
   string s2(s1.rbegin(),s1.rend());
   cout << s1 << "; " << s2 << endl;

   const string temp(s1);

   for(int i = 1; i <= s1.length(); i++)
   {
       cout << "temp: " << temp << endl;
       s1[i-1] = temp[temp.length()-i];
   }
   cout << "Nowy s1: " << s1 << endl;

   return 0;
}
