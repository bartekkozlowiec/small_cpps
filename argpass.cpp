#include <iostream>
#include <chrono>

/************************************************************************************************************************/

class matrix2x2
{
    double matrix[4];

public:
    matrix2x2( double = 0.0, double = 0.0, double = 0.0, double = 0.0 );
    matrix2x2( const matrix2x2& );
    matrix2x2( matrix2x2&& );
    matrix2x2& operator=( const matrix2x2& );
    matrix2x2& operator=( matrix2x2&& );
    matrix2x2& operator+( const matrix2x2& );
    matrix2x2& operator+( matrix2x2&& );
    matrix2x2& operator*( double&& );

    matrix2x2& operator*( const double& ) = delete;

    friend std::ostream& operator<<( std::ostream& str, const matrix2x2& Matrix );
};

/************************************************************************************************************************/

matrix2x2::matrix2x2( double m11, double m12, double m21, double m22 )
{
    std::cout << "Constructor working... ";
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    matrix[0] = std::move(m11);
    matrix[1] = std::move(m12);
    matrix[2] = std::move(m21);
    matrix[3] = std::move(m22);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Work took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns.\n\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

matrix2x2::matrix2x2( const matrix2x2& other )
{
    std::cout << "Copy-constructor working... ";
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    matrix[0] = other.matrix[0];
    matrix[1] = other.matrix[1];
    matrix[2] = other.matrix[2];
    matrix[3] = other.matrix[3];

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Work took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns.\n\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

matrix2x2::matrix2x2( matrix2x2&& other )
{
    std::cout << "Move-constructor working... ";
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    matrix[0] = std::move(other.matrix[0]);
    matrix[1] = std::move(other.matrix[1]);
    matrix[2] = std::move(other.matrix[2]);
    matrix[3] = std::move(other.matrix[3]);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Work took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns.\n\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

matrix2x2& matrix2x2::operator=( const matrix2x2& other )
{
    std::cout << "Copy-assignment operator working... ";
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    matrix[0] = other.matrix[0];
    matrix[1] = other.matrix[1];
    matrix[2] = other.matrix[2];
    matrix[3] = other.matrix[3];

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Work took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns.\n\n";

    return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

matrix2x2& matrix2x2::operator=( matrix2x2&& other )
{
    std::cout << "Move-assignment operator working... ";
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    matrix[0] = std::move(other.matrix[0]);
    matrix[1] = std::move(other.matrix[1]);
    matrix[2] = std::move(other.matrix[2]);
    matrix[3] = std::move(other.matrix[3]);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Work took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns.\n\n";

    return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

matrix2x2& matrix2x2::operator+( const matrix2x2& other )
{
    std::cout << "Copy-version of addition operator working... ";
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    matrix[0] += other.matrix[0];
    matrix[1] += other.matrix[1];
    matrix[2] += other.matrix[2];
    matrix[3] += other.matrix[3];

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Work took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns.\n\n";

    return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

matrix2x2& matrix2x2::operator+( matrix2x2&& other )
{
    std::cout << "Move-version of addition operator working... ";
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    matrix[0] += other.matrix[0];
    matrix[1] += other.matrix[1];
    matrix[2] += other.matrix[2];
    matrix[3] += other.matrix[3];

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Work took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns.\n\n";

    return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

matrix2x2& matrix2x2::operator*( double&& a )
{
    std::cout << "Move-version of multiplication operator working... ";
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    matrix[0] *= a;
    matrix[1] *= a;
    matrix[2] *= a;
    matrix[3] *= a;

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Work took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << " ns.\n\n";

    return *this;
}

/************************************************************************************************************************/

std::ostream& operator<<( std::ostream& str, const matrix2x2& Matrix )
{
    return str << "Matrix looks like this:\n"
               << Matrix.matrix[0] << "\t" << Matrix.matrix[1] << "\n"
               << Matrix.matrix[2] << "\t" << Matrix.matrix[3] << "\n\n";
}

/************************************************************************************************************************/

int main()
{
    std::cout << "Creating matrix A...\n";
    matrix2x2 A(1.0, 2.0, 3.0, 4.0);
    std::cout << A;

    std::cout << "Creating matrix B...\n";
    matrix2x2 B(A);
    std::cout << B;

    std::cout << "Creating matrix C...\n";
    //matrix2x2 C(std::move(B)); // this won't work in C++11
    //matrix2x2 C = std::move(B); // this will
    matrix2x2 C{std::move(B)}; // or this
    std::cout << C;

    std::cout << "Creating matrix D...\n";
    //matrix2x2 D(matrix2x2()); // this won't work in C++11
    //matrix2x2 D = matrix2x2(); // this will
    matrix2x2 D{matrix2x2()}; // or this
    std::cout << D;

    std::cout << "Copy-assigning C to D...\n";
    D = C;
    std::cout << D;

    std::cout << "Move-assigning A doubled to D...\n";
    D = A * 2.0;
    std::cout << D;

    std::cout << "Move-assigning D to B...\n";
    B = std::move(D);
    std::cout << B;

    // Will this work?
    double a = 1.0;
    B * std::move(a);

    std::cout << "Adding A to B...\n";
    matrix2x2 E = A + B;

    std::cout << "Adding zero matrix to E...\n";
    E = E + matrix2x2();
    std::cout << E;

    return 0;
}
