#include <iostream>
#include <type_traits>
#include <algorithm>

template <typename T, std::enable_if_t<!std::is_class<T>::value>* = nullptr>
void printArray(T array[], const unsigned size)
{
    std::cout << "Array innerts: ";
    for(unsigned i=0; i<size; ++i)
        std::cout << array[i] << "\t";
    std::cout << "\n";
}

template <typename T, std::enable_if_t<!std::is_class<T>::value>* = nullptr>
void countingSort(T array[], const unsigned size, const unsigned range, const unsigned exp)
{
    // Prepare an array for the sorted elements
    T output[size];

    // Prepare an array for the number of occurences of every element within range
    unsigned count[range];
    for(unsigned i=0; i<range; ++i) count[i] = 0;

    // Count the occurences for each element in range
    for(unsigned i=0; i<size; ++i) { std::cout << "Arr idx: " << (array[i] / exp) % range << "\n";
        ++count[ (array[i] / exp) % range ]; }

    //printArray(count,range);

    // Add the sums at previous indexes to obtain the position of the "key" in the output array
    for(unsigned i=1; i<range; ++i)
        count[i] += count[i-1];

    //printArray(count,range);

    // Fill the output array starting from its end to obtain stability
    for(unsigned i=size; i-- > 0; )
    {
        //std::cout << i << " " << count[i] << " " << array[i] << "\n";
        output[count[ (array[i] / exp) % range ]-1] = array[i];
        --count[ (array[i] / exp) % range ];
    }

    // Copy the output array back to the original
    //std::memcpy(array,output,size*sizeof(T));
    for(unsigned i=0; i<size; ++i) array[i] = output[i];

    printArray(output,size);
}

template <typename T, std::enable_if_t<!std::is_class<T>::value>* = nullptr>
void radixSort(T array[], const unsigned size, const unsigned base)
{
    // Get the maximum value of the sorted array
    T arrMax = *std::max_element(array,array+size);

    // Count the number of digits of the max value in the given base
    unsigned digits = 1;
    unsigned exp = base;

    while( static_cast<int>(arrMax / exp) )
    {
        ++digits;
        exp *= base;
    }

    // Now, call counting sort for every digit
    exp = 1;
    for(unsigned i=0; i<digits; ++i)
    {
        countingSort(array,size,base,exp);
        exp *= base;
    }
}

int main()
{
    unsigned unsignArr[] = { 100, 0, 4, 7, 35, 803, 125, 587 };

    std::cout << "\nInitial array\n";
    printArray(unsignArr,sizeof(unsignArr)/sizeof(unsigned));

    radixSort(unsignArr,sizeof(unsignArr)/sizeof(unsigned),10);

    std::cout << "\nSorted array\n";
    printArray(unsignArr,sizeof(unsignArr)/sizeof(unsigned));

    return 0;
}
