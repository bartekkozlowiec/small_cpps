// https://eli.thegreenplace.net/2011/12/15/understanding-lvalues-and-rvalues-in-c-and-c/
// https://akrzemi1.wordpress.com/2018/05/16/rvalues-redefined/
// https://www.codesynthesis.com/~boris/blog/2012/06/19/efficient-argument-passing-cxx11-part1/

#include <iostream>
#include <sys/socket.h> // Linux header
#include <unistd.h>     // Linux header
#include <stdexcept>
#include <cassert>
#include <utility>      // since C++14
#include <string>       // since C++11
#include <type_traits>  // since C++11

#define STRINGIFY(s) XSTRINGIFY(s)
#define XSTRINGIFY(s) #s
#pragma message ("__cplusplus macro value: " STRINGIFY(__cplusplus))

#if __cplusplus < 201402L
  #error This module needs at least a C++14 compliant compiler!
#endif

/********************* The movable/non-movable resource class used until/after the C++17 standard was introduced *********************/

class Socket
{
    int socket_id;

public:
    explicit Socket() : socket_id{ socket(AF_INET, SOCK_STREAM, 0) }
    {
        if( socket_id < 0 ) throw std::runtime_error{ std::to_string(socket_id) };
    }

    explicit Socket( int AddressFamily ) : socket_id{ socket(AddressFamily, SOCK_STREAM, 0) }
    {
        if( socket_id < 0 ) throw std::runtime_error{ std::to_string(socket_id) };
    }


#if __cplusplus < 201703L
    //template<typename... Args> // only to make use of variadic template in Opt<T>::emplace
    Socket( Socket&& r/*, Args...*/ ) noexcept : socket_id{ std::exchange(r.socket_id,-1) } { std::cout << "Socket's move constructor called.\n"; }
#else
    Socket( Socket&& ) = delete;
#endif

    Socket( const Socket& ) = delete;

#if __cplusplus < 201703L
    bool is_valid() const { return socket_id != -1; }
#endif

    ~Socket()
    {
#if __cplusplus < 201703L
        if( is_valid() )
#endif
            close(socket_id);
    }

    int id() const { return socket_id; }

    static Socket make_inet() { return Socket{AF_INET}; }
    static Socket make_unix() { return Socket{AF_UNIX}; }
};

/*************************** The movable/non-movable container template in a pre/compliant C++17 standard ***************************/

template<typename T>
class Opt
{
    std::aligned_storage_t<sizeof(T), alignof(T)> _storage;
    bool _initialized = false;

    void* address() { return &_storage; }
    T* pointer() { return static_cast<T*>(address()); }

public:
    Opt() = default;
    Opt( const Opt& ) = delete;
    ~Opt() { if( _initialized ) pointer()->T::~T(); }

#if __cplusplus < 201703L
    Opt( Opt&& ) = delete;
#endif

    template<typename... Args>
    void emplace( Args&&... args )
    {
        assert(!_initialized);
        new (address()) T(std::forward<Args>(args)...);
        _initialized = true;
    }

    void clear()
    {
        if( _initialized )
        {
            pointer()->T::~T();
            //delete(pointer());
            _initialized = false;
        }
    }
};

/******************************** Another template which performs a forwarded in place construction *********************************/

template<typename F>
class rvalue
{
    F fun;

public:
#if __cplusplus < 201703L
    using T = typename std::result_of<F()>::type;
#else
    using T = std::invoke_result_t<F>;
#endif
    //explicit rvalue(F f) : fun(std::move(f)) {} // or consider overloading:
    explicit rvalue(const F& f) : fun(f) {} // using std::move(f) here in initilializing list won't change a thing
    explicit rvalue(F&& f) : fun(std::move(f)) {}
    operator T() { return fun(); }
};

/*********************** Simple class and set of global overloaded functions for additional semantics tests *************************/

struct A
{
    A() = default;
    A( const A& ) { std::cout << "A's copy-constructor\n"; }
    A( A&& ) { std::cout << "A's move-constructor\n"; }
};

void flr( const A& a ) { std::cout << "Inside function flr(const A&):\n"; A aloc(a); A alocm(std::move(a)); std::cout << "\n"; }
void frr( A&& a ) { std::cout << "Inside function frr(A&&):\n"; A aloc(a); A alocm(std::move(a)); std::cout << "\n"; }
void f( A a ) { std::cout << "Inside function f(A):\n"; A aloc(a); A alocm(std::move(a)); std::cout << "\n"; }

/************************************************************************************************************************************/

Socket make_socket()
{
    return Socket{};
}

const Socket select_socket( bool cond )
{
    if( cond ) return Socket{};
    return make_socket();
}

void cpp_version_printout()
{
    if (__cplusplus >= 201703L) std::cout << "C++17\n";
    else if (__cplusplus >= 201402L) std::cout << "C++14\n";
    else if (__cplusplus >= 201103L) std::cout << "C++11\n";
    else if (__cplusplus >= 199711L) std::cout << "C++98\n";
    else std::cout << "pre-standard C++\n";
}

int main()
{
    cpp_version_printout();

    A amain;
    flr(amain);
    flr(A());
    frr(A());
    f(amain);

    Socket s = select_socket(true);

    std::cout << "\nEmplacing...\n";
    Opt<Socket> os;
#if __cplusplus < 201703L
    os.emplace(Socket::make_inet()/*, Socket::make_unix()*/); // this won't work if move is not allowed
    os.clear();
    os.emplace(rvalue<Socket(*)()>{&Socket::make_inet}); // this will work even if move-constructor is deleted
    os.clear();
    os.emplace(rvalue<Socket(*)()>{[&]{ return Socket::make_inet(); }});
#else
    os.emplace(rvalue{&Socket::make_inet}); // this will work even if move-constructor is deleted
    os.clear();
    os.emplace(rvalue{[&]{ return Socket::make_inet(); }});
#endif

    std::cout << "\nA simple factory return...\n";
    return make_socket().id();
}
