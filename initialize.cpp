#include <iostream>
#include <utility>
#include <type_traits>
#include <vector>
#include <cstdio>


#define REQUIRES(...) typename std::enable_if<(__VA_ARGS__),bool>::type = true

template <bool...> struct bool_sequence {};

template <bool... Bs>
using bool_and = std::is_same<bool_sequence<Bs...>, bool_sequence<(Bs || true)...> >;

template <typename T>
std::true_type create( T v );

template <typename T, typename U>
decltype(create<U>({ std::declval<T>() })) test_nonnarrow_conv( int );

template <typename T, typename U>
std::false_type test_nonnarrow_conv( long );

template <typename T, typename U>
using is_nonnarrow_convertible = decltype(test_nonnarrow_conv<T,U>(0));

template <typename T, typename... Ts>
using nonnarrow_convertible = bool_and<is_nonnarrow_convertible<T,Ts>::value...>;

template <typename T>
class noCopyInitializedVec
{
    std::vector<T> _vect;

    template <typename U, typename... UList>
    void process( U&& v, UList&&... vs )
    {
        _vect.push_back(std::forward<U>(v));
        process(std::forward<UList>(vs)...);
    }

    void process() {}

public:
    template <typename... UList, REQUIRES(nonnarrow_convertible<T,UList...>::value)>
    noCopyInitializedVec( UList&&... vs )
    {
        _vect.reserve(sizeof...(vs));
        process(std::forward<UList>(vs)...);
    }
};

class talkingDouble
{
    double _innerts;

public:
    talkingDouble() { printf("talkingDouble::talkingDouble() called\n"); }

    talkingDouble( double arg ) {
        _innerts = arg;
        printf("talkingDouble::talkingDouble(double) called\n");
    }

    talkingDouble( const talkingDouble& arg ) {
        _innerts = arg._innerts;
        printf("talkingDouble::talkingDouble(const talkingDouble&) called\n");
    }

    talkingDouble( talkingDouble&& arg ) {
        _innerts = arg._innerts;
        printf("talkingDouble::talkingDouble(talkingDouble&&) called\n");
    }

    operator double() {
        printf("talkingDouble::operator double() called\n");
        return _innerts;
    }
};

int main()
{
    // Some tests of talkingDouble class behaviour
    talkingDouble tD1, tD2{3.0}, tD3{ tD2 };
    printf("%f + %f + %f = %f\n",double(tD1),double(tD2),double(tD3),double(tD1)+double(tD2)+double(tD3));

    // Testing noCopyInitializedVec initializing constructor vs std::vector initializing constructor
    printf("\nInitializing std::vector with a talkingDouble:\n");
    std::vector<talkingDouble> stdVec1{ tD2 };

    printf("\nInitializing noCopyInitializedVec with a talkingDouble:\n");
    //noCopyInitializedVec<talkingDouble> nciVec{ tD3 };

    printf("\nInitializing noCopyInitializedVec with different types:\n");
    noCopyInitializedVec<float> nciVec1 { 1.08f, 2.08 };//, 2L, 3, 5.984f };

    return 0;
}
