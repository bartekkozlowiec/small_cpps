#include <iostream>
#include <cassert>

///////////////////////////////////// Utilities to allow printing of only streamable types /////////////////////////////////////

template <typename T>
struct void_t_impl
{
    typedef void type;
};

template <typename T>
using void_t = typename void_t_impl<T>::type;

template <typename T>
T&& declval();

template <bool Cond, typename T = void>
struct enable_if
{
    typedef T type;
};

template <typename T>
struct enable_if<false,T> {};

template <bool Cond, typename T>
using enable_if_t = typename enable_if<Cond,T>::type;

///////////////////////////////////////// The circular queue class template using array ////////////////////////////////////////

template <typename T, unsigned maxSize>
class CircularQueue;

template <typename T, unsigned maxSize>
std::ostream& operator<<( std::ostream&, const CircularQueue<T,maxSize>& );

template <typename T, unsigned maxSize>
class CircularQueue
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type storage[maxSize];
    unsigned front, rear;

public:
    constexpr CircularQueue() : front{0}, rear{0} {}
    CircularQueue( const T arr[], const unsigned n ) { for(unsigned i=0; i<n; ++i) enqueue(arr[i]); }
    ~CircularQueue() { while( !isEmpty() ) dequeue(); }

    constexpr bool isFull() const { return ((rear+1) % maxSize) == front; }
    constexpr bool isEmpty() const { return front == rear; }
    unsigned currSize() const { return rear >= front ? rear-front : rear+maxSize-front; }
    const T& operator[]( const unsigned ) const;
    bool enqueue( const T& );
    T dequeue();

    friend std::ostream& operator<< <>( std::ostream&, const CircularQueue& );
};

template <typename T, unsigned maxSize>
const T& CircularQueue<T,maxSize>::operator[]( const unsigned idx ) const
 {
    assert(idx < currSize());
    return *reinterpret_cast<const T*>(storage+((front+1+idx) % maxSize));
}

template <typename T, unsigned maxSize>
bool CircularQueue<T,maxSize>::enqueue( const T& elem )
{
    bool enqueued = false;

    if( !isFull() ) {
        rear = (rear+1) % maxSize;
        new(storage+rear) T{elem};
        enqueued = true;
    }

    return enqueued;
}

template <typename T, unsigned maxSize>
T CircularQueue<T,maxSize>::dequeue()
{
    T retElem{};

    if( !isEmpty() ) {
        front = (front+1) % maxSize;
        retElem = *reinterpret_cast<T*>(storage+front);
        reinterpret_cast<T*>(storage+front)->~T();
    }

    return retElem;
}

template <typename T, unsigned maxSize>
std::ostream& operator<<( std::ostream& ostr, const CircularQueue<T,maxSize>& queue )
{
    for(unsigned idx=0; idx<queue.currSize(); ++idx)
        ostr << queue[idx] << " ";
    ostr << "\n";

    return ostr;
}

/////////////////////////////////////// The red-black tree class template using pointers ///////////////////////////////////////

template <typename T, unsigned maxSize = 50>
class RedBlackTree
{
    enum class Colour { red, black };

    struct Node
    {
        Node* leftChild;
        Node* rightChild;
        T data;
        Colour color;
    } *root;

    Node* search( const T& ) const;
    void rotate( Node* const, const bool );
    void LLrotate( Node* const, const bool = false );
    void LRrotate( Node* const, const bool = false );
    void RLrotate( Node* const, const bool = false );
    void RRrotate( Node* const, const bool = false );
    void recolour( Node* const, Node* const, Node* const );
    void solveConflict( Node* const, Node* const, Node* const );
    void checkConflicts();

public:
    constexpr RedBlackTree() : root{nullptr} {}
    ~RedBlackTree() { clear(); }
    bool insert( const T& );
    bool remove( const T& );
    void clear();

    CircularQueue<Node*,maxSize> levelOrderT() const;

    template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
    void display( Str&&, CircularQueue<Node*,maxSize>(RedBlackTree<T,maxSize>::*)() const ) const;
};

template <typename T, unsigned maxSize>
typename RedBlackTree<T,maxSize>::Node* RedBlackTree<T,maxSize>::search( const T& value ) const
{
    Node* p = root;
    while( p && p->data != value )
        value < p->data ? p = p->leftChild : p = p->rightChild;

    return p;
}

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::rotate( Node* const p, const bool deleting )
{
    if( p ) {
        if( p->leftChild && p->rightChild ) {
            std::cerr << "Warning: attempted to rotate around a point with both children non-null.\n";
            return;
        }

        if( p->leftChild && p->leftChild->leftChild )
            LLrotate(p, deleting);
        else if( p->leftChild && p->leftChild->rightChild )
            LRrotate(p, deleting);
        else if( p->rightChild && p->rightChild->leftChild )
            RLrotate(p, deleting);
        else if( p->rightChild && p->rightChild->rightChild )
            RRrotate(p, deleting);
        else
            std::cerr << "Warning: attempted to rotate around a point with balance factor less than 2.\n";

        if( p == root ) p->color = Colour::black;

        checkConflicts();
    }
}

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::LLrotate( Node* const p, const bool deleting )
{
    const T tempData = p->data;
    Node* const tempR = p->rightChild;
    Node* const tempL = p->leftChild;

    p->leftChild = p->leftChild->leftChild;
    p->rightChild = tempL;
    p->data = tempL->data;
    p->rightChild->leftChild = p->rightChild->rightChild;
    p->rightChild->rightChild = tempR;
    p->rightChild->data = tempData;

    if( deleting && p->rightChild->leftChild ) p->rightChild->leftChild->color = Colour::red;
}

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::LRrotate( Node* const p, const bool deleting )
{
    const T tempData = p->data;
    Node* const tempR = p->rightChild;
    Node* const tempLR = p->leftChild->rightChild;

    p->data = tempLR->data;
    p->rightChild = tempLR;
    p->rightChild->data = tempData;
    p->rightChild->rightChild = tempR;
    p->leftChild->rightChild = tempLR->leftChild;
    p->rightChild->leftChild = tempLR->rightChild;

    if( deleting && p->rightChild->leftChild ) p->rightChild->leftChild->color = Colour::red;
}

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::RLrotate( Node* const p, const bool deleting )
{
    const T tempData = p->data;
    Node* const tempL = p->leftChild;
    Node* const tempRL = p->rightChild->leftChild;

    p->data = tempRL->data;
    p->leftChild = tempRL;
    p->leftChild->data = tempData;
    p->leftChild->leftChild = tempL;
    p->rightChild->leftChild = tempRL->rightChild;
    p->leftChild->rightChild = tempRL->leftChild;

    if( deleting && p->leftChild->rightChild ) p->leftChild->rightChild->color = Colour::red;
}

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::RRrotate( Node* const p, const bool deleting )
{
    const T tempData = p->data;
    Node* const tempR = p->rightChild;
    Node* const tempL = p->leftChild;

    p->rightChild = p->rightChild->rightChild;
    p->leftChild = tempR;
    p->data = tempR->data;
    p->leftChild->rightChild = p->leftChild->leftChild;
    p->leftChild->leftChild = tempL;
    p->leftChild->data = tempData;

    if( deleting && p->leftChild->rightChild ) p->leftChild->rightChild->color = Colour::red;
}

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::recolour( Node* const parent, Node* const grandpa, Node* const uncle )
{
    if( parent->color != uncle->color && uncle->color != Colour::red && grandpa->color != Colour::black ) {
        std::cerr << "Warning: nodes with wrong set of colours sent to recolour()!\n";
        return;
    }

    parent->color = Colour::black;
    uncle->color = Colour::black;
    if( grandpa != root ) grandpa->color = Colour::red;

    checkConflicts();
}

// https://stackoverflow.com/questions/59372831/cvxopt-uses-just-one-core-need-to-run-on-all-some

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::solveConflict( Node* const child, Node* const parent, Node* const grandpa )
{
    Node* uncle;
    parent == grandpa->leftChild ? uncle = grandpa->rightChild : uncle = grandpa->leftChild;

    if( !uncle || uncle->color == Colour::black ) {
        if( parent == grandpa->leftChild ) {
            if( child == parent->leftChild )
                LLrotate(grandpa);
            else if( child == parent->rightChild )
                LRrotate(grandpa);
            else std::cerr << "Error: child node not an offspring of parent!\n";
        }
        else if( parent == grandpa->rightChild ) {
            if( child == parent->leftChild )
                RLrotate(grandpa);
            else if( child == parent->rightChild )
                RRrotate(grandpa);
            else std::cerr << "Error: child node not an offspring of parent!\n";
        }
        else std::cerr << "Error: parent node not an offspring of grandpa!\n";

        grandpa->color = Colour::black;
        grandpa->leftChild->color = Colour::red;
        grandpa->rightChild->color = Colour::red;
    }
    else if( uncle->color == parent->color && parent->color == Colour::red )
        recolour(parent, grandpa, uncle);
}

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::checkConflicts()
{
    CircularQueue<Node*,50> levelOrderQ = levelOrderT();

    for(unsigned i=levelOrderQ.currSize(); i-->0;) {
        Node* const g = levelOrderQ[i];
        CircularQueue<Node*,3> parentsQ;
        parentsQ.enqueue(g->leftChild);
        parentsQ.enqueue(g->rightChild);

        while( !parentsQ.isEmpty() ) {
            Node* p = parentsQ.dequeue();
            if( p ) {
                Node* const cl = p->leftChild;
                if( cl && cl->color == p->color && cl->color == Colour::red ) solveConflict(cl, p, g);
                Node* const cr = p->rightChild;
                if( cr && cr->color == p->color && cr->color == Colour::red ) solveConflict(cr, p, g);
            }
        }
    }
}

template <typename T, unsigned maxSize>
bool RedBlackTree<T,maxSize>::insert( const T& value )
{
    bool inserted = false;

    if( search(value) == nullptr ) {
        Node *p = root, *parent = nullptr, *grandpa = nullptr;
        if( !p )
            root = new Node{nullptr, nullptr, value, Colour::black};
        else {
            while( p ) {
                grandpa = parent;
                parent = p;
                value < p->data ? p = p->leftChild : p = p->rightChild;
            }

            Node* const q = new Node{nullptr, nullptr, value, Colour::red};
            value < parent->data ? parent->leftChild = q : parent->rightChild = q;

            if( q->color == parent->color ) solveConflict(q, parent, grandpa);
        }
        inserted = true;
    }

    return inserted;
}

template <typename T, unsigned maxSize>
bool RedBlackTree<T,maxSize>::remove( const T& value )
{
    bool removed = false;

    Node *p = root, *parent = nullptr;
    while( p && p->data != value ) {
        parent = p;
        value < p->data ? p = p->leftChild : p = p->rightChild;
    }

    if( p ) {
        if( !p->leftChild && !p->rightChild ) {
            p == parent->leftChild ? parent->leftChild = nullptr : parent->rightChild = nullptr;
            Colour col = p->color;
            delete p;

            if( col == Colour::black ) {
                Node* sibling;
                parent->leftChild ? sibling = parent->leftChild : sibling = parent->rightChild;

                if( sibling->color == Colour::black && !sibling->leftChild && !sibling->rightChild ) {
                    sibling->color = Colour::red;
                    parent->color = Colour::black;
                }
                else rotate(parent, true);
            }
            removed = true;
        }
        else {
            Node* ioNeighbor = p->leftChild;

            while( ioNeighbor && ioNeighbor->rightChild )
                ioNeighbor = ioNeighbor->rightChild;

            if( !ioNeighbor ) {
                ioNeighbor = p->rightChild;

                while( ioNeighbor && ioNeighbor->leftChild )
                    ioNeighbor = ioNeighbor->leftChild;
            }

            T temp = ioNeighbor->data;
            removed = remove(ioNeighbor->data);
            p->data = temp;
        }
    }

    return removed;
}

template <typename T, unsigned maxSize>
CircularQueue<typename RedBlackTree<T,maxSize>::Node*,maxSize> RedBlackTree<T,maxSize>::levelOrderT() const
{
    CircularQueue<Node*,50> levelOrderQ, auxQ;

    auxQ.enqueue(root);
    while( !auxQ.isEmpty() ) {
        Node* p = auxQ.dequeue();
        levelOrderQ.enqueue(p);
        if( p->leftChild ) auxQ.enqueue(p->leftChild);
        if( p->rightChild ) auxQ.enqueue(p->rightChild);
    }

    return levelOrderQ;
}

template <typename T, unsigned maxSize>
template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>*>
void RedBlackTree<T,maxSize>::display( Str&& ostr, CircularQueue<Node*,maxSize> (RedBlackTree<T,maxSize>::*orderFun)() const ) const
{
    CircularQueue<Node*,maxSize> orderQ = (this->*orderFun)();
    char col;

    for(unsigned i=0; i<orderQ.currSize(); ++i) {
        orderQ[i]->color == Colour::black ? col = 'b' : col = 'r';
        ostr << orderQ[i]->data << "(" << col << ") ";
    }

    ostr << "\n";
}

template <typename T, unsigned maxSize>
void RedBlackTree<T,maxSize>::clear()
{
    CircularQueue<Node*,maxSize> auxQ = levelOrderT();
    while( !auxQ.isEmpty() )
        delete auxQ.dequeue();

    root = nullptr;
}

/////////////////////////////////////////////////// The main driver function ///////////////////////////////////////////////////

auto main() -> int
{
    CircularQueue<unsigned,6> cq;
    cq.enqueue(1);
    cq.enqueue(2);
    cq.enqueue(3);
    cq.enqueue(4);
    cq.enqueue(5);
    cq.dequeue();
    cq.dequeue();
    cq.enqueue(6);
    cq.enqueue(7);

    std::cout << cq;
    while( !cq.isEmpty() )
        std::cout << "CQs: " << cq.dequeue() << "\n";

    RedBlackTree<int> rbt;
    rbt.insert(70);
    rbt.insert(40);
    rbt.insert(100);
    rbt.insert(20);
    rbt.insert(50);
    rbt.insert(10);
    rbt.insert(30);
    rbt.insert(60);
    rbt.insert(80);
    rbt.insert(110);
    rbt.insert(90);
    rbt.insert(120);

    rbt.display(std::cout, &RedBlackTree<int>::levelOrderT);

    rbt.remove(100);
    rbt.remove(110);
    rbt.remove(80);

    rbt.display(std::cout, &RedBlackTree<int>::levelOrderT);

    rbt.remove(120);
    rbt.remove(90);

    rbt.display(std::cout, &RedBlackTree<int>::levelOrderT);

    rbt.clear();
    rbt.insert(10);
    rbt.insert(20);
    rbt.insert(30);
    rbt.insert(50);
    rbt.insert(40);
    rbt.insert(60);
    rbt.insert(70);
    rbt.insert(80);

    rbt.display(std::cout, &RedBlackTree<int>::levelOrderT);

    rbt.insert(4);
    rbt.insert(8);

    rbt.display(std::cout, &RedBlackTree<int>::levelOrderT);

    return 0;
}

// https://landrover.navigation.com/product-list/Catalog/Catalog_Evoque_2012/en_GB/LandRoverEMEA/EUR
// https://www.satnavupgrades.co.uk/2020-21-LANDROVER-GEN-2-1-SAT-NAV-MAP-UPDATE-USB-NAVIGATION-EUROPE-p122222095
// https://nawigacje.eu/formularz/
// https://www.youtube.com/watch?v=vcOIbhWqi3M
// https://jaguar-ngi.welcome.naviextras.com/faq_en.html
// https://www.landrover.here.com/index.php
