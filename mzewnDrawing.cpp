#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/StateSet>

#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>

#include <osgDB/ReadFile>
#include <osg/Texture2D>

osg::ref_ptr<osg::StateSet> getLoadTexGeometry( osg::ref_ptr<osg::Geode> );
osg::ref_ptr<osg::StateSet> getPlatTexGeometry( osg::ref_ptr<osg::Geode> );

int main()
{
    // Creating the viewer
    osgViewer::Viewer viewer;

    // Creating the root node
    osg::ref_ptr<osg::Group> mzewnRoot(new osg::Group);

    // The modelled shapes
    const float     loadEdge  = 5.0;
    const osg::Vec3 loadCentre(0.0, 0.0, 0.0);
    const float     platEdgeX = 12.0;
    const float     platEdgeY = platEdgeX;
    const float     platEdgeZ = 0.5;
    const osg::Vec3 platCentre( 0.0, 0.0, -(loadEdge+platEdgeZ)/2 );

    osg::ref_ptr<osg::Box> loadNode(new osg::Box(loadCentre, loadEdge));
    osg::ref_ptr<osg::Box> platNode(new osg::Box(platCentre, platEdgeX, platEdgeY, platEdgeZ));

    // Platform and load shape drawables
    osg::ref_ptr<osg::ShapeDrawable> loadDrawable(new osg::ShapeDrawable(loadNode.get()));
    osg::ref_ptr<osg::ShapeDrawable> platDrawable(new osg::ShapeDrawable(platNode.get()));

    // The nodes containing the modelled shapes
    osg::ref_ptr<osg::Geode> loadGeode(new osg::Geode);
    osg::ref_ptr<osg::Geode> platGeode(new osg::Geode);

    osg::ref_ptr<osg::StateSet> stateSet = getLoadTexGeometry( loadGeode );

    // Adding shapes to the nodes
    loadGeode->addDrawable(loadDrawable.get());
    platGeode->addDrawable(platDrawable.get());

    // Adding the nodes to the root node
    mzewnRoot->addChild(loadGeode.get());
    mzewnRoot->addChild(platGeode.get());

    // Stats Event Handler s key
    viewer.addEventHandler(new osgViewer::StatsHandler);

    // Windows size handler
    viewer.addEventHandler(new osgViewer::WindowSizeHandler);

    viewer.setSceneData(mzewnRoot.get());

    return viewer.run();
}

osg::ref_ptr<osg::StateSet> getLoadTexGeometry( osg::ref_ptr<osg::Geode> loadG )
{
    // Adding texture image
    osg::ref_ptr<osg::Image> image(osgDB::readImageFile("woodenbox.png"));

    if(image.get() == 0)
    {
        std::cerr << "woodenbox.png not found!" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Adding images to textures
    osg::ref_ptr<osg::Texture2D> texture(new osg::Texture2D);
    texture->setImage(image.get());

    // Creating a state set
    osg::ref_ptr<osg::StateSet> stateSet( loadG->getOrCreateStateSet() );

    // Adding textures to the state set
    stateSet->setTextureAttributeAndModes(0, texture.get(), osg::StateAttribute::ON );

    // Creating coords for the texture
    osg::ref_ptr<osg::Vec2Array> texCoords (new osg::Vec2Array);

    texCoords->push_back (osg::Vec2 (0.0, 0.0));
    texCoords->push_back (osg::Vec2 (0.0, 1.0));
    texCoords->push_back (osg::Vec2 (1.0, 1.0));
    texCoords->push_back (osg::Vec2 (1.0, 0.0));

    // Creating a geometry
    //osg::ref_ptr<osg::Geometry> geometry(new osg::Geometry);

    // Adding the state set to the geometry
    //loadG->setTexCoordArray (0, texCoords.get());

    return stateSet;
}

osg::ref_ptr<osg::StateSet> getPlatTexGeometry( osg::ref_ptr<osg::Geode> platG )
{
    // Adding texture images
    osg::ref_ptr<osg::Image> image1(osgDB::readImageFile("woodenplanks.png"));

    // Creating an array for texture coordinates
    osg::Vec2Array* pTexCoords = new osg::Vec2Array( 4 );
    (*pTexCoords)[0];
}
