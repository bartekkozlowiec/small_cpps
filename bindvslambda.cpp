#include <cstdio>
#include <functional>

// A simple move-only type
class WrapChar
{
    char c;

public:
    constexpr explicit WrapChar(char c) : c{c} {}
    WrapChar(WrapChar&& other) = default;
    WrapChar& operator=(WrapChar&& other) = default;
    constexpr char content() const noexcept { return c; }
};

// Function showing init-capture mechanism introduced in C++14
void MoveObjToLambda14()
{
    WrapChar wc{'a'}; // a move-only object

    auto getContent = [wc = std::move(wc)]() {
        printf("The captured content is: %c\n", wc.content());
    };

    // Try printing the captured object twice
    getContent();
}

// Emulation of the above function using C++11-only utilities
void MoveObjToLambda11()
{
    WrapChar wc1{'b'};
    WrapChar wc2{'c'};

    auto getContent1 = std::bind([](const WrapChar& wc) {
        printf("The captured content is: %c\n", wc.content());
    }, std::move(wc1));

    auto getContent2 = std::bind([](WrapChar&& wc) {
        printf("The captured content is: %c\n", wc.content());
    }, std::move(wc2));

    getContent1();

    // Won't compile: can't move the move-constructed wc2 object inside the bind object (which is an lvalue)
    // into the capture object's call operator
    //getContent2();
}

int main()
{
    MoveObjToLambda14();
    MoveObjToLambda11();

    return 0;
}
