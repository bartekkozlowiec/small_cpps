#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream strum;     // definicja strumienia do pracy z plikiem
    char nazwa_pliku[80];

    //-----Otwieranie pliku moze ustawic flage bledu ios::badbit
    //w razie gdy plik nie istnieje. Strumien tkwi wowczas w stanie
    //bledu. Aby ponowic probe otwarcia pliku flage trzeba skasowac.

    for(int sukces = 0; !sukces; )
    {
        cout << "Podaj nazwe pliku: ";
        cin >> nazwa_pliku;
        cout << endl;       // kosmetyka ekranu

        //Proba otwarcia
        strum.open(nazwa_pliku, ios::in);

        //Czy sie udalo?
        if( !strum )        // == if( strum.fail() )
        {
            cout << "Blad otwarcia pliku: " << nazwa_pliku << endl;
            //Skoro proba otwarcia nie udala sie, to strumien jest
            //w stanie bledu! Musimy usuwac stan powaznego bledu strumienia.
            strum.clear( strum.rdstate() & ~ios::badbit );

            //Strumien jest juz w porzadku. W kolejnym obiegu petli:
            cout << "Ponawiamy probe...\n";
        }
        else
            sukces = 1;     // Udalo sie otworzyc wiec petle mozna zakonczyc.
    }

    //---Operacje czytanie moga wywolac ustawienie flagi bledu ios::eofbit
    //w wypadku, gdy dojdziemy do konca pliku i mimo to probujemy
    //czytac nadal. Aby pracowac z tym strumieniem musimy skasowac te flage
    //bledu (a potem ewentualnie ustawic kursor czytania w poprawne miejsce).
    int  numer;
    char znak;

    for(int sukces2 = 0; !sukces2; )
    {
        cout << "Podaj numer bajtu ktory chcesz poznac: ";
        cin >> numer;
        //Pozycjonujemy kursor czytania na tym bajcie.
        strum.seekg(numer);

        znak = strum.get();
        if( strum.eof() )
        {
            cout << "Blad pozycjonowania, prawdopodobnie plik\n\t jest krotszy"
                    " niz " << numer << " bajow.\n";
            //Strumien tkwi w stanie bledu ios::eofbit, trzeba go skasowac.
            strum.clear( strum.rdstate() & ~ios::eofbit );
            cout << "(Podaj mniejsza liczbe)\n";
            //Bedzie ponowny obieg petli.
        }
        else
        {
            sukces2 = 1;
            cout << "Ten bajt to ASCII: '" << znak << "' hexadecymalnie " << hex
                 << (int)znak << endl;
        }
    }

    //----------Proba formatowanego wczytania liczby w sytuacji, gdy
    //wlasnie oczekuje na wczytanie cos, co liczba nie jest wprowadzi
    //strumien w stan bledu ios::failbit.

    int liczba;
    //Instrukcja wczytania liczby - rozpocznie czytac zaraz po wczytanym
    //poprzednio bajcie.
    strum >> liczba;

    //Czy sie udalo?
    if( strum.fail() )
    {                   // nie!
        cout << "Blad failbit, bo najblizsze bajty \n\t nie moga byc "
             << "wczytanie jako liczba.\n";

        //Aby to cos wczytac jako string nalezy najpierw skasowac ustawiona
        //flage bledu.
        strum.clear( strum.rdstate() & ~ios::failbit );

        //Strumien nadaje sie juz do pracy, a to cos, co go zatkalo nadal
        //czeka na wczytanie.
        char slowo[50];
        strum >> slowo;
        cout << "Jest to slowo: " << slowo << endl;
    }
    else
    {                   // tak!
        cout << "Pomyslnie wczytana liczba: " << liczba << endl;
    }

    //dalsza praca z plikiem...
    ifstream pot("t016.cpp");
    char slowo1[10], slowo2[10];
    pot >> slowo1;
    pot >> slowo2;
    cout << "Slowa, slowa... " << slowo1 << " " << slowo2 << endl;

    return 0;
}
