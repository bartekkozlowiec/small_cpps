#include <iostream>

/*************************************************** Type trait utilities ***************************************************/

template <typename ... Ts>
struct void_t_impl
{
    typedef void type;
};

template <typename ... Ts>
using void_t = typename void_t_impl<Ts...>::type;

template <typename T>
T&& declval();

/*********************************** Template function to print array of streamable type ************************************/

template <typename T, typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
void printArray( const T array[], const unsigned n, Str&& ostr )
{
    ostr << "Array contents: ";
    for(unsigned i=0; i<n; ++i)
        ostr << array[i] << " ";
    ostr << "\n";
}

/********************************* A simple heap template class implementation using array **********************************/

template <typename T, bool isMin = false, unsigned maxSize = 100>
class Heap
{
    typename std::aligned_storage<alignof(T), sizeof(T)>::type storage[maxSize];
    unsigned size;

    void heapify( const unsigned );
    void swap( const unsigned, const unsigned );
    bool (*inequal)( const T&, const T& );

public:
    constexpr Heap();
    Heap( const T[], const unsigned );
    ~Heap();

    constexpr bool isEmpty() const { return size == 0; }
    constexpr bool isFull() const { return size == maxSize; }

    bool insert( const T& );
    T erase();
    const T& getRoot() const { return *reinterpret_cast<const T*>(storage); }

    template <typename Str>
    void display( Str&&, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr );
};

template <typename T, bool isMin, unsigned maxSize>
constexpr Heap<T,isMin,maxSize>::Heap() : size{0}
{
    if( isMin )
        inequal = []( const T& t1, const T& t2 ) { return t1<t2; };
    else
        inequal = []( const T& t1, const T& t2 ) { return t1>t2; };
}

template <typename T, bool isMin, unsigned maxSize>
Heap<T,isMin,maxSize>::Heap( const T arr[], const unsigned n ) : Heap()
{
    if( n > maxSize ) {
        std::cerr << "Warning: input array's size bigger than heap's max length. Trailing elements won't be added.\n";
        size = maxSize;
    }
    else size = n;

    for(unsigned i=0; i<size; ++i)
        new(storage+i) T{arr[i]};

    for(unsigned i=size; i-->0;)
        heapify(i);
}

template <typename T, bool isMin, unsigned maxSize>
void Heap<T,isMin,maxSize>::heapify( const unsigned root )
{
    if( root < (size-1) ) {
        const unsigned lChild = 2*root+1;
        const unsigned rChild = 2*root+2;

        if( rChild < size ) {
            if( inequal(*reinterpret_cast<const T*>(storage+rChild), *reinterpret_cast<const T*>(storage+root))) {
                if( inequal(*reinterpret_cast<const T*>(storage+rChild), *reinterpret_cast<const T*>(storage+lChild)) ) {
                    swap(rChild, root);
                    heapify(rChild);
                }
                else {
                    swap(lChild, root);
                    heapify(lChild);
                }
            }
            else if( inequal(*reinterpret_cast<const T*>(storage+lChild), *reinterpret_cast<const T*>(storage+root)) ) {
                swap(lChild, root);
                heapify(lChild);
            }
        }
        else if( lChild < size )
            if( inequal(*reinterpret_cast<const T*>(storage+lChild), *reinterpret_cast<const T*>(storage+root)) ) {
                swap(lChild, root);
                heapify(lChild);
            }
    }
}

template <typename T, bool isMin, unsigned maxSize>
void Heap<T,isMin,maxSize>::swap( const unsigned i1, const unsigned i2 )
{
    if( i1 != i2 ) {
        T t1 = *reinterpret_cast<const T*>(storage+i1);
        T t2 = *reinterpret_cast<const T*>(storage+i2);

        if( t1 != t2 ) {
            T temp = std::move(t1);
            *reinterpret_cast<T*>(storage+i1) = std::move(t2);
            *reinterpret_cast<T*>(storage+i2) = std::move(temp);
        }
    }
}

template <typename T, bool isMin, unsigned maxSize>
bool Heap<T,isMin,maxSize>::insert( const T& elem )
{
    bool inserted = false;

    if( !isFull() ) {
        // Insert new node at the end of storage to maintain a complete binary tree
        T* temp = new(storage+size) T{elem};
        if( temp ) {
            unsigned root, curr = size;
            bool swapped = true;

            do {
                curr % 2 ? root = curr/2 : root = (curr-1)/2;

                if( inequal(storage[curr], storage[root]) ) {
                    swap(storage+curr, storage+root);
                    curr = root;
                }
                else swapped = false;
            } while( root > 0 && swapped );

            inserted = true;
            ++size;
        }
    }

    return inserted;
}

template <typename T, bool isMin, unsigned maxSize>
T Heap<T,isMin,maxSize>::erase()
{
    T erased{};

    // In a heap you can only delete the root of the tree
    if( !isEmpty() ) {
        erased = *reinterpret_cast<const T*>(storage);
        swap(0, size-1);
        --size;
        reinterpret_cast<T*>(storage+size)->~T();
        heapify(0);
    }

    return erased;
}

template <typename T, bool isMin, unsigned maxSize>
template <typename Str>
void Heap<T,isMin,maxSize>::display( Str&& ostr, void_t<decltype(declval<Str>() << declval<T>())>* )
{
    ostr << "Heap contents: ";
    for(unsigned i=0; i<size; ++i)
        ostr << *reinterpret_cast<T*>(storage+i) << " ";
    ostr << "\n";
}

template <typename T, bool isMin, unsigned maxSize>
Heap<T,isMin,maxSize>::~Heap()
{
    for(unsigned i=0; i<size; ++i)
        reinterpret_cast<T*>(storage+i)->~T();
}

/********************************************** The main driver function ****************************************************/

auto main() -> int
{
    float fArray[] = { 8.4f, 37.2f, -26.1f, 65.7f, -2.9f, 42.0f, 14.8f, -13.6f, 21.5f, 50.3f };
    const unsigned fSize = sizeof(fArray)/sizeof(float);

    std::cout << "Input array.";
    printArray(fArray, fSize, std::cout);
    std::cout << "\n";

    Heap<float,true> fHeap{fArray, fSize};
    fHeap.display(std::cout);
    std::cout << "\n";

    // 1st sorting method
    float sfArray1[fSize];
    for(unsigned i=0; i<fSize; ++i)
        sfArray1[i] = fHeap.erase();

    std::cout << "Array after sorting by heap extraction.\n";
    printArray(sfArray1, fSize, std::cout);
    std::cout << "\n";

    // 2nd sorting method (heap sort using heapify)
    float sfArray2[fSize];
    for(unsigned i=0; i<fSize; ++i) {
        Heap<float,true> fHeap{fArray, fSize-i};
        sfArray2[i] = fHeap.getRoot();
    }

    std::cout << "Array after sorting by heapifying.\n";
    printArray(sfArray1, fSize, std::cout);

    return 0;
}
