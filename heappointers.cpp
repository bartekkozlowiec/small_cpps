#include <iostream>
#include <cassert>

/****************************************** Utilities for template metaprogramming ******************************************/

template <typename... Ts>
struct void_t_impl
{
    typedef void type;
};

template <typename... Ts>
using void_t = typename void_t_impl<Ts...>::type;

template <bool Cond, typename T = void>
struct enable_if
{};

template <typename T>
struct enable_if<true,T>
{
    typedef T type;
};

template <bool Cond, typename T = void>
using enable_if_t = typename enable_if<Cond,T>::type;

template <typename T1, typename T2, typename... Ts>
struct is_same
{
    static constexpr bool value = is_same<T1,T2>::value && is_same<T2,Ts...>::value;
};

template <typename T1, typename T2>
struct is_same<T1,T2>
{
    static constexpr bool value = false;
};

template <typename T>
struct is_same<T,T>
{
    static constexpr bool value = true;
};

template <typename T>
struct is_positive_integer
{
    static constexpr bool value = false;
};

template <>
struct is_positive_integer<unsigned>
{
    static constexpr bool value = true;
};

template <>
struct is_positive_integer<long unsigned>
{
    static constexpr bool value = true;
};

template <typename T, enable_if_t<is_positive_integer<T>::value>* = nullptr>
T power(const T base, const T exponent)
{
    auto result = 1u;
    for (T i = 0; i < exponent; ++i)
        result *= base;

    return result;
}

template <typename T>
T&& declval();

/*********************************** Template function to print array of streamable type ************************************/

template <typename T, typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
void printArray(const T array[], const unsigned size, Str& ostr)
{
    for (unsigned i = 0; i < size; ++i)
        ostr << array[i] << " ";
    ostr << "\n";
}

/*************************** An auxiliary queue template class implementation using array storage ***************************/

template <typename T, unsigned maxSize>
class CircularQueue
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type storage[maxSize];
    unsigned front, rear;

public:
    constexpr CircularQueue() noexcept : front{0}, rear{0} {}
    ~CircularQueue() { while (!isEmpty()) dequeue(); }
    constexpr unsigned getSize() noexcept { return rear >= front ? rear - front : rear + maxSize - front; }
    constexpr bool isEmpty() noexcept { return front == rear; }
    constexpr bool isFull() noexcept { return (rear + 1) % maxSize - front == 0; }

    T& operator[](unsigned index) const;
    bool enqueue(const T& elem);
    T dequeue();
};

template <typename T, unsigned maxSize>
T& CircularQueue<T,maxSize>::operator[](unsigned index) const
{
    assert (index < getSize());
    index = (front + index) % maxSize;
    return *reinterpret_cast<T*>(storage+index);
}

template <typename T, unsigned maxSize>
bool CircularQueue<T,maxSize>::enqueue(const T& elem)
{
    bool enqueued = false;

    if (!isFull()) {
        new(storage+rear) T{elem};
        (++rear) %= maxSize;
        enqueued = true;
    }

    return enqueued;
}

template <typename T, unsigned maxSize>
T CircularQueue<T,maxSize>::dequeue()
{
    T retElem = {};

    if (!isEmpty()) {
        retElem = std::move(*reinterpret_cast<T*>(storage+front));
        (++front) %= maxSize;
        reinterpret_cast<T*>(storage+front)->~T();
    }

    return retElem;
}

/******************************** A simple heap template class implementation using pointers ********************************/

template <typename T, bool isMax = true>
class Heap // MAX heap
{
    struct Node {
        Node* leftLeaf;
        Node* riteLeaf;
        T value;
    }* root;

    void swap(Node* const n1, Node* const n2);
    unsigned height() const;
    void heapify(Node* const currRoot);

    bool (*ineq)(const T elem1, const T elem2);

    template <typename Arg, typename... Args>
    void orderInsert(Arg arg, Args... args);

    template <typename Arg>
    void orderInsert(Arg arg);

public:
    constexpr Heap() noexcept;
    Heap(const T array[], const unsigned size);

    template <typename... Args, enable_if_t<is_same<T,Args...>::value>* = nullptr>
    Heap(Args... args);

    void insert(const T& elem);
    T erase();
    bool empty() const noexcept { return root == nullptr; }

    template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
    void display(Str& ostr);
};

template <typename T, bool isMax>
constexpr Heap<T,isMax>::Heap() noexcept
    : root{nullptr}
{
     ineq = isMax ? [](const T el1, const T el2) { return el1 > el2; } : [](const T el1, const T el2) { return el1 < el2; };
}

template <typename T, bool isMax>
Heap<T,isMax>::Heap(const T array[], const unsigned size) : Heap()
{
    if (size == 0)
        return;

    // Create ordinary binary tree from array elements
    for (unsigned i = 0; i < size; ++i) {
        const auto newNode = new Node{nullptr, nullptr, array[i]};

        if (!root) {
            root = newNode;
            continue;
        }

        Node* nodePtr = root;
        while (nodePtr->riteLeaf)
            nodePtr = nodePtr->riteLeaf;

        if (nodePtr->leftLeaf)
            nodePtr->riteLeaf = newNode;
        else
            nodePtr->leftLeaf = newNode;
    }

    // Store nodes using level-order traversal
    constexpr unsigned qSize = 100;
    const auto maxSizeForHeight = power(2u, height()) - 1;
    assert(qSize > maxSizeForHeight);
    CircularQueue<Node*,qSize> cq;
    Node** levelOrderT = new Node*[maxSizeForHeight]{nullptr};

    cq.enqueue(root);
    unsigned nodeCnt = 0;

    while (!cq.isEmpty()) {
        const auto nodePtr = cq.dequeue();
        levelOrderT[nodeCnt++] = nodePtr;

        if (nodePtr->leftLeaf) cq.enqueue(nodePtr->leftLeaf);
        if (nodePtr->riteLeaf) cq.enqueue(nodePtr->riteLeaf);
    }

    // Heapify the tree from bottom upwards
    while (nodeCnt-- > 0)
        heapify(levelOrderT[nodeCnt]);
}

template <typename T, bool isMax>
template <typename... Args, enable_if_t<is_same<T,Args...>::value>*>
Heap<T,isMax>::Heap(Args... args) : Heap()
{
    orderInsert(args...);
}

template <typename T, bool isMax>
template <typename Arg, typename... Args>
void Heap<T,isMax>::orderInsert(Arg arg, Args... args)
{
    insert(arg);
    orderInsert(args...);
}

template <typename T, bool isMax>
template <typename Arg>
void Heap<T,isMax>::orderInsert(Arg arg)
{
    insert(arg);
}

template <typename T, bool isMax>
void Heap<T,isMax>::swap(Node* const n1, Node* const n2)
{
    if (n1 && n2) {
        auto tempVal = std::move(n1->value);
        n1->value = std::move(n2->value);
        n2->value = std::move(tempVal);
    }
}

template <typename T, bool isMax>
unsigned Heap<T,isMax>::height() const
{
    unsigned height = root ? 1 : 0;

    const Node* nodePtr = root;
    while (nodePtr->leftLeaf) {
        nodePtr = nodePtr->leftLeaf;
        ++height;
    }

    return height;
}

template <typename T, bool isMax>
void Heap<T,isMax>::heapify(Node* const currRoot)
{
    if (!currRoot)
        return;

    // Reorder to restore heap
    Node* currPtr = currRoot;
    while (currPtr->leftLeaf) {
        Node* ptrToCheck = currPtr->leftLeaf;

        if (currPtr->riteLeaf)
            if (ineq(currPtr->riteLeaf->value, ptrToCheck->value))
                ptrToCheck = currPtr->riteLeaf;

        if (ineq(ptrToCheck->value, currPtr->value)) {
            swap(currPtr, ptrToCheck);
            currPtr = ptrToCheck;
        }
        else break;
    }
}

template <typename T, bool isMax>
void Heap<T,isMax>::insert(const T& elem)
{
    const auto newNode = new Node{nullptr, nullptr, elem};

    if (root) {
        const auto heapHeight = height() + 1;
        Node** nodesVisited = new Node*[heapHeight];
        unsigned nodeCnt = 0;
        Node* nodePtr = root;
        nodesVisited[nodeCnt++] = nodePtr;

        while (nodePtr->riteLeaf) {
            nodePtr = nodePtr->riteLeaf;
            nodesVisited[nodeCnt++] = nodePtr;
        }

        if (nodePtr->leftLeaf)
            nodePtr->riteLeaf = newNode;
        else
            nodePtr->leftLeaf = newNode;

        nodesVisited[nodeCnt] = newNode;

        while (nodeCnt && ineq(nodesVisited[nodeCnt]->value, nodesVisited[nodeCnt-1]->value)) {
            swap(nodesVisited[nodeCnt], nodesVisited[nodeCnt-1]);
            --nodeCnt;
        }
    }
    else
        root = newNode;
}

template <typename T, bool isMax>
T Heap<T,isMax>::erase()
{
    if (!root)
        return {};

    // The value to be erased from heap must come from the root node
    const auto retValue = root->value;

    Node* currPtr = root;
    Node* prevPtr = nullptr;
    bool deletedRite = true;

    while (currPtr->riteLeaf) {
        prevPtr = currPtr;
        currPtr = currPtr->riteLeaf;
    }

    if (currPtr->leftLeaf) {
        prevPtr = currPtr;
        currPtr = currPtr->leftLeaf;
        deletedRite = false;
    }

    if (currPtr == root && !prevPtr) {
        delete root;
        root = nullptr;
        return retValue;
    }

    // Swap root value that is to be deleted with the right most bottom leave
    swap(root, currPtr);

    // Delete the root value after swapping
    delete currPtr;
    deletedRite ? prevPtr->riteLeaf = nullptr : prevPtr->leftLeaf = nullptr;

    // Reorder after swapping to restore heap
    currPtr = root;
    while (currPtr->leftLeaf) {
        Node* ptrToCheck = currPtr->leftLeaf;

        if (currPtr->riteLeaf)
            if (ineq(currPtr->riteLeaf->value, ptrToCheck->value))
                ptrToCheck = currPtr->riteLeaf;

        if (ineq(ptrToCheck->value, currPtr->value)) {
            swap(currPtr, ptrToCheck);
            currPtr = ptrToCheck;
        }
        else break;
    }

    return retValue;
}

template <typename T, bool isMax>
template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>*>
void Heap<T,isMax>::display(Str& ostr)
{
    if (!height()) return;

    constexpr unsigned qSize = 100;
    assert(qSize > power(2u, height()) - 1);
    CircularQueue<Node*,qSize> cq;

    // Display using level-order traversal
    cq.enqueue(root);

    while (!cq.isEmpty()) {
        const auto nodePtr = cq.dequeue();
        ostr << nodePtr->value << " ";

        if (nodePtr->leftLeaf) cq.enqueue(nodePtr->leftLeaf);
        if (nodePtr->riteLeaf) cq.enqueue(nodePtr->riteLeaf);
    }

    ostr << "\n";
}

/*************************************** Sorting using heap creation and deletion ***************************************/

template <typename T>
void heapSortCreate(T array[], const unsigned size)
{
    Heap<T,false> heap;

    unsigned i = 0;
    for (; i < size; ++i)
        heap.insert(*(array+i));

    i = 0;
    while (!heap.empty())
        array[i++] = heap.erase();
}

/******************************************** Sorting using heapify procedure *******************************************/

template <typename T>
void heapSortHeapify(T array[], const unsigned size)
{
    Heap<T> heap(array, size);

    unsigned i = size;
    while (!heap.empty())
        array[--i] = heap.erase();
}

/*********************************************** The main driver function ***********************************************/

int main()
{
    Heap<unsigned> uHeap(7u, 1u, 8u, 5u, 4u, 9u, 5u, 2u, 3u, 0u, 6u);
    uHeap.display(std::cerr);

    while (!uHeap.empty())
        std::cout << "Deleted element: " << uHeap.erase() << "\n";

    int iArray[] = {17, -31, 8, 65, 44, 29, 75, -12, 53, -20, -6};
    const auto iSize = sizeof(iArray)/sizeof(int);
    std::cout << "\nArray of ints before sorting: ";
    printArray(iArray, iSize, std::cout);

    heapSortCreate(iArray, iSize);
    std::cout << "Array of ints sorted: ";
    printArray(iArray, iSize, std::cout);

    float fArray[] = {-21.9f, -34.5f, 52.3f, 38.7f, 5.1f, 67.6f, 26.4f, -3.0f, 70.8f, 19.2f};
    const auto fSize = sizeof(fArray)/sizeof(float);
    std::cout << "\nArray of floats before sorting: ";
    printArray(fArray, fSize, std::cout);

    heapSortHeapify(fArray, fSize);
    std::cout << "Array of floats sorted: ";
    printArray(fArray, fSize, std::cout);

    return 0;
}
