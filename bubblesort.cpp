#include <iostream>

////////////////////////////////////// Utilities to allow printing of only streamable types //////////////////////////////////////

template <typename T>
struct void_t_impl
{
    typedef void type;
};

template <typename T>
using void_t = typename void_t_impl<T>::type;

template <typename T>
T&& declval();

///////////////////////////////////////// Function template for printing of an array /////////////////////////////////////////////

template <typename T, typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
void printArray( const T array[], const unsigned n, Str&& ostr )
{
    ostr << "Array contents: ";
    for(unsigned i=0; i<n; ++i)
        ostr << array[i] << " ";
    ostr << "\n";
}

///////////////////////////////////////////////////// Sorting algorithms /////////////////////////////////////////////////////////

template <typename T>
void swap( T* const t1, T* const t2 )
{
    if( *t1 != *t2 ) {
        T temp = std::move(*t1);
        *t1 = std::move(*t2);
        *t2 = std::move(temp);
    }
}

template <typename T>
void bubbleSort( T array[], const unsigned n )
{
    for(unsigned i=0; i<n-1; ++i) {
        bool swapped = false;

        for(unsigned j=1; j<n-i; ++j)
            if( array[j] < array[j-1] ) {
                swap(array+j, array+j-1);
                swapped = true;
            }

        if( !swapped )
            break;
    }
}

template <typename T>
void insertionSort( T array[], const unsigned n )
{
    for(unsigned i=1; i<n; ++i) {
        unsigned j=i;
        T newInsert = array[i];

        while( j-->0 && array[j] > newInsert )
            array[j+1] = array[j];

        array[j+1] = newInsert;
    }
}

template <typename T>
void selectionSort( T array[], const unsigned n )
{
    for(unsigned i=0; i<n-1; ++i) {
        unsigned j, k;
        for(j=k=i+1; j<n; ++j) {
            if( array[j] < array[k] )
                k = j;
        }
        swap(array+i, array+k);
    }
}

////////////////////////////////////////////////// The main driver function //////////////////////////////////////////////////////

int main()
{
    int iArray[] = { 31, 5, 48, -23, 59, 22, 67, -4, 16, -10 };
    const unsigned iSize = sizeof(iArray)/sizeof(unsigned);
    printArray(iArray, iSize, std::cout);

    selectionSort(iArray, iSize);
    printArray(iArray, iSize, std::cout);

    return 0;
}

// https://stackoverflow.com/questions/32543475/how-python-cvxopt-solvers-qp-basically-works
// https://stackoverflow.com/questions/59372831/cvxopt-uses-just-one-core-need-to-run-on-all-some
