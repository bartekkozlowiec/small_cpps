// G-Sach sample coding tasks

#include <array>
#include <vector>
#include <list>
#include <stack>
#include <queue>
#include <deque>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <utility>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <cassert>
#include <iomanip>
#include <bitset>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <ctime>
#include <map>
#include <set>

/************************************************ Function for printing an iterable ***********************************************/

template <typename T, typename Stream>
void printIterable( T&& arrLike, Stream&& str )
{
    for(const auto& elem : arrLike)
        str << elem << " ";
    str << "\n";
}

/********************************************************* G4G excercises *********************************************************/

// https://www.geeksforgeeks.org/goldman-sachs-interview-experience-software-engineer-experienced/
// https://www.geeksforgeeks.org/tag/goldman-sachs/

// 1. Circle and Lattice Points
/// Given a circle of radius r in 2-D with origin or (0, 0) as center. The task is to find the total lattice points on circumference.
/// Lattice Points are points with coordinates as integers in 2-D space.

std::vector<std::array<int,2> > findLatticePoints1( const int r, const std::array<int,2>& origin )
{
    std::vector<std::array<int,2> > pointsAtCirc;

    const int minX = origin[0] - r;
    const int maxX = origin[0] + r;
    const int minY = origin[1] - r;
    const int maxY = origin[1] + r;

    for(int x=minX; x<=maxX; ++x)
        for(int y=minY; y<=maxY; ++y)
            if( (x-origin[0])*(x-origin[0]) + (y-origin[1])*(y-origin[1]) == r*r )
                pointsAtCirc.push_back(std::array<int,2>{x,y});

    return pointsAtCirc;
}

std::vector<std::array<int,2> > findLatticePoints2( const int r, const std::array<int,2>& origin )
{
    std::vector<std::array<int,2> > pointsAtCirc;

    const int minX = origin[0] - r;
    const int maxX = origin[0] + r;
    const int minY = origin[1] - r;
    const int maxY = origin[1] + r;

    for(int x=minX; x<=maxX; ++x) {
        const double yNeededUp = sqrt(r*r - (x-origin[0])*(x-origin[0]));
        const double yNeededDown = -yNeededUp;

        double yIntPart;
        const double yFloatPart = modf(yNeededUp, &yIntPart);
        std::cout << "Y-UP: " << yNeededUp << ", MODF: " << yFloatPart << std::endl;

        if( yFloatPart == 0.0 )
            pointsAtCirc.push_back(std::array<int,2>{x,origin[1]+static_cast<int>(yNeededUp)});
        if( static_cast<int>(yNeededUp) != static_cast<int>(yNeededDown) )
            if( yFloatPart == 0.0 )
                pointsAtCirc.push_back(std::array<int,2>{x,origin[1]+static_cast<int>(yNeededDown)});
    }

    return pointsAtCirc;
}

// 2. Count ways to reach the nth stair
/// There are n stairs, a person standing at the bottom wants to reach the top. The person can climb either 1 stair
/// or 2 stairs at a time. Count the number of ways, the person can reach the top.

unsigned countWaysToReachStair( const unsigned curr, const unsigned nth )
{
    static unsigned waysCnt = 0;

    if( curr+1 < nth )
        countWaysToReachStair(curr+1, nth);
    else if( curr+1 == nth )
        ++waysCnt;

    if( curr+2 < nth )
        countWaysToReachStair(curr+2, nth);
    else if( curr+2 == nth )
        ++waysCnt;

    return waysCnt;
}

unsigned factorial( const unsigned n )
{
    unsigned i = 1, f = 1;
    while( i <= n ) f *= (i++);
    return f;
}

unsigned countWaysToReachStair( const unsigned nth )
{
    unsigned waysCnt = 1; // Single steps only
    unsigned sum = nth;

    while( sum > 0 ) {
        if( sum % 2 == 0 ) {
            const unsigned numOfSteps = nth - sum + sum/2;
            waysCnt += factorial(numOfSteps) / (factorial(nth-sum) * factorial(sum/2));
        }
        --sum;
    }

    return waysCnt;
}

// 3. Collect maximum coins before hitting a dead end
/// Given a character matrix where every cell has one of the following values: 'C' (coin), 'E' (empty cell), '#' (blocking
/// cell - stop all movement) + initial position (0,0) + initial direction (right) -- collect maximum coins. You can only
/// move to the side or downwards - if you move downwards, the direction changes (right->left and left->right).

constexpr char sandbox[5][5] = { {'E','C','C','C','C'},
                                 {'C','#','C','#','E'},
                                 {'#','C','C','#','C'},
                                 {'C','E','E','C','E'},
                                 {'C','E','#','C','E'} };

std::vector<unsigned> getCoins(unsigned iPos, unsigned jPos, bool faceRight, unsigned currVal)
{
    static std::vector<unsigned> coinsCollected;
    bool atEnd = false;

    if( sandbox[iPos][jPos] == 'C' )
        ++currVal;
    else if( sandbox[iPos][jPos] == '#' ) {
        coinsCollected.push_back(currVal);
        atEnd = true;
    }

    if( !atEnd ) {
        if( faceRight ) {
            if( jPos < 4 )
                getCoins(iPos, jPos+1, true, currVal);

            if( iPos < 4 )
                getCoins(iPos+1, jPos, false, currVal);
        }
        else {
            if( jPos > 0 )
                getCoins(iPos, jPos-1, false, currVal);

            if( iPos < 4 )
                getCoins(iPos+1, jPos, true, currVal);
        }
    }

    return coinsCollected;
}

// 4. Find recurring sequence in a fraction
/// Given a fraction, find recurring sequence of digits if exists, otherwise print “No recurring sequence”.
/// https://www.learncpp.com/cpp-tutorial/floating-point-numbers/

unsigned sequenceInFraction( const int numerator, const int denominator )
{
    unsigned seq = 0;
    std::vector<unsigned> frNumbers;

    const float fraction = static_cast<float>(numerator)/denominator;
    const int intPartLen = log10(fabs(fraction))+1;
    float floatPart = fraction - numerator/denominator;

    for(unsigned dig=0; dig<(8-intPartLen); ++dig)
        frNumbers.push_back(static_cast<unsigned>((floatPart-=static_cast<unsigned>(floatPart))*=10));

    //std::cout << "FRACTION: " << std::setprecision(20) << fraction << " " << numerator/denominator << "\n";
    //printIterable(frNumbers,std::cout);

    std::array<unsigned,10> digits{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    unsigned i;
    for(i=0; i<frNumbers.size(); ++i) {
        ++digits[frNumbers[i]];
        printIterable(digits,std::cout);
        if( digits[frNumbers[i]] == 2 ) {
            seq = frNumbers[i++];
            break;
        }
    }

    for(; i<frNumbers.size(); ++i) {
        ++digits[frNumbers[i]];
        if( digits[frNumbers[i]] == 2 )
            (seq *= 10) += frNumbers[i];
        else
            break;
    }

    return seq;
}

// 5. Maximum sum path in a matrix from top to bottom
/// Consider a n*n matrix. Suppose each cell in the matrix has a value assigned. We can go from each cell in row i to
/// a diagonally higher cell in row i+1 only [i.e from cell(i, j) to cell(i+1, j-1) and cell(i+1, j+1) only]. Find the path
/// from the top row to the bottom row following the aforementioned condition such that the maximum sum is obtained.

constexpr int matx[4][4] = { {5,6,1,7},
                             {-2,10,8,-1},
                             {3,-7,-9,11},
                             {12,-4,2,6} };

struct pairHash
{
    template <class T1, class T2>
    unsigned operator()( const std::pair<T1,T2>& p ) const { return std::hash<T1>{}(p.first) ^ std::hash<T2>{}(p.second); }
};

void addNum( std::vector<int>& sums, int currSum, const unsigned row, const unsigned col/*,
             std::vector<std::tuple<unsigned,unsigned,int> > prevRes,
             std::unordered_map<std::pair<unsigned,unsigned>,int,pairHash>& resCache*/ )
{
    const unsigned rowLen = sizeof(matx[0])/sizeof(int);
    const unsigned colLen = sizeof(matx)/sizeof(matx[0]);

    currSum += matx[row][col];

    if( row == rowLen-1 ) {
        /*for(const auto& res : prevRes)
            resCache[std::make_pair(std::get<0>(res), std::get<1>(res))] = currSum-std::get<2>(res);*/

        sums.push_back(currSum);
        return;
    }
    /*else {
        if( resCache.find(std::make_pair(row, col)) == resCache.end() )
            prevRes.push_back(std::make_tuple(row, col, currSum));
        else {
            sums.push_back(currSum + resCache[std::make_pair(row, col)]);
            return;
        }
    }*/

    if( col > 0 && col < colLen-1 ) {
        addNum(sums, currSum, row+1, col+1/*, prevRes, resCache*/);
        addNum(sums, currSum, row+1, col-1/*, prevRes, resCache*/);
    }
    else if( col == 0 )
        addNum(sums, currSum, row+1, col+1/*, prevRes, resCache*/);
    else
        addNum(sums, currSum, row+1, col-1/*, prevRes, resCache*/);
}

int getMaxSumRecursively()
{
    std::vector<int> sums;
    const unsigned rowLen = sizeof(matx[0])/sizeof(int);
    std::unordered_map<std::pair<unsigned,unsigned>,int,pairHash> cache;

    for(unsigned i=0; i<rowLen; ++i)
        addNum(sums, 0, 0, i/*, std::vector<std::tuple<unsigned,unsigned,int> >{}, cache*/);

    return *std::max_element(sums.begin(), sums.end());
}

int getMaxSumDynamically()
{
    const unsigned rowLen = sizeof(matx[0])/sizeof(int);
    const unsigned colLen = sizeof(matx)/sizeof(matx[0]);

    std::vector<std::vector<int> > sumsSoFar;
    sumsSoFar.push_back(std::vector<int>{});
    for(unsigned i=0; i<rowLen; ++i)
        sumsSoFar[0].push_back(matx[0][i]);

    if( colLen > 1 ) {
        for(unsigned j=1; j<colLen; ++j) {
            sumsSoFar.push_back(std::vector<int>{});
            for(unsigned i=0; i<rowLen; ++i) {
                if( i == 0 )
                    sumsSoFar.back().push_back(matx[j][i] + sumsSoFar[j-1][i+1]);
                else if( i == (rowLen-1) )
                    sumsSoFar.back().push_back(matx[j][i] + sumsSoFar[j-1][i-1]);
                else
                    sumsSoFar.back().push_back(matx[j][i] + std::max(sumsSoFar[j-1][i-1], sumsSoFar[j-1][i+1]));
            }
        }
    }

    return *std::max_element(sumsSoFar.back().begin(), sumsSoFar.back().end());
}

// 6. Find the first repeated word in a string
/// Given a string, find the first repeated word in a string.

char* find1stRepeated( const char str[] ) // C style
{
    char* retWord = nullptr;
    char** words;
    unsigned i=0, wordCount=0;

    while( str[i] != '\0' ) {
        std::cout << str[i] << "\n";
        if( str[i] == ' ' ) {
            unsigned j=0;
            while( str[i] == ' ' )
                ++i, ++j;
            if( str[i] != '\0' && i != j ) ++wordCount;
        }
        else ++i;
    }
    ++wordCount;
    //std::cout << "Number of words: " << wordCount << ".\n";

    if( wordCount )
        words = new char*[wordCount];
    else
        return nullptr;

    i=0;
    unsigned j=0;
    while( str[i] != '\0' ) {
        while( str[i] == ' ' ) ++i;
        if( str[i] == '\0' ) break;
        unsigned starti = i;
        while( str[i] != ' ' && str[i] != '\0' ){ /*std::cout << i << "\n";*/ ++i; }
        words[j] = new char[i-starti+1];
        for(unsigned k=starti; k<i; ++k) words[j][k-starti] = str[k];
        words[j][i-starti] = '\0';
        //std::cout << "WORD[" << j << "]: " << words[j] << std::endl;
        ++j;
    }

    for(j=0; j<wordCount; ++j)
        for(unsigned k=j+1; k<wordCount; ++k)
            if( strcmp(words[j], words[k]) == 0 ) {
                retWord = new char[strlen(words[j])];
                strcpy(retWord, words[j]);
                break;
            }

    for(j=0; j<wordCount; ++j)
        delete [] words[j];
    delete words;

    return retWord;
}

std::string find1stRepeated( const std::string& str ) // C++ style
{
    std::string retWord = "";
    std::stringstream strStream{str};

    std::string token;
    std::vector<std::string> tokenVec;
    while( std::getline(strStream, token, ' ') ){
        retWord.clear();
        for(const auto& tok : tokenVec) {
            if( token == tok ) {
                retWord = token;
                break;
            }
        }

        if( !retWord.empty() )
            break;
        else
            tokenVec.push_back(token);
    }

    return retWord;
}

// 7. Frogs
/// There are N blocks numbered from 0 to N-1 arranged in a row. A couple of frogs were sitting together on one block
/// when they had a terrible quarrel. Now they want to jump away from one another so that the distance between them will
/// be as large as possible. The distance between blocks numbered J and K, where J<=K, is computed as K-J+1. The frogs
/// can only jump up, meaning that they can move from one block to another only if the two blocks are adjacent and the
/// second block is of the same or greater height as the first. What is the longest distance that they can possibly create
/// between each other if they also chose to sit on the optimal starting block initially?

constexpr unsigned frogBlocks[] = { 2,3,8,4,7,7,6,1,0,5,9 };

std::array<unsigned,2> separateFrogs()
{
    unsigned maxDistance = 0, optimumBlock = 0;
    constexpr unsigned frogBlocksLen = sizeof(frogBlocks) / sizeof(unsigned);

    for(unsigned i=0; i<frogBlocksLen; ++i) {
        long int f1=i, f2=i;
        while( f1 > 0 ) {
            if( frogBlocks[f1-1] >= frogBlocks[f1] )
                --f1;
            else
                break;
        }

        while( f2 < frogBlocksLen ) {
            if( frogBlocks[f2+1] >= frogBlocks[f2] )
                ++f2;
            else
                break;
        }

        unsigned distance = f2-f1+1;
        if( distance > maxDistance ) {
            maxDistance = distance;
            optimumBlock = i;
        }
    }

    return {maxDistance, optimumBlock};
}

// 8. Roman Empire
/// You are given a map of the Roman Empire. There are N+1 cities (numbered from 0 to N) and N directed roads between them.
/// The road network is connected, that is, ignoring the directions of roads, there is a route between each pair of cities.
/// The capital of the Roman Empire is Rome. We know that all roads lead to Rome. This means that there is a route from
/// each city to Rome. Your task is to find Rome on the map or decide that it is not there. The roads are described by two
/// arrays A and B of N integers each. For each integer K (0 <= K < N) there exists a road from city A[K] to city B[K].

constexpr unsigned A[] = { 0,1,2,4,5 };
constexpr unsigned B[] = { 2,3,3,3,2 };

int findRome()
{
    int romeNo = -1;

    // The task is equivalent to finding if there is only one element of the array B not present in the array A - this
    // "city" will be the end of all links/roads. We assume the array A is sorted.
    constexpr std::size_t N = sizeof(A)/sizeof(unsigned);
    static_assert( N == sizeof(B)/sizeof(unsigned), "***" );

    // Make new vector of elements of B without duplicates - O(N)
    std::array<unsigned,N> hashForB = { 0 };
    for(const auto& b : B)
        ++hashForB[b];

    std::vector<unsigned> BwithoutDups;
    for(unsigned i=0; i<N; ++i)
        if( hashForB[i] != 0 )
            BwithoutDups.push_back(i);

    // Check for presence of B's elements with duplicates removed in A - O(N*logN)
    std::vector<unsigned> romes;
    for(const auto& b : BwithoutDups)
        if( !std::binary_search(A, A+N, b) )
            romes.push_back(b);

    if( romes.size() == 1 )
        romeNo = romes[0];

    return romeNo;
}

// 9. Palindrome
/// A palindrome is a word which reads the same backward as forward. Some examples of palindromes are "kayak", "radar",
/// "mom". Write a function that, given two integers N and K, returns a palindrome of length N which consists of K distinct
/// lower-case letters (a-z).

std::string createPalindrome( const unsigned N, const unsigned K )
{
    std::string palindrome = "";
    int i = 0;

    if( N && K && (K < ceil(N/2.0)) ) {
        srand(time(NULL));
        std::vector<char> letters;

        // Draw the K distinct lower-case letters
        while( i < K ) {
            char letter = 97 + rand() % 26;
            bool include = true;

            for(const auto& let : letters)
                if( letter == let ) {
                    include = false;
                    break;
                }

            if( include ) {
                letters.push_back(letter);
                ++i;
            }
        }
        for(auto&& k : letters)
            std::cout << k << " ";
        std::cout << "\n";

        // Draw first half of the return string from K distinct lower-case letters
        for(i=0; i<ceil(N/2.0); ++i)
            palindrome.push_back(letters[rand() % K]);

        // Make sure that there are the needed K distinct letters in the half string and put them in random positions
        std::vector<unsigned> lettPositions;
        i = 0;
        while( i < K ) {
            unsigned pos = rand() % static_cast<unsigned>(ceil(N/2.0));
            std::cout << "i = " << i << ", pos = " << pos << "\n";
            bool include = true;

            for(const auto& lettPos : lettPositions) {
                if( lettPos == pos ) {
                    include = false;
                    --i;
                    break;
                }
            }
            std::cout << "i = " << i << "\n";

            if( !include ) break;

            palindrome[pos] = letters[i++];
            std::cout << "ii = " << i << "\n";
            lettPositions.push_back(pos);
        }
        for(auto&& p : lettPositions)
            std::cout << p << " ";
        std::cout << "\n";

        // Add the missing second half - mirror the first
        for(i=0; i<N/2; ++i)
            palindrome.push_back(palindrome[N/2-i-1]);
    }
    else std::cerr << "Cannot create a valid palindrome of size " << N << " and " << K << " distinct letters!\n";

    return palindrome;
}

// 10. Anagram
/// Check if the given two strings are anagrams.

/*bool isAnagram1( const std::string& s1, const std::string& s2 )
{
    bool isAnagram = true;
    std::unordered_map<char,std::size_t> m;

    for(const auto c : s1) m[c]++;
    for(const auto c : s2) m[c]--;

    for(const auto& keyVal : m)
        if( keyVal.second != 0 )
            isAnagram = false;

    return isAnagram;
}

bool isAnagram2( const std::string& s1, const std::string& s2 )
{
    std::unordered_map<char,std::size_t> m1;
    std::unordered_map<char,std::size_t> m2;

    for(const auto c : s1) ++m1[c];
    for(const auto c : s2) ++m2[c];

    return (m1 == m2);
}*/

bool isAnagram3( const std::string& s1, const std::string& s2 ) // most C-style
{
    bool isAnagram = true;
    unsigned hashTable[128] = { 0 };

    for(char c : s1) ++hashTable[c];
    for(char c : s2) --hashTable[c];

    for(unsigned i=0; i<128; ++i)
        if( hashTable[i] != 0 ) {
            isAnagram = false;
            break;
        }

    return isAnagram;
}

// 11. Median of data contained in two sorted arrays
/// You are given two sorted arrays containing some data. Write a function which returns a median value of those arrays
/// (of all the given data).

template <typename T>
T linFindMedianOf( const T array1[], const unsigned n1, const T array2[], const unsigned n2 )
{
    T median{};
    T* allData = new T[n1+n2];

    unsigned i=0, j=0, k=0;
    while( i<n1 && j<n2 ) {
        if( array1[i] < array2[j] )
            allData[k++] = array1[i++];
        else
            allData[k++] = array2[j++];
    }

    while( i<n1 )
        allData[k++] = array1[i++];

    while( j<n2 )
        allData[k++] = array2[j++];

    k % 2 ? median = allData[k/2] : median = (allData[k/2-1] + allData[k/2])/2;
    delete [] allData;

    return median;
}

template <typename T>
T logFindMedianOf( const T array1[], const unsigned n1, const T array2[], const unsigned n2 )
{
    T median{};

    unsigned min_index = 0, max_index = n1, k, l;
    n1 > n2 ? max_index = n2 : max_index = n1;

    while( min_index <= max_index ) {
        k = (min_index + max_index)/2;
        l = (n1 + n2 + 1)/2 - k;

        if( l < 0 ) {
            max_index = k-1;
            continue;
        }

        if( k < n1 && l > 0 && array2[l-1] > array1[k] )
            min_index = k+1;
        else if( k > 0 && l < n2 && array2[l] < array1[k-1] )
            max_index = k-1;
        else {
            if( k == 0 )
                median = array2[l-1];
            else if( l == 0 )
                median = array1[k-1];
            else
                median = std::max(array1[k-1], array2[l-1]);
            break;
        }
    }

    if( (n1+n2) % 2 ) return median;
    if( k == n1 ) return (median + array2[l])/2.0;
    if( l == n2 ) return (median + array1[k])/2.0;

    return (median + std::min(array1[k], array2[l]))/2.0;
}

// 12. Second lowest integer
/// Given an array of integers, find the second lowest value.

int find2ndLowestOf( const int array[], const unsigned n )
{
    int lowest = array[0];
    for(unsigned i=1; i<n; ++i)
        if( array[i] < lowest )
            lowest = array[i];

    int low2nd = INT32_MAX;
    for(unsigned i=0; i<n; ++i)
        if( array[i] < low2nd && array[i] != lowest )
            low2nd = array[i];

    return low2nd;
}

// 13. Snow between hills
/// Given a number of hills of some height, find out how many units of snow it is possible to place in between those hills.

float howMuchSnowBetween( const unsigned hills[], const unsigned n )
{
    float snowUnits = 0.0f;

    if( n>0 ) {
        std::vector<unsigned> prominents;
        if( hills[1] < hills[0] ) prominents.push_back(0);

        bool ascent = false;
        for(unsigned i=1; i<n-1; ++i) {
            if( hills[i] < hills[i-1] && ascent )
                prominents.push_back(i-1);

            ascent = hills[i] >= hills[i-1];
        }

        if( hills[n-2] < hills[n-1] ) prominents.push_back(n-1);

        if( prominents.size()<2 )
            return snowUnits;

        for(unsigned p=1; p<prominents.size(); ++p) {
            unsigned snowLevel = std::max(hills[prominents[p]], hills[prominents[p-1]]);
            for(unsigned q=prominents[p-1]; q<prominents[p]; ++q)
                snowUnits += 0.5*(snowLevel - hills[q+1]);
        }
    }

    return snowUnits;
}

// 14. Travelling salesman
/// The travelling salesman has to visit N cities and he needs to minimize the total time of his travels. Find the
/// shortest travel route connecting all cities, starting and ending at a specific point.

constexpr const float distanceMap[4][4] = { {26.9f, 82.3f, 19.4f, 40.7f},
                                            {127.1f, 38.0f, 72.5f, 63.2f},
                                            {17.3f, 14.8f, 92.6f, 143.1f},
                                            {53.4f, 86.3f, 9.7f, 106.2f} };

void addCityAndDispatch( std::vector<std::tuple<float,std::set<unsigned>,std::vector<unsigned> > >& routes,
                         const unsigned currCity, const unsigned endCity, const unsigned pathNo )
{
    if( std::get<2>(routes[pathNo]).size() )
        std::get<0>(routes[pathNo]) += distanceMap[std::get<2>(routes[pathNo]).back()][currCity];
    std::get<1>(routes[pathNo]).insert(currCity);
    std::get<2>(routes[pathNo]).push_back(currCity);
    //std::cout << "CURR CITY: " << currCity << ", PATH NO: " << pathNo << "\n";

    if( currCity == endCity )
        return;
    else {
        std::vector<unsigned> notVisited;
        for(unsigned i=0; i<sizeof(distanceMap[0])/sizeof(float); ++i)
            if( std::get<1>(routes[pathNo]).find(i) == std::get<1>(routes[pathNo]).end() && i != endCity )
                notVisited.push_back(i);

        if( notVisited.size() == 0 )
            notVisited.push_back(endCity);

        unsigned curr = 0;
        const auto currCopy = routes[pathNo];
        addCityAndDispatch(routes, notVisited[curr++], endCity, pathNo);

        while( curr < notVisited.size() ) {
            routes.push_back(std::make_tuple(std::get<0>(currCopy), std::get<1>(currCopy),
                                             std::get<2>(currCopy)));
            addCityAndDispatch(routes, notVisited[curr], endCity, pathNo+curr);
            ++curr;
        }
    }
}

std::pair<float,std::vector<unsigned> > findShortestRoute( const unsigned start, const unsigned end )
{
    std::vector<std::tuple<float,std::set<unsigned>,std::vector<unsigned> > > routes;
    routes.push_back(std::make_tuple(0.0f, std::set<unsigned>{}, std::vector<unsigned>{}));
    addCityAndDispatch(routes, start, end, 0);

    auto minRoute = *std::min_element(routes.begin(), routes.end(),
                                      []( const auto& l, const auto& r ){ return std::get<0>(l)<std::get<0>(r); });

    return std::make_pair(std::get<0>(minRoute), std::get<2>(minRoute));
}

// 15. Find longest string available
/// Given a string and a set of words, find the longest string(s) that can be built using the characters in the given string.

std::vector<std::string> findLongestString( std::string base, const std::vector<std::string>& strings )
{
    int baseMap[128] = {0};
    for(const char b : base)
        ++baseMap[b];

    std::vector<std::string> matchingStrs, longestStrs;

    for(const auto& str : strings) {
        int baseMapCp[128];
        std::memcpy(baseMapCp, baseMap, sizeof(baseMap));

        for(const char c : str)
            --baseMapCp[c];

        bool matching = true;
        for(unsigned i=0; i<128; ++i)
            if( baseMapCp[i] < 0 ) {
                matching = false;
                break;
            }

        if( matching ) matchingStrs.push_back(str);
    }

    if( matchingStrs.size() ) {
        unsigned longestSize = 0;
        for(const auto& mStr : matchingStrs)
            if( mStr.size() > longestSize )
                longestSize = mStr.size();

        for(const auto& mStr : matchingStrs)
            if( mStr.size() == longestSize )
                longestStrs.push_back(mStr);
    }

    return longestStrs;
}

// 16. Find words starting with a prefix
/// Given a string, find indices of all the words that start with a given prefix (not case sensitive, without using regex)

std::vector<unsigned> findIndicesStartingWith( std::string prefix, std::string document )
{
    std::vector<unsigned> indices;
    std::transform(prefix.begin(), prefix.end(), prefix.begin(), []( const char c ){ return std::tolower(c); });
    std::transform(document.begin(), document.end(), document.begin(), []( const char c ){ return std::tolower(c); });

    if( !document.empty() && !prefix.empty() ) {
        std::stringstream docStream{document};

        std::string token;
        while( std::getline(docStream, token, ' ') )
            if( token.rfind(prefix, 0) == 0 )
                indices.size() ? indices.push_back(document.find(token, indices.back()))
                               : indices.push_back(document.find(token));
    }

    return indices;
}

// 17. Collect maximum rocks on your way from So-Cal to NY
/// You are given an array immitating a map of places with number of rocks hidden in each cell. Collect maximum number
/// of stones on the way from So-Cal (the lower left corner) to New York (the upper right corner), provided that you can
/// only travel north (up) or east (right).

constexpr const unsigned rockMap[4][5] = { {5, 8, 2, 0, 11},
                                           {6, 12, 9, 10, 1},
                                           {14, 3, 7, 13, 4},
                                           {15, 16, 17, 18, 0} };

unsigned getMaxRocksDynamically()
{
    const unsigned rows = sizeof(rockMap)/sizeof(rockMap[0]);
    const unsigned cols = sizeof(rockMap[0])/sizeof(unsigned);
    std::vector<std::vector<unsigned> > partialSums(rows, std::vector<unsigned>(cols, 0));

    partialSums[rows-1][0] = rockMap[rows-1][0];

    if( cols > 1 ) for(unsigned j=1; j<cols; ++j)
        partialSums[rows-1][j] = partialSums[rows-1][j-1] + rockMap[rows-1][j];
    if( rows > 1 ) for(unsigned i=rows-1; i-->0;)
        partialSums[i][0] = partialSums[i+1][0] + rockMap[i][0];
    if( rows > 1 && cols > 1 ) for(unsigned i=rows-1; i-->0;)
        for(unsigned j=1; j<cols; ++j)
            partialSums[i][j] = std::max(partialSums[i+1][j], partialSums[i][j-1]) + rockMap[i][j];

    return partialSums[0][cols-1];
}

void addRocksFrom( const unsigned r, const unsigned c, unsigned currSum, std::vector<unsigned>& sums,
                   const unsigned rowsNo, const unsigned colsNo )
{
    currSum += rockMap[r][c];

    if( r == 0 && c == colsNo-1 ) {
        sums.push_back(currSum);
        return;
    }

    if( r == 0 )
        addRocksFrom(r, c+1, currSum, sums, rowsNo, colsNo);
    else if( c == colsNo-1 )
        addRocksFrom(r-1, c, currSum, sums, rowsNo, colsNo);
    else {
        addRocksFrom(r, c+1, currSum, sums, rowsNo, colsNo);
        addRocksFrom(r-1, c, currSum, sums, rowsNo, colsNo);
    }
}

unsigned getMaxRocksRecursively()
{
    const unsigned rows = sizeof(rockMap)/sizeof(rockMap[0]);
    const unsigned cols = sizeof(rockMap[0])/sizeof(unsigned);

    std::vector<unsigned> sums;
    addRocksFrom(rows-1, 0, 0, sums, rows, cols);

    return *std::max_element(sums.begin(), sums.end());
}

// 18. Student with highest grade
/// Given a 2D string array of student-marks, find the student with the highest average and output his average score. If
/// the average is in decimals, floor it down to the nearest integer.

std::pair<std::string,int> highestGrade( const std::string studMarks[][2], const unsigned n )
{
    std::string bestName = "";
    int bestGrade = 0;

    for(unsigned i=0; i<n; ++i) {
        int grade = static_cast<int>(std::stof(studMarks[i][1]));
        if( grade > bestGrade ) {
            bestName = studMarks[i][0];
            bestGrade = grade;
        }
    }

    return std::make_pair(bestName, bestGrade);
}

// 19. Power of 10
/// Given an integer find out whether it is a power of 10 or not.

bool isPowerOf10( int num )
{
    bool is10p = true;

    do {
        if( std::abs(num) > 9 ) {
            if( num % 10 ) {
                is10p = false;
                break;
            }
        }
        else if( num % 10 != 1 )
            is10p = false;
    } while( num /= 10 );

    return is10p;
}

// 20. Two-egg-problem
/// A building has 100 floors. One of the floors is the highest floor an egg can be dropped from without breaking. If an
/// egg is dropped from above that floor, it will break. If it is dropped from that floor or below, it will be completetly
/// undamaged and you can drop the egg again. Given two eggs, find the highest floor an egg can be dropped from without
/// breaking with as few drops as possible.

std::pair<unsigned,unsigned> findHighestFloor( const unsigned soughtFloor, const unsigned floorsNum = 100 )
{
    assert( soughtFloor <= floorsNum );

    bool egg1 = true, egg2 = true;
    unsigned dropCount = 0;

    unsigned eggThrow1 = floorsNum/2, eggThrow2 = 1, prevEggThrow;
    while( egg1 ) {
        ++dropCount;
        if( eggThrow1 > soughtFloor ) {
            egg1 = false;
            if( dropCount > 1 ) eggThrow2 = prevEggThrow;
        }
        else {
            prevEggThrow = eggThrow1;
            eggThrow1 += eggThrow1/2;
        }
    }

    while( egg2 ) {
        ++dropCount;
        ++eggThrow2;
        if( eggThrow2 > soughtFloor ) egg2 = false;
    }

    return std::make_pair(eggThrow2-1, dropCount);
}

// 21. Second largest element in a BST
/// Find the second largest element in a binary search tree.

template <typename T>
class BST;

template <typename T>
std::ostream& operator<<( std::ostream& s, const BST<T>& t );

template <typename T>
BST<T> convertToBalanced( const BST<T>& );

template <typename T>
class BST
{
    struct Node{
        T value;
        Node *left, *right;
    } *root;

    void add( const T& elem )
    {
        Node *t = root, *prev = nullptr;
            if( !t ) root = new Node{elem, nullptr, nullptr};
        else {
            while(t){ prev = t; elem > t->value ? t = t->right : t = t->left; }
            elem > prev->value ? prev->right = new Node{elem, nullptr, nullptr} : prev->left = new Node{elem, nullptr, nullptr};
        }
    }
    void inOrderN( std::vector<Node*>& vec, Node* const t ) const
    {
        if( t ) {
            inOrderN(vec, t->left);
            vec.push_back(t);
            inOrderN(vec, t->right);
        }
    }
    std::vector<Node*> levelOrderN() const
    {
        std::vector<Node*> lOT;
        std::deque<Node*> workQ{root};
        while( !workQ.empty() ) {
            Node* t = workQ.front();
            workQ.pop_front();
            lOT.push_back(t);
            if( t->left ) workQ.push_back(t->left);
            if( t->right ) workQ.push_back(t->right);
        }
        return lOT;
    }

public:
    BST( const std::vector<T>& vec ) : root{nullptr} { for(const auto& v : vec) add(v); }
    ~BST() { std::vector<Node*> nodes; inOrderN(nodes, root); for(Node* t : nodes) delete t; }
    friend float find2ndLargestOf( const BST<float>& );
    friend BST<T> convertToBalanced <>( const BST<T>& );
    friend std::ostream& operator<< <>( std::ostream&, const BST<T>& );
};

template <typename T>
std::ostream& operator<<( std::ostream& s, const BST<T>& t )
{
    std::vector<typename BST<T>::Node*> nodes = t.levelOrderN();
    for(unsigned i=0; i<nodes.size(); ++i) {
        s << nodes[i]->value << " ";
        if( std::bitset<32>{i+2}.count() == 1 ) s << "; ";
    }
    s << "\n";
    return s;
}

float find2ndLargestOf( const BST<float>& tree )
{
    float big2nd;

    const BST<float>::Node *t = tree.root, *prev = nullptr;
    while( t ) {
        prev = t;
        t = t->right;
    }
    prev->left ? big2nd = prev->left->value : big2nd = prev->value;

    return big2nd;
}

// 22. Convert a normal BST to a balanced BST
/// Given a BST (Binary Search Tree) that may be unbalanced, convert it to a balanced BST that has minimum possible height.

template <typename T>
BST<T> convertToBalanced( const BST<T>& tree )
{
    std::vector<T> elements;
    std::vector<typename BST<T>::Node*> nodes;

    tree.inOrderN(nodes, tree.root);

    const std::function<void(const int, const int)> sortedVecToBST = [&]( const int start, const int end )
    {
        if( start > end ) return;

        const int mid = (start+end)/2;
        elements.push_back(nodes[mid]->value);
        sortedVecToBST(start, mid-1);
        sortedVecToBST(mid+1, end);
    };
    sortedVecToBST(0, nodes.size()-1);

    return BST<T>{elements};
}

// 23. Remove more than k-times-repetitive characters from a string
/// Given a string and a constant integer k, remove all single-character substrings of size larger than k from the string.
/// Keep in mind that after a removal, additional strings eligible for removal can be formed.

void removeNeighboringCharsFrom( std::string& str, const unsigned k )
{
    std::vector<std::pair<unsigned,unsigned> > toRemove;

    do {
        toRemove.clear();
        unsigned lenToRemove = 0;

        for(unsigned i=0; i<str.length()-k; ) {
            unsigned j=1;
            while( i+j < str.length() && str[i] == str[i+j] ) ++j;

            if( j > k ) {
                toRemove.push_back(std::make_pair(i-lenToRemove,j));
                lenToRemove += j;
            }
            i += j;
        }

        for(const auto& idxs : toRemove)
            str.erase(idxs.first, idxs.second);
    } while( !toRemove.empty() );
}

// 24. Sum of bit differences among all pairs
/// Given an array of n integers, find sum of bit differences in all pairs that can be formed from array elements. Bit
/// difference of a pair (x, y) is count of different bits at same positions in binary representations of x and y. For
/// example, bit difference for 2 and 7 is 2. Binary representation of 2 is 010 and 7 is 111 (first and last bits differ
/// in two numbers).

unsigned sumOfBitDiffs( const int* arr, const unsigned n )
{
    unsigned theSum = 0;
    std::array<unsigned,32> sumsOfBits = {0};

    int bitMask = 1;
    for(unsigned i=0; i<32; ++i) {
        for(unsigned j=0; j<n; ++j)
            sumsOfBits[i] += unsigned(bool(arr[j] & bitMask));
        bitMask *= 2;
    }

    for(unsigned i=0; i<32; ++i)
        theSum += (n-sumsOfBits[i])*sumsOfBits[i];

    return theSum;
}

// 25. Teacher's counting-out
/// You are given a list of pupils as integers 1,2,3,4,5,6,7,... Their teacher begins to count out a rhyme on them. Each
/// word falls on a single pupil. The pupil who hears the last word is dropped out. After that the teacher restarts the
/// counting out with the next pupil until there is only one pupil left. Write a function which returns the number of the
/// last pupil standing, given the total number of participating pupils and the number of words in the counting-out rhyme.

unsigned countingOutRhyme( const unsigned numOfPupils, const unsigned numOfWords )
{
    std::list<unsigned> pupils;
    for(unsigned i=1; i<=numOfPupils; ++i)
        pupils.push_back(i);

    int currLast = -1;
    while( pupils.size() > 1 ) {
        for(unsigned j=0; j<numOfWords; ++j) {
            ++currLast;
            currLast %= pupils.size();
        }
        auto curr = pupils.begin();
        for(int i=0; i<currLast; ++i) ++curr;
        pupils.erase(curr);

        --currLast;
    }

    return pupils.back();
}

// 26. The chess horse
/// Given a chessboard with a chess horse on it at a known starting position, calculate probability of the figure to escape
/// the board during a sequence of moves. The horse can move to 8 positions from every place it is standing and the choice
/// is entirely random.

float chessHorseEscape( const std::pair<unsigned,unsigned>& startPos, const unsigned movesNo )
{
    const unsigned minX = 1, maxX = 8, minY = minX, maxY = maxX;
    float probability = 0.0f;

    if( startPos.first < minX || startPos.first > maxX || startPos.second < minY || startPos.second > maxY )
        std::cerr << "Error: the horse's starting position is outside the chess board! Returning 0...\n";
    else {
        unsigned escapeCnt = 0, insideCnt = 0;
        std::stack<std::array<unsigned,3> > workStack;
        workStack.push({startPos.first, startPos.second, 0});

        while( !workStack.empty() ) {
            const auto curr = workStack.top();
            workStack.pop();

            if( curr[0] < minX || curr[0] > maxX || curr[1] < minY || curr[1] > maxY )
                ++escapeCnt;
            else {
                if( curr[2] < movesNo ) {
                    workStack.push({curr[0]+1, curr[1]+2, curr[2]+1});
                    workStack.push({curr[0]+2, curr[1]+1, curr[2]+1});
                    workStack.push({curr[0]+1, curr[1]-2, curr[2]+1});
                    workStack.push({curr[0]+2, curr[1]-1, curr[2]+1});
                    workStack.push({curr[0]-1, curr[1]-2, curr[2]+1});
                    workStack.push({curr[0]-2, curr[1]-1, curr[2]+1});
                    workStack.push({curr[0]-1, curr[1]+2, curr[2]+1});
                    workStack.push({curr[0]-2, curr[1]+1, curr[2]+1});
                }
                else
                    ++insideCnt;
            }
        }
        std::cout << "ESC CNT " << escapeCnt << ", INS CNT " << insideCnt << "\n";
        probability = static_cast<float>(escapeCnt) / (escapeCnt+insideCnt);
    }

    return probability;
}

// 27. Kth row of Pascal's Triangle
/// Given an index k, return the kth row of the Pascal’s triangle.

std::vector<unsigned> PascalsRowRecursive( const unsigned k )
{
    std::vector<unsigned> kth;

    if( k <= 1 ) {
        kth.push_back(1);

        if( k == 1 )
            kth.push_back(1);
    }
    else {
        kth.push_back(1);
        const std::vector<unsigned> kthPrev = PascalsRowRecursive(k-1);
        for(unsigned i=0; i<kthPrev.size()-1; ++i)
            kth.push_back(kthPrev[i]+kthPrev[i+1]);
        kth.push_back(1);
    }

    return kth;
}

std::vector<unsigned> PascalsRowIterative( const unsigned k )
{
    std::vector<unsigned> kth;

    const auto factorial = [](const unsigned n){ unsigned base = 1; for(unsigned i=2; i<=n; ++i) base *= i; return base; };

    unsigned kthFact = factorial(k);
    for(unsigned i=0; i<=k; ++i)
        kth.push_back(kthFact/(factorial(i) * factorial(k-i)));

    return kth;
}

// 28. Grid unique paths
/// A robot is located at the top-left corner of an AxB grid. The robot can only move either down or right at any point in
/// time. The robot is trying to reach the bottom-right corner of the grid. How many possible unique paths are there?

unsigned gridUniques( const unsigned A, const unsigned B )
{
    unsigned pathsNum = 0;
    std::vector<std::vector<unsigned> > partials;

    if( A == 0 || B == 0 )
        std::cerr << "Error: both dimensions of the grid must be positive integers. Returning 0...\n";
    else {
        for(unsigned i=0; i<A; ++i) {
            partials.push_back(std::vector<unsigned>{});
            for(unsigned j=0; j<B; ++j)
                partials.back().push_back(0);
        }

        for(unsigned i=0; i<A; ++i) partials[i][0] = 1;
        for(unsigned j=0; j<B; ++j) partials[0][j] = 1;

        for(unsigned i=1; i<A; ++i)
            for(unsigned j=1; j<B; ++j)
                partials[i][j] = partials[i-1][j] + partials[i][j-1];

        pathsNum = partials[A-1][B-1];
    }

    return pathsNum;
}

// 29. Component for checking task status
/// Design a component (e.g. of a server) which will be able to store the index of the first not yet finished
/// (unacknowledged) task from all the requests sent to its parent entity. There may be thousands of tasks sent within
/// a minute but we are interested in the chronologically first which needs to be accomplished.

class Component
{
    std::unordered_set<long unsigned> ackeds;
    long unsigned unacked;

    void update();

public:
    Component( long unsigned num ) : unacked{num} {}
    void reinit( long unsigned num ) { ackeds.clear(); unacked = num; }
    void acknowledge( long unsigned num ) { ackeds.insert(num); }
    long unsigned get() { update(); return unacked; }
};

void Component::update()
{
    if( ackeds.find(unacked) != ackeds.end() ) {
        unacked = *std::max_element(ackeds.begin(), ackeds.end()) + 1;
        ackeds.clear();
    }
}

// 30. Longest common prefix
/// Given the array of strings, find the longest string which is the prefix of all the strings in the array.

std::string longestPrefix( const std::string stringArr[], const unsigned n )
{
    std::string prefix;

    if( n == 0 )
        std::cerr << "Error: zero passed as array size. Returning empty string...\n";
    else if( n == 1 ) {
        std::cerr << "Warning: one passed as array size. Returning the first word of the array...\n";
        prefix = stringArr[0];
    }
    else {
        unsigned theLen = stringArr[0].length();
        for(unsigned j=1; j<n; ++j) {
            unsigned i=0;
            for(; i<stringArr[0].size(); ++i)
                if( stringArr[0][i] - stringArr[j][i] != 0 )
                    break;

            if( i < theLen ) theLen = i;
        }

        for(unsigned i=0; i<theLen; ++i) prefix.push_back(stringArr[0][i]);
    }

    return prefix;
}

// 31. The gas stations' rally
/// You are given two integer arrays A and B of size N. There are N gas stations along a circular route, where the amount
/// of gas at station i is A[i]. You have a car with an unlimited gas tank and it costs B[i] of gas to travel from station i
/// to its next station i+1. You begin the journey with an empty tank at one of the gas stations. Return the minimum
/// starting gas station’s index if you can travel around the circuit once, otherwise return -1. You can only travel in
/// one direction: i to i+1, i+2, ..., n-1, 0, 1, ... . Completing the circuit means starting at i and ending up at i again.

int find1stGasStation( const unsigned supplies[], const unsigned demands[], const unsigned N )
{
    int gasStation = -1;

    for(unsigned start=0; start<N; ++start) {
        int gasInTank = 0, cnt = 0;

        do {
            unsigned currSt = (start+cnt) % N;
            gasInTank += supplies[currSt];
            gasInTank -= demands[currSt];
            ++cnt;
        }
        while( gasInTank >= 0 && cnt < N );

        if( cnt == N ) {
            gasStation = start;
            break;
        }
    }

    return gasStation;
}

// 32. Flip 2 and 3
/// Write a function which returns 2 if it's given 3 and 3 if it's given 2 provided that it can't be given any other
/// integer value and you cannot use any conditional statements in your code.

unsigned flip2and3( const unsigned flipMe )
{
    assert( flipMe == 2 || flipMe == 3 );
    // Binary representation:
    // 2: 10 -> 10 ^ 01 -> 11
    // 3: 11 -> 11 ^ 01 -> 10
    return flipMe ^ 1;
}

// 33. The Cake Thief
/// As a renowned cake thief, you break into the world's largest privately owned stock of cakes—the vault of the Queen of
/// England. While Queen Elizabeth has a limited number of types of cake, she has an unlimited supply of each type.  Each
/// type of cake has a weight and a value, stored in a tuple with two indices:
/// 0 - an integer representing the weight of the cake in kilograms
/// 1 - an integer representing the monetary value of the cake in British shillings.
/// You brought a duffel bag that can hold limited weight, and you want to make off with the most valuable haul possible.
/// Write a function that takes a list of cake type tuples and a weight capacity, and returns the maximum monetary value
/// the duffel bag can hold.

unsigned maxDuffelBagValue( const std::vector<std::pair<unsigned,unsigned> >& cakeTypes, const unsigned capacity )
{
    std::vector<unsigned> results;

    const std::function<void(const unsigned, const unsigned)> getACake = [&]( const unsigned weight, const unsigned value )
    {
        for( const auto& cakeType : cakeTypes ) {
            if( weight + cakeType.first > capacity )
                results.push_back(value);
            else
                getACake(weight+cakeType.first, value+cakeType.second);
        }
    };
    getACake(0,0);

    return *std::max_element(results.begin(), results.end());
}

// 34. Detecting a cycle
/// You are given an array of integers and a starting index. Starting from array[startingIndex], follow each element to
/// the index it points to. Continue doing this until you find a cycle. Return the length of the cycle. If no cycle is
/// found return -1.

template <unsigned long size>
int detectCycleIn( const std::array<unsigned,size>& array, unsigned idx )
{
    int cycleLen = -1, workLen = 0;
    std::unordered_set<unsigned> visited;

    while( idx < size ) {
        idx = array[idx];
        if( visited.find(idx) == visited.end() ) {
            visited.insert(idx);
            ++workLen;
        }
        else {
            cycleLen = workLen;
            break;
        }
    }

    return cycleLen;
}

// 35. Interleaving strings
/// Given three strings A, B, C, find whether C is formed by the interleaving of A and B.

bool byInterleaving( const std::string& A, const std::string& B, const std::string& C )
{
    bool formedBy = false;
    std::multimap<char,std::tuple<bool,unsigned,bool> > charsMap; // char -> A or B, position in str, not used yet

    for(unsigned i=0; i<A.length(); ++i)
        charsMap.insert({A[i], std::make_tuple(true, i, true)});

    for(unsigned i=0; i<B.length(); ++i)
        charsMap.insert({B[i], std::make_tuple(false, i, true)});

    std::function<void(const unsigned, const unsigned, const unsigned, std::multimap<char,std::tuple<bool,unsigned,bool> >&&)>
            recCheck = [&](const unsigned a, const unsigned b, const unsigned c,
            std::multimap<char,std::tuple<bool,unsigned,bool> >&& cMap)
    {
        if( c == C.length() ) {
            formedBy = true;
            return;
        }

        auto cMapRange = cMap.equal_range(C[c]);
        if( cMapRange.first == cMap.end() )
            return;

        for(auto it = cMapRange.first; it != cMapRange.second; ++it) {
            if( std::get<0>(it->second) ) {
                if( std::get<1>(it->second) < a || !std::get<2>(it->second) ) continue;
                else {
                    std::get<2>(it->second) = false;
                    std::multimap<char,std::tuple<bool,unsigned,bool> > cMapCopy = cMap;
                    recCheck(std::get<1>(it->second), b, c+1, std::move(cMapCopy));
                    std::get<2>(it->second) = true;
                }
            }
            else {
                if( std::get<1>(it->second) < b || !std::get<2>(it->second) ) continue;
                else {
                    std::get<2>(it->second) = false;
                    std::multimap<char,std::tuple<bool,unsigned,bool> > cMapCopy = cMap;
                    recCheck(a, std::get<1>(it->second), c+1, std::move(cMapCopy));
                    std::get<2>(it->second) = true;
                }
            }
        }
    };
    recCheck(0, 0, 0, std::move(charsMap));

    return formedBy;
}

// 36. Luck Balance
/// Lena is preparing for an important coding competition that is preceded by a number of sequential preliminary contests.
/// Initially, her luck balance is 0. She believes in "saving luck", and wants to check her theory. Each contest is
/// described by two integers L[i], T[i] where:
/// L[i] is the amount of luck associated with a contest. If Lena wins the contest, her luck balance will decrease by L[i];
/// if she loses it, her luck balance will increase by L[i].
/// T[i] denotes the contest's importance rating. It's equal to 1 if the contest is important and it's equal to 0 otherwise.
/// If Lena loses no more than k important contests, what is the maximum amount of luck she can have after competing
/// in all the preliminary contests? This value may be negative.

int luckBalance( const unsigned k, const std::vector<std::array<int,2> >& contests)  {
    int balance = 0;

    std::multimap<int,bool> contMap;
    for(const auto& contest : contests) {
        contMap.insert(std::make_pair(contest[0], contest[1]));
    }

    unsigned loseCnt = 0;
    for(auto cont = contMap.rbegin(); cont != contMap.rend(); ++cont) {
        if(loseCnt < k && cont->second) {
            balance += cont->first;
            ++loseCnt;
        }
        else if(cont->second == 0)
            balance += cont->first;
        else
            balance -= cont->first;
    }

    return balance;
}

// 37. Fair array
/// You are given an array of integers arr and a single integer k. You must create an array of length k from elements
/// of arr such that its unfairness is minimized. Call that array arr'. Unfairness of an array is calculated as:
/// max(arr') - min(arr')
/// Write a function which would return the minimum possible unfairness.

int minUnfairness( const unsigned k, const std::vector<int>& arr ) {
    int spread = 0;
    std::map<int,unsigned> num2occ;

    for(int elem : arr) {
        ++num2occ[elem];
        if(num2occ[elem] >= k)
            return spread;
    }

    std::multimap<int,unsigned> diff2occ;
    for(auto it = num2occ.begin(); it != --num2occ.end(); ++it) {
        unsigned sum = it->second;
        auto nextIt = ++it;
        --it;
        for(auto jt = nextIt; jt != num2occ.end(); ++jt) {
            std::cout << "CURR: " << it->first << " " << jt->first << " " << sum << "\n";
            sum += jt->second;
            diff2occ.insert(std::make_pair(jt->first - it->first, sum));
        }
    }

    for(const auto& it : diff2occ)
        if(it.second >= k) {
            spread = it.first;
            break;
        }

    return spread;
}

int minUnfair( const unsigned k, const std::vector<int>& arr )
{
    std::vector<int> sortedArr = arr;
    std::sort(sortedArr.begin(), sortedArr.end());

    std::vector<int> spreads;
    for(unsigned i=0; i<sortedArr.size()-k+1; ++i)
        spreads.push_back(sortedArr[i+k-1]-sortedArr[i]);

    return *std::min_element(spreads.begin(), spreads.end());
}

// 38. Max Subset Sum
/// Given an array of integers, find the subset of non-adjacent elements with the maximum sum. Calculate the sum of that
/// subset. It is possible that the maximum sum is 0, the case when all elements are negative.

int maxSubsetSum(const std::vector<int>& arr)
{
    int theSum = 0;
    std::map<unsigned,int> idxSumMap;

    for (unsigned i=0; i<arr.size(); ++i)
        if (arr[i] > 0)
            idxSumMap[i] = arr[i];

    for (auto idxSumIt = idxSumMap.begin(); idxSumIt != idxSumMap.end(); ++idxSumIt) {
        auto idxSumCp = std::next(idxSumIt);

        while (idxSumCp != idxSumMap.end()) {
            if (idxSumCp->first - idxSumIt->first > 1 && idxSumIt->second + arr[idxSumCp->first] > idxSumCp->second) {
                idxSumCp->second = idxSumIt->second + arr[idxSumCp->first];
                if (idxSumCp->second > theSum)
                    theSum = idxSumCp->second;
            }

            ++idxSumCp;
        }
    }

    return theSum;
}

int maxSubsetSum2(const std::vector<int>& arr)
{
    if (!arr.size())
        return 0;

    // 3, 7, 2, 6, 5 arr
    // 0, 3, 7, 7, 13 first
    // 3, 7, 5, 13, 12 second

    std::vector<std::pair<int,int>> resStore;
    resStore.emplace_back(0, arr[0]);

    for (unsigned i=1; i<arr.size(); ++i) {
        auto res1 = std::max(resStore[i-1].first, resStore[i-1].second);
        auto res2 = resStore[i-1].first + arr[i];
        resStore.emplace_back(res1, res2);
    }

    return std::max(0, std::max(resStore[arr.size()-1].first, resStore[arr.size()-1].second));
}

// 39. Balanced Brackets
/// Two brackets are considered to be a matched pair if an opening bracket (i.e. (, [, or {) occurs to the left of a closing
/// bracket(i.e. ), ], or }) of the exact same type. A sequence of brackets is balanced if the following conditions are met:
/// * it contains no unmatched brackets,
/// * the subset of brackets enclosed within the confines of a matched pair of brackets is also a matched pair of brackets.
/// Given a string of brackets, determine whether each sequence of brackets is balanced. If a string is balanced, return YES.
/// Otherwise, return NO.

std::string isBalanced(const std::string& s)
{
    const std::array<std::pair<char,char>,3> brPairs{
        std::make_pair('(', ')'),
        std::make_pair('{', '}'),
        std::make_pair('[', ']')
    };

    std::stack<char> brStack;

    for (auto c : s) {
        for (const auto& brp : brPairs) {
            if (c == brp.first) {
                brStack.push(c);
                break;
            }

            if (c == brp.second) {
                if (!brStack.empty() && brStack.top() == brp.first)
                    brStack.pop();
                else
                    brStack.push(c);
                break;
            }
        }
    }

    return brStack.empty() ? "YES" : "NO";
}

// 40. Largest Rectangle
/// Given an array of integers h, complete the function largestRectangle that would return an integer representing the
/// largest rectangle that can be formed within the bounds of consecutive elements. The rectangle that is formed by k
/// consecutive elements has the area of k * min(h[i], h[i+1], ..., h[i+k-1]), where i is between 1 and length of h.

long largestRectangle(std::vector<int> h)
{
    long area = 0;

    std::deque<std::pair<int,size_t>> dq;
    h.push_back(0);

    for (size_t i=0; i<h.size(); ++i) {
        size_t j = i;
        while (dq.size() > 0 && dq.back().first >= h[i]) {
            auto lastPair = dq.back();
            dq.pop_back();
            j = lastPair.second;
            area = std::max(area, static_cast<long>((i-j)*static_cast<size_t>(lastPair.first)));
        }
        dq.emplace_back(h[i], j);
    }

    return area;
}

// h = [1,2,3]
// i = 0: dq = [[1,0]]
// i = 1: dq.back().first = 1, h[i] = 2, dq = [[1,0],[2,1]]
// i = 2: dq.back().first = 2, h[i] = 3, dq = [[1,0],[2,1],[3,2]]
// i = 3: dq.back().first = 3, h[i] = 0, lastPair = [3,2], j = 2, area = max(0,3) = 3, dq = [[1,0],[2,1]]
//        dq.back().first = 2, h[i] = 0, lastPair = [2,1], j = 1, area = max(3,4) = 4, dq = [[1,0]]
//        dq.back().first = 1, h[i] = 0, lastPair = [1,0], j = 0, area = max(4,3) = 4, dq = [[]]

long largestRectangleO2(std::vector<int> h)
{
    long area = *h.begin();
    std::vector<std::pair<std::queue<int>,int>> heights;
    heights.push_back(std::make_pair(std::queue<int>(), *h.begin()));
    heights.begin()->first.push(*h.begin());

    int curMin = *h.begin();
    for (unsigned i=1; i<h.size(); ++i) {
        if (h[i] <= curMin) {
            curMin = h[i];
            const auto firstHeight = *heights.begin();
            heights.clear();
            heights.push_back(firstHeight);
        }
        else if (h[i] > h[i-1])
            heights.push_back(std::make_pair(std::queue<int>(), h[i]));

        for (auto& hts : heights) {
            hts.first.push(h[i]);

            if (h[i] < hts.second)
                hts.second = h[i];

            const long curArea = hts.second * hts.first.size();
            if (curArea > area)
                area = curArea;
        }
    }

    return area;
}

std::vector<int> parseFile(const std::string& fileName)
{
    std::vector<int> retVec;
    std::ifstream inFile(fileName);
    size_t inSize;

    inFile >> inSize;
    for (size_t i=0; i<inSize; ++i) {
        int number;
        inFile >> number;
        retVec.push_back(number);
    }
    inFile.close();

    return retVec;
}

// 41. Invert bytes
/// Having a 4-byte integer number, return its representation with bytes in reverse order. Print both the old representation
/// and the new one to screen for comparison.

uint32_t invertBytes(uint32_t number)
{
    uint8_t* ptr = reinterpret_cast<uint8_t*>(&number);
    std::reverse(ptr, ptr+sizeof(uint32_t));

    return number;
}

/// templated version that uses bit operators
template <typename T, std::enable_if_t<std::is_integral<T>::value>* = nullptr>
T invertBytes(T number)
{
    T mask = 0xFF, retVal = 0;

    for (unsigned i=0; i<sizeof(T); ++i) {
        auto byteVal = number & mask;

        if (i < sizeof(T)/2)
            byteVal <<= 8*(sizeof(T)-2*i-1);
        else
            byteVal >>= 8*(2*i-sizeof(T)+1);

        retVal |= byteVal;
        mask <<= 8;
    }

    return retVal;
}

/************************************************ The main driver function ************************************************/

int main()
{
    // 1. Find lattice points for circle of radius = 5, origin at (-1,2)
    auto latPoints = findLatticePoints2(5, std::array<int,2>{0,0});
    for( const auto& latPoint : latPoints )
        printIterable(latPoint, std::cout);
    std::cout << "\n";

    // 2. Ways to reach the nth stair
    const unsigned n = 5;
    std::cout << "There are " << countWaysToReachStair(0,n) << " ways to reach " << n << "th stair.\n\n";

    // 3. Collect maximum coins at global sandbox array
    auto coinsCollected = getCoins(0, 0, true, 0);
    std::cout << "Maximum " << *std::max_element(coinsCollected.begin(), coinsCollected.end()) << " coins collected.\n";
    printIterable(coinsCollected, std::cout);
    std::cout << "\n";

    // 4. Find recurring sequence in a fraction
    const int num = 5, den = 3;
    const unsigned seq = sequenceInFraction(num,den);
    if( seq ) std::cout << "The recurring sequence in " << num << "/" << den << " is: " << seq << ".\n\n";

    // 5. Maximum sum path in a matrix from top to bottom
    assert(getMaxSumDynamically() == getMaxSumRecursively());
    std::cout << "Maximum collected sum is " << getMaxSumDynamically() << ".\n";
    std::cout << "\n";

    // 6. Find the first repeated word in a string
    const char str[] = "  had  had   ";
    const char* firstRepeated = find1stRepeated(str);
    firstRepeated ? std::cout << "The first repeated word is: " << firstRepeated << ".\n" : std::cout << "No repeated word.\n";

    std::string stdstr{str};
    std::string stdFirstRepeated = find1stRepeated(stdstr);
    stdFirstRepeated.empty() ? std::cout << "No repeated word.\n\n" : std::cout << "The first repeated word is: " <<
                                            stdFirstRepeated << ".\n\n";

    // 7. Frogs
    const auto frogRes = separateFrogs();
    std::cout << "The maximum distance between frogs is " << frogRes[0] << " when frogs' quarrel was at " << frogRes[1] << ".\n\n";

    // 8. Roman Empire
    const int rome = findRome();
    rome >= 0 ? std::cout << "Rome is the city no. " << rome << ".\n" : std::cout << "There is no Rome on the map.\n\n";

    // 9. Palindrome
    const unsigned N = 6, K = 2;
    std::cout << "A random palindrome consisting of " << K << " distinct lower-case letters and total size of " << N <<
                 ": " << createPalindrome(N,K) << ".\n\n";

    // 10. Anagram
    std::cout << "Anagram results:\n" << std::boolalpha;
    std::cout << /*isAnagram1("abc", "bca") << " " << isAnagram2("abc", "bca") << " " <<*/ isAnagram3("abc", "bca") << "\n"; // Positive
    std::cout << /*isAnagram1("abcc", "cbca") << " " << isAnagram2("abcc", "cbca") << " " <<*/ isAnagram3("abcc", "cbca") << "\n"; // Positive
    std::cout << /*isAnagram1("abbc", "bcca") << " " << isAnagram2("abbc", "bcca") << " " <<*/ isAnagram3("abbc", "bcca") << "\n"; // Negative
    std::cout << /*isAnagram1("abbc", "bca") << " " << isAnagram2("abbc", "bca") << " " <<*/ isAnagram3("abbc", "bca") << "\n\n"; // Negative

    // 11. Median of two arrays
    const float arr1[] = { -25.4f, -2.6f, 4.9f, 28.2f, 33.1f };
    const float arr2[] = { -12.7f, 16.5f, 44.8f, 57.0f, 61.3f };
    std::cout << "The median value of two arrays is "
              << logFindMedianOf(arr1, sizeof(arr1)/sizeof(float), arr2, sizeof(arr2)/sizeof(float)) << ".\n\n";

    // 12. Second lowest integer
    const int arr[] = { 61, 44, -25, -2, 4, 28, 33, -12, 16, 57 };
    std::cout << "The second lowest integer is " << find2ndLowestOf(arr, sizeof(arr)/sizeof(int)) << ".\n\n";

    // 13. Snow between hills
    const unsigned hills[] = { 0, 1, 2, 1, 0, 3, 1, 2 };
    std::cout << "The amount of snow between the hills is: " << howMuchSnowBetween(hills, sizeof(hills)/sizeof(unsigned)) << ".\n\n";

    // 14. Travelling salesman
    const auto shortest = findShortestRoute(1,2);
    std::cout << "The shortest route takes " << shortest.first << " units of time and contains cities in order: ";
    printIterable(shortest.second, std::cout);
    std::cout << "\n";

    // 15. Find longest string available
    const std::vector<std::string> tokens{"toes", "toe", "abc", "stop", "baseball", "etot"};
    const std::string base = "toestp";
    const auto longestStrs = findLongestString(base, tokens);
    std::cout << "The longest strings that can be created from " << base << " are: ";
    printIterable(longestStrs, std::cout);
    std::cout << "\n";

    // 16. Find words starting with a prefix
    const std::vector<unsigned> startIdxs = findIndicesStartingWith("aa", "aa  aaa AaC   a  bb");
    std::cout << "Words starting with the given prefix start at indices: ";
    for(const unsigned sIdx : startIdxs)
        std::cout << sIdx << " ";
    std::cout << "\n\n";

    // 17. Collect maximum rocks on your way from So-Cal to NY
    assert(getMaxRocksDynamically() == getMaxRocksRecursively());
    std::cout << "You can collect maximum " << getMaxRocksDynamically() << " stones on your way.\n\n";

    // 18. Student with highest grade
    const std::string studentsMarks[][2] = { {"Bob","87"}, {"Mike","35"}, {"Bob","52"}, {"Jason","35"},
                                             {"Mike","55"}, {"Jessica","97.5"} };
    const auto bestStud = highestGrade(studentsMarks,6);
    std::cout << "The student with the highest grade is " << bestStud.first << " (" << bestStud.second << ").\n\n";

    // 19. Power of 10
    std::cout << "Is 10 power of 10? " << std::boolalpha << isPowerOf10(10)
              << "\nIs 100 power of 10? " << isPowerOf10(100)
              << "\nIs 1020 power of 10? " << isPowerOf10(1020)
              << "\nIs 7603 power of 10? " << isPowerOf10(7603)
              << "\nIs -10000 power of 10? " << isPowerOf10(-10000) << "\n\n";

    // 20. Two-egg-problem
    const std::array<unsigned,10> limFloors = {1, 2, 10, 20, 50, 51, 70, 75, 80, 100};
    for(const auto limit : limFloors) {
        const auto res = findHighestFloor(limit);
        std::cout << "The floor for which an egg breaks is " << res.first << " and was found using " << res.second << " drops.\n";
    }
    std::cout << "\n";

    // 21. Second largest element in a BST
    BST<float> bst1{ std::vector<float>{3.5f, -2.8f, 14.9f, -15.2f, 7.6f, -5.0f} };
    std::cout << "The second largest element of the binary search tree is " << find2ndLargestOf(bst1) << ".\n\n";

    // 22. Convert a normal BST to a balanced BST
    BST<float> bst2{ std::vector<float>{-15.2f, -5.0f, -2.8f, 3.5f, 7.6f, 14.9f} };
    std::cout << "The balanced version of the given tree is: " << convertToBalanced(bst2) << ".\n\n";

    // 23. Remove more than k-times-repetitive characters from a string
    std::string example = "jktttksssskkahhhdj";
    removeNeighboringCharsFrom(example, 2);
    std::cout << "The string after removal of twice-repetitive chars is " << example << ".\n\n";

    // 24. Sum of bit differences among all pairs
    const int intArr[] = { 0, 1, 2, 3, 4 };
    std::cout << "Sum of bit differences is " << sumOfBitDiffs(intArr, sizeof(intArr)/sizeof(int)) << ".\n\n";

    // 25. Teacher's counting-out
    std::cout << "The last pupil left after the counting-out has number: " << countingOutRhyme(10, 3) << ".\n\n";

    // 26. The chess horse
    std::cout << "The probability of the chess horse to move out of the chess board in a single move is "
              << chessHorseEscape(std::make_pair(4,4), 1) << " when starting at field D4 and "
              << chessHorseEscape(std::make_pair(1,1), 2) << " when starting at field A1 (in 2 moves).\n\n";

    // 27. Kth row of Pascal's Triangle
    std::cout << "The 3rd row of Pascal's Triangle is: ";
    printIterable(PascalsRowRecursive(3), std::cout);
    std::cout << "The 4th row of Pascal's Triangle is: ";
    printIterable(PascalsRowIterative(4), std::cout);
    std::cout << "\n";

    // 28. Grid unique paths
    const unsigned A = 3, B = 3;
    std::cout << "There are " << gridUniques(A,B) << " unique paths in a " << A << "x" << B << " grid.\n\n";

    // 29. Component for checking task status
    Component c{7600006};
    c.acknowledge(7600008);
    c.acknowledge(7600007);
    std::cout << "The first unacknowledged task has no. " << c.get() << "\n";
    c.acknowledge(7600009);
    c.acknowledge(7600006);
    c.acknowledge(7600010);
    std::cout << "The first unacknowledged task has no. " << c.get() << "\n\n";

    // 30. Longest common prefix
    const std::string strArr[] = {"abcdefgh", "abcghijk", "abcefgh"};
    std::cout << "The longest common prefix of the strings ";
    printIterable(strArr, std::cout);
    std::cout << "is " << longestPrefix(strArr, sizeof(strArr)/sizeof(std::string)) << ".\n\n";

    // 31. The gas stations' rally
    const unsigned arrA[] = {1, 2}, arrB[] = {2, 1};
    std::cout << "The first possible starting point of the gas stations' rally is "
              << find1stGasStation(arrA, arrB, sizeof(arrA)/sizeof(unsigned)) << ".\n\n";

    // 32. Flip 2 and 3
    std::cout << "Flipping 2 we get " << flip2and3(2) << ", flipping 3 we get " << flip2and3(3) << ".\n\n";

    // 33. The Cake Thief
    const std::vector<std::pair<unsigned,unsigned> > cakeTypes{ std::make_pair(7,160), std::make_pair(3,90), std::make_pair(2,15) };
    const unsigned capacity = 20;
    std::cout << "The biggest haul you can get from the Queen's vault in a bag with a capacity of " << capacity
              << " is " << maxDuffelBagValue(cakeTypes, capacity) << ".\n\n";

    // 34. Detect a cycle
    const std::array<unsigned,3> uArr{1, 2, 0};
    std::cout << "A cycle is hit when touring the array after " << detectCycleIn(uArr, 0) << " moves.\n\n";

    // 35. Interleaving strings
    std::string checkStr = " ";
    if( !byInterleaving("aabcc", "dbbca", "aadbbcbcac") ) checkStr = " not ";
    std::cout << "It is" << checkStr << "possible to get C by interleaving A and B.\n";
    byInterleaving("aabcc", "dbbca", "aadbbbaccc") ? checkStr = " " : checkStr = " not ";
    std::cout << "It is" << checkStr << "possible to get C by interleaving A and B.\n\n";

    // 36. Luck Balance
    const unsigned k = 3;
    const std::vector<std::array<int,2> > contests{ {5,1}, {2,1}, {1,1}, {8,1}, {10,0}, {5,0} };
    std::cout << "The amount of luck gathered by Lena is " << luckBalance(k, contests) << ".\n\n";

    // 37. Fair array
    std::cout << "The minimum possible unfairness is " << minUnfair(3, std::vector<int>{10,100,200,300,1000,20,30}) << ".\n\n";

    // 38. Max Array sum
    const std::vector<int> array{3, 7, 4, 6, 5};
    std::cout << "The maximum element sum for a subset of non-adjacent elements for array: ";
    printIterable(array, std::cout);
    std::cout << "is " << maxSubsetSum2(array) << ".\n\n";

    // 39. Balanced Brackets
    const std::vector<std::string> bracketStrings = {"}][}}(}][))]", "[](){()}", "()", "({}([][]))[]()", "{)[](}]}]}))}(())(", "([[)"};
    for (const auto& brStr : bracketStrings)
        std::cout << "Is the bracket string: " << brStr << " balanced? " << isBalanced(brStr) << "\n";

    // 40. Largest Rectangle
    const std::vector<int> h = parseFile("/Users/training/Documents/input08.txt");//{8979, 4570, 6436, 5083, 7780, 3269, 5400, 7579, 2324, 2116};
    std::cout << "\nThe largest rectangle that can be formed from the array: ";
    printIterable(h, std::cout);
    std::cout << "is " << largestRectangle(h) << ".\n\n"; /// Expected: 26152 // 12984467

    // 41. Invert bytes
    const std::vector<uint32_t> numbers32{0, 255, 0xAABBCCDD};
    for (const auto num : numbers32) {
        std::cout << std::hex << "The incoming number in hexadecimal format is: " << num << "\n";
        std::cout << "After inversion: " << invertBytes(num) << "\n";
    }

    const std::vector<uint64_t> numbers64{0, 255, 0xAABBCCDD, 0xAABBCCDDEEFF1122};
    for (const auto num : numbers64) {
        std::cout << std::hex << "The incoming number in hexadecimal format is: " << num << "\n";
        std::cout << "After inversion: " << invertBytes(num) << "\n";
    }

    return 0;
}
