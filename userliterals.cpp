#include <cstdio>
#include <exception>
#include <string>
#include <cstring>
#include <limits>

/****************************************** Cooked literal "Kilograms" class ********************************************/

class Kilograms
{
    double rawWeight;

public:
    class DoubleIsKilos {}; // a tag
    explicit constexpr Kilograms(DoubleIsKilos, double wgt) : rawWeight{wgt} {}
    operator double() const { return rawWeight; }
};

constexpr Kilograms operator"" _kg( long double wgt )
{
    return Kilograms( Kilograms::DoubleIsKilos(), static_cast<double>(wgt) );
}

constexpr Kilograms operator"" _lb( long double wgt )
{
    return Kilograms( Kilograms::DoubleIsKilos(), static_cast<double>(0.453592 * wgt) );
}

/************************************** Cooked literal "Probability" class & co. ****************************************/

class BadProbability : public std::exception
{
    const long double v;
public:
    explicit BadProbability( long double v ) : v{v} {}
    virtual const char* what() const throw() {
        std::string s{ "Exception while creating user-defined probability literal of value " };
        s += std::to_string(v);
        s += "\n";
        return s.c_str(); }
};

class Probability
{
    long double value;
    // invariant: 0.0 <= value && value <= 1.0
public:
    class LDoubleIsProb {};
    explicit constexpr Probability(LDoubleIsProb, long double val) : value{val} {}
    operator double() const { return static_cast<double>(value); }
};

constexpr Probability operator"" _prob( long double val )
{
    return val > 1.0 ? throw BadProbability(val) : Probability( Probability::LDoubleIsProb(), val );
}

/**************************************** Cooked literal _s std::string operator ****************************************/

std::string operator"" _s( const char* str, std::size_t len )
{
    return std::string{str};
}

void takeString( const char str[] ) { printf("takeString(const char[]) with argument %s\n",str); return; }
void takeString( std::string str ) { printf("takeString(std::string) with argument %s\n",str.c_str()); return; }

/***************************************** Raw literal binary integer operator ******************************************/

template <typename T>
constexpr size_t NumberOfBits()
{
    static_assert(std::numeric_limits<T>::is_integer,"Only integers allowed!\n");
    return std::numeric_limits<T>::digits;
}

unsigned operator"" _b( const char* str )
{
    if( strlen(str) > NumberOfBits<unsigned>() )
        throw std::runtime_error("Exception: binary literal too long.\n");

    unsigned bin = 0;

    for(unsigned i=0; i<strlen(str); ++i) {
        bin *= 2;
        if(str[i] == '0')
            continue;
        else if(str[i] == '1')
            bin += 1;
        else
            throw std::runtime_error("Exception: only 0s and 1s allowed in a binary literal.\n");
    }

    return bin;
}


/************************************ Constexpr raw literal binary integer operator *************************************/

class BadBinary : public std::exception {
    const char c;

public:
    BadBinary( char c ) : c{c} {}
    virtual const char* what() const noexcept {
        std::string message{"Exception when parsing "};
        message += c;
        message += ": only 0s and 1s allowed in a binary literal initialization.\n";
        return message.c_str();
    }
};

constexpr unsigned calculateVal( const char* str, unsigned val, unsigned pos = 0 )
{
    return assert(pos < NumberOfBits<unsigned>()),
           (str[pos] == '1' ? calculateVal(str,2*val+1,pos+1) :
           (str[pos] == '0' ? calculateVal(str,2*val,pos+1) :
           (str[pos] == '\0' ? val : throw BadBinary(str[pos]) )));
}

constexpr unsigned operator"" _bx( const char* str )
{
    return calculateVal(str,0);
}

/***************************** Constexpr raw literal binary integer size-dependent operator *****************************/

template <std::size_t SIZE>
struct unsignedIntT
{
    static_assert(SIZE <= NumberOfBits<unsigned long long>(), "Too many digits in the binary literal!\n");

    template <std::size_t size, bool isLong>
    struct unsignedLong
    {
        typedef unsigned long type;
    };

    template <std::size_t size>
    struct unsignedLong<size,false>
    {
        typedef unsigned long long type;
    };

    template <std::size_t size, bool isShort>
    struct unsignedShort
    {
        typedef unsigned type;
    };

    template <std::size_t size>
    struct unsignedShort<size,false>
    {
        typedef typename unsignedLong<size, size <= NumberOfBits<unsigned long>()>::type type;
    };

    typedef typename unsignedShort<SIZE, SIZE <= NumberOfBits<unsigned>()>::type type;
};

template <std::size_t size>
using unsignedInt = typename unsignedIntT<size>::type;

template <typename uInt, uInt val>
constexpr uInt calculateVal()
{
    return val;
}

template <typename uInt, uInt val, char first, char... str>
constexpr uInt calculateVal()
{
    return (first == '1' ? calculateVal<uInt,2*val+1,str...>() :
           (first == '0' ? calculateVal<uInt,2*val,str...>() :
           (first == '\0' ? calculateVal<uInt,val,str...>() : throw BadBinary(first))));
}

template <char... str>
constexpr unsignedInt<sizeof...(str)> operator"" _bs()
{
    return calculateVal<unsignedInt<sizeof...(str)>,0,str...>();
}

//////////////////////////////////////////////// The main driver function ////////////////////////////////////////////////

//Kilograms wgt_err = 100.2; // compile-time error
Kilograms wgt1 = 100.2_kg;
Kilograms wgt2 = 200.5_lb;
constexpr Probability prb = 0.2_prob;
//constexpr Probability prb_err = 1.3_prob; // compile-time error

unsigned bin1 = 101_b, bin2 = 1000_b;
constexpr unsigned bin3 = 111_bx;
//constexpr unsigned bin4 = 12_bx; // compile-time error with exception
//constexpr unsigned bin5 = 100010001000100010001000100010001000_bx; // compile-time error with assert
constexpr auto bin6 = 111111111_bs;
//constexpr auto bin7 = 12_bs; // compile-time error with exception
constexpr auto bin8 = 1000100010001000_bs; // compile-time error with assert

auto main() -> int
{
    printf("Kilograms-class variables: %.3f, %.3f\n", static_cast<double>(wgt1), static_cast<double>(wgt2));
    printf("Probability-class variable: %.2f\n", static_cast<double>(prb));

    try {
        Probability prob = 1.2_prob;
    } catch( const std::exception& e ) {
        fprintf(stderr,"%s",e.what());
    }

    takeString("dog");
    takeString("dog"_s);

    assert(bin1 == 5);
    assert(bin2 == 8);
    printf("Non-constexpr binary variables' values: %u, %u, %u\n",bin1,bin2,bin3);

    try {
        unsigned bin = 212_b;
        assert(false); // we should never get here
    } catch( const std::runtime_error& e ) {
        fprintf(stderr,"%s",e.what());
    }

    try {
        unsigned bin = 100010001000100010001000100010001000_b;
        assert(false);
    } catch( const std::runtime_error& e ) {
        fprintf(stderr,"%s",e.what());
    }

    try {
        unsigned bin = 911_bx;
        assert(false);
    } catch( const BadBinary& e ) {
        fprintf(stderr,"%s",e.what());
    }

    auto i{010_bs};

    return 0;
}
