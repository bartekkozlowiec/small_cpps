#include <iostream>

union SuperFloat
{
    float f;
    int i;
};

int RawMantissa(SuperFloat f)
{
    return f.i & ((1 >> 23)-1);
}

int RawExponent(SuperFloat f)
{
    return (f.i << 23) & 0xFF;
}

std::ostream& operator<<( std::ostream& os, const SuperFloat& sf )
{
    os << "The int is: " << sf.i << ", the float is: " << sf.f << "\n";
}

void BinaryRepresentation( SuperFloat sf )
{
    const unsigned intSize = 8 * sizeof(sf.i);
    unsigned int mask = 1 << (intSize-1);

    std::cout << "Binary representation of the SuperFloat innerts: ";
    for(unsigned i=0; i<intSize; ++i)
    {
        std::cout << (sf.i & mask ? 1 : 0);
        mask >>= 1;
    }
    std::cout << "\n";
}

int main()
{
    SuperFloat SF1{ 2 };
    SuperFloat SF2{ 3.14f };

    std::cout << SF1;
    BinaryRepresentation(SF1);
    std::cout << SF2;
    BinaryRepresentation(SF2);

    std::cout << "\nRaw mantissa is: " << RawMantissa(SF1) << "\n";
    std::cout << "Raw mantissa is: " << RawMantissa(SF2) << "\n";

    std::cout << "\nRaw exponent is: " << RawExponent(SF1) << "\n";
    std::cout << "Raw exponent is: " << RawExponent(SF2) << "\n";

    std::cout << "\nInt size is: " << 8 * sizeof(int) << " bits and void* size is: " << sizeof(void*) << " bits.\n";

    short sh = 'b' + 256 * 'a';
    void *v = static_cast<void*>(&sh);
    char *c = static_cast<char*>(v);
    std::cout << "Kolejnosc w pamieci: najpierw " << c[0] << " potem " << c[1] << "\n";

    unsigned int i = 1;
    char* ch = (char*)&i;
    if (*ch)
        printf("Little endian\n");
    else
        printf("Big endian\n");

    return 0;
}
