// Warning: no main function included, compile with the "-c" flag

#include <iostream>
#include <string>

// Some trivial meta-functions - building blocks for the next steps

struct TrueType
{
    static constexpr bool value = true;
};

struct FalseType
{
    static constexpr bool value = false;
};

/// A small check

static_assert(TrueType::value, "***");
static_assert(!FalseType::value, "***");

// A metafunction that detects if given types T and U are identical

template <typename T, typename U>
struct IsSame : FalseType {};

template <typename T>
struct IsSame<T,T> : TrueType {};

/// Checking

static_assert(IsSame<int,int>::value, "***");

// A metafunction which tells if the given type has a size of 1 byte

template <typename T, bool Small = (sizeof(T) == 1)>
struct IsSmall : FalseType {};

template <typename T>
struct IsSmall<T,true> : TrueType {};

/// Checking

static_assert(IsSmall<char>::value, "***");
static_assert(!IsSmall<int>::value, "***");

class Empty {};
static_assert(IsSmall<Empty>::value, "***");
static_assert(IsSmall<bool>::value, "***");

// Building a local version of enable_if type trait

template <bool Cond, typename = void>
struct enable_if {};

template <typename T>
struct enable_if<true,T>
{
    typedef T type;
};

/// This simplification is legal only since C++14

template <bool Cond, typename T = void>
using enable_if_t = typename enable_if<Cond,T>::type;

/// Checking

static_assert(enable_if_t<IsSmall<char>::value,TrueType>::value, "***");

template <typename T>
struct voidTester
{
    typedef enable_if_t<IsSmall<T>::value> type;
    static bool constexpr returnValue() { return true; }
};

static_assert(voidTester<char>::returnValue(), "***");

// An alternative version of IsSmall type_trait

template <typename T, typename = void>
struct IsSmall2 : FalseType {};

template <typename T>
struct IsSmall2<T, enable_if_t<sizeof(T) == 1>> : TrueType {};

/// Checking

static_assert(IsSmall2<char>::value, "***");
static_assert(!IsSmall2<int>::value, "***");

static_assert(IsSmall2<Empty>::value, "***");
static_assert(IsSmall2<bool>::value, "***");

// A local version of void_t type trait

template <typename ...>
struct void_t_impl
{
    typedef void type;
};

template <typename ... T>
using void_t = typename void_t_impl<T...>::type;

/// Checking - defining two functions

void_t<int,double,char,std::string> foo1() {};
void_t<enable_if_t<sizeof(char) == 1> > foo2() {};


/******************************* Building the desired type trait *******************************/
/*
 * Requirements:
 * T::result_type   - is a type
 * T::set_limit(1)  - static function that takes one argument of type int, return type unimportant
 * x.get_result()   - member function returning const result_type&, declared not to throw
 *
 * This is to be checked by a class template boolean member is_acceptable<T>::value
 */

// The primary template

template <typename T, typename = void>
struct is_acceptable : FalseType {};

// The specialization - requirement #1

template <typename T>
struct is_acceptable<T,void_t<typename T::result_type> > : TrueType {};

/// Checking

struct Req1Tester
{
    struct result_type {};
};

static_assert(is_acceptable<Req1Tester>::value, "***");
