#include <iostream>
#include <cstring>

using namespace std;

void KonwersjeTrywialne();
void KonwersjePoPromocji();
void KonwersjeStandardowe();

class A
{
    char c;

public:
    A( char c = 'a' ) : c(c)
    { }

    char getc() const
    {
        return c;
    }

    char getcv() volatile
    {
        return c;
    }

    void changec( char c )
    {
        this->c = c;
    }
};

class B : public A
{
    char* cc;

public:
    B( char* cc = "napis" ) : cc(new char[strlen(cc)+1]), A('z')
    {
        strcpy(this->cc,cc);
    }

    B( const B& Bcop ) : cc(strcpy(new char[strlen(Bcop.cc)+1],Bcop.cc)) // wersja kopiujaca w jednej instrukcji
    { }

    B& operator=( const B& Bcop )
    {
        if(this != &Bcop)
        {
            delete cc;
            cc = strcpy(new char[strlen(Bcop.cc)+1],Bcop.cc);
        }
        return *this;
    }

    ~B()
    {
        delete cc;
    }

    char* getcc()
    {
        return cc;
    }
};

int main()
{
    // Przeglad konwersji niejawnych
    KonwersjeTrywialne();
    KonwersjePoPromocji();
    KonwersjeStandardowe();

    return 0;
}

void KonwersjeTrywialne()
{
    /// Konwersje trywialne - typ --> referencja i vice versa
    A a1, a2('b');

    A& refa = a1;
    refa    = a2;
    a1      = refa;

    cout << "a1.c = " << a1.getc() << ", a2.c = " << a2.getc() << ", refa.c = " << refa.getc() << endl;

    a1.changec('a');

    /// Konwersje trywialne - tablica --> wskaznik
    A  taba[] = { a1, a2 };
    A* wska;

    wska = taba;

    cout << "taba[0].c = " << taba[0].getc() << ", *wska = " << wska->getc() << ", taba[1].c = " << taba[1].getc() << ", *(wska + 1) = " << (wska+1)->getc() << endl;

    /// Konwersje trywialne - typ --> typ ustalony i typ volatile
    const A a3 = a1;
    volatile A a4 = a2;

    cout << "a3.c = " << a3.getc() << ", a4.c = " << a4.getcv() << endl;

    /// Konwersje trywialne - wskaznik --> wskaznik ustalony i wskaznik volatile
    A*          wska1 = &a1;
    const A*    wskac;
    volatile A* wskav;

    wskac = wska1;
    wskav = wska1;

    cout << "wska1->c = " << wska1->getc() << ", wskac->c = " << wskac->getc() << ", wskav->c = " << wskav->getcv() << endl << endl;

    //wska1 = wskac;  // blad!
    //wska1 = wskav;  // blad!
}

void KonwersjePoPromocji()
{
    /// float --> double
    float  f1 = 1.1;
    double d1 = f1;

    cout << "d1 = " << d1 << endl;

    /// signed char, short, int --> int
    signed char  sc1 = 1;
    signed short ss1 = 2;
    signed int   si1 = 3;
    int i1, i2, i3;
    i1 = sc1;
    i2 = ss1;
    i3 = si1;

    cout << "i1 = " << i1 << ", i2 = " << i2 << ", i3 = " << i3 << endl;

    /// unsigned char, short, int --> int
    unsigned char  uc1 = 4;
    unsigned short us1 = 5;
    unsigned int   ui1 = 6;
    int i4, i5, i6;
    i4 = uc1;
    i5 = us1;
    i6 = ui1;

    cout << "i4 = " << i4 << ", i5 = " << i5 << ", i6 = " << i6 << endl << endl;
}

void KonwersjeStandardowe()
{
    /// double --> float
    double d1 = 2.2;
    float  f1 = d1;

    cout << "f1 = " << f1 << endl;

    /// typ zmiennoprzecinkowy --> typ calkowity i vice versa
    int   i = 1, ii;
    float f = 1.5, ff;
    ii = f;
    ff = i;

    cout << "ii = " << ii << ", ff = " << ff << endl;

    /// wskaznik na typ pochodny --> wskaznik na typ podstawowy
    A a1;
    A* wska1;
    B b1("elox");
    B* wskb1 = &b1;

    wska1 = wskb1;

    cout << "wska1->c = " << wska1->getc() << ", wskb1->c = " << wskb1->getc() << ", wskb1->cc = " << wskb1->getcc() << endl;
}
