#include <iostream>
#include <cstring>

using namespace std;

/******************** Deklaracja wyprzedzajaca ***********************/

template<typename T>
class spryciarz;

/***************** Definicje klas i szablonow klas *******************/

class wektor
{
    unsigned wskazanie;

    friend class spryciarz<wektor>;

public:
    float x, y, z;

    //-----konstruktor
    wektor( float a, float b, float c ) : x(a), y(b), z(c), wskazanie(0)
    { }

    //-----kilka zwyklych funkcji skladowych
    wektor operator*=( float mnoznik )
    {
        x *= mnoznik; y *= mnoznik; z *= mnoznik;
        return *this;
    }

    void pokaz()
    {
        cout << "x = " << x << ", y = " << y << ", z = " << z << endl;
    }
};

/*********************************************************************/

template<typename T>
class spryciarz
{
    T* wsk;
    T* (pamietnik[10]);
    int uzycie;

public:
    //-----operator przypisania
    void operator=( T* w )
    {
        wsk = w;
        (w->wskazanie)++; // ??
    }

    //-----konstruktor (takze domyslny)
    spryciarz( T* adr = NULL );

    //-----przeladowanie operatora ->
    T* operator->();

    void statystyka();
};

/****************** Definicje funkcji skladowych ********************/

template<typename T>
spryciarz<T>::spryciarz( T* adr ) : wsk(adr), uzycie(0)
{
    for(int i=0; i<10; i++)
        pamietnik[i] = NULL;
}

/********************************************************************/

template<typename T>
T* spryciarz<T>::operator->()
{
    //-----akcja sprytna: wpisujemy do akt
    pamietnik[uzycie] = wsk;
    uzycie = (++uzycie) % 10;
    //-----zwykla akcja
    return wsk;
}

/********************************************************************/

template<typename T>
void spryciarz<T>::statystyka()
{
    cout << "Ostatnie 10 wypadkow uzycia odbylo sie "
         << "dla obiektow \no adresach:\n";

    for(int i=0; i<10; i++)
        cout << pamietnik[(uzycie + i) % 10] << ((i==4) ? "\n" : ", ");
    cout << endl;
}

/****************** Definicje funkcji globalnych *******************/

char* stringcat( char* dest, const char* src )
{
    char* s = dest + strlen(dest);
    cout << "strlen(dest) = " << strlen(dest) << endl;
    cout << "stringcat: s = " << s << endl;
    cout << "stringcat: s-1 = " << s-1 << endl;
    cout << "stringcat: s-2 = " << s-2 << endl;
    cout << "stringcat: s-3 = " << s-3 << endl;
    cout << "stringcat: dest = " << dest << endl;
    strcpy(s,src);
    cout << "stringcat: s = " << s << endl;
    cout << "stringcat: dest = " << dest << endl << endl;
    return dest;
}

/*******************************************************************/

int main()
{
    char s1[] = "Ula";
    char s2[] = "Ola";
    cout << "********* Czesc pierwsza programu *********" << endl;
    cout << "s1 = " << s1 << ", s2 = " << s2 << endl;

    char* s3;
    s3 = stringcat(s1,s2);
    cout << "s3 = " << s3 << endl;

    stringcat(s1,s2);
    cout << "Now s1 = " << s1 << endl;

    cout << "\n\n"
            "********** Czesc druga programu **********" << endl;

    float m;
    wektor w1(1,1,1);

    wektor* zwykly_wsk;
    spryciarz<wektor> zreczny_wsk;

    zwykly_wsk  = &w1;
    zreczny_wsk = &w1;

    cout << "Operacja za pomoca zwyklego wskaznika:\n";
    m = zwykly_wsk->x;
    cout << "m = " << m << endl;
    cout << "Operacja za pomoca zrecznego wskaznika:\n";
    m = zreczny_wsk->x;
    cout << "m = " << m << endl;

    wektor w2(2,2,2), w3(3,3,3), w4(44,10,1);

    zreczny_wsk = &w2;
    zreczny_wsk->operator*=(2);
    zreczny_wsk->pokaz();

    zreczny_wsk = &w3;
    zreczny_wsk->operator*=(2);
    zreczny_wsk->pokaz();

    zreczny_wsk = &w4;
    zreczny_wsk->operator*=(2);
    zreczny_wsk->pokaz();

    zreczny_wsk.statystyka();

    return 0;
}
