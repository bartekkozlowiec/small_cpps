#include <iostream>

using namespace std;

enum kolor{ czarny, czerwony, niebieski, bialy };

/////////////////////// Definicje klas //////////////////////////

class pojazd
{
protected:
    int iloscKol;

public:
    pojazd( int ile ) : iloscKol(ile)
    { }
    pojazd( const pojazd& );
    pojazd operator=( const pojazd& );
};

/////////////////////////////////////////////////////////////////

class automobil : public pojazd
{
    int   stanLicznika;
    kolor kolorKaroserii;

public:
    automobil( int kola, kolor k, int licznik ) : pojazd(kola)
    {
        stanLicznika   = licznik;
        kolorKaroserii = k;
    }
    automobil( const automobil& );
    automobil operator=( const automobil& );
    friend ostream& operator<<( ostream&, const automobil& );
};

////////////////// Definicje funkcji skadowych ///////////////////

pojazd::pojazd( const pojazd& wzor )
{
    iloscKol = wzor.iloscKol;
}

//////////////////////////////////////////////////////////////////

automobil::automobil( const automobil& wzorzec ) : pojazd(wzorzec)
{
    stanLicznika   = 0;
    kolorKaroserii = wzorzec.kolorKaroserii;
}

//////////////////////////////////////////////////////////////////

pojazd pojazd::operator=( const pojazd& wz )
{
    iloscKol = wz.iloscKol;
    return *this;
}

//////////////////////////////////////////////////////////////////

automobil automobil::operator=( const automobil& wzor )
{
    // Do wyboru jeden z czterech rownorzednych sposobow (2 jawnych
    // i 2 niejawnych)
    // this->pojazd::operator=(wzor);
    // (*this).pojazd::operator=(wzor);
    // *((pojazd*)this) = wzor;
    (pojazd&)(*this) = wzor;

    // Dalej dokonczenie kopiowania dla skladnikow pochodnych
    kolorKaroserii = wzor.kolorKaroserii;
    stanLicznika   = wzor.stanLicznika + 2;
    return *this;
}

/**************** Definicje funkcji globalnych ******************/

ostream& operator<<( ostream& ekran, const automobil& obj )
{
    ekran << "Liczba kol: " << obj.iloscKol
          << ", stan licznika: " << obj.stanLicznika
          << ", kolor nr " << obj.kolorKaroserii << endl;
    return ekran;
}

/****************************************************************/

int main()
{
    automobil moj(4, bialy, 30000);
    automobil twoj = moj;

    cout << "Lista istniejacych automobili\n"
         << "moj: " << moj
         << "twoj: " << twoj;

    automobil dziwak(3, bialy, 5000);
    cout << "Jeszcze jeden o nazwie dziwak:\n"
         << dziwak;

    dziwak = moj;

    cout << "dziwak po przypisaniu:\n" << dziwak;

    const automobil muzealny(4, czerwony, 25000);
    dziwak = twoj = muzealny; // Uwaga! w tej kaskadzie uzyty konstr. kopiujacy!

    cout << "Muzealny: " << muzealny;
    cout << "Twoj po...: " << twoj;
    cout << "Dziwak po...: " << dziwak;

    return 0;
}
