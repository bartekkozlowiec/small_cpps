#include <iostream>
#include <optional>

/********************************* The possibly failable functions we need to call ********************************/

std::optional<int> f1( int a );
std::optional<int> f2( int b, int c );
std::optional<int> f3( int d );
std::optional<int> f4( int e );

std::optional<int> f1( int a )
{
    int pivot = a % 3;
    if( pivot )
        return pivot;
    else // this is like throwing an exception
        return std::nullopt;
}

std::optional<int> f2( int b, int c )
{
    return b + c;
}

std::optional<int> f3( int d )
{
    return d*d;
}

std::optional f4( int e )
{
    return 100 % e;
}

/*********************************************** The monad utlities ***********************************************/

template <typename T, typename TtoOptionalU>
auto operator>>=( std::optional<T> const& t, TtoOptionalU f ) -> decltype(f(*t))
{
    if( t )
        return f(*t);
    else
        return std::nullopt;
}

template <typename R, typename ... P>
auto make_failable( R (*f)( P ... ps ) )
{
    return [&f]( std::optional<P> ... xs ) -> std::optional<R>
    {
        if(( xs && ... ))
            return { f(*(xs)...) };
        else
            return {};
    };
}

/********************************************* Printing of an optional ********************************************/

template <typename ... T>
struct void_t_impl
{
    typedef void type;
};

template <typename ... T>
using void_t = void_t_impl<T...>::type;

template <typename T>
T&& declval();

template <typename T, typename Stream, void_t<decltype( declval<Stream>() << declval<T>() )>* = nullptr>
void printResult( const std::optional<T>& res, Stream&& ostr )
{
    if( res )
        ostr << "The result is: " << *res << "\n";
    else
        ostr << "Result not available.\n";
}

/*********************************************** The driver function **********************************************/

int main()
{
    // No monad, just optionals
    std::optional<int> result1;

    std::optional<int> b = f1(2);
    if( b )
    {
        std::optional<int> c = f2(b,b);
        if( c )
        {
            std::optional<int> d = f3(c);
            if( d )
            {
                result1 = f4(d);
            }
        }
    }
    std::cout << "No monad solution. ";
    printResult(result1,std::cout);

    // Monad with the ">>=" operator
    std::optional<int> result2 = f1(2) >>= []( int b ){ return
                f2(b,b) >>= []( int c ){ return
                f3(c) >>= []( int d ){ return
                f4(d); }; }; };

    std::cout << "Monad with operator \">>=\". ";
    printResult(result2,std::cout);

    // Monad with make_failable function template
    auto failable_f1 = make_failable(f1);
    auto failable_f2 = make_failable(f2);
    auto failable_f3 = make_failable(f3);
    auto failable_f4 = make_failable(f4);

    auto result3 = failable_f4( failable_f3( failable_f2( failable_f1(2), failable_f1(2) ) ) );

    std::cout << "Monad with make_failable function template. ";
    printResult(result3,std::cout);

    return 0;
}
