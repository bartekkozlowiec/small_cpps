#include <iostream>

/****************************************** Some utility structures ******************************************/

template <typename ... T>
struct void_t_impl
{
    typedef void type;
};

template <typename ... T>
using void_t = typename void_t_impl<T...>::type;

template <bool Cond, typename T = void>
struct enable_if
{
    typedef T type;
};

template <typename T>
struct enable_if<false,T>
{};

template <bool Cond, typename T>
using enable_if_t = typename enable_if<Cond,T>::type;

template <typename T>
T&& declval();

/************************************* Template function to print array **************************************/

template <typename T, typename Stream>
void printArray( const T array[], const unsigned n, Stream&& ostr,
                 void_t<decltype(declval<Stream>() << declval<T>())>* = nullptr )
{
    ostr << "Array contents: ";
    for(unsigned i=0; i<n; ++i)
        ostr << array[i] << " ";
    ostr << "\n";
}

/******************************************* Sorting algorithms **********************************************/

template <typename T>
void rMergeSort( T array[], const unsigned n, void_t<decltype(declval<T>() < declval<T>())>* = nullptr )
{
    if( n > 1 )
    {
        const unsigned lowerSize = n/2;
        const unsigned higherSize = n-n/2;
        T* lowerArr  = new T[lowerSize];
        T* higherArr = new T[higherSize];

        for(unsigned i=0; i<lowerSize; ++i)
            lowerArr[i] = array[i];

        for(unsigned i=n/2; i<n; ++i)
            higherArr[i-n/2] = array[i];

        rMergeSort(lowerArr,lowerSize);
        rMergeSort(higherArr,higherSize);

        unsigned currLidx = 0;
        unsigned currHidx = 0;
        for(unsigned i=0; i<n; ++i) {
            if( currLidx < lowerSize && currHidx < higherSize )
            {
                if( lowerArr[currLidx] < higherArr[currHidx] )
                    array[i] = std::move(lowerArr[currLidx++]);
                else
                    array[i] = std::move(higherArr[currHidx++]);
            }
            else if( currLidx < lowerSize )
                array[i] = std::move(lowerArr[currLidx++]);
            else
                array[i] = std::move(higherArr[currHidx++]);
        }

        delete [] higherArr;
        delete [] lowerArr;
    }
}

template <typename T>
void merge( T array[], const unsigned low, const unsigned mid, const unsigned high,
            void_t<decltype(declval<T>() < declval<T>())>* = nullptr )
{
    if( high > low ) {
        unsigned i = low;
        unsigned j = mid+1;
        unsigned k = 0;
        T* tempArr = new T[high+1-low];

        while( i <= mid && j <= high ) {
            if( array[i] < array[j] )
                tempArr[k++] = array[i++];
            else
                tempArr[k++] = array[j++];
        }

        for(; i<=mid; ++i)
            tempArr[k++] = array[i];
        for(; j<=high; ++j)
            tempArr[k++] = array[j];

        for(i=low; i<=high; ++i)
            array[i] = tempArr[i-low];

        delete [] tempArr;
    }
}

template <typename T>
void iMergeSort( T array[], const unsigned n )
{
    unsigned p;
    for(p=2; p<=n; p*=2) {
        for(unsigned low=0; low+p-1<n; low+=p) {
            const unsigned high = low+p-1;
            const unsigned mid = (low+high)/2;

            merge(array, low, mid, high);

            if( n-high == 2 )
                merge(array, low, high, n-1);
        }
    }

    if( p/2 < n )
        merge(array, 0, p/2-1, n-1);
}

template <typename T>
unsigned getIdxOfCurrMin( T& minVal, unsigned currIdcs[], const unsigned n, const unsigned maxLen, T array[] )
{
    unsigned whichArrToIncr = 0;
    const unsigned currIdx = currIdcs[n-1];

    if( array[currIdx < minVal] ) {
        minVal = array[currIdx];
        whichArrToIncr = currIdx;
    }

    return whichArrToIncr;
}

template <typename T, typename ... Ts>
unsigned getIdxOfCurrMin( T& minVal, unsigned currIdcs[], const unsigned n, const unsigned maxLen, T array[], Ts... arrays[]  )
{
    unsigned whichArrToIncr = 0;

    constexpr unsigned nLeft = sizeof...(Ts);
    const unsigned currArr = n-nLeft-1;
    const unsigned currIdx = currIdcs[currArr];

    if( currIdx < maxLen && array[currIdx] < minVal ) {
        minVal = array[currIdx];
        whichArrToIncr = currIdx;
    }

    const unsigned nextStepIdx = getIdxOfCurrMin(minVal, currIdcs, n, maxLen, arrays...);
    if( nextStepIdx != 0 )
        whichArrToIncr = nextStepIdx;

    return whichArrToIncr;
}

template <typename ... Ts>
void nWayMerge( Ts... arrays[], const unsigned ns[] )
{
    constexpr unsigned n = sizeof...(Ts);
    unsigned currIters[n] = {0};

    const unsigned size = ns[0];
    for(unsigned i=0; i<n; ++i)
        if( ns[i] != size ) {
            std::cerr << "Arrays of different lengths passed to nWayMerge. Aborting...\n";
            return;
        }

    while( true ) {
        bool stop = true;
        for(unsigned i=0; i<n; ++i)
            if( currIters[i] < size )
                stop = false;

        if( stop ) break;
    }
}

/****************************** A class to check the mergeSort "<" constraint ********************************/

class wrappedInt
{
    int content;

public:
    wrappedInt() = default;
    wrappedInt( int arg ) : content(arg) {}
    bool operator<( const wrappedInt& rh ) { return this->content < rh.content; }

    friend std::ostream& operator<<( std::ostream& str, const wrappedInt& wi ) { return str << wi.content; }
};

/******************************************* The driver function *********************************************/

int main()
{
    // Check of void_t
    void_t<int,double>* voidPtr1 = nullptr;

    // Check of enable_if
    enable_if<true>::type* voidPtr2 = nullptr;
    enable_if<true,unsigned>::type unsignedObj = 1u;
    enable_if_t<true,int> intObj = 1;

    // Array of ints
    int iArray[] = { 25, -12, 17, 58, -26, 74, 43, -9, 61, 30, 5 };
    const unsigned iSize = sizeof(iArray) / sizeof(int);
    printArray(iArray,iSize,std::cout);

    // Sorting via mergeSort
    iMergeSort(iArray,iSize);
    printArray(iArray,iSize,std::cout);

    // Array of wrappedInts
    wrappedInt wiArray[] = { 3, 2, 1 };
    const unsigned wiSize = sizeof(wiArray) / sizeof(wrappedInt);
    rMergeSort(wiArray, wiSize);
    printArray(wiArray,wiSize,std::cout);

    std::cout << "Hit any button to finish: " << std::getchar();

    return 0;
}
