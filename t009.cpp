#include <iostream>
#include <cstring>

using namespace std;

///////////////////////////////////////////////////////////////////////////////////////

class kalibracja
{
    float a, b;
    char  nazwa[80];

public:
    kalibracja( float, float, char* );
    kalibracja( kalibracja& );

    float energia( int kanal )
    {
        return( (a * kanal) + b );
    }

    char* opis()
    {
        return nazwa;
    }
};

///////////////////////////////////////////////////////////////////////////////////////

kalibracja::kalibracja( float wsp_a, float wsp_b, char* txt ) : a(wsp_a), b(wsp_b)
{
    strcpy(nazwa,txt);
}

///////////////////////////////////////////////////////////////////////////////////////

kalibracja::kalibracja( kalibracja& wzorzec )
{
    a = wzorzec.a;
    b = wzorzec.b;

    strcpy(nazwa,"-- To ja, konstruktor kopiujacy!!! --");
}

//-------------------------------------------------------------------------------------

void       fun_pierwsza( kalibracja );
kalibracja fun_druga();

/*************************************************************************************/

int main()
{
    kalibracja kobalt(1.07,2.4,(char*)"ORYGINALNA KOBALTOWA");

    kalibracja europ(kobalt);

    cout << "O ktory kanal widma chodzi? : ";

    int kanal;
    cin >> kanal;

    cout << "\nWedlug kalibracji kobalt, \nopisanej jako " << kobalt.opis()
         << "\nkanalowi nr " << kanal << " odpowiada energia "
         << kobalt.energia(kanal) << endl;

    cout << "\nWedlug kalibracji europ, \nopisanej jako " << europ.opis()
         << "\nkanalowi nr " << kanal << " odpowiada energia "
         << europ.energia(kanal) << endl;

    cout << endl << "\nDo funkcji pierwszej wysylam kalibracje " << kobalt.opis() << endl;

    fun_pierwsza(kobalt);

    cout << "\nTeraz wywolam funkcje druga, a jej rezultat\n"
            "podstawie do innej kalibracji.\n";

    cout << "Obiekt chwilowy zwrocony jako rezultat funkcji \nma opis "
         << fun_druga().opis() << endl;
}

/**************************************************************************************/

void fun_pierwsza( kalibracja odebrana )
{
    cout << "Natomiast w funkcji pierwszej odebralem te kalibracje\n"
            "\topisana jako: " << odebrana.opis() << endl;
}

/**************************************************************************************/

kalibracja fun_druga()
{
    kalibracja wewn(2,1,(char*)"WEWNETRZNA");

    cout << "W funkcji fun_druga definiuje kalibracje i ma \nona opis: "
         << wewn.opis() << endl;

    return wewn;
}
