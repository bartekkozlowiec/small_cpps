#include <cstdio>
#include <chrono>

struct Parent
{
    virtual void virtMethod( double ) = 0;
    virtual ~Parent() { printf("Destroying parent...\n"); }
};

struct Child1 : public Parent
{
    void overLoaded( double param ) { printf("Calling overLoaded from child no. 1 with param %f...\n",param); }
    void virtMethod( double param ) override { printf("Calling virtMethod from child no. 1 with param %f...\n",param); }
    ~Child1() override { printf("Destroying child no. 1...\n"); }
};

struct Child2 : public Parent
{
    void overLoaded( double param ) { printf("Calling overLoaded from child no. 2 with param %f...\n",param); }
    void virtMethod( double param ) override { printf("Calling virtMethod from child no. 2 with param %f...\n",param); }
    ~Child2() override { printf("Destroying child no. 2...\n"); }
};

int main()
{
    unsigned numberOfCalls = 1e6;

    Child1 child1;
    Child2 child2;
    Parent* parentPtr1 = &child1, *parentPtr2 = &child2;

    auto t1 = std::chrono::high_resolution_clock::now();
    for(unsigned i=0; i<numberOfCalls; ++i)
    {
        parentPtr1->virtMethod(1.0);
        parentPtr2->virtMethod(1.0);
    }
    auto t2 = std::chrono::high_resolution_clock::now();

    auto duration1 = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();

    auto t3 = std::chrono::high_resolution_clock::now();
    for(unsigned i=0; i<numberOfCalls; ++i)
    {
        dynamic_cast<Child1*>(parentPtr1)->overLoaded(1.0);
        dynamic_cast<Child2*>(parentPtr2)->overLoaded(1.0);
    }
    auto t4 = std::chrono::high_resolution_clock::now();

    auto duration2 = std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count();
    printf("Duration of variant with virtual methods: %lli,\n"
           "Duration of variant with dynamic casting: %lli.\n",duration1,duration2);

    return 0;
}
