#include <iostream>
#include <vector>
#include <tuple>
#include <cstdlib>
#include <math.h> //<cmath>
#include <typeinfo>
#include <cassert>
#include <algorithm>

/******************************* Utilities to limit printing only to streamable types **********************************/

template <typename ... T>
struct void_t_impl
{
    typedef void type;
};

template <typename ... T>
using void_t = typename void_t_impl<T...>::type;

template <typename T>
T&& declval();

/***************************************** Vector printing function template *******************************************/

template <typename T, typename Stream>
void printVector( const std::vector<T>& vec, Stream&& str, void_t<decltype(declval<Stream>() << declval<T>())>* = nullptr )
{
    str << "Results vector's contents: ";
    for(const auto& elem : vec)
        str << elem << " ";
    str << "\n";
}

/************************************** Functions returning collections of results **************************************/

std::vector<int> f1( int a );
std::vector<int> f2( int b, int c );
std::vector<int> f3( int d );

std::vector<int> f1( int a )
{
    std::vector<int> res;

    if( a < 0 )
        for(int i=0; i>a; --i)
            res.push_back(i);
    else
        for(int i=0; i<a; ++i)
            res.push_back(i);

    return res;
}

std::vector<int> f2( int b, int c )
{
    std::vector<int> res;

    if( c == 0 )
        res.push_back(c);
    else
    {
        int quotient = /*std::*/floor(b / c);
        int modulo = b % c;

        for(int i=0; i<std::abs(quotient); ++i)
            res.push_back(c);

        res.push_back(modulo);
    }

    return res;
}

std::vector<int> f3( int d )
{
    std::vector<int> res;
    int sum{ 0 };
    res.push_back(sum);

    if( d > 0 )
        while( sum < d )
            res.push_back(++sum);
    else
        while( sum > d )
            res.push_back(--sum);

    return res;
}

/************************************************* The monad utilities *************************************************/

template <typename T, typename TtoVectorU>
auto operator>>=( const std::vector<T>& tSeq, TtoVectorU f ) -> decltype(f(tSeq.front()))
{
    decltype(f(tSeq.front())) uSeq;

    for(const T& t : tSeq)
    {
        auto ft = f(t);
        std::copy(ft.begin(), ft.end(), std::back_inserter(uSeq));
    }

    return uSeq;
}

// https://stackoverflow.com/questions/11044504/any-solution-to-unpack-a-vector-to-function-arguments-in-c
// https://stackoverflow.com/questions/10766112/c11-i-can-go-from-multiple-args-to-tuple-but-can-i-go-from-tuple-to-multiple
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace detail
{
    template <typename F, typename Tuple, bool Done, int Total, int... N>
    struct call_impl
    {
        static auto call(F f, Tuple&& t)
        {
            return call_impl<F, Tuple, Total == 1 + sizeof...(N), Total, N..., sizeof...(N)>::call(f, std::forward<Tuple>(t));
        }
    };

    template <typename F, typename Tuple, int Total, int... N>
    struct call_impl<F, Tuple, true, Total, N...>
    {
        static auto call(F f, Tuple && t)
        {
            return f(std::get<N>(std::forward<Tuple>(t))...);
        }
    };
}

// user invokes this
template <typename F, typename Tuple>
auto call(F f, Tuple&& t)
{
    typedef typename std::decay<Tuple>::type ttype;
    return detail::call_impl<F, Tuple, 0 == std::tuple_size<ttype>::value, std::tuple_size<ttype>::value>::call(f, std::forward<Tuple>(t));
}

//////////////////////////////////// Utilities to allow iterating over std::tuple ///////////////////////////////////////

template <bool Cond, typename T = void>
struct enable_if
{};

template <typename T>
struct enable_if<true,T>
{
    typedef T type;
};

template <bool Cond, typename T = void>
using enable_if_t = typename enable_if<Cond,T>::type;

template <typename T>
unsigned get_vectors_size( const std::vector<T>& vec )
{
    return vec.size();
}

template <std::size_t I = 0, typename ... Args>
inline typename enable_if<I == sizeof...(Args), std::vector<unsigned> >::type
get_tvec_size( const std::tuple<std::vector<Args>...>& ) // no names for unused args
{
    return std::vector<unsigned>{};
}

template <std::size_t I = 0, typename ... Args>
inline typename enable_if<I < sizeof...(Args), std::vector<unsigned> >::type
get_tvec_size( const std::tuple<std::vector<Args>...>& t )
{
    std::vector<unsigned> sizes, partialSizes;
    sizes.push_back(std::get<I>(t).size());
    partialSizes = get_tvec_size<I+1,Args...>(t);
    sizes.insert(sizes.end(), partialSizes.begin(), partialSizes.end());
    return sizes;
}

template <std::size_t I, typename ... Args>
typename enable_if<I == sizeof...(Args)>::type build_tuple_for_call(const std::tuple<std::vector<Args>...>&,
                                                                    std::tuple<Args...>& )
{
    return; // End recursion
}

template <std::size_t I = 0, typename ... Args>
typename enable_if<I < sizeof...(Args)>::type build_tuple_for_call(const std::tuple<std::vector<Args>...>& vecT,
                                                                   std::tuple<Args...>& argT )
{
    std::get<I>(argT) = std::get<I>(vecT)[0];
    build_tuple_for_call<I+1,Args...>(vecT,argT);
    return;
}

template <std::size_t I = 0, typename R, typename ... Args>
std::vector<R> apply_fun_to_tuple_permutations( std::tuple<std::vector<Args>...> vecT,
                                                std::vector<R> (*f)( Args... args ),
                                                enable_if_t<I == sizeof...(Args)>* = nullptr )
{
    return std::vector<R>{}; // End recursion
}

template <std::size_t I = 0, typename R, typename ... Args>
std::vector<R> apply_fun_to_tuple_permutations( std::tuple<std::vector<Args>...> vecT,
                                                std::vector<R> (*f)( Args... args ),
                                                enable_if_t<I < sizeof...(Args)>* = nullptr )
{
    std::vector<R> partRes, localRes;
    auto& currVec = std::get<I>(vecT);
    std::tuple<Args...> argT;
    //std::cerr << "I = " << I << std::endl;
    //std::cerr << "currVec inside a_f_t_t_p: "; printVector(currVec,std::cerr);

    for(auto it = currVec.begin(); it != currVec.end(); ++it) {
        std::iter_swap(currVec.begin(),it);
        build_tuple_for_call(vecT,argT);
        (I == 0 || it != currVec.begin()) ? localRes = call(f,argT) : localRes = {}; // chyba powtarzamy wywolanie
        //std::cerr << "localRes inside a_f_t_t_p: "; printVector(localRes,std::cerr);
        std::vector<R> recursiveRes = apply_fun_to_tuple_permutations<I+1,R,Args...>(vecT,f);
        //std::cerr << "recursiveRes inside a_f_t_t_p: "; printVector(recursiveRes,std::cerr);
        localRes.insert(localRes.end(), recursiveRes.begin(), recursiveRes.end());
        partRes.insert(partRes.end(), localRes.begin(), localRes.end());
    }

    //std::cerr << "prtRes inside a_f_t_t_p: "; printVector(partRes,std::cerr);
    return partRes;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename R, typename ... Args>
auto make_multiple( std::vector<R> (*f)( Args... args ) )
{
    return [f]( const std::vector<Args>&... inVecs ) -> std::vector<R> {
        auto tupledArgs = std::tuple<std::vector<Args>...>(inVecs...);
        assert(std::tuple_size<decltype(tupledArgs)>::value == sizeof...(inVecs));

        //std::cerr << "Type ID: " << typeid(allResults).name() << "\n";
        //printf("Printing vec inside make_multiple template: %i, %lu, %lu\n",inVecs[0]...,sizeof...(inVecs));
        // if sizeof...(inVecs) > 2 the above only prints inVecs[0]...

        /*if( sizeof...(inVecs) > 1 ){
            for(std::size_t i=0; i<sizeof...(inVecs); ++i) {
                std::vector<R> partRes = f(inVecs[i]...);
                allRes.insert(allRes.end(), partRes.begin(), partRes.end());
            }
            printf("LONGER CASE\n");
        }*/

        // Calculate the number of inVecs... permutations
        unsigned nPerm = 1;
        std::vector<unsigned> tupledArgsSizes = get_tvec_size(tupledArgs);
        for(unsigned i=0; i<sizeof...(inVecs); ++i)
            nPerm *= tupledArgsSizes[i];
        std::cerr << "Available inVecs... permutations: " << nPerm << "\n";

        std::vector<R> allResults = apply_fun_to_tuple_permutations(tupledArgs,f);

        return allResults;// allRes;
    };
}

/*template <typename R, typename Arg>
auto make_multiple( std::vector<R> (*f)( Arg arg ) ) -> std::function<std::vector<R>(const std::vector<Arg>&)>
{
    return [f]( const std::vector<Arg>& inVec ) -> std::vector<R> {
        std::vector<R> allRes;

        for(const auto& arg : inVec) {
            std::vector<R> partRes = f(arg);
            allRes.insert(allRes.end(), partRes.begin(), partRes.end());
        }

        return allRes;
    };
}*/

/************************************************* The driver function *************************************************/

int main()
{
    // Obtaining all possible results of f1, f2, f3 function calls combination

    // No monad, just loops
    std::vector<int> results1;

    std::vector<int> b = f1(9);
    std::vector<int> c = f1(5);

    for(int bElem : b)
    {
        for(int cElem : c)
        {
            std::vector<int> d = f2(bElem,cElem);
            for(int dElem : d)
            {
                auto e = f3(dElem);
                std::copy(e.begin(), e.end(), std::back_inserter(results1));
            }
        }
    }

    std::cout << "No monad solution.\n";
    printVector(results1,std::cout);

    // Monad with the ">>=" operator
    std::vector<int> results2 = f1(9) >>= [](int b){ return
                                f1(5) >>= [b](int c){ return
                                f2(b,c) >>= [](int d){ return
                                f3(d); }; }; };

    std::cout << "Monad with operator \">>=\".\n";
    printVector(results2,std::cout); std::cout << "SIZE OF RES: " << results2.size() << "\n";

    // Monad with the make_multiple function template
    auto multiple_f1 = make_multiple(f1);
    auto multiple_f2 = make_multiple(f2);
    auto multiple_f3 = make_multiple(f3);

    /*auto res11 = multiple_f1({9});
    auto res12 = multiple_f1({5});
    printf("MULTIPLE_F2 BELOW:\n");
    auto res2 = multiple_f2(res11,res12);
    printf("Printing res of multiple_f1: ");
    for(auto& r : res11)
        printf("%i ",r);
    printf("\n");
    printf("Printing res of multiple_f2: size %i ",res2.size());
    for(auto& r : res2)
        printf("%i ",r);
    printf("\n");
    std::vector<int> d;
    for(int bElem : b)
        for(int cElem : c) { std::vector<int> f = f2(bElem,cElem); printf("PARTRES-F: "); printVector(f,std::cout);
            d.insert(d.end(), f.begin(), f.end()); }
    printf("COMPARE-D: size %i ",d.size());
    for(const auto& r : d)
        printf("%i ",r);
    printf("\n");*/
    std::vector<int> results3 = multiple_f3(multiple_f2(multiple_f1({9}),multiple_f1({5})));

    std::cout << "Monad with make_multiple template function.\n";
    printVector(results3,std::cout); std::cout << "SIZE OF RES: " << results3.size() << "\n";

    int* pInt{ new int[3]{1,2,3} };

    return 0;
}
