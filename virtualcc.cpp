#include <cstdio>
#include <vector>

struct Base
{
    Base() : a(-1) { }
    Base(int i) : a (i) {}
    int a;

    virtual Base* copyMe() const {
        return new Base(*this);
    }

    virtual bool equals (Base* other) const { return other->a == this->a; }
};

struct Derived1 : public Base
{
    Derived1(int i) : Base(), b(i) {}

    int b;

    virtual Derived1* copyMe() const override {
        return new Derived1(*this);
    }

    virtual bool equals (Base* other) const override
    {
        if (Derived1* ptr = dynamic_cast<Derived1*>(other); ptr != nullptr)
            return ptr->b == this->b && Base::equals(other);
        return false;
    }
};

struct Derived2 : public Base
{
    Derived2(int i) : Base(), c(i) {}
    int c;

    virtual Derived2* copyMe() const override {
        return new Derived2(*this);
    }

    virtual bool equals (Base* other) const override
    {
        if (Derived2* ptr = dynamic_cast<Derived2*>(other); ptr != nullptr)
            return ptr->c == this->c && Base::equals(other);
        return false;
    }
};

std::vector<Base*> deep_copy(std::vector<Base*> original)
{
    std::vector<Base*> retVec;

    // THE WAY TO ACHIEVE DEEP COPY OF ELEMENTS
    /*for (auto elem : original) {
        retVec.push_back(elem->copyMe());
    }*/

    // THE TEDIOUS WAY TO ACHIEVE DEEP COPY OF ELEMENTS
    for (auto elem : original) {
        if (auto castedElem = dynamic_cast<Derived1*>(elem)) {
            retVec.push_back(new Derived1(*castedElem));
        }
        else if (auto castedElem = dynamic_cast<Derived2*>(elem)) {
            retVec.push_back(new Derived2(*castedElem));
        }
        else {
            retVec.push_back(new Base(elem->a));
        }
    }

    return retVec;
}

int main()
{
    std::vector<Base*> vec;
    vec.push_back(new Base(2));
    vec.push_back(new Derived1(5));
    vec.push_back(new Derived2(7));
    vec[1]->a = 3;

    std::vector<Base*> vec2 = deep_copy(vec);

    if (vec.size() != vec2.size())
    {
        printf("false size\n");
    }
    else
    {
        for(size_t i = 0; i < vec.size(); ++i) {
            if (vec[i] == vec2[i] || !vec[i]->equals(vec2[i]))
                printf("false at %lu", i);
        }
    }

    return 0;
}
