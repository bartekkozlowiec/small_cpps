#include <cstdio>

struct Square
{
    int side{ 0 };

    explicit Square(const int side) : side(side) {}
};

struct Rectangle
{
    virtual int width() const = 0;
    virtual int height() const = 0;

    int area() const { return width() * height(); }
};

struct SquareToRectangleAdapter : Rectangle
{
    SquareToRectangleAdapter(const Square& square) : length{square.side} {}
    int width() const override { return length; }
    int height() const override { return length; }

private:
    const int& length;
};

void printRectangle(const Rectangle& rec)
{
    printf("The rectangle is of size %d x %d and has area of %d.\n", rec.height(), rec.width(), rec.area());
}

int main()
{
    Square sq{4};
    printRectangle(SquareToRectangleAdapter{sq});

    return 0;
}
