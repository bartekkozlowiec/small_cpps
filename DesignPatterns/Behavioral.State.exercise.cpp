#include <iostream>
#include <vector>
#include <string>
#include <cassert>

using namespace std;

class CombinationLock
{
    vector<int> combination;

public:
    string status;

    CombinationLock(const vector<int> &combination) : combination(combination), status{"LOCKED"}
    {}

    void enter_digit(int digit)
    {
        // TODO
        if(status == "OPEN" || status == "ERROR" || status == "LOCKED")
            status.clear();

        if(digit < 0 || digit > 9)
            return;

        status += to_string(digit);

        if(status.size() == combination.size()) {
            bool open = true;
            for(unsigned i=0; i<status.size(); ++i)
                if(static_cast<int>(status[i])-48 != combination[i]) {
                    cout << int(status[i])-48 << " vs " << combination[i] << endl;
                    open = false;
                    break;
                }

            open ? status = "OPEN" : status = "ERROR";
        }
    }
};

int main()
{
    CombinationLock cl({1,2,3});

    assert(cl.status == "LOCKED");
    cl.enter_digit(1);
    assert(cl.status == "1");
    cl.enter_digit(2);
    assert(cl.status == "12");
    cl.enter_digit(3);
    assert(cl.status == "OPEN");

    return 0;
}
