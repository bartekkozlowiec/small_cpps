#include <string>
#include <cstdio>
#include <cctype>
#include <map>

struct ExpressionProcessor
{
    std::map<char,int> variables;
    enum Operation { addition, subtraction };

    int calculate(const std::string& expression)
    {
        // TODO
        int result = 0, currNumber = 0;
        Operation lastOp = Operation::addition;

        for(unsigned i=0; i<expression.size(); ++i)
        {
            if(isdigit(expression[i])) {
                std::string token;
                token.push_back(expression[i]);
                int j = i+1;
                while(j < expression.size() && isdigit(expression[j]))
                {
                    token.push_back(expression[j]);
                    ++j;
                }
                i = j;
                currNumber = std::stoi(token);
            }
            else if(isalpha(expression[i]))
                currNumber = variables[expression[i]];
            else if(expression[i] == '+') {
                lastOp = Operation::addition;
                continue;
            }
            else if(expression[i] == '-') {
                lastOp = Operation::subtraction;
                continue;
            }

            switch(lastOp) {
            case Operation::addition:
                result += currNumber;
                break;
            case Operation::subtraction:
                result -= currNumber;
            }
        }

        return result;
    }
};

int main()
{
    ExpressionProcessor ep;
    printf("1+2 == %d\n", ep.calculate("1+2"));
    printf("%s %d\n", std::to_string(-2).c_str(), -2);

    return 0;
}
