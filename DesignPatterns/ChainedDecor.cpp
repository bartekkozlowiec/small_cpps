#include <string>
#include <vector>
#include <iostream>
using namespace std;

struct Flower
{
    std::vector<Flower*> flowers;
    virtual string str() const = 0;
};

struct Rose : Flower
{
    Rose() { flowers.push_back(this); }

    string str() const override {
        return "A rose";
    }
};

struct BlueFlower : Flower
{
    BlueFlower(Flower& f)
    {
        flowers.insert(flowers.end(), f.flowers.begin(), f.flowers.end());
        flowers.push_back(this);
    }

    string str() const override {
        bool alreadyBlue = false;
        string output = flowers.rbegin()[1]->str();

        for(int i=0; i<flowers.size()-1; ++i)
            if(dynamic_cast<BlueFlower*>(flowers[i])) alreadyBlue = true;

        if(dynamic_cast<Rose*>(flowers.rbegin()[1]))
            output += " that is blue";
        else if(!alreadyBlue)
            output += " and blue";

        return output;
    }
};

struct RedFlower : Flower
{
    RedFlower(Flower& f)
    {
        flowers.insert(flowers.end(), f.flowers.begin(), f.flowers.end());
        flowers.push_back(this);
    }

    string str() const override {
        bool alreadyRed = false;
        string output = flowers.rbegin()[1]->str();

        for(int i=0; i<flowers.size()-1; ++i)
            if(dynamic_cast<RedFlower*>(flowers[i])) alreadyRed = true;

        if(dynamic_cast<Rose*>(flowers.rbegin()[1]))
            output += " that is red";
        else if(!alreadyRed)
            output += " and red";

        return output;
    }
};

int main()
{
    Rose r;
    RedFlower rf{r};
    BlueFlower brf{rf};
    RedFlower rbrf{brf};

    std::cout << r.str() << "\n";
    std::cout << rf.str() << "\n";
    std::cout << brf.str() << "\n";
    std::cout << rbrf.str() << "\n";

    return 0;
}
