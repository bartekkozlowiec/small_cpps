#include "Foo.h"

class Foo::impl
{
public:
    void bar(Foo* foo);
};

void Foo::bar()
{
    printf("Launching the outer class implementation function...\n");
    impl->bar(this);
}

void Foo::impl::bar(Foo *foo)
{
    printf("The inner class implementation function called.\n");
}


int main()
{
    Foo foo;
    foo.bar();

    return 0;
}
