#pragma once

#include "pimpl.h"

#include <cstdio>

class Foo
{
    class impl;
    pimpl<impl> impl;

public:
    void bar();
};
