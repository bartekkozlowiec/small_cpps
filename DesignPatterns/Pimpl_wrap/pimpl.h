// pimpl.h

#include <memory>

template <typename T>
class pimpl
{
private:
    std::unique_ptr<T> impl;
public:
    pimpl() : impl(new T{}) {}
    ~pimpl() {}

    template <typename ...Args>
    pimpl(Args&&... args) : impl{new T{std::forward<Args...>(args...)}} {}

    T* operator->() { return impl.get(); }
    T& operator*() { return *impl.get(); }
};
