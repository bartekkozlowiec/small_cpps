cmake_minimum_required(VERSION 3.6)
project(ObserverDemo)

set(CMAKE_CXX_STANDARD 17)
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

# see https://cmake.org/cmake/help/v3.5/module/FindBoost.html
find_package(Boost REQUIRED)

if( "${OS_X_VERSION}" STREQUAL "" )
    set(BOOST_ROOT "/usr/local/Cellar/boost/1.75.0_3" CACHE PATH "Path to boost")
    set(BOOST_INCLUDE_DIRS ${BOOST_ROOT}/include)
    include_directories(${Boost_INCLUDE_DIRS})
else()
    link_directories("c:/boost/stage/lib32")
    include_directories(${Boost_INCLUDE_DIR})
endif()

set(SOURCE_FILES main.cpp headers.hpp Observer.hpp Observable.hpp SaferObservable.hpp)
add_executable(ObserverDemo ${SOURCE_FILES})


#file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/../Singleton/capitals.txt
#        DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
