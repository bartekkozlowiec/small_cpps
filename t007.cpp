#include <iostream>
#include <cstring>

using namespace std;

class Tab
{
    int* tab;
    int  size;

public:
    Tab( int* tab, int size ) : tab(new int[size]), size(size)
    {
        memcpy(this->tab,tab,size);
    }

    void printTable()
    {
        for(int i = 0; i < size; i++)
            cout << tab[i] << endl;
    }

    ~Tab()
    {
        delete [] tab;
    }
};

int main()
{
    const int dim1 = 2;
    const int dim2 = 3;

    int tab1[dim1][dim2] = { {1,2,3}, {4,5,6} };

    int* t1 = reinterpret_cast<int*>(tab1);
    for(int i = 0; i < dim1*dim2; i++)
        cout << t1[i] << endl;
    cout << endl;

    Tab  Tab1(t1,dim1*dim2);
    Tab1.printTable();

    return 0;
}
