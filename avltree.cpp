#include <iostream>

/////////////////////////////////////// Utilites to allow printing of only streamable types ///////////////////////////////////////

template <typename T>
struct void_t_impl
{
    typedef void type;
};

template <typename T>
using void_t = typename void_t_impl<T>::type;

template <typename T>
T&& declval();

/////////////////////////////////////////////// Circular queue class using pointers ///////////////////////////////////////////////

template <typename T>
class CircularQueue
{
    struct Node
    {
        Node* next;
        T data;
    } *front, *rear;

public:
    constexpr CircularQueue() : front{nullptr}, rear{nullptr} {}
    CircularQueue( const T[], const unsigned );
    ~CircularQueue();

    unsigned currLength() const;
    bool isEmpty() const { return front == nullptr; }
    T& operator[]( const unsigned );
    bool enqueue( const T& );
    T dequeue();
};

template <typename T>
CircularQueue<T>::CircularQueue( const T arr[], const unsigned n )
{
    Node* prev = nullptr;
    for(unsigned i=0; i<n; ++i) {
        Node* p = new Node{front, arr[i]};
        prev ? prev->next = p : front = p;
        prev = p;
    }
    rear = prev;
    if( rear ) rear->next = front;
}

template <typename T>
unsigned CircularQueue<T>::currLength() const
{
    unsigned length = 0;
    Node* p = front;

    if( !isEmpty() ) {
        ++length;
        while( p != rear ) {
            p = p->next;
            ++length;
        }
    }

    return length;
}

template <typename T>
T& CircularQueue<T>::operator[]( const unsigned idx )
{
    T retVal{};

    if( !isEmpty() && idx < currLength() ) {
        Node* p = front;
        retVal = p->data;
        p = p->next;
    }

    return retVal;
}

template <typename T>
bool CircularQueue<T>::enqueue( const T& elem )
{
    bool enqueued = true;

    try {
        Node* p = new Node{front, elem};
        rear ? rear->next = p : front = p;
        rear = p;
        rear->next = front;
    } catch( std::bad_alloc ) {
        enqueued = false;
    }

    return enqueued;
}

template <typename T>
T CircularQueue<T>::dequeue()
{
    T retVal{};

    if( !isEmpty() ) {
        retVal = front->data;
        Node* p = front;
        if( front->next != front ) {
            front = front->next;
            rear->next = front;
        }
        else
            rear = front = nullptr;
        delete p;
    }

    return retVal;
}

template <typename T>
CircularQueue<T>::~CircularQueue()
{
    while( front != rear ) {
        Node* p = front;
        front = front->next;
        delete p;
    }
    if( front ) delete front;
}

//////////////////////////////////////// Static stack class template using asligned_storage ///////////////////////////////////////

template <typename T, unsigned maxSize>
class StaticStack
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type storage[maxSize];
    unsigned currSize; // instead of top

public:
    constexpr StaticStack() : currSize{0} {}
    StaticStack( const T[], const unsigned );
    ~StaticStack();

    constexpr bool isEmpty() const { return currSize == 0; }
    constexpr bool isFull() const { return currSize >= maxSize; }
    bool push( const T& );
    T pop();
};

template <typename T, unsigned maxSize>
StaticStack<T,maxSize>::StaticStack( const T arr[], const unsigned n )
{
    if( n > maxSize ) {
        std::cerr << "Warning: input array longer than StaticStack's max size. Creating empty stack...\n";
        return;
    }

    for(unsigned i=0; i<n; ++i) {
        new(storage+i) T{arr[i]};
        ++currSize;
    }
}

template <typename T, unsigned maxSize>
StaticStack<T,maxSize>::~StaticStack()
{
    for(unsigned i=0; i<currSize; ++i)
        reinterpret_cast<T*>(storage+i)->~T();
}

template <typename T, unsigned maxSize>
bool StaticStack<T,maxSize>::push( const T& elem )
{
    bool pushed = false;

    if( !isFull() ) {
        new(storage+currSize) T{elem};
        ++currSize;
        pushed = true;
    }

    return pushed;
}

template <typename T, unsigned maxSize>
T StaticStack<T,maxSize>::pop()
{
    T retElem{};

    if( !isEmpty() ) {
        retElem = *reinterpret_cast<T*>(&storage[--currSize]);
        reinterpret_cast<T*>(storage+currSize)->~T();
    }

    return retElem;
}

/////////////////////////////////////////// AVL binary search tree class using pointers ///////////////////////////////////////////

template <typename T>
class AVLtree
{
    struct Node
    {
        Node* leftChild;
        T data;
        Node* rightChild;
        unsigned height;
    }* root;

    void updateHeights();
    void rotate( Node* const );
    void LLrotate( Node* const );
    void LRrotate( Node* const );
    void RLrotate( Node* const );
    void RRrotate( Node* const );
    int balanceFactorOf( Node* const );
    Node* search( const T& val ) const;

public:
    constexpr AVLtree() : root{nullptr} {}
    ~AVLtree();
    CircularQueue<Node*> levelOrderT() const;

    bool insert( const T& );
    bool remove( const T& );
    bool find( const T& val ) const { return search(val) != nullptr; }

    template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
    void display( Str&&, CircularQueue<Node*>(AVLtree::*)() const ) const;
};

template <typename T>
CircularQueue<typename AVLtree<T>::Node*> AVLtree<T>::levelOrderT() const
{
    CircularQueue<Node*> levelQ, auxQ;
    Node* p = root;
    auxQ.enqueue(p);

    while( p && !auxQ.isEmpty() ) {
        p = auxQ.dequeue();
        levelQ.enqueue(p);
        if( p->leftChild ) auxQ.enqueue(p->leftChild);
        if( p->rightChild ) auxQ.enqueue(p->rightChild);
    }

    return levelQ;
}

template <typename T>
void AVLtree<T>::updateHeights()
{
    CircularQueue<Node*> nodes = levelOrderT();
    StaticStack<Node*,50> auxS;

    while( !nodes.isEmpty() )
        auxS.push(nodes.dequeue());

    while( !auxS.isEmpty() ) {
        Node* p = auxS.pop();

        if( !p->leftChild && !p->rightChild ) {
            if( p->height != 1 )
                std::cerr << "Error: leaf node with height not equal to 1!\n";
            continue;
        }

        unsigned hL = 0, hR = 0;
        if( p->leftChild ) hL = p->leftChild->height;
        if( p->rightChild ) hR = p->rightChild->height;

        hL > hR ? p->height = hL+1 : p->height = hR+1;
        rotate(p);
    }
}

template <typename T>
void AVLtree<T>::rotate( typename AVLtree<T>::Node* const p )
{
    if( p ) {
        const int bFparent = balanceFactorOf(p);
        if( bFparent == 2 ) {
            const int bFlChild = balanceFactorOf(p->leftChild);
            if( bFlChild == 1 )
                LLrotate(p);
            else if( bFlChild == -1 )
                LRrotate(p);
            else
                std::cerr << "Conflicting balance factors: parent has " << bFparent << ", left child has " << bFlChild << "\n";
        }
        else if( bFparent == -2 ) {
            const int bFrChild = balanceFactorOf(p->rightChild);
            if( bFrChild == 1 )
                RLrotate(p);
            else if( bFrChild == -1 )
                RRrotate(p);
            else
                std::cerr << "Conflicting balance factors: parent has " << bFparent << ", left child has " << bFrChild << "\n";
        }
        else if( bFparent > 2 || bFparent < -2 )
            std::cerr << "Error: balance factor for this node has |" << bFparent << "| > 2. Rotation too late!\n";
    }
}

template <typename T>
void AVLtree<T>::LRrotate( typename AVLtree<T>::Node* const p )
{
    const T tempData = p->data;
    Node* const tempR = p->rightChild;

    p->data = p->leftChild->rightChild->data;
    p->rightChild = p->leftChild->rightChild;
    p->rightChild->data = tempData;
    Node* const tempRL = p->rightChild->leftChild;
    p->rightChild->leftChild = p->rightChild->rightChild;
    p->rightChild->rightChild = tempR;
    p->leftChild->rightChild = tempRL;

    if( !p->leftChild->leftChild && !p->leftChild->rightChild )
        p->leftChild->height = 1;
    if( !p->rightChild->leftChild && !p->rightChild->rightChild )
        p->rightChild->height = 1;

    updateHeights();
}

template <typename T>
void AVLtree<T>::LLrotate( typename AVLtree<T>::Node* const p )
{
    const T tempData = p->data;
    Node* const tempR = p->rightChild;
    Node* const tempL = p->leftChild;

    p->leftChild = tempL->leftChild;
    p->data = tempL->data;

    p->rightChild = tempL;
    p->rightChild->data = tempData;
    p->rightChild->leftChild = tempL->rightChild;
    p->rightChild->rightChild = tempR;
    if( !p->rightChild->leftChild && !p->rightChild->rightChild )
        p->rightChild->height = 1;

    updateHeights();
}

template <typename T>
void AVLtree<T>::RLrotate( typename AVLtree<T>::Node* const p )
{
    const T tempData = p->data;
    Node* const tempL = p->leftChild;

    p->data = p->rightChild->leftChild->data;
    p->leftChild = p->rightChild->leftChild;
    p->leftChild->data = tempData;
    Node* const tempLR = p->leftChild->rightChild;
    p->leftChild->rightChild = p->leftChild->leftChild;
    p->leftChild->leftChild = tempL;
    p->rightChild->leftChild = tempLR;

    if( !p->leftChild->leftChild && !p->leftChild->rightChild )
        p->leftChild->height = 1;
    if( !p->rightChild->leftChild && !p->rightChild->rightChild )
        p->rightChild->height = 1;

    updateHeights();
}

template <typename T>
void AVLtree<T>::RRrotate( typename AVLtree<T>::Node* const p )
{
    const T tempData = p->data;
    Node* const tempR = p->rightChild;
    Node* const tempL = p->leftChild;

    p->rightChild = tempR->rightChild;
    p->data = tempR->data;

    p->leftChild = tempR;
    p->leftChild->data = tempData;
    p->leftChild->rightChild = tempR->leftChild;
    p->leftChild->leftChild = tempL;
    if( !p->leftChild->leftChild && !p->leftChild->rightChild )
        p->leftChild->height = 1;

    updateHeights();
}

template <typename T>
int AVLtree<T>::balanceFactorOf( typename AVLtree<T>::Node* const p )
{
    int bF = 0;

    if( p ) {
        unsigned hL = 0, hR = 0;
        if( p->leftChild ) hL = p->leftChild->height;
        if( p->rightChild ) hR = p->rightChild->height;
        bF = static_cast<int>(hL) - static_cast<int>(hR);
    }

    return bF;
}

template <typename T>
typename AVLtree<T>::Node* AVLtree<T>::search( const T& value ) const
{
    Node* p = root;
    while( p && p->data != value )
        value < p->data ? p = p->leftChild : p = p->rightChild;

    return p;
}

template <typename T>
bool AVLtree<T>::insert( const T& value )
{
    bool inserted = true;
    CircularQueue<Node*> elems = levelOrderT();

    for(unsigned i=0; i<elems.currLength(); ++i)
        if( elems[i]->data == value ) {
            inserted = false;
            break;
        }

    if( inserted ) {
        Node *p = root, *prev = nullptr;
        while( p ) {
            prev = p;
            value < p->data ? p = p->leftChild : p = p->rightChild;
        }

        if( prev ) {
            if( value < prev->data )
                prev->leftChild = new Node{nullptr, value, nullptr, 1};
            else
                prev->rightChild = new Node{nullptr, value, nullptr, 1};
        }
        else
            root = new Node{nullptr, value, nullptr, 1};

        updateHeights();
    }

    return inserted;
}

template <typename T>
bool AVLtree<T>::remove( const T& value )
{
    bool removed = false;

    Node *p = root, *prev = nullptr;
    while( p && p->data != value ) {
        prev = p;
        value < p->data ? p = p->leftChild : p = p->rightChild;
    }

    if( p ) {
        if( !p->leftChild && !p->rightChild ) {
            delete p;
            if( prev )
                value < prev->data ? prev->leftChild = nullptr : prev->rightChild = nullptr;
            else
                root = nullptr;
        }
        else {
            Node *ioPredecessor = p->leftChild, *ioPprev = p;
            Node *ioSuccessor = p->rightChild, *ioSprev = p;

            while( ioPredecessor->rightChild ) {
                ioPprev = ioPredecessor;
                ioPredecessor = ioPredecessor->rightChild;
            }
            while( ioSuccessor->leftChild ) {
                ioSprev = ioSuccessor;
                ioSuccessor = ioSuccessor->leftChild;
            }

            if( !ioPredecessor->leftChild ) {
                p->data = ioPredecessor->data;
                ioPprev == p ? p->leftChild = nullptr : ioPprev->rightChild = nullptr;
                rotate(ioPprev);
                delete ioPredecessor;
            }
            else {
                p->data = ioSuccessor->data;

                if( !ioSuccessor->rightChild )
                    ioSprev == p ? p->rightChild = nullptr : ioSprev->leftChild = nullptr;
                else
                    ioSprev->leftChild = ioSuccessor->rightChild;

                rotate(ioSprev);
                delete ioSuccessor;
            }
        }
        removed = true;
    }

    return removed;
}

template <typename T>
template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>* >
void AVLtree<T>::display( Str&& str, CircularQueue<Node*>(AVLtree<T>::* orderFun)() const ) const
{
    CircularQueue<Node*> elems = (this->*orderFun)();
    str << "AVL tree contents: ";
    while( !elems.isEmpty() ) { Node* p = elems.dequeue();
        str << p->data << "->" << p->height << " "; }
    str << "\n";
}

template <typename T>
AVLtree<T>::~AVLtree()
{
    CircularQueue<Node*> toBeDeleted = levelOrderT();
    while( !toBeDeleted.isEmpty() )
        delete toBeDeleted.dequeue();
}

//////////////////////////////////////////////////// The main driver function /////////////////////////////////////////////////////

int main()
{
    AVLtree<unsigned> tree;
    tree.insert(80);
    tree.insert(70);
    tree.insert(60);
    tree.insert(50);
    tree.insert(40);
    tree.insert(30);
    tree.insert(20);
    std::cout << "20 INSERTED\n";
    tree.display(std::cout, &AVLtree<unsigned>::levelOrderT);
    std::cout << "Is " << 70 << " present? " << std::boolalpha << tree.find(70) << "\n";

    tree.remove(50) ? std::cout << "REMOVED 50!\n" : std::cout << "50 NOT REMOVED.\n";
    tree.display(std::cout, &AVLtree<unsigned>::levelOrderT);

    return 0;
}
