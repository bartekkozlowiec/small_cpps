#include <iostream>
#include <type_traits>
#include <typeinfo>
#include <tuple>


/********************************************* Helper templates *********************************************/

template <typename ...>
struct void_t_impl
{
    typedef void type;
};

template <typename ... T>
using void_t = typename void_t_impl<T...>::type;

template <typename T, typename S, typename = void>
struct is_ostreamable : std::false_type {};

template <typename T, typename S>
struct is_ostreamable<T,S,void_t<decltype(std::declval<S&>() << std::declval<T>())> > : std::true_type {};

/************************************************************************************************************/

template <typename T>
T** allocate2dArray( unsigned m, unsigned n )
{
    T* allElemsArr = new T[m*n];
    T** rowBeginArr = new T*[m];

    for(unsigned i=0; i<m; ++i)
        rowBeginArr[i] = allElemsArr + i*n;

    return rowBeginArr;
}

template <typename T>
T*** allocate3dArray( unsigned l, unsigned m, unsigned n )
{
    T* allElemsArr = new T[l*m*n];
    T** rowBeginArr = new T*[l*m];
    T*** planeBeginArr = new T**[l];

    for(unsigned i=0; i<l; ++i)
    {
        planeBeginArr[i] = rowBeginArr + i*m;
        for(unsigned j=0; j<m; ++j)
            rowBeginArr[i*m+j] = allElemsArr + (i*m+j) * n;
    }

    return planeBeginArr;
}

// The function template below is eligible only since C++17
/*template <typename T>
std::ostream& operator<<( std::ostream& os, std::tuple<T**, unsigned, unsigned> array )
{
    if constexpr( !std::is_class<T>::value || is_ostreamable<T,std::ostream>::value )
    {
        os << "PRINTING ARRAY:\n";
        for(unsigned i=0; i<std::get<1>(array); ++i)
        {
            for(unsigned j=0; j<std::get<2>(array); j++)
                os << std::get<0>(array)[i][j] << "\t";
            os << "\n";
        }
    }
    else
        std::cerr << "\nPrinting to std::ostream of an array of type without overloaded << operator aborted.\n";

    return os;
}*/

template <typename T,
          typename std::enable_if<( !std::is_class<T>::value || is_ostreamable<T,std::ostream>::value )>::type* = nullptr>
std::ostream& operator<<( std::ostream& os, std::tuple<T**, unsigned, unsigned> array )
{
    os << "PRINTING ARRAY:\n";
    for(unsigned i=0; i<std::get<1>(array); ++i)
    {
        for(unsigned j=0; j<std::get<2>(array); j++)
            os << std::get<0>(array)[i][j] << "\t";
        os << "\n";
    }

    return os;
}

template <typename T,
          typename std::enable_if<( std::is_class<T>::value && !is_ostreamable<T,std::ostream>::value )>::type* = nullptr>
void operator<<( std::ostream& os, std::tuple<T**, unsigned, unsigned> array )
{
    std::cerr << "\nPrinting to std::ostream of an array of type without overloaded << operator aborted.\n";
}

template <typename T>
void assignSame( T** array, unsigned m, unsigned n, T value )
{
    for(unsigned i=0; i<m; ++i)
        for(unsigned j=0; j<n; ++j)
            array[i][j] = value;
}

template <typename T>
void deallocate2dArray( T** array )
{
    delete [] *(array);
    delete [] array;
}

template <typename T>
void deallocate3dArray( T*** array )
{
    delete [] **(array);
    delete [] *(array);
    delete [] array;
}

class DumbClass {};
struct ChattyClass {};

std::ostream& operator<<( std::ostream& os, const ChattyClass& )
{
    os << "CHATTY-CLASS!";
    return os;
}

template <typename T, unsigned u = 100>
void Foo( T input )
{
    T arr[u];
    std::cout << "We have " << u << " objects in the array. The input arg is: " << input << "\n";
}

int main()
{
    constexpr unsigned m = 3, n = 4;
    double** double2dArr = allocate2dArray<double>(m,n);

    assignSame(double2dArr,m,n,11.0);
    std::cout << std::make_tuple(double2dArr,m,n);

    DumbClass** dumb2dArr = allocate2dArray<DumbClass>(n,m);
    ChattyClass** chatty2dArr = allocate2dArray<ChattyClass>(n,m);

    std::cout << std::make_tuple(dumb2dArr,n,m);
    std::cout << std::make_tuple(chatty2dArr,n,m);

    deallocate2dArray(double2dArr);

    constexpr unsigned l = 2;
    float*** float3dArr = allocate3dArray<float>(l,m,n);
    deallocate3dArray(float3dArr);

    Foo<unsigned,4>(2);
    Foo(3);
    return 0;
}
