#include <iostream>

using namespace std;

class AA
{
public:
    virtual int  geti()  const = 0;
    virtual int* getpi() const = 0;
};

class BB : public AA
{
    int  b;
    int* pb;

public:
    BB( int in = 0 ) : b(in), pb(&b) { }

    int geti() const
    {
        return *pb;
    }

    int* getpi() const
    {
        return pb;
    }
};

class CC : public BB
{
    int  c;
    int* pc;

public:
    CC( int in = 0 ) : c(in), pc(&c), BB() { }

    int geti() const
    {
        return *pc;
    }

    int* getpi() const
    {
        return pc;
    }
};

int main()
{
    BB  obj1(1);
    CC  obj2(2);
    AA  *pobj1, *pobj2;
    pobj1 = &obj1;
    pobj2 = &obj2;

    cout << "obj1.b       = " << obj1.geti()   << endl;
    cout << "pobj1->b (?) = " << pobj1->geti() << endl;
    cout << "obj2.b       = " << obj2.geti()   << endl; // nie, wywola sie metoda z klasy CC
    cout << "pobj2->b (?) = " << ((BB*)pobj2)->geti() << endl; // rzutowanie na BB nic tu nie da, jw.
    cout << "obj2.c       = " << obj2.geti()   << endl;
    cout << "pobj2->c (?) = " << pobj2->geti() << endl;

    return 0;
}
