#include <iostream>

//////////////////////////////////////// Utilities to allow printing of only streamable types ////////////////////////////////////////

template <typename ... T>
struct void_t_impl
{
    typedef void type;
};

template <typename ... T>
using void_t = typename void_t_impl<T...>::type;

template <bool Cond, typename T = void>
struct enable_if {};

template <typename T>
struct enable_if<true,T>
{
    typedef T type;
};

template <bool Cond, typename T = void>
using enable_if_t = typename enable_if<Cond,T>::type;

template <typename T>
T&& declval();

template <typename ... T>
struct is_integer
{
    static constexpr bool value = false;
};

template <>
struct is_integer<int>
{
    static constexpr bool value = true;
};

template <>
struct is_integer<unsigned>
{
    static constexpr bool value = true;
};

/////////////////////////////////////////// Template function for printing array contents ////////////////////////////////////////////

template <typename T, typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
void printArray( const T array[], const unsigned n, Str&& ostr )
{
    ostr << "Array contents: ";
    for(unsigned i=0; i<n; ++i)
        ostr << array[i] << " ";
    ostr << "\n";
}

//////////////////////////////////////////// Circular queue class template using pointers ////////////////////////////////////////////

template <typename T>
class CircularQueue;

template <typename T>
std::ostream& operator<<( std::ostream&, const CircularQueue<T>& );

template <typename T>
class CircularQueue
{
    struct Node
    {
        Node* next;
        T data;
    };
    Node *front, *rear;

public:
    constexpr CircularQueue() : front{nullptr}, rear{nullptr} {}
    CircularQueue( const T arr[], const unsigned n ) : front{nullptr}, rear{nullptr} { for(unsigned i=0; i<n; ++i) enqueue(arr[i]); }
    ~CircularQueue() { while( !isEmpty() ) dequeue(); }

    constexpr bool isEmpty() const { return front == nullptr; }
    bool enqueue( const T& );
    T dequeue();

    T operator[]( unsigned );
    friend std::ostream& operator<< <>( std::ostream&, const CircularQueue& );
};

template <typename T>
bool CircularQueue<T>::enqueue( const T& value )
{
    Node* p = new Node{nullptr,value};
    bool enqueued = true;

    if( p ) {
        if( isEmpty() )
            rear = front = p;
        else {
            rear->next = p;
            rear = p;
        }
        rear->next = front;
    }
    else enqueued = false;

    return enqueued;
}

template <typename T>
T CircularQueue<T>::dequeue()
{
    T dequeuedVal{};

    if( !isEmpty() ) {
        dequeuedVal = front->data;
        Node* p = front;
        if( front == rear )
            front = nullptr;
        else {
            front = front->next;
            rear->next = front;
        }
        delete p;
    }

    return dequeuedVal;
}

template <typename T>
T CircularQueue<T>::operator[]( unsigned idx )
{
    T retVal = {};

    if( !isEmpty() ) {
        Node* p = front;
        for(unsigned i=0; i<idx; ++i)
            p = p->next;
        retVal = p->data;
    }

    return retVal;
}

template <typename T>
std::ostream& operator<<( std::ostream& ostr, const CircularQueue<T>& queue )
{
    ostr << "Queue contents: ";

    typename CircularQueue<T>::Node* p = queue.front;
    while( p != queue.rear ) {
        ostr << p->data << " ";
        p = p->next;
    }

    if( queue.rear ) ostr << queue.rear->data;
    ostr << "\n";

    return ostr;
}

///////////////////////////////////////////////////////// Sorting algorithms /////////////////////////////////////////////////////////

template <typename T>
T findMin( const T array[], const unsigned n, void_t<decltype(declval<T>() < declval<T>())>* = nullptr )
{
    T minElem = array[0];
    for(unsigned i=1; i<n; ++i)
        if( array[i] < minElem )
            minElem = array[i];

    return minElem;
}

template <typename T>
T findMax( const T array[], const unsigned n, void_t<decltype(declval<T>() > declval<T>())>* = nullptr )
{
    T maxElem = array[0];
    for(unsigned i=1; i<n; ++i)
        if( array[i] > maxElem )
            maxElem = array[i];

    return maxElem;
}

template <typename T, enable_if_t<is_integer<T>::value>* = nullptr>
void bucketSort( T array[], const unsigned n )
{
    T minElem = findMin(array, n);
    T maxElem = findMax(array, n);
    unsigned size = static_cast<unsigned>(maxElem) - static_cast<unsigned>(minElem) + 1;

    CircularQueue<T>* buckets = new CircularQueue<T>[size];

    for(unsigned i=0; i<n; ++i)
        buckets[array[i]-static_cast<unsigned>(minElem)].enqueue(array[i]);

    unsigned i=0, j=0;
    while( j<size ) {
        while( !buckets[j].isEmpty() )
            array[i++] = buckets[j].dequeue();
        ++j;
    }
}

template <typename T, enable_if_t<is_integer<T>::value>* = nullptr>
void radixSort( T array[], const unsigned n )
{
    unsigned divider=1, notEmptyBuckets;
    constexpr unsigned bucketsNum = 10;
    CircularQueue<T> buckets[bucketsNum];

    do {
        for(unsigned i=0; i<n; ++i) {
            const unsigned mod = (array[i]/divider) % 10;
            buckets[mod].enqueue(array[i]);
        }
        divider *= 10;

        notEmptyBuckets = 0;
        unsigned i=0;
        for(unsigned j=0; j<bucketsNum; ++j) {
            if( !buckets[j].isEmpty() ) {
                ++notEmptyBuckets;
                while( !buckets[j].isEmpty() )
                    array[i++] = buckets[j].dequeue();
            }
        }
    }
    while( notEmptyBuckets > 1 );
}

// https://stackoverflow.com/questions/29059343/why-cant-a-weak-ptr-be-constructed-from-a-unique-ptr
////////////////////////////////////////////////////// The main driver function //////////////////////////////////////////////////////

auto main() -> int
{
    int iArray[] = { 27, -9, 34, 58, -13, 62, 16, 45, 70, 1 };
    unsigned iSize = sizeof(iArray)/sizeof(int);

    printArray(iArray, iSize, std::cout);

    bucketSort(iArray, iSize);
    printArray(iArray, iSize, std::cout);

    unsigned uArray[] = { 451, 289, 73, 813, 33, 560, 328, 101, 632, 715 };
    unsigned uSize = sizeof(uArray)/sizeof(unsigned);

    printArray(uArray, uSize, std::cout);

    radixSort(uArray, uSize);
    printArray(uArray, uSize, std::cout);

    float fArray[] = { 2.8f, -7.4f, 15.3f };
    unsigned fSize = sizeof(fArray)/sizeof(float);
    CircularQueue<float> cq{fArray, fSize};

    std::cout << cq;

    return 0;
}
