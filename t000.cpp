#include <iostream>
#include <cstring>

using namespace std;

//////////////////////////////////// Definicje i deklaracje klas //////////////////////////////////////////////////////////////////

class countingPtr;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class countingPtd
{
    unsigned int counter;
    bool         createdWithNew;

    friend class countingPtr;

public:
    countingPtd() : counter(0), createdWithNew(true) { }
    countingPtd* operator&();
    virtual ~countingPtd() { cout << "Dziala destruktor klasy countingPtd." << endl; }
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Object : public countingPtd
{
    char* letters;

public:
    Object( const char* napis = "Napis domyslny." ) : letters(new char[strlen(napis)+1]), countingPtd() { strcpy(letters,napis); }
    Object( const Object& obj ) : letters(new char[strlen(obj.letters)+1]), countingPtd() { strcpy(letters,obj.letters); }
    Object& operator=( const Object& );
    ~Object();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class countingPtr
{
    countingPtd* address;

public:
    countingPtd* operator->() { return address; }
    countingPtr() : address(NULL) { }
    countingPtr( countingPtd* );
    void operator=( countingPtd* );
    ~countingPtr();
};

///////////////////////////////////////////////// Definicje funkcji skladowych ////////////////////////////////////////////////////

countingPtd* countingPtd::operator&()
{
    createdWithNew = false;
    return this;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

countingPtr::countingPtr( countingPtd* address )
{
    cout << "Dziala konstruktor klasy countingPtr." << endl;

    this->address = address;
    (address->counter)++;

    cout << "Licznik uzyc wskazywanego obiektu: " << (*address).counter << endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void countingPtr::operator=( countingPtd* address )
{
    cout << "Dziala operator przypisania klasy countingPtr." << endl;

    this->address = address;
    if( address->createdWithNew )
        (*address).counter++;

    cout << "Licznik uzyc wskazywanego obiektu: " << (*address).counter << endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

countingPtr::~countingPtr()
{
    this->address->counter--;

    if(this->address->counter == 0)
        delete this->address;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Object& Object::operator=( const Object& obj )
{
    if( this->letters )
    {
        delete [] letters;
        letters = new char[strlen(obj.letters)+1];
        strcpy(letters,obj.letters);
    }
    return (*this);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Object::~Object()
{
    cout << "Dziala destruktor klasy Object." << endl;
    delete [] letters;
    //~countingPtd();
}

/****************************************************** Funkcje globalne *********************************************************/

int main()
{
    cout.setf(ios::unitbuf);

    Object      obj1;
    countingPtd ctd1;

    countingPtr objptr1 = new Object();
    countingPtr ctdptr1 = new countingPtd();

    countingPtr objptr2, ctdptr2;
    objptr2 = &obj1;
    ctdptr2 = &ctd1; // ZLE, bo niszczony jest obiekt utworzony na stosie | edit: po dodaniu skladnika 'createdWithNew' mozna uzywac!
    //objptr2 = new Object();
    //ctdptr2 = new countingPtd(); // OK!

    return 0;
}
