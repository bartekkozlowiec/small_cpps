#include <iostream>
#include <typeinfo>

using namespace std;

template <typename typ>
unsigned funkcja( typ& Tr, typ* Tp, int i )
{
    cout << "Dziala funkcja szablonowa o typach " << typeid(Tp).name() << ", "
         << typeid(Tr).name() << " i numerze wywolania " << i << "." << endl;
    return i+1;
}

void funkcja( string& s, string* stab, int size )
{
    cout << "Dziala funkcja specjalizowana. Mowi, ze: " << endl;

    for(int count = 0; count < size; count++)
        cout << "wiersz " << count << " ma zawartosc: " << stab[count] << endl;
}

int main()
{
    cout << "Programik do porownywania argumentow funkcji szablonowych i specjalizowanych\n\n";
    cout << "========= Wywolania funkcji specjalizowanej ==========\n\n";

    string tytSceny("Dialog Poety i Panny Mlodej.");
    string wiersze[] = { "- Po calym swiecie", "mozesz szukac Polski, panno mloda",
                       "i nigdzie jej nie znajdziecie.", "- To moze i szukac szkoda.",
                       "- A jest jedna mala klatka -", "o, niech tak Jagusia przymknie",
                       "reke pod piers.", "- To zakladka", "gorseta, zeszyta troche przyciasnie.",
                       "- A tam puka?", "- I coz to za nauka?", "Serce!?",
                       "- A to Polska wlasnie."};

    int liczWiersze = sizeof(wiersze)/sizeof(wiersze[0]);

    funkcja(tytSceny,wiersze,liczWiersze);

    cout << "\n========== Wywolania funkcji szablonowych ==========\n\n";

    int tabint[2];
    int i;
    char tabchar[3];
    char c;
    float tabfloat[2];
    float f;

    unsigned licznikF = 0;
    licznikF = funkcja(i,tabint,licznikF);
    licznikF = funkcja(c,tabchar,licznikF);
    licznikF = funkcja(f,tabfloat,licznikF);

    cout << "\nLiczba wywolan funkcji szablonowych: " << licznikF << endl;

    return 0;
}
