// This program serves to demonstrate the usage of libgit2 library's basic comments
// to reset changes made to a file inside a git repository

// To compile on MacOS:
// brew install libgit2
// clang++ -I/usr/local/include -std=c++17 libgitusage.cpp -L/usr/local/lib -lgit2 -o libgituse

// To compile on Ubuntu:
// sudo apt get install libgit2-dev
// g++ -std=c++17 libgitusage.cpp -lgit2 -o libgituse

#include <cstdio>
#include <cctype>
#include <cassert>
#include <fstream>
#include <algorithm>
#include <filesystem>
#include <string>
#include <tuple>

#include <git2.h>

/*************************************** Some utility templates for type manipulation ***************************************/

template <typename... Ts>
struct void_t_impl
{
    typedef void type;
};

template <typename... Ts>
using void_t = typename void_t_impl<Ts...>::type;

template <typename T>
T&& declval();

/*************** Function template for appending an arbitrary streamable content to a file on specified line ****************/

template <typename T> class FileModifier;

template <typename T, void_t<decltype(declval<std::fstream>() << declval<T>())>* = nullptr>
void appendToFileAtLine(const std::string& fileName, const T& content, const size_t lineNo)
{
    size_t numLines{0};
    std::ifstream ifs{fileName};

    if (ifs) {
        std::string unused;
        while (std::getline(ifs, unused))
            ++numLines;
    }
    else {
        fprintf(stderr, "Failed to open file: %s. Aborting...\n", fileName.c_str());
        return;
    }
    ifs.close();

    FileModifier<T> fm{fileName, lineNo, content};
    if (lineNo > numLines)
        fm.appendToEnd();
    else
        fm.appendAtLine();
}

/****************************** Class template to encapsulate functions for file manipulation ******************************/

template <typename T>
class FileModifier
{
    const std::string fileName;
    const size_t lineNum;
    const T content;

    // This class simply ensures that the following methods are hidden from the outside world
    void appendToEnd();
    void appendAtLine();

    FileModifier(const std::string& fn, size_t ln, const T& ct) : fileName{fn}, lineNum{ln}, content{ct} {}

    friend void appendToFileAtLine<>(const std::string& fileName, const T& content, const size_t lineNo);
};

template <typename T>
void FileModifier<T>::appendToEnd()
{
    std::ofstream ofs;

    ofs.open(fileName, std::ios_base::app);
    if (ofs) ofs << content;
}

std::string getPathFromFileName(const std::string& fN);

template <typename T>
void FileModifier<T>::appendAtLine()
{
    auto tempFile = getPathFromFileName(fileName);
    tempFile += "/temp.txt";
    std::ifstream ifs{fileName};
    std::ofstream ofs{tempFile};

    if (!ifs || !ofs)
        return;

    size_t lineCnt{0};
    std::string line;
    while (std::getline(ifs, line) && lineCnt < lineNum) {
        ofs << line << "\n";
        ++lineCnt;
    }

    ofs << line << content << "\n";

    while (std::getline(ifs, line))
        ofs << line << "\n";

    ifs.close();
    ofs.close();

    if (std::remove(fileName.c_str()) != 0)
        fprintf(stderr, "Error removing file: %s\n", fileName.c_str());
    else printf("File %s removed successfully.\n", fileName.c_str());

    if (std::rename(tempFile.c_str(), fileName.c_str()) != 0)
        fprintf(stderr, "Error renaming file: %s\n", tempFile.c_str());
    else printf("File %s renamed successfully.\n", tempFile.c_str());
}

/************************* Class template to encapsulate handling of git repository using libgit2 **************************/

class RepoHandler
{
    git_repository* repo_;
    git_buf rootBuf_;

    std::string getPureRootPath();

public:
    explicit RepoHandler(const std::string& fileName);
    ~RepoHandler();

    constexpr git_repository* get() const noexcept { return repo_; }
    std::string getRelativeRepoPath(const std::string& fileName);

    RepoHandler(const RepoHandler&) = delete;
    RepoHandler& operator=(const RepoHandler&) = delete;
    RepoHandler(RepoHandler&&) = delete;
    RepoHandler& operator=(RepoHandler&&) = delete;
};

RepoHandler::RepoHandler(const std::string& fileName) : repo_{nullptr}, rootBuf_{nullptr, 0, 0}
{
    git_libgit2_init();

    const auto filePath = getPathFromFileName(fileName);
    int error = git_repository_open_ext(&repo_, filePath.c_str(), 0, nullptr);
    if (error != 0)
        throw std::runtime_error("Failed to open git repository!");

    error = git_repository_discover(&rootBuf_, filePath.c_str(), 0, nullptr);
    if (error != 0)
        throw std::runtime_error("Failed to obtain repository root from the given path!");
}

RepoHandler::~RepoHandler()
{
    git_buf_free(&rootBuf_);
    git_repository_free(repo_);
    git_libgit2_shutdown();
}

std::string RepoHandler::getPureRootPath()
{
    std::string pureRoot;

    if (rootBuf_.ptr) {
        auto idx = rootBuf_.size - 1;
        if (rootBuf_.ptr[idx] == '/' || rootBuf_.ptr[idx] == '\\')
            --idx;

        while (idx > 0) {
            if (*(rootBuf_.ptr + idx) == '/' || *(rootBuf_.ptr + idx) == '\\')
                break;
            --idx;
        }

        pureRoot = std::string(rootBuf_.ptr, idx);
    }

    return pureRoot;
}

std::string RepoHandler::getRelativeRepoPath(const std::string& fileName)
{
    if (!rootBuf_.ptr)
        return {};

    // Make sure both paths are absolute
    const auto filePath = std::filesystem::canonical(fileName).string();
    const auto repoPath = std::filesystem::canonical(rootBuf_.ptr).string();

    size_t idx = 0;
    while (filePath[idx] == repoPath[idx])
        ++idx;

    return filePath.substr(idx);
}

/************************** Additional global utility functions for input parsing, file reset etc. *************************/

std::string getPathFromFileName(const std::string& fileName)
{
    auto lastSlashIdx = fileName.find_last_of('/');
    return fileName.substr(0, lastSlashIdx);
}

bool stringToBool(std::string boolStr)
{
    assert(boolStr.size() > 0 && "Empty string passed in to stringToBool!");
    std::transform(boolStr.begin(), boolStr.end(), boolStr.begin(), ::toupper);

    bool retBool = false;
    if (boolStr.front() == 'T')
        retBool = true;
    else if (boolStr.front() != 'F')
        fprintf(stderr, "Warning: string passed as Boolean doesn't start with T/t nor F/f!\n");

    return retBool;
}

auto parseInputs(const char* argv[])
{
    std::string fileNameStr{argv[1]};
    auto modBool = stringToBool(argv[2]);
    size_t lineNo = std::stoul(argv[3]);
    std::string contentStr{argv[4]};

    return std::make_tuple(fileNameStr, modBool, lineNo, contentStr);
}

void resetFileChanges(const std::string& fileName)
{
    RepoHandler rh{fileName};

    auto relPath = rh.getRelativeRepoPath(fileName);
    char* paths[] = {const_cast<char*>(relPath.c_str())};
    git_checkout_options opts = GIT_CHECKOUT_OPTIONS_INIT;
    opts.paths.strings = paths;
    opts.paths.count = 1;
    opts.checkout_strategy = GIT_CHECKOUT_FORCE;

    int error = git_checkout_head(rh.get(), &opts);

    if (error != 0)
        fprintf(stderr, "Error: unable to do git checkout on file from path: %s", fileName.c_str());
    else
        printf("File %s was successfully reset in the repo.\n", fileName.c_str());
}

/************************************************ The main driver function *************************************************/

int main(const int argc, const char* argv[])
{
    if (argc != 5) {
        fprintf(stderr, "Error: wrong number of input arguments. The following args are necessary:\n"
                "1. path to file to be modified\n"
                "2. T/F or true/false to indicate whether to reset the file at program end using git\n"
                "3. integer line number\n"
                "4. string to be appended\n"
                "Aborting...\n");
        return 1;
    }

    auto inputArgs = parseInputs(argv);

    appendToFileAtLine(std::get<0>(inputArgs), std::get<3>(inputArgs), std::get<2>(inputArgs));

    if (std::get<1>(inputArgs))
        resetFileChanges(std::get<0>(inputArgs));

    return 0;
}
