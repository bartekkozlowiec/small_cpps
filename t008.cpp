#include <iostream>

using namespace std;

void Allocate2DMatrix( int, int );
void Allocate3DMatrix( int, int, int );
void Allocate4DMatrix( int, int, int, int );

int main()
{
    Allocate2DMatrix(2,2);
    Allocate3DMatrix(2,2,2);
    Allocate4DMatrix(2,2,2,2);

    cout << "sizeof(int) = " << sizeof(int) << ", sizeof(int*) = " << sizeof(int*) << ", sizeof(int**) = " << sizeof(int**) << endl;

    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

void Allocate2DMatrix( int dim1, int dim2 )
{
    int** TabWierszy;
    int*  TabElementow;

    TabWierszy    = new int*[dim1];
    TabElementow  = new int [dim1*dim2];

    for(int i = 0; i < dim1; i++)
        TabWierszy[i] = TabElementow + i*dim2;

    for(int i = 0; i < dim1; i++)
        for(int j = 0; j < dim2; j++)
            TabWierszy[i][j] = i+j, cout << TabWierszy[i][j] << " ";
    cout << endl;

    delete [] TabElementow;
    delete [] TabWierszy;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

void Allocate3DMatrix( int dim1, int dim2, int dim3 )
{
    int*** TabPlaszczyzn;
    int**  TabWierszy;
    int*   TabElementow;

    TabPlaszczyzn = new int**[dim1];
    TabWierszy    = new int* [dim1*dim2];
    TabElementow  = new int  [dim1*dim2*dim3];

    for(int i = 0; i < dim1; i++)
    {
        TabPlaszczyzn[i] = TabWierszy + i*dim2;

        for(int j = 0; j < dim2; j++)
            TabWierszy[i*dim2 + j] = TabElementow + i*dim2*dim3 + j*dim3;
    }

    for(int i = 0; i < dim1; i++)
        for(int j = 0; j < dim2; j++)
            for(int k = 0; k < dim3; k++)
                TabPlaszczyzn[i][j][k] = i+j+k, cout << TabPlaszczyzn[i][j][k] << " ";
    cout << endl;

    delete [] TabElementow;
    delete [] TabWierszy;
    delete [] TabPlaszczyzn;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

void Allocate4DMatrix( int dim1, int dim2, int dim3, int dim4 )
{
    int**** TabPrzestrzeni;
    int***  TabPlaszczyzn;
    int**   TabWierszy;
    int*    TabElementow;

    TabPrzestrzeni = new int***[dim1];
    TabPlaszczyzn  = new int** [dim1*dim2];
    TabWierszy     = new int*  [dim1*dim2*dim3];
    TabElementow   = new int   [dim1*dim2*dim3*dim4];

    for(int i = 0; i < dim1; i++)
    {
        TabPrzestrzeni[i] = TabPlaszczyzn + i*dim2;

        for(int j = 0; j < dim2; j++)
        {
            TabPlaszczyzn[i*dim2 + j] = TabWierszy + i*dim2*dim3 + j*dim3;

            for(int k = 0; k < dim3; k++)
            {
                TabWierszy[i*dim2*dim3 + j*dim3 + k] = TabElementow + i*dim2*dim3*dim4 + j*dim2*dim3 + k*dim4;
            }
        }
    }

    for(int i = 0; i < dim1; i++)
        for(int j = 0; j < dim2; j++)
            for(int k = 0; k < dim3; k++)
                for(int l = 0; l < dim4; l++)
                    TabPrzestrzeni[i][j][k][l] = i+j+k+l, cout << TabPrzestrzeni[i][j][k][l] << " ";
    cout << endl;

    delete [] TabElementow;
    delete [] TabWierszy;
    delete [] TabPlaszczyzn;
    delete [] TabPrzestrzeni;
}
