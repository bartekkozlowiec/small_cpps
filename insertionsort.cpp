#include <iostream>

////////////////////////////////// Utilities to allow printing of only printable types' array //////////////////////////////////

template <typename T>
struct void_t_impl
{
    typedef void type;
};

template <typename T>
using void_t = typename void_t_impl<T>::type;

template <typename T>
T&& declval();

template <typename T, typename Stream, void_t<decltype(declval<Stream>() << declval<T>())>* = nullptr>
void printArray( T array[], unsigned n, Stream&& str )
{
    str << "Array contents:\n";
    for(unsigned i=0; i<n; ++i)
        str << array[i] << " ";
    str << "\n";
}

///////////////////////////////////////////////////// Sorting algorithms //////////////////////////////////////////////////////

template <typename T>
void insertionSort( T array[], unsigned n )
{
    for(unsigned i=1; i<n; ++i)
    {
        for(unsigned j=0; j<i; ++j)
        {
            if( array[i] < array[j] )
            {
                T temp = array[i];
                for(unsigned k=i; k-->j;)
                    array[k+1] = array[k];
                array[j] = temp;
            }
        }
    }
}

////////////////////////////////////////////////// The main driver function ///////////////////////////////////////////////////

auto main() -> int
{
    // Defining an array of ints
    int intArray[]{ 26, 3, 37, -15, 58, 44, 71, 19, 62, 80, -9 };
    unsigned intSize = sizeof(intArray) / sizeof(int);
    printArray(intArray,intSize,std::cout);

    // Sorting via insertion sort
    insertionSort(intArray,intSize);
    printArray(intArray,intSize,std::cout);

    return 0;
}
