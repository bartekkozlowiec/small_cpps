#include <cstdio>
#include <utility>

class NonExplConstr
{
    float content_;

public:
    NonExplConstr(float arg) : content_{arg} { printf("NonExplConstr::NonExplConstr(float) called!\n"); }
    operator float() { return content_; }
};

class UsesNonExplConstr
{
    NonExplConstr content_;

public:
    UsesNonExplConstr(NonExplConstr arg) : content_{arg} { printf("UsesNonExplConstr::UsesNonExplConstr(NonExplConstr) called!\n"); }
    NonExplConstr copyContent() const { return content_; }
};


class ExplConstr
{
    double content_;

public:
    explicit ExplConstr(double arg) : content_{arg} { printf("ExplConstr::ExplConstr(double) called!\n"); }
    operator double() { return content_; }
};

class UsesExplConstr
{
    ExplConstr content_;

public:
    UsesExplConstr(ExplConstr arg) : content_{arg} { printf("UsesExplConstr::UsesExplConstr(ExplConstr) called!\n"); }
    ExplConstr copyContent() const { return content_; }
};

class ExplCopyConstr
{
    unsigned content_;

public:
    explicit ExplCopyConstr(unsigned arg) : content_{arg} { printf("ExplCopyConstr::ExplCopyConstr(unsigned) called!\n"); }
    explicit ExplCopyConstr(const ExplCopyConstr& other) : content_{other.content_} {
        printf("ExplCopyConstr::ExplCopyConstr(const ExplCopyConstr&) called!\n"); }
    operator unsigned() { return content_; }
};

class UsesExplCopyConstr
{
    ExplCopyConstr content_;

public:
    UsesExplCopyConstr(ExplCopyConstr arg) : content_{arg} { printf("UsesExplCopyConstr::UsesExplCopyConstr(ExplCopyConstr) called!\n"); }
    ExplCopyConstr copyContent() const { return ExplCopyConstr(content_); } // compiles only in C++17 if ExplCopyConstr's copy-constructor
                                                                            // is defined; comment out the copy constructor (let compiler
                                                                            // generate move constructor) to compile
                                                                            // UseExplCopyConstr::copyContent also in C++11
};

class ExplMoveConstr
{
    unsigned content_;

public:
    explicit ExplMoveConstr(unsigned arg) : content_{arg} { printf("TwoExplConstr::TwoExplConstr(unsigned) called!\n"); }
    explicit ExplMoveConstr(ExplMoveConstr&& other) : content_{other.content_} {
        printf("ExplMoveConstr::ExplMoveConstr(ExplMoveConstr&&) called!\n"); }
    operator unsigned() { return content_; }
};

class UsesExplMoveConstr
{
    ExplMoveConstr content_;

public:
    UsesExplMoveConstr(ExplMoveConstr arg) : content_{std::move(arg)} { printf("UsesExplMoveConstr::UsesExplMoveConstr(ExplMoveConstr) called!\n"); }
    ExplMoveConstr&& moveContent() { return std::move(content_); } // compiles only in C++17 if ExplCopyConstr's copy-constructor
                                                                   // is defined; comment out the copy constructor (let compiler
                                                                   // generate move constructor) to compile
                                                                   // UseExplCopyConstr::copyContent also in C++11
};

int main()
{
    NonExplConstr nec1(2.4f);
    NonExplConstr nec2 = 3.5f;

    ExplConstr ec1(2.3);
    //ExplConstr ec2 = 1.2; // won't compile: no known conversion from double to const ExplConstr& !
    ExplConstr ec2 = ExplConstr(1.2); // implicitly call the copy constructor of ExplConstr to copy from temporary into ec2

    ExplCopyConstr ecc1(1u);
    //ExplCopyConstr ecc2 = 2u; // won't compile: no viable conversion from unsigned int to ExplCopyConstr !
    //ExplCopyConstr ecc2 = ExplCopyConstr(2u); // won't compile: explicit copy constructor is not a candidate for initialization
                                                // of ExplCopyConstr ! BUT: compiles fine in C++17
                                                // will compile only if both ExplCopyConstr copy and move constructors are commented out
    ExplCopyConstr ecc3(ExplCopyConstr(3u)); // compiles fine because move/copy constructor is called explicitly !

    ExplMoveConstr emc1(4u);
    //ExplMoveConstr emc2 = 5u;
    ExplMoveConstr emc2 = ExplMoveConstr(5u); // won't compile: call to implicitly-deleted copy constructor of ExplMoveConstr !
                                                // BUT: compiles fine in C++17
                                                // will compile only if both ExplMoveConstr copy and move constructors are commented out

    UsesNonExplConstr une1(8.7f);

    //UsesExplConstr ue1(2.3); // won't compile: no known conversion from double to UsesExplConstr nor to ExplConstr !
    UsesExplConstr ue1(ExplConstr(2.3));

    //UsesExplCopyConstr uec1(2u); // won't compile: no known conversion from unsigned int to UsesExplCopyConstr nor to ExplCopyConstr !
    UsesExplCopyConstr uec1(ExplCopyConstr(2u)); // won't compile: no matching constructor for initialization of ExplCopyConstr !
                                                 // BUT: compiles fine in C++17

    //UsesExplMoveConstr uem1(3u); // won't compile: no known conversion from unsigned int to UsesExplMoveConstr nor to ExplMoveConstr !
    UsesExplMoveConstr uem1(ExplMoveConstr(3u)); // won't compile: call to implicitly-deleted copy constructor of ExplMoveConstr !
                                                 // BUT: compiles fine in C++17

    auto ec1_from_ue1 = ue1.copyContent();
    auto ecc1_from_uec1 = uec1.copyContent();
    auto&& emc1ref_from_uem1 = uem1.moveContent();

    return 0;
}
