#include <iostream>

using namespace std;

/************************* Definicja zwyklej klasy ze skladnikami statycznymi *************************/

class zamek
{
    const char* nazwa;

public:
    static zamek* wsk_siedziby;

    zamek( const char* n ) : nazwa(n) { }
    void opowiedz();
    static void gdzie_krol();
};

/////////////////////////////////////////// Definicje metod ////////////////////////////////////////////

void zamek::opowiedz()
{
    cout << "Tu " << nazwa << ", wsk_siedziby = " << wsk_siedziby->nazwa << endl;
}

void zamek::gdzie_krol()
{
    cout << "Funkcja statyczna informuje, ze siedziba krola jest teraz zamek: "
         << wsk_siedziby->nazwa << endl;
}

///////////////////////////////// Globalna definicja skladnika statycznego //////////////////////////////

zamek* zamek::wsk_siedziby;

/************************* Definicja szablonu klasy ze skladnikami statycznymi *************************/

template <typename typ>
class szato
{
    const char* nazwa;

public:
    static int iii;
    static typ ttt;
    static szato<typ>* wsk_stolicy;

    szato( const char* n ) : nazwa(n) { }
    void opowiedz();
    static void funstat();
};

/////////////////////////////////////////// Definicje metod ////////////////////////////////////////////

template <typename typ>
void szato<typ>::opowiedz()
{
    cout << "Tu " << nazwa << " [" << iii << ", " << ttt << "]. Stolica jest: " << wsk_stolicy->nazwa
         << endl;
}

template <typename typ>
void szato<typ>::funstat()
{
    cout << "Funkcja statyczna informuje, ze stolica jest teraz\n\t" << wsk_stolicy->nazwa << endl;
}

//////////////////////////////// Globalne definicje skladnikow statycznych //////////////////////////////

template <typename typ>
int szato<typ>::iii;

template <typename typ>
typ szato<typ>::ttt;

template <typename typ>
szato<typ>* szato<typ>::wsk_stolicy;

/******************************************** Funkcja globalna ******************************************/

int main()
{
    cout << "\n*** Bawimy sie zwykla klasa. ***\n";

    zamek niepolomice("Niepolomice");
    zamek wawel("Wawel");

    zamek::wsk_siedziby = &wawel;

    wawel.opowiedz();
    niepolomice.opowiedz();
    zamek::gdzie_krol();

    cout << "Krol wyjezdza na polowanie do Puszczy Niepolomickiej...\n";
    wawel.wsk_siedziby = &niepolomice;

    wawel.opowiedz();
    niepolomice.opowiedz();
    zamek::gdzie_krol();


    cout << "\n*** Bawimy sie klasa szablonowa szato<int> ***\n";

    szato<int> luwr("Paryz (Luwr)");
    szato<int> blois("Blois");

    blois.iii = 7;
    szato<int>::ttt = 77;
    szato<int>::wsk_stolicy = &luwr;

    luwr.opowiedz();
    blois.opowiedz();
    szato<int>::funstat();

    cout << "Krol jedzie do BLOIS...\n";
    szato<int>::wsk_stolicy = &blois;

    luwr.opowiedz();
    blois.opowiedz();
    szato<int>::funstat();


    cout << "\n*** Bawimy sie klasa szablonowa szato<char> ***\n";

    szato<char> tower("Tower");
    szato<char> windsor("Windsor");

    szato<char>::iii = 166;
    szato<char>::ttt = 'x';
    szato<char>::wsk_stolicy = &tower;

    windsor.opowiedz();
    tower.opowiedz();
    szato<char>::funstat();

    cout << "Nie zmienilo to sytuacji w zamkach francuskich:\n";
    luwr.opowiedz();
    szato<int>::funstat();

    return 0;
}
