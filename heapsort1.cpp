#include <iostream>
#include <stdexcept>
#include <vector>

/*********************** Utilities for printing array of streamable type only *************************/

template <typename T>
struct void_t_impl
{
    typedef void type;
};

template <typename T>
using void_t = typename void_t_impl<T>::type;

template <typename T>
T&& declval();

/************************************ Printing function templates *************************************/

template <typename T, typename Stream, void_t<decltype(declval<Stream>() << T{})>* = nullptr>
void printArray( T arr[], const unsigned n, Stream&& str )
{
    str << "Array contents:\t";
    for(unsigned i=0; i<n; ++i)
        str << arr[i] << " ";
    str << "\n";
}

template <typename T, typename Stream, void_t<decltype(declval<Stream>() << T{})>* = nullptr>
void print3Darray( T*** arr, const unsigned l, const unsigned m, const unsigned n, Stream&& str )
{
    str << "3D array contents:\n";
    for(unsigned i=0; i<l; ++i) {
        for(unsigned j=0; j<m; ++j) {
            for(unsigned k=0; k<n; ++k) {
                str << "i: " << i << ", j: " << j << ", k: " << k << ", val = " << arr[i][j][k] << "\n";
            }
        }
    }
    str << "\n";
}

template <typename T, typename Stream, void_t<decltype(declval<Stream>() << T{})>* = nullptr>
void printVecOfPairs( const std::vector<T[2]>& vecOfPairs, Stream&& str )
{
    str << "Pairs:\t";
    for(const auto& pair : vecOfPairs)
        str << pair[0] << "+" << pair[1] << ", ";
    str << "\n";
}

/***************************************** Sorting algorithms *****************************************/

template <typename T>
void swap( T& t1, T& t2 )
{
    if( t1 != t2 ) {
        T temp = std::move(t2);
        t2 = std::move(t1);
        t1 = std::move(temp);
    }
}

template <typename T>
void heapify( T arr[], const unsigned n, const unsigned root ) // do size checking
{
    const unsigned /*root = n/2-1,*/ left = 2*root+1, right = 2*root+2;

    if( right < n ) {
        if( arr[root] < arr[left] && arr[root] < arr[right] ) {
            if( arr[left] < arr[right] ) {
                swap(arr[root], arr[right]);
                heapify(arr,n,right);
            }
            else {
                swap(arr[root], arr[left]);
                heapify(arr,n,left);
            }
        }
        else if( arr[root] < arr[left] ) {
            swap(arr[root], arr[left]);
            heapify(arr,n,left);
        }
        else if( arr[root] < arr[right] ) {
            swap(arr[root], arr[right]);
            heapify(arr,n,right);
        }
    }
    else if( left < n ) {
        if( arr[root] < arr[left] )
            swap(arr[root], arr[left]);
    }
}

template <typename T>
void heapSort( T arr[], const unsigned n )
{
    unsigned size = n;

    // Convert the array into a max binary heap
    for(unsigned root=n/2; root-->0; )
        heapify(arr,size,root);

    while( size > 1 ) {
        swap(arr[0],arr[size-1]);
        heapify(arr,--size,0);
    }
}

/****************************************** Rotating an array *****************************************/

template <typename T>
void rotateArr( T arr[], const unsigned n, const unsigned places = 1, const bool r2l = true )
{
    for(unsigned p=0; p<places; ++p) {
        if( r2l ) {
            T temp = std::move(arr[0]);
            for(unsigned i=0; i<n-1; ++i)
                arr[i] = std::move(arr[i+1]);
            arr[n-1] = std::move(temp);
        }
        else {
            T temp = std::move(arr[n-1]);
            for(unsigned i=n-1; i>0; --i)
                arr[i] = std::move(arr[i-1]);
            arr[0] = std::move(temp);
        }
    }
}

/************************************* Checking if array is sorted ************************************/

template <typename T>
bool isSorted( const T arr[], const unsigned n )
{
    bool increasing, sorted = true;
    if( n > 1 )
        increasing = (arr[0] < arr[1]);
    else {
        std::cout << "Warning! The input array is of size 1 only.\n";
        return true;
    }

    for(unsigned i=1; i<n-1; ++i) {
        if( increasing ) {
            if( arr[i] > arr[i+1] ) {
                sorted = false;
                break;
            }
        }
        else {
            if( arr[i] < arr[i+1] ) {
                sorted = false;
                break;
            }
        }
    }

    return sorted;
}

/******************* Finding pairs of elements which add up to k in a sorted array ********************/

template <typename T>
std::vector<T[2]> pairsWithSumOf( T k, const T arr[], const unsigned n )
{
    std::vector<T[2]> pairsVec;

    unsigned i=0, j=n-1;
    while( i<j ) {
        if( arr[i]+arr[j] < k ) ++i;
        else if( arr[i]+arr[j] > k ) --j;
        else pairsVec.push_back({arr[i],arr[j]});
    }

    return pairsVec;
}

/**************************** Allocating and deallocating a 3D array in heap **************************/

template <typename T>
T** alloc2Darray( const unsigned m, const unsigned n )
{
    T* elems = new T[m*n];
    T** rowsOfElems = new T*[m];

    for(unsigned i=0; i<m; ++i)
        rowsOfElems[i] = elems + i*n;

    return rowsOfElems;
}

template <typename T>
void dealloc2Darray( T**& arr )
{
    delete [] arr[0];
    delete [] arr;
    arr = nullptr;
}

template <typename T>
T*** alloc3Darray( const unsigned l, const unsigned m, const unsigned n, T initVal )
{
    T* elems = new T[l*m*n];
    T** rowsOfElems = new T*[l*m];
    T*** planesOfElems = new T**[l];

    for(unsigned i=0; i<l; ++i) {
        planesOfElems[i] = rowsOfElems + i*m;

        for(unsigned j=0; j<m; ++j)
            rowsOfElems[i*m+j] = elems + i*m*n + j*n;
    }

    for(unsigned i=0; i<l; ++i)
        for(unsigned j=0; j<m; ++j)
            for(unsigned k=0; k<n; ++k)
                planesOfElems[i][j][k] = initVal;

    return planesOfElems;
}

template <typename T>
void dealloc3Darray( T***& arr )
{
    delete [] arr[0][0];
    delete [] arr[0];
    delete [] arr;
    arr = nullptr;
}

/******************************** Finding an element other than min/max *******************************/

template <typename T>
T findNonMinMax( const T arr[], const unsigned n )
{
    if( n < 3 ) throw std::runtime_error("Cannot find element other than min/max in array of size < 3!");

    T elem;
    unsigned i=0, j=1, k=2;

    while( i < n ) {
        while( j < n && k < n ) {
            if( arr[i] < arr[j] ) {
                if( arr[i] > arr[k] ) {
                    elem = arr[i];
                    i = n;
                    break;
                }
                else ++k;
            }
            else if( arr[i] > arr[j] ) {
                if( arr[i] < arr[k] ) {
                    elem = arr[i];
                    i = n;
                    break;
                }
                else ++k;
            }
            else ++j;
        }
        ++i;
        j=0;
        k=1;
    }

    return elem;
}

/*************************************** Checking for palindrome **************************************/

template <typename T>
bool isPalindrome( const T arr[], const unsigned n )
{
    bool isPal = true;
    unsigned i, j;

    if( n > 0 ) {
        for(i=0, j=n-1; i<j; ++i, --j) {
            if( arr[i] != arr[j] ) {
                isPal = false;
                break;
            }
        }
    }

    return isPal;
}

/************************ Checking for duplicated string elements using bitset ************************/

std::vector<char> findDuplicatesUsingBits( const char str[] )
{
    long unsigned uTable[2] = { 0u, 0u };
    uint8_t* bytePtr = (uint8_t*) uTable;

    std::cout << "Bit representation of array of two long unsigned 0s: ";
    for(std::size_t i=0; i<sizeof(uTable); ++i) {
        uint8_t byte = bytePtr[i];

        for(int bit=0; bit<8; ++bit) {
            std::cout << (byte & 1);
            byte >>= 1;
        }
    }
    std::cout << "\n";

    std::vector<char> duplicates;
    long unsigned hashTable1 = 0u, hashTable2 = 0u;

    for(unsigned i=0; str[i]!='\0'; ++i) {
        if( str[i] < 64 ) {
            if( (hashTable1 & (1u << str[i])) == 0 )
                hashTable1 = hashTable1 | (1u << str[i]);
            else
                duplicates.push_back(str[i]);
        }
        else {
            std::cout << str[i] << " ";
            if( (hashTable2 & (1u << (str[i]-64))) == 0 )
                hashTable2 = hashTable2 | (1u << (str[i]-64));
            else
                duplicates.push_back(str[i]);
        }
    }

    return duplicates;
}

/***************************************** Array permutations *****************************************/

template <typename T>
std::vector<T*> findPermutationsOf( T array[], const unsigned n, const unsigned start )
{
    if( n <= 1 )
        return std::vector<T*>{ array };

    std::vector<T*> results;
    unsigned l=start, h=n-1, i;

    for(i=l; i<=h; ++i) {
        T* newArray = new T[n];
        memcpy(newArray, array, n*sizeof(T));
        swap(newArray[l], newArray[i]);
        if( start == 0 || i != l ) results.push_back(newArray);

        std::vector<T*> subResults = findPermutationsOf(newArray, n, start+1);
        for(T* res : subResults)
            results.push_back(res);
    }

    return results;
}

template <typename T>
std::vector<T*> findPermutationsOf( const T origins[], T array[], const unsigned n, unsigned k )
{
    std::vector<T*> results;
    static std::vector<bool> takens = std::vector<bool>(n, false);

    if( k == n ) {
        T* newArray = new T[n];
        memcpy(newArray, array, n*sizeof(T));
        results.push_back(newArray);
    }
    else {
        for(unsigned i=0; i<n; ++i) {
            if( takens[i] == false ) {
                std::cout << "Enter - k: " << k << ", i: " << i << "\n";
                array[k] = origins[i];
                takens[i] = true;

                std::vector<T*> resContainer = findPermutationsOf(origins, array, n, k+1);
                for( T* res : resContainer )
                    results.push_back(res);

                takens[i] = false;
                std::cout << "Exit - takens: " << takens[0] << " " << takens[1] << " " << takens[2] << " " << takens[3] << "\n";
            }
        }
    }

    return results;
}

/************************************** The main driver function **************************************/

auto main() -> int
{
    // Defining an array of floats
    float fArray[] = { 13.8f, -14.3f, 4.7f, 56.2f, 71.1f, -5.9f, 48.4f, 32.6f, 67.0f, 22.5f };
    const auto fSize = sizeof(fArray)/sizeof(float);
    printArray(fArray, fSize, std::cout);
    std::cout << std::boolalpha << "Is the array sorted? " << isSorted(fArray, fSize) << "\n";

    // Sorting via heapSort
    heapSort(fArray, fSize);
    printArray(fArray, fSize, std::cout);
    std::cout << std::boolalpha << "Is the array sorted? " << isSorted(fArray, fSize) << "\n";

    // Rotating by 2 places
    rotateArr(fArray, fSize, 2);
    printArray(fArray, fSize, std::cout);

    // Unrotating by 3 places
    rotateArr(fArray, fSize, 3, false);
    printArray(fArray, fSize, std::cout);
    std::cout << std::boolalpha << "Is the array sorted? " << isSorted(fArray, fSize) << "\n";


    // Defining an array of ints
    int iArray[] = { 23, -17, 4, 78, 51, -5, 32, 46, 69, 11 };
    const auto iSize = sizeof(iArray)/sizeof(int);
    printArray(iArray, iSize, std::cout);

    // Sorting via heapSort
    heapSort(iArray, iSize);
    printArray(iArray, iSize, std::cout);

    // Checking for pairs of elements which sum to 6
    //std::list<int[2]> iPairs = pairsWithSumOf(6, iArray, iSize);
    //printVecOfPairs(iPairs, std::cout);

    // Creating a 3D array of doubles in heap
    const unsigned l=2, m=3, n=4;
    double*** d3Darray = alloc3Darray(l,m,n,0.0);

    double start = 0.0;
    for(unsigned i=0; i<l; ++i)
        for(unsigned j=0; j<m; ++j)
            for(unsigned k=0; k<n; ++k)
                d3Darray[i][j][k] = (start += 0.1);

    print3Darray(d3Darray, l, m, n, std::cout);
    dealloc3Darray(d3Darray);

    // Defining an array of unsigned ints
    unsigned uArray[] = { 1u, 2u, 3u, 4u };
    const auto uSize = sizeof(uArray)/sizeof(unsigned);
    printArray(uArray, uSize, std::cout);

    unsigned nonMinMax = findNonMinMax(uArray, uSize);
    std::cout << "Non min/max element of the array is: " << nonMinMax << "\n";

    unsigned* uArrCopy = new unsigned[uSize];
    memcpy(uArrCopy, uArray, sizeof(uArray));
    std::vector<unsigned*> permutations = findPermutationsOf(/*uArrCopy, */uArray, uSize, 0);
    std::cout << "There are " << permutations.size() << " permutations of the array.\n";
    for(unsigned* p : permutations) {
        printArray(p, uSize, std::cout);
        delete [] p;
    }
    delete [] uArrCopy;
    std::cout << "\n";

    // Defining an array of chars
    char cArray[] = "kobylamamalybok";
    const auto cSize = sizeof(cArray)/sizeof(char)-1;
    printArray(cArray, cSize, std::cout);

    std::cout << "Is the string a palindrome? " << std::boolalpha << isPalindrome(cArray, cSize) << "\n";

    std::cout << "\nHow many bits in long double? " << sizeof(long unsigned)*8 << "\n";
    std::vector<char> duplicates = findDuplicatesUsingBits(cArray);
    std::cout << "Duplicates in " << cArray << " are: ";
    for( const auto& d : duplicates ) std::cout << d << " ";
    std::cout << "\n";

    return 0;
}
