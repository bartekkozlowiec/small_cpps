#include <iostream>
#include <math.h>
#include <new>

/********************************* Utilities to allow printing of only printable types *********************************/

template <typename T>
struct void_t_impl
{
    typedef void type;
};

template <typename T>
using void_t = typename void_t_impl<T>::type;

template <typename T>
T&& declval();

/********************************************* Dynamic queue template class ********************************************/

template <typename T>
class Queue;

template <typename T>
std::ostream& operator<<( std::ostream& str, const Queue<T>& queue );

template <typename T>
class Queue
{
    struct Node
    {
        Node* next;
        T data;
    };
    Node *front, *rear;

public:
    constexpr Queue() : front{nullptr}, rear{nullptr} {}
    Queue( const T array[] ) { for(const T& elem : array) enqueue(elem); }
    ~Queue() { while( !isEmpty() ) dequeue(); }
    bool enqueue( const T& elem );
    T dequeue();
    bool isEmpty() const { return front == nullptr; }
    unsigned length() const;

    friend std::ostream& operator<< <>( std::ostream& str, const Queue& queue );
};

template <typename T>
bool Queue<T>::enqueue( const T& elem )
{
    bool enqueued = true;
    Node* p;

    try {
        p = new Node;
        p->data = elem;
        p->next = nullptr;
        if( isEmpty() )
            front = rear = p;
        else {
            rear->next = p;
            rear = p;
        }
    } catch( std::bad_alloc& ) {
        enqueued = false;
    }

    return enqueued;
}

template <typename T>
T Queue<T>::dequeue()
{
    T retVal{};
    Node* p;

    if( !isEmpty() ) {
        p = front;
        retVal = p->data;
        front = front->next;
        delete p;
    }

    return retVal;
}

template <typename T>
unsigned Queue<T>::length() const
{
    unsigned len = 0;
    const Node* p = front;

    while( p ) {
        ++len;
        p = p->next;
    }

    return len;
}

template <typename T>
std::ostream& operator<<( std::ostream& str, const Queue<T>& queue )
{
    typename Queue<T>::Node* p = queue->first;

    while( p != queue->rear )
        str << p->data << " ";
    str << "\n";

    return str;
}

/********************************************* Linked list class template **********************************************/

template <typename T>
class LinkedList;

template <typename T>
std::ostream& operator<<( std::ostream& str, const LinkedList<T>& list );

template <typename T>
T findInterNode( const LinkedList<T>& list1, const LinkedList<T>& list2 );

template <typename T>
class LinkedList
{
    struct Node
    {
        Node* next;
        T data;
        Queue<Node*> prevs;
    };
    Node* first;

public:
    constexpr LinkedList() : first{nullptr} {}
    LinkedList( const T array[], const unsigned n );
    ~LinkedList();

    unsigned length() const;
    void insert( T&& elem, const unsigned idx );
    T remove( const unsigned idx );
    Node* findMiddle() const;
    void attachTo( Node* );

    template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
    void display( Str&& ) const;

    friend std::ostream& operator<< <>( std::ostream& str, const LinkedList& list );
    friend T findInterNode <>( const LinkedList& list1, const LinkedList& list2 );
};

template <typename T>
std::ostream& operator<<( std::ostream& str, const LinkedList<T>& list )
{
    auto p = list.first;
    while( p ) {
        str << p->data << " ";
        p = p->next;
    }
    str << "\n";
    return str;
}

template <typename T>
LinkedList<T>::LinkedList( const T array[], const unsigned n )
{
    Node* next = nullptr;
    for(unsigned i=n; i-->0;) {
        Node* node = new Node;
        node->data = array[i];
        node->next = next;
        if( next ) next->prevs.enqueue(node);
        next = node;
    }
    first = next;
}

template <typename T>
LinkedList<T>::~LinkedList()
{
    Node* p = first;
    Queue<Node*> intersections;
    intersections.enqueue(first);

    while( p ) {
        if( p->prevs.length() > 1 ) {
            intersections.enqueue(p);
            while( !p->prevs.isEmpty() ) {
                Node* t = p->prevs.dequeue();
                t->next = nullptr;
            }
        }
        p = p->next;
    }

    while( !intersections.isEmpty() ) {
        p = intersections.dequeue();
        while( p ) {
            Node* q = p;
            p = p->next;
            delete q;
        }
    }
}

template <typename T>
unsigned LinkedList<T>::length() const
{
    unsigned len = 0;
    Node* p = first;

    while( p ) {
        p = p->next;
        ++len;
    }

    return len;
}

template <typename T>
void LinkedList<T>::insert( T&& elem, const unsigned idx )
{
    if( idx < 0 || idx > length() )
        return;

    Node* newNode = new Node;
    newNode->data = elem;

    if( idx == 0 ) {
        newNode->next = first;
        if( first ) first->prevs.enqueue(newNode);
        first = newNode;
    }
    else {
        Node* p = first;
        for(unsigned i=0; i<(idx-1); ++i)
            p = p->next;

        newNode->next = p->next;
        if( p->next ) p->next->prevs.enqueue(newNode);
        newNode->prevs.enqueue(p);
        p->next = newNode;
    }
}

template <typename T>
T LinkedList<T>::remove( const unsigned idx )
{
    if( idx<1 || idx>length() )
        return T{};

    Node* p = first;

    if( idx == 1 ) {
        first = first->next;
        first->prevs = Queue<Node*>{};
    }
    else {
        Node* t = nullptr;
        for(unsigned i=0; i<idx-1; ++i) {
            t = p;
            p = p->next;
        }
        t->next = p->next;
        if( p->next ) p->next->prevs = p->prevs;
    }

    T retVal = p->data;
    delete p;

    return retVal;
}

template <typename T>
typename LinkedList<T>::Node* LinkedList<T>::findMiddle() const
{
    auto p = first;
    auto midIdx = static_cast<unsigned>(floor(length()/2));

    for(unsigned i=0; i<midIdx; ++i)
        p = p->next;

    return p;
}

template <typename T>
void LinkedList<T>::attachTo( LinkedList<T>::Node* node )
{
    Node* p = first;

    while( p->next )
        p = p->next;

    p->next = node;
    node->prevs.enqueue(p);
}

template <typename T>
template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>*>
void LinkedList<T>::display( Str&& str ) const
{
    const Node* p = first;
    while( p ) {
        str << p/*->data*/ << " ";
        p = p->next;
    }
    str << "\n";
}

/********************************************* Simple stack template class *********************************************/

template <typename T, unsigned size>
class StaticStack;

template <typename T, unsigned size>
std::ostream& operator<<( std::ostream& str, const StaticStack<T,size>& stack );

template <typename T, unsigned size>
class StaticStack
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type array[size];
    unsigned curSize; // instead of top

public:
    constexpr StaticStack<T,size>() : curSize{0} {}
    ~StaticStack();

    bool push( const T& );
    T pop();
    bool isEmpty() const { return curSize == 0; }
    bool isFull() const { return curSize >= size; }

    friend std::ostream& operator<< <>( std::ostream& str, const StaticStack<T,size>& stack );
};

template <typename T, unsigned size>
std::ostream& operator<<( std::ostream& str, const StaticStack<T,size>& stack )
{
    for(unsigned i=0; i<stack.curSize; ++i)
        str << *reinterpret_cast<const T*>(&stack.array[i]) << " ";
    str << "\n";

    return str;
}

template <typename T, unsigned size>
bool StaticStack<T,size>::push( const T& val )
{
    bool pushed;

    if( isFull() )
        pushed = false;
    else {
        new(array+curSize) T{val};
        ++curSize;
        pushed = true;
    }

    return pushed;
}

template <typename T, unsigned size>
T StaticStack<T,size>::pop()
{
    T retVal{};

    if( !isEmpty() ) {
        retVal = *reinterpret_cast<T*>(&array[--curSize]);
        reinterpret_cast<T*>(array+curSize)->~T();
    }

    return retVal;
}

template <typename T, unsigned size>
StaticStack<T,size>::~StaticStack()
{
    for(unsigned i=0; i<curSize; ++i)
        reinterpret_cast<T*>(array+i)->~T();
}

/**************************** A function which finds intersection node of two linked lists *****************************/

template <typename T>
T findInterNode( const LinkedList<T>& list1, const LinkedList<T>& list2 )
{
    StaticStack<typename LinkedList<T>::Node*,20> stack1, stack2;
    typename LinkedList<T>::Node *p1 = list1.first, *p2 = list2.first, *res;

    while( p1 ) {
        stack1.push(p1);
        p1 = p1->next;
    }

    while( p2 ) {
        stack2.push(p2);
        p2 = p2->next;
    }

    p1 = p2 = nullptr;
    while( p1 == p2 ) {
        res = p1;
        p1 = stack1.pop();
        p2 = stack2.pop();
    }

    return res ? res->data : T{};
}

/******************************** A small wrapper for int with no default constructor **********************************/

class intWrapper
{
    int content;

public:
    constexpr intWrapper(int c) : content{c} {}
    constexpr int get() const { return content; }
};

/********************************************** The main driver function ***********************************************/

int main()
{
    int intArr[] = { 1, -3, 8, 4, 6 };
    LinkedList<int> intList1(intArr, sizeof(intArr)/sizeof(int));

    intList1.insert(7,5);
    intList1.insert(-9,0);
    std::cout << intList1;

    std::cout << intList1.remove(1) << "\n";
    std::cout << intList1;

    std::cout << intList1.remove(7) << "\n";
    std::cout << intList1.remove(6) << "\n";
    std::cout << intList1;

    std::cout << "The middle element of the list is: " << intList1.findMiddle()->data << "\n";

    LinkedList<int> intList2;
    intList2.insert(-3,0);
    intList2.insert(3,0);
    intList2.insert(-4,0);
    intList2.insert(4,0);

    intList2.attachTo(intList1.findMiddle());
    std::cout << "The two lists by values:\n";
    std::cout << intList1;
    std::cout << intList2;

    auto interNode = findInterNode(intList1, intList2);
    std::cout << "The intersection node is: " << interNode << "\n";

    //////////////////////////////////////////////////////////////////////////////////////////

    StaticStack<float,10> floatStack;
    floatStack.push(8.2f);
    floatStack.push(-3.4f);
    floatStack.push(5.7f);
    floatStack.push(-6.1f);

    std::cout << floatStack;

    std::cout << floatStack.pop() << "\n";
    std::cout << floatStack.pop() << "\n";

    std::cout << floatStack;

    //////////////////////////////////////////////////////////////////////////////////////////

    StaticStack<intWrapper,10> intWrapStack;
    //std::cout << intWrapStack.pop().get();
    intWrapStack.push(-1);
    intWrapStack.push(2);
    std::cout << "AT END\n";

    std::getchar();

    return 0;
}
