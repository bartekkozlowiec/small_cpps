#include <iostream>

/*********************************** Tools to allow printing streamable types ***********************************/

template <typename ... T>
struct void_t_impl
{
    typedef void type;
};

template <typename ... T>
struct void_t
{
    using void_t_impl<T...>::type;
};

template <typename T>
T&& declval();

template <typename T, typename Stream, void_t<decltype(declval<Stream>() << declval<T>())>* = nullptr>
void printArray( T array[], Stream&& ostr, const unsigned n )
{
    ostr << "Array contents: ";
    for(unsigned i=0; i<n; ++i)
        ostr << array[i] << " ";
    ostr << "\n";
}

struct notStreamable {};

/********************************************** Sorting algorithms ***********************************************/

template <typename T>
void swap( T* first, T* second )
{
    if( first != second )
    {
        T temp = *first;
        *first = *second;
        *second = temp;
    }
}

template <typename T>
void heapify( T array[], const unsigned root, const unsigned n )
{
    unsigned left = 2*root + 1;
    unsigned rite = left + 1;

    if( left < n && array[root] < array[left] )
    {
        swap(array+root,array+left);
        //heapify changed branch
        heapify(array,left,n);
    }
    if( rite < n && array[root] < array[rite] )
    {
        swap(array+root,array+rite);
        //heapify changed branch
        heapify(array,rite,n);
    }
}

template <typename T>
void heapSort( T array[], const unsigned n )
{
    // First, create a max heap
    for(unsigned root = n/2; root-- > 0;)
        heapify(array,root,n);

    // Now, the highest element is at index 0
    //swap(array,array+n-1);
    //heapify(array,0,n-1);

    //swap(array,array+n-2);
    //heapify(array,0,n-2);
    for(unsigned i=1; i<n; ++i)
    {
        swap(array,array+n-i);
        heapify(array,0,n-i);
    }
}

/********************************************** The driver function **********************************************/

int main()
{
    int iArray[] = { 34, -17, 28, -6, 71, 45, 9, 53, -22, 69 };
    const unsigned iSize = sizeof(iArray) / sizeof(int);
    std::cout << "Input array\n";
    printArray(iArray,std::cout,iSize);

    notStreamable nArray[] = { notStreamable{} };
    //printArray(nArray,std::cout,1);

    // Heap-sorting
    heapSort(iArray,iSize);
    std::cout << "Sorted array\n";
    printArray(iArray,std::cout,iSize);

    return 0;
}
