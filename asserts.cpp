#include <cstdio>
#include <string>

template <bool Cond, typename T = void>
struct local_enable_if
{};

template <typename T>
struct local_enable_if<true,T>
{
    typedef T type;
};

template <bool Cond, typename T = void>
struct local_enable_if_t
{
    using local_enable_if<Cond,T>::type;
};

template <typename T>
struct local_void_t_impl
{
    typedef void type;
};

template <typename T>
struct local_void_t
{
    using local_void_t_impl<T>::type;
};

template <unsigned N>
class FixedStr
{
    char _array[N+1]; // for terminating \0

public:
    char operator[]( unsigned i ) const; /*expects: i < N*/
};

int main()
{
    // Quick demonstration of void_t vs enable_if usage difference
    local_void_t<int>* void_ptr1 = nullptr; // == void* void_ptr
    local_void_t<std::string>* void_ptr2 = nullptr; // valid only if <string> is #included
    local_enable_if_t<true>* void_ptr3 = nullptr;

    return 0;
}
