#include <iostream>
#include <cassert>
#include <cstdint>
#include <new>

/******************************* Utilities for allowing printing of only streamable types *******************************/

template <typename T>
struct void_t_impl
{
    typedef void type;
};

template <typename T>
using void_t = typename void_t_impl<T>::type;

template <typename T>
T&& declval();

template <bool Cond, typename T = void>
struct enable_if
{};

template <typename T>
struct enable_if<true, T>
{
    typedef T type;
};

template <bool Cond, typename T>
using enable_if_t = typename enable_if<Cond,T>::type;

/******************************************* Circular queue class using array ******************************************/

template <typename T, unsigned size>
class CircularQueue
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type array[size];
    unsigned front, rear;

public:
    constexpr CircularQueue() : front{0}, rear{0} {}
    CircularQueue( const T[], const unsigned );
    ~CircularQueue() { while( !isEmpty() ) dequeue(); }

    constexpr bool isEmpty() const { return front == rear; }
    constexpr bool isFull() const { return (rear+1) % size == front; }

    bool enqueue( const T& );
    T dequeue();
    T& operator[]( const unsigned idx ) { return *reinterpret_cast<T*>(array + ((front+idx+1) % size)); }
    unsigned currLength() const { return rear >= front ? rear-front : size-front+rear; }
    CircularQueue<T,size> splitBy( const T& );

    template <void_t<decltype(declval<std::ostream>() << declval<T>())>*>
    friend std::ostream& operator<<( std::ostream&, CircularQueue<T,size>& );
};

template <typename T, unsigned size>
CircularQueue<T,size>::CircularQueue( const T arr[], const unsigned n ) : front{0}, rear{0}
{
    assert(n < size);
    for(unsigned i=0; i<n; ++i) {
        new(&array[++rear]) T{arr[i]};
    }
}

template <typename T, unsigned size>
bool CircularQueue<T,size>::enqueue( const T& elem )
{
    bool enqueued = true;

    if( !isFull() )
        new(&array[(++rear) % size]) T{elem};
    else
        enqueued = false;

    return enqueued;
}

template <typename T, unsigned size>
T CircularQueue<T,size>::dequeue()
{
    T retVal{};

    if( !isEmpty() ) {
        retVal = *reinterpret_cast<T*>(&array[(++front) % size]);
        reinterpret_cast<T*>(array+front)->~T();
    }

    return retVal;
}

template <typename T, unsigned size>
CircularQueue<T,size> CircularQueue<T,size>::splitBy( const T& elem )
{
    CircularQueue<T,size> retQueue;

    while( !isEmpty() ) {
        T deq = dequeue();
        if( deq != elem )
            retQueue.enqueue(deq);
        else
            break;
    }

    return retQueue;
}

template <typename T, unsigned size, void_t<decltype(declval<std::ostream>() << declval<T>())>* = nullptr>
std::ostream& operator<<( std::ostream& str, CircularQueue<T,size>& queue )
{
    str << "Queue contents:\n";
    for(unsigned i=0; i<queue.currLength(); ++i)
        str << queue[i] << " ";
    str << "\n";
    return str;
}

/****************************************** Dynamic stack class using pointers *****************************************/

template <typename T>
class LinkedStack
{
    struct Node
    {
        T data;
        Node* next;
    }* last;

public:
    constexpr LinkedStack() : last{nullptr} {}
    LinkedStack( const T[], const unsigned );
    ~LinkedStack();

    constexpr bool isEmpty() const { return last == nullptr; }
    bool push( const T& );
    T pop();
    const T& top() const;
};

template <typename T>
LinkedStack<T>::LinkedStack( const T arr[], const unsigned n )
{
    Node* prev = nullptr;
    for(unsigned i=0; i<n; ++i) {
        Node* p = new Node;
        p->data = arr[i];
        p->next = prev;
        prev = p;
    }
    last = prev;
}

template <typename T>
LinkedStack<T>::~LinkedStack()
{
    Node* p = last;

    while( p ) {
        p = p->next;
        Node* t = p;
        delete t;
    }
}

template <typename T>
bool LinkedStack<T>::push( const T& elem )
{
    Node* p;
    bool pushed = true;

    try {
        p = new Node;
    } catch( const std::bad_alloc& ) {
        p = nullptr;
        pushed = false;
    }

    if( p ) {
        p->data = elem;
        p->next = last;
        last = p;
    }

    return pushed;
}

template <typename T>
T LinkedStack<T>::pop()
{
    T retData = top();

    if( !isEmpty() ) {
        Node* p = last;
        last = last->next;
        delete p;
    }

    return retData;
}

template <typename T>
const T& LinkedStack<T>::top() const
{
    if( isEmpty() )
        return T{};
    else
        return last->data;
}

/******************************************* Binary tree class using pointers ******************************************/

template <typename T, unsigned maxSize>
class BinaryTree
{
    struct Node
    {
        Node* leftChild;
        T data;
        Node* rightChild;
    } *root;

public:
    constexpr BinaryTree() : root{nullptr} {}
    BinaryTree( const T[], const unsigned, const T[], const unsigned );
    ~BinaryTree();

    CircularQueue<Node*,maxSize> preOrderT() const;
    CircularQueue<Node*,maxSize> inOrderT() const;
    CircularQueue<Node*,maxSize> postOrderT() const;
    CircularQueue<Node*,maxSize> levelOrderT() const;

    template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
    void display( Str&&, CircularQueue<Node*,maxSize> (BinaryTree<T,maxSize>::*)() const ) const;
};

template <typename T, unsigned maxSize>
BinaryTree<T,maxSize>::BinaryTree( const T preOrderArr[], const unsigned size1, const T inOrderArr[], const unsigned size2 )
{
    struct Pair{
        Node* parent;
        CircularQueue<T,maxSize> queue;
        bool toTheRight;
    };

    root = nullptr;

    if(size1 != size2) {
        std::cerr << "Arrays of unequal sizes passed! Creating empty tree...\n";
        return;
    }

    Node* p = nullptr;
    CircularQueue<T,maxSize> qs{inOrderArr,size2};
    CircularQueue<Pair,maxSize> Qs;
    Qs.enqueue({nullptr,qs,true});
    std::cout << "qS-len: " << qs.currLength() << "\n";

    for(unsigned i=0; i<size1; ++i) {
        for(unsigned j=0; j<Qs.currLength(); ++j) {
            for(unsigned k=0; k<Qs[j].queue.currLength(); ++k) {
                CircularQueue<T,maxSize>& Qr = Qs[j].queue;
                //std::cout << "Qr: " << Qs[j].queue << "\n";

                //std::cout << "i,j,k: " << i << " " << j << " " << k << "\n";
                //std::cout << "Qr[" << k << "] = " << Qr[k] << ", preOrderArr[" << i << "] = " << preOrderArr[i] << "\n";
                if( preOrderArr[i] == Qr[k] ) {
                    p = new Node{nullptr, preOrderArr[i], nullptr};
                    if( Qs[j].parent )
                        Qs[j].toTheRight ? Qs[j].parent->rightChild = p : Qs[j].parent->leftChild = p;
                    else
                        root = p;
                    Qs[j].parent = p; // Don't forget to update parent!
                    auto Ql = Qr.splitBy(preOrderArr[i]);
                    Qs.enqueue({p,Ql,false});
                    Qs[j].toTheRight = true;
                    goto nestedLoopsEnd;
                }
            }
        }

        nestedLoopsEnd:
        if( !p ) {
            std::cerr << "Preorder and inorder arrays have uncommon elements!\n";
            return;
        }
    }
}

template <typename T, unsigned maxSize>
CircularQueue<typename BinaryTree<T,maxSize>::Node*,maxSize> BinaryTree<T,maxSize>::preOrderT() const
{
    CircularQueue<Node*,maxSize> preQ;
    LinkedStack<Node*> auxS;

    Node* p = root;
    while( p || !auxS.isEmpty() ) {
        if( !p ) p = auxS.pop();
        preQ.enqueue(p);
        if( p->rightChild ) auxS.push(p->rightChild);
        p = p->leftChild;
    }

    return preQ;
}

template <typename T, unsigned maxSize>
CircularQueue<typename BinaryTree<T,maxSize>::Node*,maxSize> BinaryTree<T,maxSize>::inOrderT() const
{
    CircularQueue<Node*,maxSize> inQ;
    LinkedStack<Node*> auxS;

    Node* p = root;
    bool visited = false;
    while( p || !auxS.isEmpty() ) {
        if( !p ) {
            p = auxS.pop();
            inQ.enqueue(p);
            p = auxS.pop();
            visited = true;
        }
        else if( !visited ) {
            if( p->rightChild ) auxS.push(p->rightChild);
            auxS.push(p);
            p = p->leftChild;
        }
        else {
            inQ.enqueue(p);
            p = auxS.pop();
            visited = false;
        }
    }

    return inQ;
}

template <typename T, unsigned maxSize>
CircularQueue<typename BinaryTree<T,maxSize>::Node*,maxSize> BinaryTree<T,maxSize>::postOrderT() const
{
    CircularQueue<Node*,maxSize> postQ;
    LinkedStack<intptr_t> auxS;

    Node* p = root;
    bool wentLeft = false;
    while( p || !auxS.isEmpty() ) {
        if( p->leftChild || p->rightChild ) {
            if( p->leftChild && !wentLeft ) {
                auxS.push(reinterpret_cast<intptr_t>(p));
                p = p->leftChild;
                continue;
            }
            if( p->rightChild ) {
                auxS.push(-reinterpret_cast<intptr_t>(p));
                p = p->rightChild;
                wentLeft = false;
            }
        }
        else {
            postQ.enqueue(p);
            intptr_t popped = auxS.pop();
            wentLeft = true;
            if( popped > 0 ) {
                p = reinterpret_cast<Node*>(popped);
                continue;
            }
            while( popped < 0 ) {
                p = reinterpret_cast<Node*>(-popped);
                postQ.enqueue(p);
                popped = auxS.pop();
            }
            p = reinterpret_cast<Node*>(popped);
        }
    }

    return postQ;
}

template <typename T, unsigned maxSize>
CircularQueue<typename BinaryTree<T,maxSize>::Node*,maxSize> BinaryTree<T,maxSize>::levelOrderT() const
{
    CircularQueue<Node*,maxSize> levelQ, auxQ;
    Node* p = root;
    auxQ.enqueue(p);

    while( p && !auxQ.isEmpty() ) {
        p = auxQ.dequeue();
        levelQ.enqueue(p);
        if( p->leftChild ) auxQ.enqueue(p->leftChild);
        if( p->rightChild ) auxQ.enqueue(p->rightChild);
    }

    return levelQ;
}

template <typename T, unsigned maxSize>
template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>*>
void BinaryTree<T,maxSize>::display( Str&& str,
                                     CircularQueue<BinaryTree<T,maxSize>::Node*,maxSize> (BinaryTree<T,maxSize>::* optr)() const ) const
{
    CircularQueue<Node*,maxSize> queue = (this->*optr)();
    str << "Binary tree contents: ";
    while( !queue.isEmpty() )
        str << (queue.dequeue())->data << " ";
    str << "\n";
}

template <typename T, unsigned maxSize>
BinaryTree<T,maxSize>::~BinaryTree()
{
    CircularQueue<Node*,maxSize> toBeDeleted = levelOrderT();

    while( !toBeDeleted.isEmpty() ) {
        Node* p = toBeDeleted.dequeue();
        delete p;
    }
}

/*********************************************** The main driver function **********************************************/

auto main() -> int
{
    int t1[] = {1,2,4,8,9,5,3,6,7};
    int t2[] = {8,4,9,2,5,1,6,3,7};
    BinaryTree<int,20> tree1{t1,sizeof(t1)/sizeof(int),t2,sizeof(t2)/sizeof(int)};
    tree1.display(std::cout, &BinaryTree<int,20u>::preOrderT);
    tree1.display(std::cout, &BinaryTree<int,20u>::inOrderT);
    tree1.display(std::cout, &BinaryTree<int,20u>::levelOrderT);
    tree1.display(std::cout, &BinaryTree<int,20u>::postOrderT);

    return 0;
}
