#include <iostream>
#include <cstring>
#include <type_traits>

const unsigned charRange = 127;
const unsigned unsignedRange = 10;

// A printing function for an array
template<typename T, std::enable_if_t<!std::is_class<T>::value>* = nullptr>
void printArray( T array[], unsigned n )
{
    std::cout << "Array contents: ";
    for(unsigned i=0; i<n; ++i)
        std::cout << array[i] << "\t";
    std::cout << "\n";
}

template<typename T, std::enable_if_t<!std::is_class<T>::value>* = nullptr>
void stableCountSort( T array[], const unsigned n, const unsigned range )
{
    // Array for the sorted elements
    T outputArray[n];

    // Array for the count of the "keys"
    unsigned countKeys[range];

    // Set all the counting keys array elements to 0
    std::memset(countKeys,0,sizeof(countKeys));
    //for(unsigned i=0; i<range; ++i)
    //    countKeys[i] = 0;

    // Count the occurence of every key from range in the input array
    // An element of input array is an index of count array
    for(unsigned i=0; i<n; ++i)
        ++countKeys[array[i]];

    // Now, add the previous counts to every count element to obtain an array of final output positions
    for(unsigned i=1; i<range; ++i)
        countKeys[i] += countKeys[i-1];

    // Finally, fill the output array - insert every input element at the position given by count array
    // We start from the array's tail to achieve stable sort
    for(int i=(n-1); i>=0; --i)
    {
        outputArray[countKeys[array[i]]-1] = array[i];
        --countKeys[array[i]];
    }

    // Last thing is to copy output into the original array
    for(unsigned i=0; i<n; ++i)
        array[i] = outputArray[i];
}

template<typename T, std::enable_if_t<!std::is_class<T>::value>* = nullptr>
void merge( T array[], const unsigned l, const unsigned m, const unsigned r )
{
    //std::cerr << "\nMerge called with: l = " << l << ", m  = " << m << ", r = " << r << "\n";
    const unsigned n1 = m - l + 1;
    const unsigned n2 = r - m;
    //std::cerr << "n1 = " << n1 << ", n2  = " << n2 << "\n";

    T leftArr[n1], rightArr[n2];
    for(unsigned i=0; i<n1; ++i) leftArr[i] = array[l+i];
    for(unsigned i=0; i<n2; ++i) rightArr[i] = array[m+1+i];
    //std::cerr << "Left array - "; printArray(leftArr,n1);
    //std::cerr << "Right array - "; printArray(rightArr,n2);

    unsigned i = 0, j = 0, k = 0;
    while( i < n1 && j < n2 )
    {
        if( leftArr[i] < rightArr[j] )
        {
            array[l+k] = leftArr[i];
            ++i;
        }
        else
        {
            array[l+k] = rightArr[j];
            ++j;
        }
        ++k;
    }

    while( i < n1 )
    {
        array[l+k] = leftArr[i];
        ++i; ++k;
    }

    while( j < n2 )
    {
        array[l+k] = rightArr[j];
        ++j; ++k;
    }
}

template<typename T, std::enable_if_t<!std::is_class<T>::value>* = nullptr>
void mergeSort( T array[], const unsigned l, const unsigned r )
{
    if( l < r )
    {
        const unsigned m = l + (r - l)/2;
        mergeSort(array,l,m);
        mergeSort(array,m+1,r);

        merge(array,l,m,r);
    }
}

int main()
{
    // The check of count sort algorithm
    char charArray[] = "geeksforgeeks";
    std::cout << "Initial array\n";
    printArray(charArray,std::strlen(charArray));

    stableCountSort(charArray,std::strlen(charArray),charRange);

    std::cout << "Sorted array\n";
    printArray(charArray,std::strlen(charArray));


    unsigned unsignedArray[] = {1,6,8,6,7,9,1};
    std::cout << "Initial array\n";
    printArray(unsignedArray,7);

    stableCountSort(unsignedArray,7,unsignedRange);

    std::cout << "Sorted array\n";
    printArray(unsignedArray,7);

    // The check of merge sort algorithm
    int intArray[] = { 10, 0, -20, 30, 0, 50, 40, 60, -60, 30, -10 };
    std::cout << "Initial array\n";
    printArray(intArray,11);

    mergeSort(intArray,0,10);

    std::cout << "Sorted array\n";
    printArray(intArray,11);

    return 0;
}
