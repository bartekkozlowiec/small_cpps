#include <iostream>
#include <typeinfo>

using namespace std;

////////////////////////// Definicje klas ////////////////////////////

class A
{
    int skladnik;

public:
    A( int a = 0 ) : skladnik(a)
    { }
    int operator/( const A* in )
    {
        return (this->skladnik / in->skladnik );
    }

    int getsklad() const
    {
        return skladnik;
    }
};

class B : public A
{
    int dodatkowySkladnik;

public:
    B( int b = 0, int a = 0 ) : dodatkowySkladnik(b), A(a)
    { }

    int getdodsklad() const
    {
        return dodatkowySkladnik;
    }
};

/////////////////// Definicje funkcji globalnych /////////////////////

int operator*( const A& a1, const A& a2 )
{
    return (a1.getsklad() * a2.getsklad());
}

ostream& operator<<( ostream& ekran, A** tabwsk )
{
    ekran << "Zawartosc obiektow wskazywanych przez tablice wskaznikow: "
          << (*(tabwsk[0])).getsklad() << ", " << (*(tabwsk[1])).getsklad() << endl;
    return ekran;
}

ostream& operator<( ostream& ekran, A (*wsktab)[2] )
{
    ekran << "Zawartosc wskazywanej tablicy obiektow: " << ((*wsktab)[0]).getsklad()
          << ", " << ((*wsktab)[1]).getsklad() << endl;
    return ekran;
}

ostream& operator<=( ostream& ekran, A* tab )
{
    ekran << "Zawartosc tablicy obiektow: " << (tab[0]).getsklad()
          << ", " << (tab[1]).getsklad() << endl;
    return ekran;
}

int main()
{
    // Definicja dwoch obiektow: klasy A oraz klasy B
    A a(1);
    B b(2,3);
    cout << "Zawartosc obiektu a: " << a.getsklad() << endl;
    cout << "Zawartosc obiektu b: " << b.getsklad() << ", " << b.getdodsklad() << endl;

    // Wywolanie globalnego operatora* dla powyzszych obiektow
    // z wykorzystaniem konwersji standardowej B& -> A&
    cout << "a*b = " << a*b << endl;

    // Wywolanie operatora/ bedacego funkcja skladowa klasy A
    // z wykorzystaniem konwersji standardowej B* -> A*
    cout << "a/b = " << a/(&b) << endl;

    // Definicja dwuelementowej tablicy obiektow klasy A
    A Atab[] = { a, b };

    // Definicja tablicy wskaznikow do obiektow klasy A
    A* Atabwsk[2];

    // Inicjalizacja tablicy
    Atabwsk[0] = &a;
    Atabwsk[1] = &b;

    // Definicja wskaznika do dwuelementowej tablicy obiektow klasy A
    A (*Awsktab)[2];

    // Inicjalizacja wskaznika
    Awsktab = &Atab;

    // Wywolanie operatora <= dla Atab
    cout <= Atab;

    // Wywolanie operatora << dla Atabwsk
    cout << Atabwsk;

    // Wywolanie operatora < dla Awsktab
    cout < Awsktab;

    // Proba wywolania operatorow << i < dla tablicy wskaznikow i wskaznika
    // do tablicy klasy B
    cout << endl;
    B  Btab[] = { b, b };  // blad: { a, b }

    B* Btabwsk[2];
    Btabwsk[0] = &b; // blad: &a
    Btabwsk[1] = &b;

    B (*Bwsktab)[2];
    Bwsktab = &Btab;

    cout <= Btab; // Tu ciekawostka: w sumie BLAD, bo kolejno odczytywane sa inty z pierwszego obiektu tablicy
                  // tzn. kompilator przeskakuje do kolejnego el. tablicy o sizeof(A)
    //cout << Btabwsk;  blad!
    //cout < Bwsktab;   blad!

    return 0;
}
