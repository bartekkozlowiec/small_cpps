#include <iostream>
#include <iomanip>
#include <cstring>

using namespace std;

//////////////////////////////// Definicje klas //////////////////////////////////

class plynChlodniczy
{
    float temperatura, gestosc;
    int   wysokoscSlupa;

public:
    plynChlodniczy( float t = 50.0, float g = 1e3, int ws = 5 );
    float getTemp() { return temperatura; }
    float getDens() { return gestosc; }
    int   getColH() { return wysokoscSlupa; }
};

//////////////////////////////////////////////////////////////////////////////////

class samochod
{
public:
    int            rokProdukcji;
    plynChlodniczy plynCh;

    virtual void rzecznik( ostream& );
};

//////////////////////////////////////////////////////////////////////////////////

class mercedes : public samochod
{
public:
    char* model;

    mercedes( const char* init = "0" );
    mercedes( const mercedes& );
    mercedes& operator=( const mercedes& );
    ~mercedes();
    void rzecznik( ostream& );
};

/////////////////////////// Defnicje funkcji skladowych //////////////////////////

plynChlodniczy::plynChlodniczy( float t, float g, int ws )
{
    temperatura   = t;
    gestosc       = g;
    wysokoscSlupa = ws;
}

//////////////////////////////////////////////////////////////////////////////////

void samochod::rzecznik( ostream& strum )
{
    strum << rokProdukcji;
}

//////////////////////////////////////////////////////////////////////////////////

mercedes::mercedes( const char *init ) : model(new char[strlen(init)+1])
{
    strcpy(model,init);
}

//////////////////////////////////////////////////////////////////////////////////

mercedes::mercedes( const mercedes& wzor )
{
    model = new char[strlen(wzor.model)+1];
    strcpy(model,wzor.model);
}

//////////////////////////////////////////////////////////////////////////////////

mercedes& mercedes::operator=( const mercedes& wzor )
{
    if(this != &wzor)
    {
        delete this->model;
        model = strcpy(new char[strlen(wzor.model)+1],wzor.model);
    }
    return *this;
}

//////////////////////////////////////////////////////////////////////////////////

mercedes::~mercedes()
{
    delete [] model;
}

//////////////////////////////////////////////////////////////////////////////////

void mercedes::rzecznik( ostream& strum )
{
    samochod::rzecznik(strum);
    strum << " " << model;
}

/************************* Definicje funkcji globalnych *************************/

ostream& operator<<( ostream& strum, samochod& x )
{
    x.rzecznik(strum);
    return strum;
}

/******************************** Manipulatory **********************************/

ostream& tem( ostream& strum )
{
    strum << setprecision(1) << resetiosflags(ios::showpos) << setiosflags(ios::fixed)
          << resetiosflags(ios::scientific);
    return strum;
}

/********************************************************************************/

ostream& wys( ostream& strum )
{
    strum << setiosflags(ios::showpos);
    return strum;
}

/********************************************************************************/

ostream& ges( ostream& strum )
{
    strum << resetiosflags(ios::fixed) << setiosflags(ios::scientific)
          << setprecision(4);
    return strum;
}

/********************************************************************************/

int main()
{
    samochod a, b;

    a.rokProdukcji = 1990;
    b.rokProdukcji = 1992;

    mercedes m("sportowy");

    m.rokProdukcji = 1991;
    //m.model = "sportowy";

    cout << a << endl;
    cout << b << endl;
    cout << m << endl;

    // Stworzenie kopii obiektu m
    mercedes n = m;

    // Na co wskazuja adresy zawarte w polach "model"?
    cout << "m.model = " << (void*)(m.model) << ", n.model = " << (void*)(n.model) << endl << endl;

    // Prezentacja dzialania manipulatorow
    cout << "Parametry plynu chlodniczego obiektu mercedes:" << endl;
    cout << "temperatura = " << tem << m.plynCh.getTemp() << endl
         << "gestosc = " << ges << m.plynCh.getDens() << endl
         << "wysokosc slupa = " << wys << m.plynCh.getColH() << endl;

    cout << "Parametry plynu chlodniczego obiektu samochod:" << endl;
    cout << "temperatura = " << tem << a.plynCh.getTemp() << endl
         << "gestosc = " << ges << a.plynCh.getDens() << endl
         << "wysokosc slupa = " << wys << a.plynCh.getColH() << endl;

    return 0;
}
