#include <cstdio>

class Singleton
{
    Singleton() = default;

    static Singleton m_Instance;

public:
    static Singleton& Instance() { return m_Instance; }
    void MethodA() { printf("Method A called!\n"); }
    void MethodB() { printf("Method B called!\n"); }
};

int main()
{
    auto& s = Singleton::Instance();
    s.MethodA();

    //Singleton s1; // won't compile!

    return 0;
}
