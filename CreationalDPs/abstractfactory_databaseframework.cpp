// An example application which allows its clients to work with different kinds of databases
// with the help of the Abstract Factory design pattern. The Abstract Factory will serve as
// means of creating sets of classes (Connection, Command, ResultSet) which are needed for
// all the database types, but which need to be implemented in another way for each DB type.

#include <cstdio>
#include <string>
#include <vector>

// An interface for connection types
class Connection
{
    std::string m_connectionString;

public:
    void SetConnectionString(const std::string& connStr) { m_connectionString = connStr; }
    const std::string& GetConnectionString() const { return m_connectionString; }

    virtual void Open() = 0;
    virtual ~Connection() = default;
};

// An interface for accessing rows returned after performing a database query
class RecordSet
{
public:
    // The interface for getting the result row. In reality there would be multiple Get methods
    // each returning a different data type.
    virtual const std::string& Get() = 0;

    // The interface for telling if the next row is available
    virtual bool HasNext() = 0;

    virtual ~RecordSet() = default;
};

// An interface for database command types
class Command
{
    std::string m_commandString;

protected:
    Connection* m_pConnection;

public:
    void SetCommandString(const std::string& commStr) { m_commandString = commStr; }
    void SetConnection(Connection* const connection) { m_pConnection = connection; }
    const std::string& GetCommandString() const { return m_commandString; }
    Connection* GetConnection() const { return m_pConnection; }

    // The interface for database commands that do not return results
    virtual void ExecuteCommand() = 0;
    // Used to execute queries that return results
    virtual RecordSet* ExecuteQuery() = 0;

    virtual ~Command() = default;
};

// The implementations for SQL database follow.
class SqlConnection : public Connection
{
public:
    void Open() override { printf("[SqlConnection]: Connection opened.\n"); }
};

class SqlRecordSet : public RecordSet
{
    // Vector simulating database contents
    const std::vector<std::string> m_db {
        "Terminator",
        "End of Days",
        "The 6th Day",
        "Predator",
        "Eraser",
        "Expendables"
    };

    std::vector<std::string>::const_iterator m_cursor;

public:
    SqlRecordSet() { m_cursor = m_db.cbegin(); }
    const std::string& Get() override { return *m_cursor++; }
    bool HasNext() override { return m_cursor != m_db.cend(); }
};

class SqlCommand : public Command
{
public:
    void ExecuteCommand() override { printf("[SqlCommand]: Executing command on %s.\n",
                                            m_pConnection->GetConnectionString().c_str()); }

    // This method is actually a Factory Method as it creates an instance of another class.
    // Note that it is allowed to return a covariant child type instead of RecordSet as the
    // interface would suggest.
    SqlRecordSet* ExecuteQuery() override { printf("[SqlCommand]: Executing query.\n");
        return new SqlRecordSet{}; }
};

// The implementations for MySql database follow.
class MySqlConnection : public Connection
{
public:
    void Open() override { printf("[MySqlConnection]: Connection opened.\n"); }
};

class MySqlRecordSet : public RecordSet
{
    // Vector simulating database contents
    const std::vector<std::string> m_db {
        "Escape Plan",
        "Rambo",
        "Rocky Balboa",
        "Judge Dredd",
        "Demolition Man",
        "Cobra"
    };

    std::vector<std::string>::const_iterator m_cursor;

public:
    MySqlRecordSet() { m_cursor = m_db.cbegin(); }
    const std::string& Get() override { return *m_cursor++; }
    bool HasNext() override { return m_cursor != m_db.cend(); }
};

class MySqlCommand : public Command
{
public:
    void ExecuteCommand() override { printf("[MySqlCommand]: Executing command on: %s\n",
                                            m_pConnection->GetConnectionString().c_str()); }
    RecordSet* ExecuteQuery() override { printf("[MySqlCommand]: Executing query.\n");
        return new MySqlRecordSet{}; }
};

enum class DbTypes {
    sql,
    mysql
};

// The monostate class that acts as a Factory Method and separates the instantiation from the
// dependency on concrete types. It cannot ensure that the Connection and Command objects it
// instantiates are chosen from the same set/family of products.
class DbFactory
{
public:
    static Connection* CreateConnection(DbTypes dbType) {
        if (dbType == DbTypes::sql) return new SqlConnection{};
        if (dbType == DbTypes::mysql) return new MySqlConnection{};
        return nullptr;
    }
    static Command* CreateCommand(DbTypes dbType) {
        if (dbType == DbTypes::sql) return new SqlCommand{};
        if (dbType == DbTypes::mysql) return new MySqlCommand{};
        return nullptr;
    }
};

// This abstract factory class serves as an interface for ensuring the reduction of the class names
// from the instantiation process at the call site AND enforcing that all the objects needed to
// communicate with a database are from the same set/flavour.
class DbAbstractFactory
{
protected:
    virtual ~DbAbstractFactory() = default;

public:
    virtual Connection* CreateConnection() = 0;
    virtual Command* CreateCommand() = 0;
};

// The implementations of the abstract factory interface follow. The factories can be implemented
// as singletons to ensure the usage of only one set of products at a time.
class SqlFactory : public DbAbstractFactory
{
public:
    Connection* CreateConnection() override { return new SqlConnection{}; }
    Command* CreateCommand() override { return new SqlCommand{}; }
};

class MySqlFactory : public DbAbstractFactory
{
public:
    Connection* CreateConnection() override { return new MySqlConnection{}; }
    Command* CreateCommand() override { return new MySqlCommand{}; }
};

void CommonPartOfUsing(Connection* const pCon, Command* const pCmd)
{
    pCon->SetConnectionString("uid=bakz;db=movies;table=actors");
    pCon->Open();

    pCmd->SetConnection(pCon);
    pCmd->SetCommandString("select * from actors");

    auto* pRec = pCmd->ExecuteQuery();
    while (pRec->HasNext())
        printf("%s\n", pRec->Get().c_str());

    delete pRec;
    delete pCmd;
    delete pCon;
}

// An example working with different class sets with usage of macros (that can be defined in-place)
// Cons: the code gets littered with preprocessor statements and the choice of set must be made
// at compile time. Also, a user can still mix by mistake classes from different sets.
void UsingMacros()
{
#define SQL

#ifdef SQL
    auto* pCon = new SqlConnection{};
    auto* pCmd = new SqlCommand{};
#elif defined MySQL
    auto* pCon = new MySqlConnection{};
    auto* pCmd = new MySqlCommand{};
#endif

    CommonPartOfUsing(pCon, pCmd);
}

// The approach using conditional statements has the advantage of enabling the user to choose the
// type at runtime, but all other drawbacks remain.
void UsingConditionals(DbTypes dbType)
{
    Connection* pCon = nullptr;
    Command* pCmd = nullptr;

    if (dbType == DbTypes::sql) {
        pCon = new SqlConnection{};
        pCmd = new SqlCommand{};
    }
    else if (dbType == DbTypes::mysql) {
        pCon = new MySqlConnection{};
        pCmd = new MySqlCommand{};
    }

    CommonPartOfUsing(pCon, pCmd);
}

// Here, the code is no longer dependent on the names of the classes it instantiates. It is still
// possible however, that the user mixes classes from different sets during instantiation.
void UsingFactoryMethod(DbTypes dbType)
{
    auto* pCon = DbFactory::CreateConnection(dbType);
    auto* pCmd = DbFactory::CreateCommand(dbType);

    CommonPartOfUsing(pCon, pCmd);
}

// The optimal solution: the user cannot mistakenly mix classes from different sets and their
// instantiation is loosely coupled with their concrete types.
void UsingAbstractFactory(DbAbstractFactory* const pFactory)
{
    const auto pCon = pFactory->CreateConnection();
    const auto pCmd = pFactory->CreateCommand();

    CommonPartOfUsing(pCon, pCmd);
}

// The main driver function
int main()
{
    // The code that simulates querying a database of the desired type using one of the database
    // flavour (set of classes) defined above. Such usage of the two (or more) class flavours is
    // tedious and error-prone.
    /*printf("Solution 0: pure usage of the new operator\n");
    auto* pCon = new MySqlConnection{};
    pCon->SetConnectionString("uid=bakz;db=movies;table=actors");
    pCon->Open();

    auto* pCmd = new MySqlCommand{};
    pCmd->SetConnection(pCon);
    pCmd->SetCommandString("select * from actors");

    auto* pRec = pCmd->ExecuteQuery();
    while (pRec->HasNext())
        printf("%s\n", pRec->Get().c_str());*/

    // One remedy to the above: the usage of macros
    printf("\nSolution 1: usage of preprocessor macros\n");
    UsingMacros();

    // Another naive attempt: use conditional statements
    printf("\nSolution 2: usage of conditional statements\n");
    UsingConditionals(DbTypes::mysql);

    // Smarter: take advantage of the Factory Method design pattern
    printf("\nSolution 3: usage of Factory Method DP\n");
    UsingFactoryMethod(DbTypes::sql);

    // Ultimate: use the Abstract Factory DB and tackle all issues
    printf("\nSolution 4: usage of Abstract Factory DP\n");
    MySqlFactory factory{};
    UsingAbstractFactory(&factory);

    return 0;
}
