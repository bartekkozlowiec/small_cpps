// A simple mocking application of a shooter-game with missile objects cached in an object pool

#include <cstdio>
#include <thread>
#include <chrono>
#include <memory>
#include <vector>

class Missile
{
    // When a missile is created, it's visible by default.
    bool m_isVisible{true};

public:
    Missile() { printf("+++++ Missile created +++++\n"); }
    ~Missile() { printf("~~~~~ Missile destroyed ~~~~~\n"); }

    void SetVisible(bool visible) { m_isVisible = visible; }
    bool IsVisible() const { return m_isVisible; }
    void Update() { printf("-> "); }
};

using MissilePtr = std::shared_ptr<Missile>;
static std::vector<MissilePtr> missiles{}; // static as not intended to be used outside this TU

// Object pool - the remedy of the repetitive creation and destruction of missiles
class MissilePool
{
    // Inline member variables are a C++17 feature
    inline static std::vector<MissilePtr> s_missiles{};
    MissilePool() = default;

public:
    // Monostate implementation as the pool class should have only one global instance.
    static MissilePtr AcquireMissile();
    static void ReleaseMissile(const MissilePtr& missile);
};

// If a missile becomes invisible (e.g. goes out of game view or hits something), it can
// be reused so can be acquired from the pool.
MissilePtr MissilePool::AcquireMissile()
{
    for (auto& missile : s_missiles) {
        if (!missile->IsVisible()) {
            missile->SetVisible(true);
            printf("[POOL] Acquiring an existing instance\n");
            return missile;
        }
    }

    printf("[POOL] Creating a new instance\n");
    auto missile = std::make_shared<Missile>();
    s_missiles.push_back(missile);
    return missile;
}

// When releasing a missile, we search for it in the pool and make it invisible so that
// it can be required later.
void MissilePool::ReleaseMissile(const MissilePtr& missile)
{
    for (auto& m : s_missiles)
        if (m == missile)
            m->SetVisible(false);
}

// Firing involves creating objects on the heap; this alone can be expensive, however, repetitive
// heap allocation and deallocation (in calls to Explode) may cause the heap to be fragmented,
// decreasing performance even further.
void Fire()
{
    //missiles.push_back(std::make_shared<Missile>());
    //missiles.push_back(std::make_shared<Missile>());

    // The solution - acquiring missiles from the pool instance
    missiles.push_back(MissilePool::AcquireMissile());
    missiles.push_back(MissilePool::AcquireMissile());
}

void Animate()
{
    for (auto& m : missiles)
        m->Update();
}

void Explode()
{
    using namespace std;

    printf("X\n");
    for (auto& m : missiles)
        // The following is equal to releasing a missile via MissilePool::ReleaseMissile.
        //m->SetVisible(false);
        MissilePool::ReleaseMissile(m);

    missiles.clear();
    std::this_thread::sleep_for(1s);
    printf("\n\n");
}

void GameLoop()
{
    using namespace std::literals;
    unsigned counter{};

    while (true) {
        ++counter;
        if (counter == 1) {
            Fire();
        }
        if (counter >= 1 && counter <= 5) {
            Animate();
        }
        if (counter > 5) {
            Explode();
            counter = 0;
        }

        fflush(stdout);
        // Enforce a delay for animation purposes; the second literal used is a C++14 feature
        std::this_thread::sleep_for(1s);
    }
}

int main()
{
    GameLoop();
}
