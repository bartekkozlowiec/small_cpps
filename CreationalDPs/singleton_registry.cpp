#include <cstdio>
#include <string>
#include <unordered_map>
#include <exception>
#include <mutex>

class Printer
{
protected:
    Printer() = default;
    virtual ~Printer() = default;

public:
    Printer(const Printer&) = delete;
    Printer& operator=(const Printer&) = delete;

    virtual void Print(const std::string&) = 0;
};

class LocalPrinter : public Printer
{
    static LocalPrinter m_Instance;
    LocalPrinter();

public:
    // The following are no longer required if LocalPrinter inherits from Printer
    //LocalPrinter(const LocalPrinter&) = delete;
    //LocalPrinter& operator=(const LocalPrinter&) = delete;

    static LocalPrinter& EagerInstance() { return m_Instance; }
    ~LocalPrinter() override = default;

    void Print(const std::string& text) override { printf("[LOCALPRINTER]: %s\n", text.c_str()); }
};

LocalPrinter LocalPrinter::m_Instance;

class PDFPrinter : public Printer
{
    PDFPrinter();

public:
    static PDFPrinter& MeyersInstance() {
        static PDFPrinter instance;
        return instance;
    }

    void Print(const std::string& text) override { printf("[PDFPRINTER]: %s\n", text.c_str()); }
};

// The remedy to the open-close principle violation: a provider class, implemented as Monostate,
// which stores a map with singular instanes of the printer classes. The responsibility of creating
// instances of Printer class children is given to the PrinterProvider.
// The pattern is called: Registry of singletons or Multiton.

class PrinterProvider
{
    inline static std::unordered_map<std::string, Printer*> m_Printers;
    inline static std::recursive_mutex m_Mtx;

    PrinterProvider() = default;

public:
    static void RegisterPrinter(const std::string& key, Printer* p) {
        std::lock_guard<std::recursive_mutex> lg{m_Mtx};

        if (m_Printers.find(key) == std::end(m_Printers))
            m_Printers[key] = p;
        else printf("Already registered.\n");
    }

    // In order to make the lazy singleton instance to work, the following
    // GetPrinter* methods would have to also create the instance in the
    // map, which would create a tight coupling between the PrinterProvider
    // and the Printer classes and violate the open-closed principle

    static Printer* GetPrinterPtr(const std::string& key) {
        std::unique_lock<std::recursive_mutex> lg{m_Mtx};

        if (m_Printers.find(key) != std::end(m_Printers))
            return m_Printers.at(key);
        else return nullptr;
    }

    static Printer& GetPrinterRef(const std::string& key) {
        std::lock_guard<std::recursive_mutex> lg{m_Mtx};

        auto p = GetPrinterPtr(key);
        if (p) return *p;
        throw std::runtime_error("No such printer!");
    }
};

LocalPrinter::LocalPrinter() {
    PrinterProvider::RegisterPrinter("local", this);
}

PDFPrinter::PDFPrinter() {
    PrinterProvider::RegisterPrinter("pdf", this);
}

void PrintSales() {
    PrinterProvider::GetPrinterRef("local").Print("Sales data");
}

// The main driver function
int main()
{
    auto lp = PrinterProvider::GetPrinterPtr("local");
    if (lp) lp->Print("Printing data to local printer");

    // This won't work for now as lazy instance has not been created yet!
    // PDFPrinter class doesn't contain a static instance, therefore its constructor
    // doesn't get called anywhere.
    auto pp = PrinterProvider::GetPrinterPtr("pdf");
    if (pp) pp->Print("Printing data to PDF printer");

    PrintSales();

    return 0;
}
