// THE GOAL: implement a creator class which would be able to handle multiple product
// classes depending on the user choice without having to add a corresponding creator
// subclass for each new product subclass.

#include <cstdio>
#include <memory>
#include <string>

class Document
{
public:
    virtual void Read() = 0;
    virtual void Write() = 0;
    virtual ~Document() = default;
};

using DocumentPtr = std::unique_ptr<Document>;

class TextDocument : public Document
{
public:
    void Read() override { printf("%s\n", __PRETTY_FUNCTION__); }
    void Write() override { printf("%s\n", __PRETTY_FUNCTION__); }
};

class SpreadsheetDocument : public Document
{
public:
    void Read() override { printf("%s\n", __PRETTY_FUNCTION__); }
    void Write() override { printf("%s\n", __PRETTY_FUNCTION__); }
};

// The parametrized factory class - the product of the creator class will depend on the parameter
// passed to the factory method. The parametrized factory is implemented as a monostate.
class DocumentFactory
{
public:
    // The type of the parameter chosen here is string
    static DocumentPtr Create(const std::string& type) {
        if (type == "text") return std::make_unique<TextDocument>();
        else if (type == "spreadsheet") return std::make_unique<SpreadsheetDocument>();
        return nullptr;
    }
};

class Application
{
    DocumentPtr m_pDocument = nullptr;

public:
    // In the following function had we used the operator new with the TextDocument class
    // constructor, the Application class would get tightly coupled with the TextDocument
    // concrete class! What if we want to use the Applcation with a diffrent kind of Document?
    void New() { m_pDocument = DocumentFactory::Create("text"); }
    void Open() { if (!m_pDocument) m_pDocument = DocumentFactory::Create("text"); m_pDocument->Read(); }
    void Save() { if (!m_pDocument) m_pDocument = DocumentFactory::Create("text"); m_pDocument->Write(); }
};

int main()
{
    Application app;
    app.Open();
    app.Save();

    return 0;
}
