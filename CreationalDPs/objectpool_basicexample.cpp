// Basic implementation of a class for creating limited number of objects that can be
// handed over to the user entities.

#include <cstdio>
#include <vector>
#include <memory>

#define printName printf("%s\n", __PRETTY_FUNCTION__);

class SharedObject
{
    bool m_isUsed = true;

    SharedObject() = default;
    friend class ObjectPool;

public:
    bool IsUsed() const { return m_isUsed; }
    void SetUsedState(bool state) { m_isUsed = state; }
    void MethodA() { printName }
    void MethodB() { printName }
    void Reset() { printf("Resetting state...\n"); }
};

// This class should have only one instance, so it may be implemented as a Singleton
// or a Monostate.
class ObjectPool
{
    inline static std::unique_ptr<ObjectPool> s_pInstance{};
    inline static std::vector<std::shared_ptr<SharedObject>> s_PooledObjects{};

public:
    static ObjectPool& LazyInstance();

    // A method for acquiring a SharedOjbect instance
    std::shared_ptr<SharedObject> AcquireObject();

    // A method for releasing a SharedObject instance and returning it to the pool
    void ReleaseObject(std::shared_ptr<SharedObject> const& pSO);
};

ObjectPool& ObjectPool::LazyInstance()
{
    if (!s_pInstance)
        s_pInstance.reset(new ObjectPool);

    return *s_pInstance;
}

std::shared_ptr<SharedObject> ObjectPool::AcquireObject()
{
    for (auto& so : s_PooledObjects) {
        if (!so->IsUsed()) {
            printf("[POOL]: Returning an existing ojbect\n");
            so->SetUsedState(true);
            so->Reset();
            return so;
        }
    }

    printf("[POOL]: Creating a new object\n");
    // Cannot use make_shared below, as SharedObject's constructor is declared private.
    std::shared_ptr<SharedObject> so{new SharedObject};
    s_PooledObjects.push_back(so);
    return so;
}

void ObjectPool::ReleaseObject(std::shared_ptr<SharedObject> const& pSO)
{
    for (auto& so : s_PooledObjects)
        if (so == pSO)
            so->SetUsedState(false); // optionally reset the state here instead of in AcquireObject
}

int main()
{
    auto& op = ObjectPool::LazyInstance();

    auto s1 = op.AcquireObject();
    s1->MethodA();
    s1->MethodB();

    auto s2 = op.AcquireObject();
    s2->MethodA();
    s2->MethodB();

    op.ReleaseObject(s1);

    // Now the s3 pointer should get access to an existing object
    auto s3 = op.AcquireObject();
    s3->MethodA();

    return 0;
}
