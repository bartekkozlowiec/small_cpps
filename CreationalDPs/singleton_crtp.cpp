#include <cstdio>
#include <thread>

// The CRTP base class implementing the singleton feature
template <typename T>
class BaseSingleton
{
protected:
    BaseSingleton() = default;
    BaseSingleton(const BaseSingleton&) = delete;
    BaseSingleton& operator=(const BaseSingleton&) = delete;

public:
    static T& MeyersInstance() {
        static T instance;
        return instance;
    }
};

// The following macro is designed to make the use of the CRTP base class easier in the children classes
#define MAKE_SINGLETON(class_name)private: class_name() { printf("%s\n", __PRETTY_FUNCTION__); }\
friend class BaseSingleton<class_name>;

class GameManager : public BaseSingleton<GameManager>
{
private:
    // The following two declarations are important as we want instances of GameManager to be
    // created by the CRTP base class
    GameManager() { printf("%s\n", __PRETTY_FUNCTION__); }
    friend class BaseSingleton<GameManager>;

public:
    void LoadScenario() { printf("%s\n", __PRETTY_FUNCTION__); }
    void Run() { printf("%s\n", __PRETTY_FUNCTION__); }
};

class GameLoader : public BaseSingleton<GameLoader>
{
    MAKE_SINGLETON(GameLoader)

public:
    void LoadAssets() { printf("%s\n", __PRETTY_FUNCTION__); }
};

int main()
{
    auto t1 = std::thread([]() {
        auto& gl1 = GameLoader::MeyersInstance();
        gl1.LoadAssets();

        auto& gm1 = GameManager::MeyersInstance();
        gm1.LoadScenario();
    });

    auto& gl = GameLoader::MeyersInstance();
    gl.LoadAssets();

    auto& gm = GameManager::MeyersInstance();
    gm.Run();

    t1.join();

    return 0;
}
