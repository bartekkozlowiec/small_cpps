// THE GOAL: create an application represented by the Application class which will be able
// to work with any kind of user-defined Document class instance.

#include <cstdio>
#include <memory>

class Document
{
public:
    virtual void Read() = 0;
    virtual void Write() = 0;
    virtual ~Document() = default;
};

using DocumentPtr = std::unique_ptr<Document>;

class TextDocument : public Document
{
public:
    void Read() override { printf("%s\n", __PRETTY_FUNCTION__); }
    void Write() override { printf("%s\n", __PRETTY_FUNCTION__); }
};

class SpreadsheetDocument : public Document
{
public:
    void Read() override { printf("%s\n", __PRETTY_FUNCTION__); }
    void Write() override { printf("%s\n", __PRETTY_FUNCTION__); }
};

class Application
{
    DocumentPtr m_pDocument = nullptr;

public:
    // In the following function had we used the operator new with the TextDocument class
    // constructor, the Application class would get tightly coupled with the TextDocument
    // concrete class! What if we want to use the Applcation with a diffrent kind of Document?
    void New() { m_pDocument = Create(); }
    void Open() { if (!m_pDocument) m_pDocument = Create(); m_pDocument->Read(); }
    void Save() { if (!m_pDocument) m_pDocument = Create(); m_pDocument->Write(); }

    // The factory method defers the instantiation process to the child classes
    virtual DocumentPtr Create() { return nullptr; }
};

class TextApplication : public Application
{
    DocumentPtr Create() override { return std::make_unique<TextDocument>(); }
};

class SpreadsheetApplication : public Application
{
    DocumentPtr Create() override { return std::make_unique<SpreadsheetDocument>(); }
};

int main()
{
    TextApplication appT;
    appT.New();
    appT.Open();
    appT.Save();

    std::unique_ptr<Application> appS = std::make_unique<SpreadsheetApplication>();
    appS->Open();
    appS->Save();

    return 0;
}
