// THE PATTERN'S GOAL: define an interface for creating an object, but let classes
// decide which class to instantiate.
// The Factory Method pattern lets the class defer instantiation to its subclasses.
// The pattern is sometimes called as virtual constructor.
// A drawback of this design pattern is that every time you add a concrete product class,
// you also have to implement a corresponding creator class.

#include <cstdio>

class Product
{
protected:
    virtual ~Product() = default;

public:
    virtual void Operation() = 0;
};

class ConcreteProduct : public Product
{
    void Operation() override { printf("%s\n", __PRETTY_FUNCTION__); }
    ~ConcreteProduct() override = default;
};

class ConcreteProduct2 : public Product
{
    void Operation() override { printf("%s\n", __PRETTY_FUNCTION__); }
    ~ConcreteProduct2() override = default;
};

class Creator
{
    Product* m_pProduct;

public:
    // The method that needs to instantiate the product object.
    void AnOperation() {
        // What if we want to create an instance of ConcreteProduct2 instead?
        // In a naive implementation you could simply call the constructor of
        // the other child class. This would, however, mean making modifications
        // to the existing code.
        //m_pProduct = new ConcreteProduct;

        // Using the factory method here would also allow to get rid of including
        // the concrete class header file in case the project is divided into modules.
        m_pProduct = Create();
        m_pProduct->Operation();
    }

protected:
    // The factory method comes to play here; can be pure virtual, but this would require
    // introducing a child class for Creator.
    virtual Product* Create() const /* = 0*/ { return nullptr; }
};

// The Creator class defers the instantiation process of the product member variable
// to its subclasses.
class ConcreteCreator : public Creator
{
    Product* Create() const override { return new ConcreteProduct; }
};

class ConcreteCreator2 : public Creator
{
    Product* Create() const override { return new ConcreteProduct2; }
};

int main()
{
    ConcreteCreator ct;
    ct.AnOperation();

    // Now: if we use class hierarchy that utilizes the factory method, we can simply
    // instantiate a different Creator child class and use it to get the appropriate Product.
    ConcreteCreator2 ct2;
    ct2.AnOperation();

    return 0;
}
