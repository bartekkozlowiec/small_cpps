#include <cstdio>
#include <string>
#define _CRT_SECURE_NO_WARNINGS

class Logger
{
    FILE* m_pStream;
    std::string m_FileName;
    std::string m_Tag;

    Logger(std::string fileName = "applog.txt");
    static Logger m_Instance;

public:
    ~Logger();
    static Logger& Instance() { return m_Instance; }

    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;

    void WriteLog(const char* pMessage);
    void SetTag(const char* pTag);
};

Logger::Logger(std::string fileName) : m_FileName(std::move(fileName)) {
    m_pStream = fopen(m_FileName.c_str(), "w");
}

Logger::~Logger() {
    fclose(m_pStream);
}

void Logger::WriteLog(const char* pMessage) {
    fprintf(m_pStream, "[%s]: %s\n", m_Tag.c_str(), pMessage);
    fflush(m_pStream);
}

void Logger::SetTag(const char* pTag) {
    m_Tag = pTag;
}

void OpenConnection() {
    auto& lg = Logger::Instance();
    lg.WriteLog("Attempting to open a connection...\n");
}

Logger Logger::m_Instance;

int main() {
    auto& lg = Logger::Instance();
    lg.SetTag("192.168.1.101");
    lg.WriteLog("Application has started.");
    OpenConnection();
    lg.WriteLog("Application is shutting down.");

    return 0;
}
