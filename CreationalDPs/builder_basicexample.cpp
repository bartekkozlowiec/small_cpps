// Basic implementation of the Builder design pattern - an interface which allows the client
// to separate the construction process from the product class whose instance is to be created.
// The use case for the pattern is when the construction is complex (depending on the desired
// state of the object being constructed this would require a constructor bonanza if the Builder
// pattern was to be omitted) and/or needs to be performed in steps.

#include <cstdio>

class Builder
{
public:
    virtual void BuildPart() = 0;
    virtual ~Builder() = default;
};

// The class whose construction process is complex or whose instances may need to have different states
class Product
{};

class ConcreteBuilder : public Builder
{
    Product* m_pProduct;

public:
    ConcreteBuilder() { printf("[ConcreteBuilder]: Created\n"); }
    ~ConcreteBuilder() override { delete m_pProduct; }
    void BuildPart() override;
    Product* GetResult(); // the method which returns the constructed instance
};

void ConcreteBuilder::BuildPart() {
    printf("[ConcreteBuilder]: Building...\n");

    // Simulation of multiple construction steps
    printf("\tPart A\n");
    printf("\tPart B\n");
    printf("\tPart C\n");

    m_pProduct = new Product;
}

Product* ConcreteBuilder::GetResult() {
    printf("[ConcreteBuilder]: Returning result...\n");
    return m_pProduct;
}

// Director - the class which has control over the Builder instance's construction process
// If a new type of the product class is needed, the director won't change, but the client
// will need a new concrete builder implementation.
class Director
{
    Builder* m_pBuilder;

public:
    Director(Builder* builder) : m_pBuilder{builder} { printf("[Director]: Created\n"); }
    void Construct();
};

void Director::Construct() {
    printf("[Director]: Construction process started\n");
    m_pBuilder->BuildPart();
}

// The main driver code
int main()
{
    ConcreteBuilder builder;
    Director director(&builder);
    director.Construct();

    auto* product = builder.GetResult();

    return 0;
}
