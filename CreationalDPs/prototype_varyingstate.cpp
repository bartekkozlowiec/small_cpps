// A simple demonstration of a "rider on a busy road" game with multiple vehicles being
// spawned using the Prototype design pattern.

#include <cstdio>
#include <chrono>
#include <memory>
#include <random>
#include <string>
#include <string_view>
#include <thread>
#include <unordered_map>
#include <vector>

// The base animation class, responsible for "loading" animations for different types of vehicles
// from a file
class Animation
{
    std::string m_animationData{};

public:
    Animation() = default;
    // std::string_view is a C++17 feature
    Animation(std::string_view animationFileName);

    const std::string& GetAnimationData() const { return m_animationData; }
    void SetAnimationData(const std::string& animationData) { m_animationData = animationData; }
};

// The constructor which simulates loading of the data from a file
Animation::Animation(std::string_view animationFileName)
{
    // Chrono literals are a C++14 feature
    using namespace std::chrono_literals;

    // Loading operation is costly and always takes a fair amount of time
    printf("[Animation]: Loading %s ", animationFileName.data());
    for (size_t i=0; i<10; ++i) {
        printf(".");
        fflush(stdout);
        std::this_thread::sleep_for(200ms);
    }
    printf("\n");
    m_animationData.assign("^^^^^^");
}

// The struct describing the position of a vehicle
struct Position
{
    int x;
    int y;
    void print() const { printf("(%d, %d)\n", x, y); }
};

// The base vehicle interface
class Vehicle;
using VehiclePtr = std::shared_ptr<Vehicle>;

class Vehicle
{
    int m_speed{};
    int m_hitPoints{};
    std::string m_name;
    Animation* m_pAnimation{};
    Position m_position;
    std::string m_color; // this property allows to differentiate between the classes that would otherwise
                         // need to be split into e.g. RedCar, GreenCar etc.

public:
    Vehicle() { m_pAnimation = new Animation; } // Create a default instance
    Vehicle(int speed, int hps, const std::string& name, std::string_view animFile, const Position& position,
            const std::string& color) :
        m_speed{speed}, m_hitPoints{hps}, m_name{name}, m_position{position}, m_color{color} {
        m_pAnimation = new Animation(animFile);
    }
    // If we rely on the default implementation of copy-constructor, we'll get a shallow copy of m_pAnimation
    // member variable. Hence, we stick to the rule of 5 which says that a class with ownership semantics should
    // have user-defined following 5 methods:
    //Vehicle(const Vehicle& other) = default;
    Vehicle(const Vehicle& other);
    Vehicle& operator=(const Vehicle& other);
    Vehicle(Vehicle&& other) noexcept;
    Vehicle& operator=(Vehicle&& other) noexcept;
    virtual ~Vehicle() { delete m_pAnimation; }

    int GetSpeed() const { return m_speed; }
    int GetHitPoints() const { return m_hitPoints; }
    Position GetPosition() const { return m_position; }
    const std::string& GetName() const { return m_name; }
    const std::string& GetColor() const { return m_color; }
    const std::string& GetAnimation() const { return m_pAnimation->GetAnimationData(); }

    void SetSpeed(const int speed) { m_speed = speed; }
    void SetHitPoints(const int hps) { m_hitPoints = hps; }
    void SetPosition(const Position& position) { m_position = position; }
    void SetName(const std::string& name) { m_name = name; }
    void SetColor(const std::string& color) { m_color = color; }
    void SetAnimationData(const std::string& animData) { m_pAnimation->SetAnimationData(animData); }

    // This virtual function will be called by the game engine for all the vehicles; all the vehicle-related
    // activity will be implemented inside.
    virtual void Update() = 0;

    // The Prototype design pattern - cloning functionality (a virtual copy constructor) which speeds up the
    // creation process of Vehicle instances. It also allows to copy class instances and change their state to
    // constitute an object which would otherwise require a separate class ("blue" Bus object vs BlueBus instance).
    virtual VehiclePtr Clone() = 0;
};

Vehicle::Vehicle(const Vehicle& other) : m_speed{other.m_speed}, m_hitPoints{other.m_hitPoints},
    m_name{other.m_name}, m_pAnimation{new Animation{}}, m_position{other.m_position},
    m_color{other.m_color} {
    m_pAnimation->SetAnimationData(other.GetAnimation());
}

Vehicle& Vehicle::operator=(const Vehicle& other) {
    if (this != &other) {
        m_speed = other.m_speed;
        m_hitPoints = other.m_hitPoints;
        m_name = other.m_name;
        m_pAnimation->SetAnimationData(other.GetAnimation());
        m_position = other.m_position;
        m_color = other.m_color;
    }
    return *this;
}

Vehicle::Vehicle(Vehicle&& other) noexcept : m_speed{other.m_speed}, m_hitPoints{other.m_hitPoints},
    m_name{std::move(other.m_name)}, m_pAnimation{other.m_pAnimation}, m_position{other.m_position} {
    other.m_pAnimation = nullptr;
    other.m_position = {0, 0};
    other.m_hitPoints = 0;
    other.m_speed = 0;
    other.m_name.clear();
}

Vehicle& Vehicle::operator=(Vehicle&& other) noexcept {
    if (this != &other) {
        m_speed = other.m_speed;
        m_hitPoints = other.m_hitPoints;
        m_name = std::move(other.m_name);
        delete m_pAnimation; // delete the current resource
        m_pAnimation = other.m_pAnimation;
        m_position = other.m_position;
    }
    return *this;
}

// The Vehicle types implementations follow. Since they have no user-defined constructors, the compiler will
// automatically synthesize the methods from the Rule of 5 for them. Those default methods will be sufficient
// as the child classes don't have any additional ownership semantics.

class Car : public Vehicle
{
    using Vehicle::Vehicle;

    // Member variables enabling a RedCar object to temporarily increase its speed
    float m_speedFactor{1.5f}; // the factor of speed
    std::default_random_engine m_engine{100}; // the seed value
    std::bernoulli_distribution m_dist{.5}; // the probability of generating a true Boolean value

    VehiclePtr Clone() override { printf("Cloning: %s\n", GetName().c_str()); return VehiclePtr(new Car(*this)); }

public:
    void SetSpeedFactor(const float factor) { m_speedFactor = factor; }
    void Update() override;
};

void Car::Update() {
    printf("[%s %s]\n\tAnimation: %s",
           GetColor().c_str(), GetName().c_str(), GetAnimation().c_str());

    if (m_dist(m_engine)) // if a random Boolean is true
        printf("\n\tIncrease speed temporarily to: %f", GetSpeed() * m_speedFactor);
    else
        printf("\n\tSpeed: %d", GetSpeed());

    printf("\n\tHitPoints: %d\n\tPosition: ",
           GetHitPoints()); GetPosition().print();
}

class Bus : public Vehicle
{
    using Vehicle::Vehicle;

    // Member variables enabling a BlueBus object to randomly move out of the way when the player honks
    std::default_random_engine m_engine{500};
    std::bernoulli_distribution m_dist{.25}; // yellow bus is less likely to move out of the way

    VehiclePtr Clone() override { printf("Cloning: %s\n", GetName().c_str()); return VehiclePtr(new Bus(*this)); }

public:
    void Update() override;
};

void Bus::Update() {
    printf("[%s %s]\n\tAnimation: %s",
           GetColor().c_str(), GetName().c_str(), GetAnimation().c_str());

    if (GetColor() == "Red" && m_dist(m_engine)) // if a random Boolean is true
        printf("\n\tMoving out of the way...");

    printf("\n\tSpeed: %d\n\tHitPoints: %d\n\tPosition: ",
           GetSpeed(), GetHitPoints()); GetPosition().print();
}

// Optimization: instead of creating a set of instances of Vehicle descendants, we create one instance of Car
// and one instance of Bus at the beginning of the program, store them in a manager class and later clone the
// stored prototypes according to our needs.
class VehiclePrototypes
{
    // Inline static member variables are a C++17 feature
    inline static std::unordered_map<std::string,VehiclePtr> m_prototypes;

    // Prevent the instantiation - the class acts as a Monostate
    VehiclePrototypes() = default;

public:
    // The application needs to have a way of registering and de-registering the prototype instances.
    static void RegisterPrototype(const std::string& key, VehiclePtr prototype);
    static VehiclePtr DeregisterPrototype(const std::string& key);

    static VehiclePtr GetPrototype(const std::string& key);

    // The method for allowing the client to enumerate and find out how many prototypes are available
    static std::vector<std::string> GetKeys();
};

std::vector<std::string> VehiclePrototypes::GetKeys()
{
    std::vector<std::string> keys;
    keys.reserve(m_prototypes.size());

    for (const auto& kv : m_prototypes)
        keys.push_back(kv.first);

    return keys;
}

void VehiclePrototypes::RegisterPrototype(const std::string& key, VehiclePtr prototype)
{
    // If-initialization statements are a C++17 feature
    if (auto it = m_prototypes.find(key); it == std::end(m_prototypes))
        m_prototypes[key] = prototype;
    else printf("Key already exists!\n");
}

VehiclePtr VehiclePrototypes::DeregisterPrototype(const std::string& key)
{
    if (auto it = m_prototypes.find(key); it != std::end(m_prototypes)) {
        auto vehicle = m_prototypes.at(key);
        m_prototypes.erase(key);
        return vehicle;
    }
    return nullptr;
}

VehiclePtr VehiclePrototypes::GetPrototype(const std::string& key)
{
    if (auto it = m_prototypes.find(key); it != std::end(m_prototypes))
        return m_prototypes.at(key)->Clone(); // be sure to clone and not return the prototype itself!

    return nullptr;
}

// The GameManager class is the class which calls Update overridden functions on all the vehicle objects
class GameManager
{
    std::vector<VehiclePtr> m_vehicles;

public:
    // The method containing the game loop
    void Run();
};

// The Run method in its most naive implementation is tightly coupled with the vehicle implementation classes
// To counter this problem, we can use the Parametrized Factory Method design pattern. Here, we implement the
// factory method as a global function as no state preservation is needed.
VehiclePtr Create(std::string_view type,
                int speed, int hps,
                const std::string& name,
                std::string_view animFile,
                const Position& position)
{
    if (type == "redcar")
        return VehiclePtr(new Car{speed, hps, name, animFile, position, "Red"});
    if (type == "greencar")
        return VehiclePtr(new Car{speed, hps, name, animFile, position, "Green"});
    if (type == "yellowbus")
        return VehiclePtr(new Bus{speed, hps, name, animFile, position, "Yellow"});
    if (type == "bluebus")
        return VehiclePtr(new Bus{speed, hps, name, animFile, position, "Blue"});
    return nullptr;
}

// GLobal functions returning registered prototypes and adding desired flavour to them
VehiclePtr GetRedCar()
{
    auto vehicle = VehiclePrototypes::GetPrototype("car");
    vehicle->SetColor("Red");
    vehicle->SetHitPoints(10);
    vehicle->SetSpeed(30);
    vehicle->SetPosition({0, 0});
    Animation anim("red.anim");
    vehicle->SetAnimationData(anim.GetAnimationData());
    return vehicle;
}

VehiclePtr GetGreenCar()
{
    auto vehicle = VehiclePrototypes::GetPrototype("car");
    vehicle->SetColor("Green");
    vehicle->SetHitPoints(5);
    vehicle->SetSpeed(30);
    vehicle->SetPosition({100, 0});
    Animation anim("green.anim");
    vehicle->SetAnimationData(anim.GetAnimationData());
    return vehicle;
}

VehiclePtr GetYellowBus()
{
    auto vehicle = VehiclePrototypes::GetPrototype("bus");
    vehicle->SetColor("Yellow");
    vehicle->SetHitPoints(10);
    vehicle->SetSpeed(25);
    vehicle->SetPosition({100, 200});
    Animation anim("ybus.anim");
    vehicle->SetAnimationData(anim.GetAnimationData());
    return vehicle;
}

VehiclePtr GetBlueBus()
{
    auto vehicle = VehiclePrototypes::GetPrototype("bus");
    vehicle->SetColor("Blue");
    vehicle->SetHitPoints(25);
    vehicle->SetSpeed(25);
    vehicle->SetPosition({200, 200});
    Animation anim("bbus.anim");
    vehicle->SetAnimationData(anim.GetAnimationData());
    return vehicle;
}

void GameManager::Run() {
    m_vehicles.push_back(GetRedCar());
    m_vehicles.push_back(GetGreenCar());
    m_vehicles.push_back(GetYellowBus());
    m_vehicles.push_back(GetBlueBus());

    using namespace std::literals;

    size_t count = 5;
    while (count != 0) {
        std::this_thread::sleep_for(1s);
        std::system("cls"); // clearing the output screen
        for (auto vehicle : m_vehicles)
            vehicle->Update();

        // Simulating the creation of new vehicles as the player proceeds through the level
        if (count == 2)
            m_vehicles.push_back(Create("redcar", 30, 15, "Car", "red.anim", {50, 50}));
        else if (count == 3) {
            //m_vehicles.push_back(Create("yellowbus", 20, 20, "YellowBus", "ybus.anim", {150, 250}));
            // Create operation is always costly due to loading of an animation. To overcome this issue we would like
            // to utilize cloning.
            // Straight usage of copy constructor would take as back again to tight coupling between GameManager and
            // vehicle implementation classes and would also require a dynamic type cast:
            //RedCar rc{*m_vehicles[0]};

            // The proper solution to the problem of excessive object creation - usage of overridden "copy constructor"
            auto vehicle = m_vehicles[2]->Clone();
            vehicle->SetPosition({150, 250});
            vehicle->SetSpeed(10);
            m_vehicles.push_back(vehicle);
        }

        --count;
    }
}

// The main driver function
int main()
{
    // Before using the prototype manager class we need to register the prototypes.
    VehiclePrototypes::RegisterPrototype("car", std::make_shared<Car>());
    VehiclePrototypes::RegisterPrototype("bus", std::make_shared<Bus>());
    printf("Number of prototypes stored: %lu\n", VehiclePrototypes::GetKeys().size());

    GameManager mgr;
    mgr.Run();

    return 0;
}
