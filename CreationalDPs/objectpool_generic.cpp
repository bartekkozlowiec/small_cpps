// A simple application showing an example use of objects cached in a generic object pool

#include <cstdio>
#include <vector>
#include <limits>

// The default allocator/deallocator class for allowing the pool's clients to instantiate objects
// without calling new operator on generic types
template <typename T>
class DefaultAllocator
{
public:
    T* operator()() { return new T{}; } // Do we need the object class to be default-constructible?
    void operator()(const T* ptr) { delete ptr; }

    // This method restores the state of the allocator after calling Destroy on the object pool
    // so that the same instance of allocator can be reused by the object pool.
    void Reset() {}
};

// The pool class storing generic objects as well as information whether the object is used or not.
// The MaxSize template parameter serves for setting the maximum size of the pool at compile time. It tells
// the client how many instances can be cached inside the pool - the alternative "Factory Method" design
// pattern would always create a new instance when called.
// The use of the allocator type in the object pool class deserves its own name as a design patter - a Strategy.
// It serves for allowing the client to customize some of the behaviour of the object pool class without modifying
// the pool class itself.
template <typename T, size_t MaxSize = std::numeric_limits<size_t>::max(), typename AllocatorT = DefaultAllocator<T>>
class ObjectPool
{
    struct ObjectInfo {
        bool m_IsUsed{};
        T* m_pObject{};
    };
    // Inline member variables are a C++17 feature
    inline static std::vector<ObjectInfo> s_pooledObjects{};
    inline static AllocatorT s_allocator{};

public:
    // The interface through which the client can acquire and release the object from the pool
    // [[nodiscard]] is a C++17 function attribute indicating that the caller needs to store the return value;
    // if the return value is ignored, the compiler emits a warning.
    // Additionally specifying the warning message is a C++20 feature.
    [[nodiscard("Object will remain unused")]] static T* Acquire();
    static void Release(const T* pObj);

    // The method for deleting all the objects stored in the pool
    static void Destroy();
};

template <typename T, size_t MaxSize, typename AllocatorT>
T* ObjectPool<T,MaxSize,AllocatorT>::Acquire()
{
    for (auto& obj : s_pooledObjects) {
        if (!obj.m_IsUsed) {
            obj.m_IsUsed = true;
            printf("[POOL] Returning an existing object\n");
            return obj.m_pObject;
        }
    }
    if (s_pooledObjects.size() == MaxSize) {
        printf("Pool is full!\n");
        return nullptr;
    }

    printf("[POOL] Creating a new object\n");
    auto pObj = s_allocator(); //new T{}; // this would generate a compiler error when T's constructor is private!
    s_pooledObjects.push_back({true, pObj});
    return pObj;
}

template <typename T, size_t MaxSize, typename AllocatorT>
void ObjectPool<T,MaxSize,AllocatorT>::Release(const T* pObj)
{
    for (auto& obj : s_pooledObjects)
        if (obj.m_pObject == pObj) {
            obj.m_IsUsed = false;
            break;
        }
}

template <typename T, size_t MaxSize, typename AllocatorT>
void ObjectPool<T,MaxSize,AllocatorT>::Destroy()
{
    for (auto& obj : s_pooledObjects) {
        if (obj.m_IsUsed)
            printf("[POOL] Warning! Deleting an object still in use!\n");
        s_allocator(obj.m_pObject); //delete obj.m_pObject; // this would cause problems when the destructor is private
    }
    s_allocator.Reset();
    s_pooledObjects.clear();
}

// A simple "mocking" class with a private constructor. For such a class the client can provide
// its own custom allocator and/or deallocator to overcome the privacy issue.
class Test
{
    Test() = default;

public:
    void Foo() {}
    friend class TestAllocator;
};

class TestAllocator
{
public:
    Test* operator()() { return new Test{}; }
    void operator()(const Test* ptr) { delete ptr; }
    void Reset() {}
};

int main()
{
    //using Pool = ObjectPool<int>;
    using TestPool = ObjectPool<Test,2,TestAllocator>;

    auto o1 = TestPool::Acquire();
    auto o2 = TestPool::Acquire();
    auto o3 = TestPool::Acquire();

    TestPool::Release(o1);
    auto o4 = TestPool::Acquire();

    TestPool::Destroy();

    return 0;
}
