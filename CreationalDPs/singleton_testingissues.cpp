#include <cstdio>
#include <string>
#include <memory>

class Printer
{
protected:
    Printer() = default;
    virtual ~Printer() = default;

public:
    Printer(const Printer&) = delete;
    Printer& operator=(const Printer&) = delete;

    static Printer& Instance(const std::string& key);

    virtual void Print(const std::string&) = 0;
};

class LocalPrinter : public Printer
{
    static LocalPrinter m_Instance;
    LocalPrinter() = default;

public:
    // The following are no longer required if LocalPrinter inherits from Printer
    //LocalPrinter(const LocalPrinter&) = delete;
    //LocalPrinter& operator=(const LocalPrinter&) = delete;

    static LocalPrinter& EagerInstance() { return m_Instance; }
    ~LocalPrinter() override = default;

    void Print(const std::string& text) override { printf("[LOCALPRINTER]: %s\n", text.c_str()); }
};

class NetworkPrinter : public Printer
{
    static std::unique_ptr<NetworkPrinter> m_pInstance;
    NetworkPrinter() = default;

public:
    static NetworkPrinter& LazyInstance() {
        if (!m_pInstance) m_pInstance.reset(new NetworkPrinter);
        return *m_pInstance;
    }
    ~NetworkPrinter() override = default;

    void Print(const std::string& text) override { printf("[NETWORKPRINTER]: %s\n", text.c_str()); }
};

LocalPrinter LocalPrinter::m_Instance;
std::unique_ptr<NetworkPrinter> NetworkPrinter::m_pInstance = nullptr;

// The following implementation of the base class' Instance method violates the open-to-extension,
// close-to-modification principle: if a new printer class is added, the implementation needs
// to be modified

Printer& Printer::Instance(const std::string& key) {
    if (key == "local") return LocalPrinter::EagerInstance();
    if (key == "network") return NetworkPrinter::LazyInstance();
}

// The singleton class as used below violates the dependency inversion principle: if the user
// would like to replace the LocalPrinter with a NetworkPrinter class object, he would not be able
// to do so without modifications at the call site. Same with replacing using mock objects for
// testing.
// A remedy would be to create a base class for the singleton class (see the Printer class above).

void PrintSales() {
    LocalPrinter::EagerInstance().Print("Sales data");
}

// The main driver function
int main()
{
    auto& lp = Printer::Instance("local");
    lp.Print("Printing data to local printer");

    auto& np = Printer::Instance("network");
    np.Print("Printing data to network printer");

    return 0;
}
