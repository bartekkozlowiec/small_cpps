// A simple demonstration of a "rider on a busy road" game with multiple vehicles being
// spawned using the Prototype design pattern.

#include <cstdio>
#include <chrono>
#include <string>
#include <string_view>
#include <random>
#include <thread>
#include <vector>

// The base animation class, responsible for "loading" animations for different types of vehicles
// from a file
class Animation
{
    std::string m_animationData{};

public:
    Animation() = default;
    // std::string_view is a C++17 feature
    Animation(std::string_view animationFileName);

    const std::string& GetAnimationData() const { return m_animationData; }
    void SetAnimationData(const std::string& animationData) { m_animationData = animationData; }
};

// The constructor which simulates loading of the data from a file
Animation::Animation(std::string_view animationFileName)
{
    // Chrono literals are a C++14 feature
    using namespace std::chrono_literals;

    // Loading operation is costly and always takes a fair amount of time
    printf("[Animation]: Loading %s ", animationFileName.data());
    for (size_t i=0; i<10; ++i) {
        printf(".");
        fflush(stdout);
        std::this_thread::sleep_for(200ms);
    }
    printf("\n");
    m_animationData.assign("^^^^^^");
}

// The struct describing the position of a vehicle
struct Position
{
    int x;
    int y;
    void print() const { printf("(%d, %d)\n", x, y); }
};

// The base vehicle interface
class Vehicle
{
    int m_speed{};
    int m_hitPoints{};
    std::string m_name{};
    Animation* m_pAnimation{};
    Position m_position;

public:
    Vehicle(int speed, int hps, const std::string& name, std::string_view animFile, const Position& position) :
        m_speed{speed}, m_hitPoints{hps}, m_name{name}, m_position{position} {
        m_pAnimation = new Animation(animFile);
    }
    // If we rely on the default implementation of copy-constructor, we'll get a shallow copy of m_pAnimation
    // member variable. Hence, we stick to the rule of 5 which says that a class with ownership semantics should
    // have user-defined following 5 methods:
    //Vehicle(const Vehicle& other) = default;
    Vehicle(const Vehicle& other);
    Vehicle& operator=(const Vehicle& other);
    Vehicle(Vehicle&& other) noexcept;
    Vehicle& operator=(Vehicle&& other) noexcept;
    virtual ~Vehicle() { delete m_pAnimation; }

    int GetSpeed() const { return m_speed; }
    int GetHitPoints() const { return m_hitPoints; }
    Position GetPosition() const { return m_position; }
    const std::string& GetName() const { return m_name; }
    const std::string& GetAnimation() const { return m_pAnimation->GetAnimationData(); }

    void SetSpeed(const int speed) { m_speed = speed; }
    void SetHitPoints(const int hps) { m_hitPoints = hps; }
    void SetPosition(const Position& position) { m_position = position; }
    void SetName(const std::string& name) { m_name = name; }
    void SetAnimationData(const std::string& animData) { m_pAnimation->SetAnimationData(animData); }

    // This virtual function will be called by the game engine for all the vehicles; all the vehicle-related
    // activity will be implemented inside.
    virtual void Update() = 0;

    // The Prototype design pattern - cloning functionality (a virtual copy constructor) which speeds up the
    // creation process of Vehicle instances.
    virtual Vehicle* Clone() = 0;
};

Vehicle::Vehicle(const Vehicle& other) : m_speed{other.m_speed}, m_hitPoints{other.m_hitPoints},
    m_name{other.m_name}, m_pAnimation{new Animation{}}, m_position{other.m_position} {
    m_pAnimation->SetAnimationData(other.GetAnimation());
}

Vehicle& Vehicle::operator=(const Vehicle& other) {
    if (this != &other) {
        m_speed = other.m_speed;
        m_hitPoints = other.m_hitPoints;
        m_name = other.m_name;
        m_pAnimation->SetAnimationData(other.GetAnimation());
        m_position = other.m_position;
    }
    return *this;
}

Vehicle::Vehicle(Vehicle&& other) noexcept : m_speed{other.m_speed}, m_hitPoints{other.m_hitPoints},
    m_name{std::move(other.m_name)}, m_pAnimation{other.m_pAnimation}, m_position{other.m_position} {
    other.m_pAnimation = nullptr;
    other.m_position = {0, 0};
    other.m_hitPoints = 0;
    other.m_speed = 0;
    other.m_name.clear();
}

Vehicle& Vehicle::operator=(Vehicle&& other) noexcept {
    if (this != &other) {
        m_speed = other.m_speed;
        m_hitPoints = other.m_hitPoints;
        m_name = std::move(other.m_name);
        delete m_pAnimation; // delete the current resource
        m_pAnimation = other.m_pAnimation;
        m_position = other.m_position;
    }
    return *this;
}

// The Vehicle types implementations follow. Since they have no user-defined constructors, the compiler will
// automatically synthesize the methods from the Rule of 5 for them. Those default methods will be sufficient
// as the child classes don't have any additional ownership semantics.
class GreenCar : public Vehicle
{
    // The inheriting constructor is a C++11 feature - it removes the need to invoke the base class constructor
    // from the derived class (the GreenCar class doesn't have itself any attributes that would need initializtion).
    using Vehicle::Vehicle;

    Vehicle* Clone() override { printf("Cloning: %s\n", GetName().c_str()); return new GreenCar(*this); }

public:
    void Update() override { printf("[%s]\n\tAnimation: %s\n\tSpeed: %d\n\tHitPoints: %d\n\tPosition: ",
               GetName().c_str(), GetAnimation().c_str(), GetSpeed(), GetHitPoints()); GetPosition().print(); }
};

class RedCar : public Vehicle
{
    using Vehicle::Vehicle;

    // Member variables enabling a RedCar object to temporarily increase its speed
    float m_speedFactor{1.5f}; // the factor of speed
    std::default_random_engine m_engine{100}; // the seed value
    std::bernoulli_distribution m_dist{.5}; // the probability of generating a true Boolean value

    Vehicle* Clone() override { printf("Cloning: %s\n", GetName().c_str()); return new RedCar(*this); }

public:
    void SetSpeedFactor(const float factor) { m_speedFactor = factor; }
    void Update() override;
};

void RedCar::Update() {
    printf("[%s]\n\tAnimation: %s",
               GetName().c_str(), GetAnimation().c_str());

    if (m_dist(m_engine)) // if a random Boolean is true
        printf("\n\tIncrease speed temporarily to: %f", GetSpeed() * m_speedFactor);
    else
        printf("\n\tSpeed: %d", GetSpeed());

    printf("\n\tHitPoints: %d\n\tPosition: ",
           GetHitPoints()); GetPosition().print();
}

class BlueBus : public Vehicle
{
    using Vehicle::Vehicle;

    // Member variables enabling a BlueBus object to randomly move out of the way when the player honks
    std::default_random_engine m_engine{500};
    std::bernoulli_distribution m_dist{.5};

    Vehicle* Clone() override { printf("Cloning: %s\n", GetName().c_str()); return new BlueBus(*this); }

public:
    void Update() override;
};

void BlueBus::Update() {
    printf("[%s]\n\tAnimation: %s",
           GetName().c_str(), GetAnimation().c_str());

    if (m_dist(m_engine)) // if a random Boolean is true
        printf("\n\tMoving out of the way...");

    printf("\n\tSpeed: %d\n\tHitPoints: %d\n\tPosition: ",
           GetSpeed(), GetHitPoints()); GetPosition().print();
}

class YellowBus : public Vehicle
{
    using Vehicle::Vehicle;

    // Member variables enabling a BlueBus object to randomly move out of the way when the player honks
    std::default_random_engine m_engine{500};
    std::bernoulli_distribution m_dist{.25}; // yellow bus is less likely to move out of the way

    Vehicle* Clone() override { printf("Cloning: %s\n", GetName().c_str()); return new YellowBus(*this); }

public:
    void Update() override;
};

void YellowBus::Update() {
    printf("[%s]\n\tAnimation: %s",
           GetName().c_str(), GetAnimation().c_str());

    if (m_dist(m_engine)) // if a random Boolean is true
        printf("\n\tMoving out of the way...");

    printf("\n\tSpeed: %d\n\tHitPoints: %d\n\tPosition: ",
           GetSpeed(), GetHitPoints()); GetPosition().print();
}

// The GameManager class is the class which calls Update overridden functions on all the vehicle objects
class GameManager
{
    std::vector<Vehicle*> m_vehicles;

public:
    ~GameManager() { for (auto vehicle : m_vehicles) delete vehicle; }

    // The method containing the game loop
    void Run();
};

// The Run method in its most naive implementation is tightly coupled with the vehicle implementation classes
// To counter this problem, we can use the Parametrized Factory Method design pattern. Here, we implement the
// factory method as a global function as no state preservation is needed.
Vehicle* Create(std::string_view type,
                int speed, int hps,
                const std::string& name,
                std::string_view animFile,
                const Position& position)
{
    if (type == "redcar")
        return new RedCar{speed, hps, name, animFile, position};
    if (type == "greencar")
        return new GreenCar{speed, hps, name, animFile, position};
    if (type == "yellowbus")
        return new YellowBus{speed, hps, name, animFile, position};
    if (type == "bluebus")
        return new BlueBus{speed, hps, name, animFile, position};
    return nullptr;
}

void GameManager::Run() {
    m_vehicles.push_back(Create("redcar", 30, 10, "RedCar", "red.anim", {0, 0}));
    m_vehicles.push_back(Create("greencar", 30, 15, "GreenCar", "green.anim", {100, 0}));
    m_vehicles.push_back(new YellowBus{25, 20, "YellowBus", "ybus.anim", {100, 200}}); // not using the factory method here
    m_vehicles.push_back(new BlueBus{25, 25, "BlueBus", "bbus.anim", {200, 200}}); // for demonstration purposes

    using namespace std::literals;

    size_t count = 5;
    while (count != 0) {
        std::this_thread::sleep_for(1s);
        std::system("cls"); // clearing the output screen
        for (auto vehicle : m_vehicles)
            vehicle->Update();

        // Simulating the creation of new vehicles as the player proceeds through the level
        if (count == 2)
            m_vehicles.push_back(Create("redcar", 30, 15, "RedCar", "red.anim", {50, 50}));
        else if (count == 3) {
            //m_vehicles.push_back(Create("yellowbus", 20, 20, "YellowBus", "ybus.anim", {150, 250}));
            // Create operation is always costly due to loading of an animation. To overcome this issue we would like
            // to utilize cloning.
            // Straight usage of copy constructor would take as back again to tight coupling between GameManager and
            // vehicle implementation classes and would also require a dynamic type cast:
            //RedCar rc{*m_vehicles[0]};

            // The proper solution to the problem of excessive object creation - usage of overridden "copy constructor"
            auto vehicle = m_vehicles[2]->Clone();
            vehicle->SetPosition({150, 250});
            vehicle->SetSpeed(10);
            m_vehicles.push_back(vehicle);
        }

        --count;
    }
}

// The main driver function
int main()
{
    GameManager mgr;
    mgr.Run();

    return 0;
}
