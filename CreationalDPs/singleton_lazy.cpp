#include <cstdio>
#include <string>
#include <memory>
#include <mutex>
#include <thread>
#define _CRT_SECURE_NO_WARNINGS

class Logger
{
    // The Deleter class is declared within the Logger class and as such can call its private destructor
    struct Deleter {
        void operator()(Logger* const p) { delete p; }
    };

    inline static std::unique_ptr<Logger, Deleter> m_pInstance{}; // C++17 feature: initialize static member inside the class
    inline static std::mutex m_Mtx;

    FILE* m_pStream;
    std::string m_FileName;
    std::string m_Tag;

    Logger(std::string fileName = "applog.txt");

    ~Logger();

public:
    static Logger& Instance();
    static Logger& MeyersInstance();
    static Logger& CallOnceInstance();

    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;

    void WriteLog(const char* pMessage);
    void SetTag(const char* pTag);
};

Logger& Logger::Instance() {
    //if (!m_pInstance) { // The infamous Double-Checked Locking Pattern; may fail as m_pInstance may be read here
                          // and concurrently written to in the critical section; the calling thread that won't get into
                          // the critical section will then get an invalid value in return
    m_Mtx.lock(); // this prohibits creating many singletons in multithreaded context (and avoids the data race too)
                  // but the guarding lock is needed only during the first usage of the Instance method (and will cause
                  // unnecessary performance hit later on)
    if (!m_pInstance)
        m_pInstance.reset(new Logger); // calling std::make_unique would need to call the private constructor
                                       // there are actually 3 steps performed here, equivalent to the following:
                                       // void* p = operator new(sizeof(Logger)); // (1)
                                       // new(p) Logger{}; // (2)
                                       // m_pInstance.reset(p); // (3)
                                       // the steps (2) and (3) may be freely reordered by the compiler -> DLCP may fail!
    m_Mtx.unlock();
    //}

    return *m_pInstance;
}

// The following is the Meyer's solution to the problem of a thread-safe lazy singleton implementation.
// From C++11 onwards static construction is thread-safe.
Logger& Logger::MeyersInstance()
{
    static Logger instance;
    return instance;
}

// This is the solution using C++11's std::call_once entity: the function/function object passed as parameter
// to std::call_once is guraranteed to be invoked only once, even if it is called concurrently by many threads.
// The solution may however be less efficient than using function-local statics (see MeyersInstance).
std::once_flag flag;
Logger& Logger::CallOnceInstance()
{
    std::call_once(flag, [](){
        m_pInstance.reset(new Logger);
    });
    return *m_pInstance;
}

Logger::Logger(std::string fileName) : m_FileName(std::move(fileName)) {
    printf("%s\n", __PRETTY_FUNCTION__);
    m_pStream = fopen(m_FileName.c_str(), "w");

    // The following registered lambda can be used to delete the Logger static pointer without using unique_ptr
    //atexit([]() { delete m_pInstance; });
}

Logger::~Logger() {
    printf("%s\n", __PRETTY_FUNCTION__);
    fclose(m_pStream);
}

void Logger::WriteLog(const char* pMessage) {
    fprintf(m_pStream, "[%s]: %s\n", m_Tag.c_str(), pMessage);
    fflush(m_pStream);
}

void Logger::SetTag(const char* pTag) {
    m_Tag = pTag;
}

void OpenConnection() {
    auto& lg = Logger::CallOnceInstance(); // Try different solutions here
    lg.WriteLog("Attempting to start a second thread...\n");
}

int main() {
    auto t1 = std::thread([]() {
        auto& lg = Logger::CallOnceInstance(); // ...and here.
        lg.SetTag("192.168.1.101");
        lg.WriteLog("Thread 1 has started.");
    });

    auto t2 = std::thread([]() {
        OpenConnection();
    });

    t1.join();
    t2.join();

    // To disable the following usecase, the Logger destructor must be private
    //auto* p = &lg;
    //delete p;

    return 0;
}
