#include <cstdio>
#include <string>
#include <unordered_map>
#include <exception>
#include <memory>
#include <mutex>

class Printer
{
protected:
    Printer() = default;
    virtual ~Printer() = default;

public:
    Printer(const Printer&) = delete;
    Printer& operator=(const Printer&) = delete;

    virtual void Print(const std::string&) = 0;
};

// Thanks to stroing shared pointers inside the PrinterProvider class's map member
// we have the control over the lifetime of the singleton (as opposed to when
// using e.g. Meyer's singleton and storing raw pointers).

using PrinterPtr = std::shared_ptr<Printer>;
using CreatorFun = std::function<PrinterPtr()>;

class LocalPrinter : public Printer
{
    LocalPrinter() = default;

public:
    // The following are no longer required if LocalPrinter inherits from Printer
    //LocalPrinter(const LocalPrinter&) = delete;
    //LocalPrinter& operator=(const LocalPrinter&) = delete;

    // The use of std::make_shared would require calling the private constructor
    // from outside the class
    static PrinterPtr LazyInstance() { return std::shared_ptr<LocalPrinter>(new LocalPrinter); }
    ~LocalPrinter() override { printf("%s\n", __PRETTY_FUNCTION__); }

    void Print(const std::string& text) override { printf("[LOCALPRINTER]: %s\n", text.c_str()); }
};

class PDFPrinter : public Printer
{
    PDFPrinter() = default;

public:
    static PrinterPtr LazyInstance() { return std::shared_ptr<PDFPrinter>(new PDFPrinter); }
    ~PDFPrinter() override = default;

    void Print(const std::string& text) override { printf("[PDFPRINTER]: %s\n", text.c_str()); }
};

// The remedy to the open-close principle violation: a provider class,
// implemented as Monostate, which stores a map with singular instanes of
// the printer classes. The pattern is called: Registry of singletons or Multiton.

// In order to be able to create the Printer instances lazily and avoid
// tight coupling between the PrinterProvider and Printer classes, the
// provider class will instantiate the singletons via a callback function
// inside the GetPrinter method.

class PrinterProvider
{
    struct InstanceInfo
    {
        PrinterPtr m_Printer{};
        CreatorFun m_Creator{};
    };

    inline static std::unordered_map<std::string, InstanceInfo> m_Printers;
    inline static std::mutex m_Mtx;

    PrinterProvider() = default;

public:
    static void RegisterCreator(const std::string& key, CreatorFun c) {
        std::lock_guard<std::mutex> lg{m_Mtx};

        if (m_Printers.find(key) == std::end(m_Printers))
            m_Printers[key].m_Creator = c;
        else printf("Already registered.\n");
    }

    static PrinterPtr GetPrinter(const std::string& key) {
        std::lock_guard<std::mutex> lg{m_Mtx};

        if (m_Printers.find(key) != std::end(m_Printers)) {
            if (!m_Printers.at(key).m_Printer)
                m_Printers.at(key).m_Printer = m_Printers.at(key).m_Creator();
            return m_Printers.at(key).m_Printer;
        }

        printf("Unknown key.\n");
        return nullptr;
    }
};

void PrintSales() {
    PrinterProvider::GetPrinter("local")->Print("Sales data");
}

// The main driver function
int main()
{
    PrinterProvider::RegisterCreator("local", &LocalPrinter::LazyInstance);
    PrinterProvider::RegisterCreator("pdf", &PDFPrinter::LazyInstance);

    auto lp = PrinterProvider::GetPrinter("local");
    if (lp) lp->Print("Printing data to local printer");

    auto pp = PrinterProvider::GetPrinter("pdf");
    if (pp) pp->Print("Printing data to PDF printer");

    PrintSales();

    return 0;
}
