// A simple mocking application of a shooter-game with multiple game objects cached in an object pool

#include <cstdio>
#include <thread>
#include <chrono>
#include <memory>
#include <functional>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <vector>

// The base class for all game object classes
class Actor
{
    // When a game object is created, it's visible by default.
    bool m_isVisible = true;

public:
    void SetVisible(bool visible) { m_isVisible = visible; }
    bool IsVisible() const { return m_isVisible; }

    virtual void Update() = 0;

protected:
    virtual ~Actor() = default;
};

class Alien : public Actor
{
public:
    Alien() { printf("+++++ Alien created +++++\n"); }
    ~Alien() override { printf("~~~~~ Alien destroyed ~~~~~\n"); }
    void Update() override { printf("@ "); }
};

class Missile : public Actor
{
public:
    Missile() { printf("+++++ Missile created +++++\n"); }
    ~Missile() override { printf("~~~~~ Missile destroyed ~~~~~\n"); }
    void Update() override { printf("-> "); }
};

using ActorPtr = std::shared_ptr<Actor>;
static std::vector<ActorPtr> actors{}; // static as not intended to be used outside this TU

// The creator function acting as a factory method inside the actor pool
using Creator = std::function<ActorPtr()>;

// Object pool - the remedy of the repetitive creation and destruction of game objects
class ActorPool
{
    // Inline member variables are a C++17 feature
    //inline static std::unordered_map<std::string,std::vector<ActorPtr>> s_actorPool{};
    struct ActorInfo {
        std::vector<ActorPtr> m_Actors;
        Creator m_Creator;
    };
    // The following cache includes the info about the factory method
    inline static std::unordered_map<std::string,ActorInfo> s_actorPool{};

    ActorPool() = default;
    static ActorPtr internalCreate(const std::string& key);
    static ActorPtr findActor(const std::vector<ActorPtr>& actors);

public:
    // Monostate implementation as the pool class should have only one global instance.
    static ActorPtr AcquireActor(const std::string& key);
    static void ReleaseActor(const std::string& key, const ActorPtr& missile);

    static void RegisterCreator(const std::string& key, Creator creator);
};

ActorPtr ActorPool::internalCreate(const std::string& key)
{
    printf("[POOL] Creating new actor of type: %s\n", key.c_str());

    // Warning: tight coupling between the actor pool and the types it instantiates!
    // This function will need to be modified if a new actor type is to be added,
    // which violates the Open-Closed principle. The remedy here is to use a factory
    // method stored inside the actor pool.
    /*if (key == "missile")
        return std::make_shared<Missile>();
    else if (key == "alien")
        return std::make_shared<Alien>();
    return nullptr;*/

    ActorPtr actor{};
    // Check if the creator function for the key has been registered (and create the entry for the key)
    if (!s_actorPool[key].m_Creator) {
        printf("Creator not registered! Returning a null pointer...\n");
        return actor;
    }

    // Add the corresponding value for the key
    actor = s_actorPool.at(key).m_Creator();
    return actor;
}

// Look for the first invisible instance of the requested actor type inside the "actors"
// part of the cache.
ActorPtr ActorPool::findActor(const std::vector<ActorPtr>& actors)
{
    auto actorIt = std::find_if(std::begin(actors), std::end(actors), [](const auto& actor) {
        return !actor->IsVisible();
    });

    if (actorIt != std::end(actors)) {
        (*actorIt)->SetVisible(true);
        // Optionally reset the state
        return *actorIt;
    }

    return nullptr;
}

void ActorPool::RegisterCreator(const std::string& key, Creator creator)
{
    s_actorPool[key].m_Creator = std::move(creator);
}

// If an actor becomes invisible (e.g. goes out of game view or hits something), it can
// be reused, i.e. can be acquired from the pool.
ActorPtr ActorPool::AcquireActor(const std::string& key)
{
    ActorPtr actor{};

    // If the key representing actor type is not present in the cache, create a new actor of the
    // requested type and store it in the cache inside a new vector.
    if (auto it = s_actorPool.find(key); it == std::end(s_actorPool)) {
        actor = internalCreate(key);
        //s_actorPool[key].push_back(actor);
        s_actorPool.at(key).m_Actors.push_back(actor);
        return actor;
    }

    //auto& actors = s_actorPool.at(key);
    auto& actors = s_actorPool[key].m_Actors;

    // Look for the actor whose visibility is false (i.e. can be reused) in the interesting part
    // of the actors cache
    actor = findActor(actors);
    if (!actor) {
        // Create a new actor if no invisible instance could be found and push to the cache
        actor = internalCreate(key);
        actors.push_back(actor);
    }
    else printf("[POOL] Returning existing actor of type: %s\n", key.c_str());

    return actor;
}

// When releasing an actor, we search for it in the pool and make it invisible so that
// it can be acquired later.
void ActorPool::ReleaseActor(const std::string& key, const ActorPtr& actor)
{
    // If the key is not present in the cache, throw an exception
    if (auto it = s_actorPool.find(key); it == std::end(s_actorPool))
        throw std::runtime_error("Unknown key!");

    // Get the interesting part of the actors' cache
    //auto& actors = s_actorPool.at(key);
    auto& actors = s_actorPool.at(key).m_Actors;

    // Find the actor in the cache
    for (auto& a : actors)
        if (a == actor) {
            a->SetVisible(false);
            break;
        }
}

// Firing involves creating objects on the heap; this alone can be expensive, however, repetitive
// heap allocation and deallocation (in calls to Explode) may cause the heap to be fragmented,
// decreasing performance even further.
void Fire()
{
    //missiles.push_back(std::make_shared<Missile>());
    //missiles.push_back(std::make_shared<Missile>());

    // The solution - acquiring missiles from the pool instance
    actors.push_back(ActorPool::AcquireActor("missile"));
    actors.push_back(ActorPool::AcquireActor("alien"));
}

void Animate()
{
    for (auto& a : actors)
        a->Update();
}

void Explode()
{
    using namespace std;

    printf("X\n");
    for (auto& a : actors)
        // The following is equal to releasing a missile via ActorPool::ReleaseMissile.
        a->SetVisible(false);

    actors.clear();
    std::this_thread::sleep_for(1s);
    printf("\n\n");
}

void GameLoop()
{
    using namespace std::literals;
    unsigned counter{};
    //unsigned loopCount{3};

    while (true) {
        //--loopCount;
        ++counter;
        if (counter == 1) {
            Fire();
        }
        if (counter >= 1 && counter <= 5) {
            Animate();
        }
        if (counter > 5) {
            Explode();
            counter = 0;
        }

        fflush(stdout);
        // Enforce a delay for animation purposes; the second literal used is a C++14 feature
        std::this_thread::sleep_for(1s);
    }
}

int main()
{
    // Register the actor creator functions before the game loop starts - this is the only place
    // that needs modification when a new type of actor is added to the pool.
    ActorPool::RegisterCreator("missile", [](){
        return std::make_shared<Missile>();
    });
    ActorPool::RegisterCreator("alien", [](){
        return std::make_shared<Alien>();
    });

    GameLoop();
}
