// Basic implementation of the Abstract Factory design pattern - an interface which allows
// creation of instances of classes that belong to a common set (context/family).

#include <cstdio>

#define printPretty printf("%s\n", __PRETTY_FUNCTION__)

// Interfaces defining a set of two products
class AbstractProductA
{
public:
    virtual void ProductA() = 0; // the name chosen so to be meaningful
    virtual ~AbstractProductA() = default;
};

class AbstractProductB
{
public:
    virtual void ProductB() = 0; // the name chosen so to be meaningful
    virtual ~AbstractProductB() = default;
};

// The implementation of products for set no. 1
class ProductA1 : public AbstractProductA
{
public:
    void ProductA() override { printPretty; }
    ~ProductA1() override = default;
};

class ProductB1 : public AbstractProductB
{
public:
    void ProductB() override { printPretty; }
    ~ProductB1() override = default;
};

// The implementation of products for set no. 2
class ProductA2 : public AbstractProductA
{
public:
    void ProductA() override { printPretty; }
    ~ProductA2() override = default;
};

class ProductB2 : public AbstractProductB
{
public:
    void ProductB() override { printPretty; }
    ~ProductB2() override = default;
};

// The base class that defines interfaces for creating the products
class AbstractFactory
{
public:
    // The following two are factory methods.
    virtual AbstractProductA* CreateProductA() = 0;
    virtual AbstractProductB* CreateProductB() = 0;

    virtual ~AbstractFactory() = default;
};

// The implementation of the above factory for creating products from set no. 1
class ConcreteFactory1 : public AbstractFactory
{
public:
    AbstractProductA* CreateProductA() override { return new ProductA1{}; }
    AbstractProductB* CreateProductB() override { return new ProductB1{}; }
    ~ConcreteFactory1() override = default;
};

// The implementation of the factory interface for creating products from set no. 2
class ConcreteFactory2 : public AbstractFactory
{
public:
    AbstractProductA* CreateProductA() override { return new ProductA2{}; }
    AbstractProductB* CreateProductB() override { return new ProductB2{}; }
    ~ConcreteFactory2() override = default;
};

// This encapsulation enforces the products created inside it to be from the same set/family.
// Additionally, its client doesn't need to know anything about the products' concrete types
// (loose coupling).
void UsePattern(AbstractFactory* pFactory)
{
    AbstractProductA* pA = pFactory->CreateProductA();
    AbstractProductB* pB = pFactory->CreateProductB();

    pA->ProductA();
    pB->ProductB();

    delete pA;
    delete pB;
}

int main()
{
    // An example usage of the product classes without the use of Abstract Factory
    // To use the products from a different set you need to modify this existing code!
    // Changing the type names used with the operator new can be very inconvenient and
    // lead to misuses across the application.
    //AbstractProductA* pA = new ProductA1{};
    //AbstractProductB* pB = new ProductB1{};

    // The remedy here is to use one of the factory implementations we have at hand - this
    // way without modifying the existing code (i.e. the class hierarchy and the funtion UsePattern)
    // nor depending on concrete types we are able to create products from another set.
    AbstractFactory* pFactory = new ConcreteFactory1{};
    UsePattern(pFactory);
    delete pFactory;

    return 0;
}
