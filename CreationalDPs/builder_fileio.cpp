// A program demonstrating implementation of the Builder design pattern for creating a wrapper class instance
// of system utilities for creating and managing file I/O operations. The builder design pattern is suitable
// for classes whose construction process is complex or whose instances can have different states.

#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <stdexcept>
#include <sys/stat.h>

constexpr const char fileNameRead[] = "datar.txt";
constexpr const char fileNameWrite[] = "dataw.txt";

class File
{
    const char* m_pFileName{};
    int m_desiredAccess; // set the file access to read/write
    int m_shareMode; // allow the file to be shared among processes
    int m_creationDisposition; // specify the action if the file with the given filename already exists
    int m_flagsAttributes; // emit exception if the file exists and the above flag is set
    int m_hFile; // the resulting file handle

public:
    // The all-argument constructor version
    File(const char* fileName, int desiredAccess, int shareMode, int creationDisposition, int flagsAttributes);

    // Simple file constructor
    File(const char* fileName, int desiredAccess);

    // File constructor with custom attributes
    File(const char* fileName, int desiredAccess, int flagAttributes);

    // The number of constructors at hand here and the number of different arguments they take make it difficult for the
    // user to choose the correct constructor for their application. The name of the constructor obviously can't be
    // changed to reflect its purpose. The number of arguments also indicates that the object of the class should be
    // constructed step by step rather than in a single shot. This justifies the use of the Builder design pattern.

    ~File() { if (m_hFile) close(m_hFile); }
    File(const File& other) = delete;
    File& operator=(const File& other) = delete;

    // The move constructor and assignment operator - needed since the class contains a handle to the file and therefore
    // has ownership semantics.
    File(File&& other) : m_pFileName(other.m_pFileName), m_desiredAccess(other.m_desiredAccess),
        m_shareMode(other.m_shareMode), m_creationDisposition(other.m_creationDisposition),
        m_flagsAttributes(other.m_flagsAttributes), m_hFile(other.m_hFile)
    {
        other.m_pFileName = nullptr;
        other.m_desiredAccess = 0;
        other.m_shareMode = 0;
        other.m_creationDisposition = 0;
        other.m_flagsAttributes = 0;
        other.m_hFile = 0;
    }
    File& operator=(File&& other) {
        if (this == &other) return *this;

        m_pFileName = other.m_pFileName;
        m_desiredAccess = other.m_desiredAccess;
        m_shareMode = other.m_shareMode;
        m_creationDisposition = other.m_creationDisposition;
        m_flagsAttributes = other.m_flagsAttributes;
        m_hFile = other.m_hFile;
        return *this;
    }

    ssize_t Read(char* pBuffer, size_t size); // read the data from the file to the buffer of the given size and return
                                              // the number of characters read
    ssize_t Write(const char* pBuffer, size_t size); // write the data from the buffer of the given size to the file and
                                               // return the number of characters written
    int Close(); // close the file handle
    bool IsOpen() const { return m_hFile != 0; }

    // We don't always need the director to create instances of the class that we want to instantiate. We can also omit
    // the multiple sibling classes for the Builder and make one builder which would create the end product with
    // different states. This approach is called "the modern builder".
    class Builder {
        // Repeated set of File properties
        const char* m_pFileName{};
        int m_desiredAccess;
        int m_shareMode;
        int m_creationDisposition{O_CREAT}; // default values in case the user doesn't define any
        int m_flagsAttributes{0};
        int m_hFile;

    public:
        Builder(const char* fileName, int desiredAccess) : m_pFileName{fileName}, m_desiredAccess{desiredAccess} {}

        // Returning reference to the object itself allows chaining the calls as part of the so called fluent interface.
        Builder& ShareMode(int shareMode) { m_shareMode = shareMode; return *this; }
        Builder& CreationDisposition(int creationDisposition) { m_creationDisposition = creationDisposition; return *this; }
        Builder& FlagsAttributes(int flagsAttributes) { m_flagsAttributes = flagsAttributes; return *this; }
        Builder& FileHandle(int fileHandle) { m_hFile = fileHandle; return *this; }

        File Build() { return File(m_pFileName, m_desiredAccess, m_shareMode, m_creationDisposition, m_flagsAttributes); }
    };
};

File::File(const char* fileName, int desiredAccess, int shareMode, int creationDisposition, int flagsAttributes)
    : m_pFileName(fileName)
    , m_desiredAccess(desiredAccess)
    , m_shareMode(shareMode)
    , m_creationDisposition(creationDisposition)
    , m_flagsAttributes(flagsAttributes)
{
    m_hFile = open(m_pFileName, m_desiredAccess | m_shareMode | m_creationDisposition | m_flagsAttributes);
    if (m_hFile < 0)
        throw std::runtime_error("File error!");
}

File::File(const char* fileName, int desiredAccess)
    : File(fileName, desiredAccess, 0) // The C++11 delegating constructor feature allows to avoid code duplication
{}

File::File(const char* fileName, int desiredAccess, int flagsAttributes)
    : File(fileName, desiredAccess, O_NONBLOCK, O_EXCL, flagsAttributes) // The C++11 delegating constructor feature
{}

ssize_t File::Read(char* pBuffer, size_t size)
{
    memset(pBuffer, 0, size);
    auto rd = read(m_hFile, pBuffer, size);
    if (rd < 0) throw std::runtime_error("Error while reading!");
    return rd;
}

ssize_t File::Write(const char* pBuffer, size_t size)
{
    auto wr = write(m_hFile, pBuffer, size);
    if (wr < 0) throw std::runtime_error("Error while writing!");
    return wr;
}

int File::Close()
{
    auto cl = close(m_hFile);
    if (cl != 0) throw std::runtime_error("Error while closing!");
    m_hFile = 0;
    return cl;
}

void ReadAFile()
{
    File f(fileNameRead, O_RDONLY, O_NONBLOCK, 0, 0);
    if (!f.IsOpen()) {
        fprintf(stderr, "Could not open the file!\n");
        return;
    }

    constexpr int SIZE = 1;
    char buffer[SIZE]{};

    while (f.Read(buffer, SIZE) != 0)
        printf("%s", buffer);
}

void WriteToAFile()
{
    File f(fileNameWrite, O_WRONLY, 0, O_CREAT, O_TRUNC);
    if (!f.IsOpen()) {
        fprintf(stderr, "Could not open the file!\n");
        return;
    }

    auto str = "Hello, World!\n";
    f.Write(str, std::strlen(str));
}

// The builder class interface with its methods arguments corresponding to the arguments of the Unix file API
class FileBuilder
{
public:
    // Methods for setting data which will be used later to construct the File object
    virtual void SetFileName(const char* fileName) = 0;
    virtual void SetDesiredAccess(int desiredAccess) = 0;
    virtual void SetShareMode() = 0;
    virtual void SetFlagsAttributes() = 0;

    // The method to create the final File object
    virtual File Build() = 0;
    virtual ~FileBuilder() = default;
};

// Simple file builder - the builder implementation corresponding to the simple file constructor
class SimpleFileBuilder : public FileBuilder
{
    const char* m_pFileName{};
    int m_desiredAccess;

public:
    void SetFileName(const char* fileName) override { m_pFileName = fileName; }
    void SetDesiredAccess(int desiredAccess) override { m_desiredAccess = desiredAccess; }
    void SetShareMode() override {}
    void SetFlagsAttributes() override {}

    File Build() override { return File(m_pFileName, m_desiredAccess, O_NONBLOCK, O_CREAT, 0); }
    ~SimpleFileBuilder() override = default;
};

// Encrypted file builder - the implementation which handles encrypted files
// The hierarchy of builders is however only really necessary if they all produce different (though sibling) class
// objects.
class EncryptedFileBuilder : public FileBuilder
{
    const char* m_pFileName{};
    int m_desiredAccess;

    // We don't want anyone to open an encrypted file while we're working with it - hence the additional property.
    int m_shareMode;

public:
    void SetFileName(const char* fileName) override { m_pFileName = fileName; }
    void SetDesiredAccess(int desiredAccess) override { m_desiredAccess = desiredAccess; }
    void SetShareMode() override { m_shareMode = O_SYNC; }
    void SetFlagsAttributes() override {}

    File Build() override { return File(m_pFileName, m_desiredAccess, m_shareMode, O_CREAT, 0); }
    ~EncryptedFileBuilder() override = default;
};

// Director - the class which has control over the Builder instance's construction process
class Director
{
    FileBuilder* m_pBuilder;

public:
    Director(FileBuilder* pBuilder) : m_pBuilder{pBuilder} {}
    void Create(const char* fileName, int desiredAccess);
};

// This method actually only takes care of setting the Builder's properties, it doesn't build anything.
void Director::Create(const char* fileName, int desiredAccess)
{
    m_pBuilder->SetFileName(fileName);
    m_pBuilder->SetDesiredAccess(desiredAccess);
    m_pBuilder->SetShareMode();
    m_pBuilder->SetFlagsAttributes();
}

// The main driver code
int main()
{
    printf("Demo 1: Reading a file...\n");
    ReadAFile();

    printf("Demo 2: Writing to a file...\n");
    WriteToAFile();

    // Use the Builder class to create an instance of File
    SimpleFileBuilder builder;
    Director director(&builder);

    director.Create("newfile.txt", O_WRONLY);
    auto file = builder.Build();

    // Save the File instance
    file.Write("Hello, there!", 13);

    // "Modern builder" example use
    File::Builder fbd{"anotherfile.txt", O_WRONLY};
    // If we want a more complex usecase, say: creating encrypted file, we will need to set some of the builder's properties.
    auto f = fbd.FlagsAttributes(O_SYNC).Build();
    f.Write("Another hello!", 15);

    return 0;
}
