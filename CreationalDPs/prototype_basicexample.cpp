// The applcation contains a basic demonstration of the Prototype design pattern.
// The pattern is sometimes refered to also as a virtual copy-constructor.

#include <cstdio>
#include <memory>

#define printFoo printf("%s\n", __PRETTY_FUNCTION__)

// The prototype base class, whose Clone method will act as a virtual CC.
class Prototype
{
public:
    Prototype() { printFoo; }
    Prototype(const Prototype& other) { printFoo; }
    virtual std::unique_ptr<Prototype> Clone() = 0;
    virtual ~Prototype() { printFoo; }
};

// The implementations of the prototype interface follow.
class ConcretePrototype1 : public Prototype
{
public:
    ConcretePrototype1() = default;
    ConcretePrototype1(const ConcretePrototype1& other) = default;
    std::unique_ptr<Prototype> Clone() override { printFoo; return std::make_unique<ConcretePrototype1>(*this); }
    ~ConcretePrototype1() override = default;
};

class ConcretePrototype2 : public Prototype
{
public:
    ConcretePrototype2() = default;
    ConcretePrototype2(const ConcretePrototype2& other) = default;
    std::unique_ptr<Prototype> Clone() override { printFoo; return std::make_unique<ConcretePrototype2>(*this); }
    ~ConcretePrototype2() override = default;
};

// The user class of the prototype objects
class Client
{
    std::unique_ptr<Prototype> m_pPrototype;

public:
    void SetPrototype(std::unique_ptr<Prototype> p) { m_pPrototype = std::move(p); }
    void OperationRequiringCloning() { auto p = m_pPrototype->Clone(); }
};

// The main driver function
int main()
{
    Client c;

    c.SetPrototype(std::make_unique<ConcretePrototype1>());
    c.OperationRequiringCloning();

    c.SetPrototype(std::make_unique<ConcretePrototype2>());
    c.OperationRequiringCloning();

    return 0;
}
