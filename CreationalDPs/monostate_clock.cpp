#include <cstdio>
#include <ctime>
#include <string>
#include <sstream>

// A monostate class enforces singularity through behaviour - even if a user creates
// multiple instances of monostate class, they will all share the same state (all the
// attributes of the class are static; member functions may or may not be static, depending
// on the implementation). This is unlike the singleton class, which prevents its user to
// create multiple instances of the class, so enforces the singularity through structure.

class Clock
{
    // The "inline" keywords below are C++17 feature and allow not to define static
    // members outside the class
    inline static int m_Hour;
    inline static int m_Minute;
    inline static int m_Second;

    Clock();
    static void currentTime();

public:
    static int GetHour();
    static int GetMinute();
    static int GetSecond();
    static std::string GetTimeString();
};

void Clock::currentTime()
{
    time_t rawTime;
    time(&rawTime); // get the system time
    tm* localTime = localtime(&rawTime); // convert the time to local time

    m_Hour = localTime->tm_hour;
    m_Minute = localTime->tm_min;
    m_Second = localTime->tm_sec;
}

Clock::Clock() {
    currentTime();
}

int Clock::GetHour() {
    currentTime();
    return m_Hour;
}

int Clock::GetMinute() {
    currentTime();
    return m_Minute;
}

int Clock::GetSecond() {
    currentTime();
    return m_Second;
}

std::string Clock::GetTimeString() {
    currentTime();
    std::stringstream out;
    out << m_Hour << ":" << m_Minute << ":" << m_Second;
    return out.str();
}

int main()
{
    printf("Current time: %s\n", Clock::GetTimeString().c_str());

    return 0;
}
