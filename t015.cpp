#include <iostream>
#include <cstring>

using namespace std;

/*///////////////////////////// Deklaracja zapowiadajaca klasy betterPtr ///////////////////////////////*/

class betterPtr;

/*/////////////////////////////////// Definicja klasy betterPointed ////////////////////////////////////*/

class betterPointed
{
    int ptdCounter;

    friend class betterPtr;

public:
    betterPointed() : ptdCounter(0) { }
    virtual ~betterPointed() = 0;
};

inline betterPointed::~betterPointed()
{ }

/*/////////////////////////////// Definicja klasy "zrecznego wskaznika" ////////////////////////////////*/

class betterPtr
{
    betterPointed* address;

public:
    betterPtr( betterPointed* );
    ~betterPtr();
    void           operator=( betterPointed* );
    betterPointed* operator->() { return address; }
};

///////////////////////////// Definicje metod z klasy "zrecznego wskaznika" //////////////////////////////

betterPtr::betterPtr( betterPointed* newObj )
{
    address = newObj;
    (newObj->ptdCounter)++;
    cout << "Tworzenie obiektu o adresie: " << address << endl;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

betterPtr::~betterPtr()
{
    cout << "Destrukcja obiektu o adresie: " << address << endl;
    address->ptdCounter--;
    if( address->ptdCounter == 0 )
        delete address;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

void betterPtr::operator=( betterPointed* newObj )
{
    address = newObj;
    (newObj->ptdCounter)++;
    cout << "Przypisanie do obiektu o adresie: " << address << endl;
}

/*////////////////////////////// Definicja klasy pochodnej od betterPointed /////////////////////////////*/

class A : public betterPointed
{
protected:
    char* a;

public:
    A( const char* a = "a" ) : a(strcpy(new char[strlen(a)+1],a)) { cout << "A-const "; }
    A( const A& sampleA ) : a(strcpy(new char[strlen(sampleA.a)+1],sampleA.a)) { cout << "A-copy-const "; }
    ~A() { delete [] a; cout << "A-destr "; }
    A& operator=( const A& );
};

////////////////////////////////////////// Definicje metod klasy A ////////////////////////////////////////

A& A::operator=( const A& sampleA )
{
    if(this != &sampleA)
    {
        delete [] a;
        a = new char[strlen(sampleA.a)+1];
        strcpy(a,sampleA.a);
    }
    return *this;
}

/*//////////////////////////////// Definicja klasy pochodnej od klasy A /////////////////////////////////*/

class B : public A
{
    int* b;

public:
    B() : b(NULL), A() { cout << "B-const "; }
    B( const B& sampleB ) : b((int*)memcpy(new int[sizeof(sampleB.b)],sampleB.b,sizeof(sampleB.b))),
                            A(sampleB) { cout << "B-copy-const "; }
    ~B() { delete [] b; cout << "B-destr "; }
    B& operator=( const B& );
};

////////////////////////////////////////// Definicje metod klasy B ////////////////////////////////////////

B& B::operator=( const B& sampleB )
{
    if(this != &sampleB)
    {
        delete [] a;
        delete [] b;

        ((A*)this)->operator=(sampleB);
        b = new int[sizeof(sampleB.b)];
        memcpy(b,sampleB.b,sizeof(sampleB.b));
    }
    return *this;
}

/*************************************** Definicje funkcji globalnych *************************************/

int main()
{
    // Tworzenie obiektow
    A objA;
    B objB;

    cout << endl;
    {
        // Tworzenie lokalnych obiektow w obszarze wolnym
        betterPtr pobjA = new A();
        betterPtr pobjB = new B();
    }

    return 0;
}
