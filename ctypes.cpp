// https://stackoverflow.com/questions/612328/difference-between-struct-and-typedef-struct-in-c
// https://stackoverflow.com/questions/575196/why-can-a-function-modify-some-arguments-as-perceived-by-the-caller-but-not-oth

#include <iostream>
#include <exception>
#include <array>

//////////////////////////////////////////// struct vs. typedef struct ////////////////////////////////////////////

struct Point
{
    int x, y;
};

int Point() { return 0; } // This is a legal hiding

class Empty {};
bool Empty = false; // This is a legal hiding

typedef struct Pt
{
    double x, y;
} Pt;

//double Pt() { return 0.0; } // This is an illegal hiding

typedef class {} Emp;
//bool Emp = false; // This is also an illegal hiding

///////////////////////////////// Constexpr use: creating compile-time constants /////////////////////////////////

constexpr int x{1}; // a compile-time constant
const int y{2};
// struct keywords below are needed because of hiding of struct Point
const struct Point point1{x,y}; // constant initialized at run-time (?)
constexpr struct Point point2{x,y};

constexpr double X{1.0};
double Y;
const Pt pt1{X,Y};

//////////////////////////////// Constexpr use: functions in constant expressions ////////////////////////////////

constexpr int factorial( int i )
{
    return (i > 1) ? i * factorial(i-1) : 1;
}

constexpr int safe_factorial( int i )
{
    return (i < 0) ?
                throw std::domain_error("Can't calculate factorial of negative value.") :
                factorial(i);
}

std::array<int,safe_factorial(3)> arr1;
//std::array<int,safe_factorial(-3)> arr2;

///////////////////// Constexpr use: creating literal type for use as compile-time constant /////////////////////

class Date
{
public:
    enum Month { Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec };
    constexpr Date( unsigned d, Month m, unsigned y );

private:
    unsigned d;
    Month m;
    unsigned y;
};

constexpr bool has30days( Date::Month m )
{
    return (m == Date::Apr || m == Date::Jun || m == Date::Sep || m == Date::Nov) ? true : false;
}

constexpr bool isLeap( unsigned y )
{
    return (y % 4 == 0) ? true : false;
}

constexpr unsigned requiresGoodDay( unsigned d, Date::Month m, unsigned y )
{
    return (d == 0 || d > 31) ?
                throw std::domain_error("Bad day number entered (0 or >31).") :
            (has30days(m) && d == 31) ?
                throw std::domain_error("Too large day number entered for a 30-day month.") :
            (m == Date::Feb && d >= 30) ?
                throw std::domain_error("Too large day number entered for February.") :
            (!isLeap(y) && m == Date::Feb && d == 29) ?
                throw std::domain_error("This is not a leap year, no 29 days in February.") :
            d;
}

constexpr Date::Date( unsigned d, Date::Month m, unsigned y ) :
    d( requiresGoodDay(d,m,y) ),
    m(m),
    y(y)
{}

constexpr Date dateX( 29, Date::Feb, 1900 );

////////////////////////////////////// Passing a pointer is a pass by value ///////////////////////////////////////

template <typename T>
void changePointerAssignmentByValue( T* pointer, T& newObject )
{
    pointer = &newObject;
    std::cout << "Inside function: the pointer (sent by value) now points to the address: " <<
                 (void*)pointer << ".\n";
}

template <typename T>
void changePointerAssignmentByReference( T*& pointer, T& newObject )
{
    pointer = &newObject;
    std::cout << "Inside function: the pointer (sent by reference) now points to the address: " <<
                 (void*)pointer << ".\n";
}

/////////////////////////////////////////////// Small inheritance test ////////////////////////////////////////////

struct Base
{
    void call() const { whoami(); }
    void whoami() const { std::cout << "I am base.\n"; }
};

class Derived : public Base
{
    void whoami() const { std::cout << "I am derived.\n"; }
};

////////////////////////////////////////////// Small forwarding test //////////////////////////////////////////////

void changeIntToZero( int& iParam ) { iParam = 0; }

template <typename T>
void forwardTheChange1( T&& TParam ) { changeIntToZero(TParam); }

template <typename T>
void forwardTheChange2( T&& TParam ) { changeIntToZero(static_cast<T&&>(TParam)); }

//////////////////////////////////////////////// The driver function //////////////////////////////////////////////

int main()
{
    std::cout << "Point outcome: " << Point() << "\n";
    std::cout << "Empty outcome: " << Empty << "\n";

    //std::cout << "Pt outcome: " << Pt() << "\n";
    //std::cout << "Emp outcome: " << Emp << "\n";

    Y = 3;
    std::cout << "x = " << x << ", y = " << y << ", point1 = {" << point1.x << "," << point1.y << "}\n";
    std::cout << "X = " << X << ", Y = " << Y << ", pt1 = {" << pt1.x << "," << pt1.y << "}\n";

    char* cPtr;
    char cObj;
    std::cout << "\nThe pointer cPtr points to the address: " << (void*)cPtr << ".\n";
    changePointerAssignmentByValue(cPtr,cObj);
    std::cout << "The pointer now points to the address: " << (void*)cPtr << ".\n";
    changePointerAssignmentByReference(cPtr,cObj);
    std::cout << "The pointer now points to the address: " << (void*)cPtr << ".\n\n";

    Base&& b = Derived();
    b.call();

    int ia = 1, ib = 1;
    forwardTheChange1(ia);
    forwardTheChange2(ib);
    std::cout << "\nThe value of ia is now: " << ia << ", the value of ib is now: " << ib << "\n";

    forwardTheChange1(1);
    //forwardTheChange2(1); // won't compile!

    std::cout << "\nWe start with one: " << 1u << "\n after first shift: " << (1u<<1) << "\n after second shift: "
              << (1u<<1<<1) << "\n after third shift: " << (1u<<1<<1<<1) << "\n";

    return 0;
}
