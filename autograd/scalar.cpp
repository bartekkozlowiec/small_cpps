/*
 * This file contains implementation of all Scalar member functions + defines the destructor of the BaseVariable
 * interface class. The BaseVariable is so small a type that it was put in the Scalar's module.
 * The static members of BaseVariable class are also defined here.
 *
 * Author: Bartek Kozlowiec
 * Date: 2020/02/02
 */

#include "scalar.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Scalar::moveAssignMeWith( Scalar& s )
{
    childrenToFree.insert(childrenToFree.end(),
                          std::make_move_iterator(s.childrenToFree.begin()),
                          std::make_move_iterator(s.childrenToFree.end()));
    s.childrenToFree.clear();

    assignMeWith(s);
}

void Scalar::assignMeWith( Scalar& s )
{
    val = s.val;
    grd = s.grd;

    if( s.rootOp ) {
        this->rootOp = std::make_unique<Operation>();
        this->rootOp->type = s.rootOp->type;

        if( s.rootOp->lOperand )
            this->rootOp->lOperand = s.rootOp->lOperand;
        else
            s.rootOp->lOperand = nullptr;

        if( s.rootOp->rOperand )
            this->rootOp->rOperand = s.rootOp->rOperand;
        else
            s.rootOp->rOperand = nullptr;
    }
    else this->rootOp = nullptr;
}

void Scalar::assignRootOp( Scalar& retS, Scalar& s, Operation::OpTypes type )
{
    retS.rootOp = std::make_unique<Operation>();
    retS.rootOp->type = type;
    //std::cout << "RETS.CHILDREN.SIZE-start: " << retS.childrenToFree.size() << "\n";

    if( !rootOp )
        retS.rootOp->lOperand = this;
    else {
        retS.rootOp->lOperand = new Scalar{*this};
        //std::cout << "NEWs1 " << static_cast<int>(type) << "\n";
        retS.childrenToFree.push_back(retS.rootOp->lOperand);
        retS.childrenToFree.insert(retS.childrenToFree.end(),
                                   std::make_move_iterator(childrenToFree.begin()),
                                   std::make_move_iterator(childrenToFree.end()));
        childrenToFree.clear();
    }

    if( !s.rootOp )
        retS.rootOp->rOperand = &s;
    else {
        retS.rootOp->rOperand = new Scalar{s};
        //std::cout << "NEWs2 " << static_cast<int>(type) << "\n";
        retS.childrenToFree.push_back(retS.rootOp->rOperand);
        retS.childrenToFree.insert(retS.childrenToFree.end(),
                                   std::make_move_iterator(s.childrenToFree.begin()),
                                   std::make_move_iterator(s.childrenToFree.end()));
        s.childrenToFree.clear();
    }
    //std::cout << "RETS.CHILDREN.SIZE-end: " << retS.childrenToFree.size() << "\n";
}

Scalar Scalar::operator-()
{
    Scalar retS{-val};

    retS.rootOp = std::make_unique<Operation>();
    retS.rootOp->type = Operation::OpTypes::unary_minus;

    if( !rootOp )
        retS.rootOp->lOperand = this;
    else {
        retS.rootOp->lOperand = new Scalar{*this};
        retS.childrenToFree.push_back(retS.rootOp->lOperand);
        retS.childrenToFree.insert(retS.childrenToFree.end(),
                                   std::make_move_iterator(childrenToFree.begin()),
                                   std::make_move_iterator(childrenToFree.end()));
        childrenToFree.clear();
    }

    return retS;
}

Scalar Scalar::operator+( Scalar& s )
{
    Scalar retS{0.0};
    retS.val = val+s.val;

    assignRootOp(retS,s,Operation::OpTypes::addition);

    return retS;
}

Scalar Scalar::operator+( Scalar&& s )
{
    return (*this)+s;
}

Scalar Scalar::operator-( Scalar& s )
{
    Scalar retS{0.0};
    retS.val = val-s.val;

    assignRootOp(retS,s,Operation::OpTypes::subtraction);

    //*this = *this+(-v);
    return retS;
}

Scalar Scalar::operator-( Scalar&& s )
{
    return (*this)-s;
}

Scalar Scalar::operator*( Scalar& s )
{
    Scalar retS{0.0};
    retS.val = val*s.val;

    assignRootOp(retS,s,Operation::OpTypes::multiplication);

    return retS;
}

Scalar Scalar::operator*( Scalar&& s )
{
    return (*this)*s;
}

Scalar& Scalar::operator+=( Scalar&& s )
{
    val += s.val;

    if( !rootOp )
        rootOp = std::move(s.rootOp);
    else {
        rootOp->lOperand = new Scalar{*this};
        childrenToFree.push_back(rootOp->lOperand);
        rootOp->rOperand = new Scalar{s};
        childrenToFree.push_back(rootOp->rOperand);
        rootOp->type = Operation::OpTypes::addition;

        childrenToFree.insert(childrenToFree.end(),
                              std::make_move_iterator(s.childrenToFree.begin()),
                              std::make_move_iterator(s.childrenToFree.end()));
        s.childrenToFree.clear();
    }

    return *this;
}

void Scalar::backprop( const float deriv )
{
    grd += deriv;

    if( rootOp ) {
        if( rootOp->type == Operation::OpTypes::unary_minus ) {
            auto backpropped = rootOp->lOperand;
            backpropped->backprop(-deriv);
        }
        else if( rootOp->type == Operation::OpTypes::addition ) {
            auto lBackpropped = rootOp->lOperand;
            lBackpropped->backprop(deriv);
            auto rBackpropped = rootOp->rOperand;
            rBackpropped->backprop(deriv);
        }
        else if( rootOp->type == Operation::OpTypes::subtraction ) {
            auto lBackpropped = rootOp->lOperand;
            lBackpropped->backprop(deriv);
            auto rBackpropped = rootOp->rOperand;
            rBackpropped->backprop(-deriv);
        }
        else if( rootOp->type == Operation::OpTypes::multiplication ) {
            auto lBackpropped = rootOp->lOperand;
            auto lVal = lBackpropped->value();
            auto rBackpropped = rootOp->rOperand;
            auto rVal = rBackpropped->value();
            lBackpropped->backprop(deriv*rVal);
            rBackpropped->backprop(deriv*lVal);
        }
    }
}

void Scalar::reset()
{
    clearGrads();
    if( rootOp ) {
        if( rootOp->lOperand ) {
            rootOp->lOperand->reset();
            rootOp->lOperand = nullptr;
        }

        if( rootOp->rOperand ) {
            rootOp->rOperand->reset();
            rootOp->rOperand = nullptr;
        }

        for(BaseVariable* child : childrenToFree){ dynamic_cast<Scalar*>(child) ? std::cout << "DELETEs\n" : std::cout << "DELETEv\n"; delete child; }
        childrenToFree.clear();

        rootOp.reset();
        rootOp = nullptr;
    }
}
