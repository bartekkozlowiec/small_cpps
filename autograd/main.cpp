/*
 * This is the main driver program for the Autograd project with a couple of tests.
 *
 * Author: Bartek Kozlowiec
 * Date: 2020/02/02
 */

#include "variable.h"

// VERY useful link: https://cs231n.github.io/optimization-2/

int main()
{
    // All so-to-say "unit tests" inside braces below
    {
        // 1. Some simple 1-dimensional (scalar) operations chained together
        Variable a{3.0f}, b{2.0f};
        Variable c = (a*b);
        auto f = (a*(b+c))-a;
        f.backprop();
        std::cout << "==============================================================\n";
        std::cout << "Function to be differentiated: f = f(a,b) = a*(b+c)-a, c = a*b\n";
        std::cout << "df/da = " << a.grad() << ", df/db = " << b.grad() << "\n\n"; fflush(stdout);

        // 2. Effect of zeroing for 1D variables
        f.zero_grad();
        std::cout << "After zeroing of f:\n";
        std::cout << "df/da = " << a.grad() << ", df/db = " << b.grad() << "\n\n"; fflush(stdout);

        // 3. Testing reassignment
        c = a+b;
        c.backprop();
        std::cout << "Reassignment of variable c to contain: c = c(a,b) = a+b\n";
        std::cout << "dc/da = " << a.grad() << ", dc/db = " << b.grad() << "\n\n"; fflush(stdout);

        // 4. Zeroing after reassignment
        c.zero_grad();
        std::cout << "After zeroing of c:\n";
        std::cout << "dc/da = " << a.grad() << ", dc/db = " << b.grad() << "\n\n"; fflush(stdout);

        // 5. Some vectorized computations on 2-dimensional matrices
        Variable<2,3> v1{0.0};
        v1.get(0,0) = 3.0f;
        v1.get(0,1) = 4.0f;

        Variable<3,4> v2{0.0};
        v2.get(0,0) = 2.0f;
        v2.get(1,0) = 5.0f;

        auto v3 = matMul(v1,v2);
        std::cout << "==============================================================\n";
        std::cout << "Matrix v1 -> " << v1 << std::endl;
        std::cout << "Matrix v2 -> " << v2 << std::endl;
        std::cout << "Matrix by matrix multiplication: v3 = v3(v1,v2) = v1*v2\n";
        std::cout << "Matrix v3 -> " << v3 << std::endl;

        v3.backprop();
        std::cout << "Derivatives of v3: dv3/dv1(1,1) = " << v1.grad()
                  << ", dv3/dv1(1,2) = " << v1.grad(0,1)
                  << ",\ndv3/dv2(1,1) = " << v2.grad()
                  << ", dv3/dv2(2,1) = " << v2.grad(1,0) << "\n\n"; fflush(stdout);

        // 6. Effect of zeroing for 2D matrices
        v3.zero_grad();
        std::cout << "After zeroing of v3: dv3/dv1(1,1) = " << v1.grad()
                  << ", dv3/dv1(1,2) = " << v1.grad(0,1)
                  << ",\ndv3/dv2(1,1) = " << v2.grad()
                  << ", dv3/dv2(2,1) = " << v2.grad(1,0) << "\n"; fflush(stdout);
    }

    std::getchar(); // just for valgrind runtime check
    return 0;
}
