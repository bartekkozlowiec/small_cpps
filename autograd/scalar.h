/*
 * This header file contains definition of an abstract class called BaseVariable which is a cornerstone for
 * memory handling in the project. Raw pointers to objects of types which inherit from BaseVariable and are
 * heap-allocated are always pushed to a static STL vector. Elements of this vector are deleted when the number of
 * existing BaseVariable-derived objects drops to 0. For this purpose the BaseVariable also has a static counter
 * of objects which is incremented by constructors of types inheriting from BaseVariable and decremented by
 * the BaseVariable destructor. Such memory management allows for easy handling of both rvalues and lvalues
 * in chained algebra operations.
 *
 * One of the types inheriting from BaseVariable is also defined here and called Scalar. This type is a building
 * block for larger 2-dimensional type called Variable and implements basic mathematical operations for this
 * Autograd project: addition, subtraction, multiplication and unary minus. These operations are described
 * using a simple structure called Operation with an enum inside. Also, the Operation contains two raw pointer
 * fields to BaseVariable types for left and right operands. Every BaseVariable-derived type object contains an
 * STL unique pointer to an Operation (initialized to null by default). This allows for building a simple
 * binary tree for every object, which is linked to its left and right constituents, if it is a product of an
 * Operation.
 *
 * Author: Bartek Kozlowiec
 * Date: 2020/02/02
 */

#ifndef SCALAR_H
#define SCALAR_H

#include <iostream>
#include <vector>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <bool Cond, typename T = void>
struct enable_if
{};

template <typename T>
struct enable_if<true,T>
{
    typedef T type;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class BaseVariable;

struct Operation
{
    enum class OpTypes : unsigned
    {
        unary_minus,
        addition,
        subtraction,
        multiplication,
        mtx_multiply
    } type;

    BaseVariable* lOperand = nullptr;
    BaseVariable* rOperand = nullptr;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class BaseVariable
{
protected:
    std::unique_ptr<Operation> rootOp;
    std::vector<BaseVariable*> childrenToFree;

public:
    BaseVariable() : rootOp{nullptr} {}

    virtual float value( const std::size_t = 0, const std::size_t = 0 ) const = 0;
    virtual void backprop( const float ) = 0;
    virtual void clearGrads() = 0;
    virtual void reset() = 0;
    virtual ~BaseVariable() {}
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<std::size_t, std::size_t>
class Variable;

class Scalar : public BaseVariable
{
    float val;
    float grd;

    void assignRootOp( Scalar&, Scalar&, Operation::OpTypes );
    void assignMeWith( Scalar& );
    void moveAssignMeWith( Scalar& );
    Scalar& operator+=( Scalar&& );

public:
    explicit Scalar( const float v = 0.0 ) : BaseVariable(), val{v}, grd{0.0f} {}
    Scalar( Scalar& s ) : BaseVariable() { std::cout << "sCC\n"; assignMeWith(s); }
    Scalar( Scalar&& s ) : BaseVariable() { std::cout << "sMC\n"; moveAssignMeWith(s); }
    Scalar& operator=( Scalar& s ) { std::cout << "sCA\n"; assignMeWith(s); return *this; }
    Scalar& operator=( Scalar&& s ) { std::cout << "sMA\n"; moveAssignMeWith(s); return *this; }
    Scalar operator-();
    Scalar operator+( Scalar& );
    Scalar operator+( Scalar&& );
    Scalar operator-( Scalar& );
    Scalar operator-( Scalar&& );
    Scalar operator*( Scalar& );
    Scalar operator*( Scalar&& );
    constexpr float& get() { return val; }
    constexpr float get() const { return val; }
    float value( const std::size_t = 0, const std::size_t = 0 ) const override { return val; }
    float grad() const { return grd; }
    void backprop( const float = 1.0f ) override;
    void clearGrads() override { grd = 0.0f; }
    void reset() override;

    //friend std::ostream& operator<<( std::ostream& ostr, const Scalar& v );
    template <std::size_t rowsNo1, std::size_t colsNo1, std::size_t rowsNo2, std::size_t colsNo2,
              typename enable_if<colsNo1==rowsNo2>::type*>
    friend Variable<rowsNo1,colsNo2> matMul( Variable<rowsNo1,colsNo1>& v1, Variable<rowsNo2,colsNo2>& v2 );
};

#endif // SCALAR_H
