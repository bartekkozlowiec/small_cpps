/*
 * This header file contains definition of a template class Variable taking two unsigned integers
 * as template arguments: number of rows and number of columns. Their defaulted values are 1, so that if
 * a simple Variable object is created, then it's a scalar (1x1) variable. The Variable class inherits
 * from a non-template interface class called BaseVariable and contains a matrix of elements
 * which are of a scalar type (named Scalar), also inheriting from BaseVariable. Thanks to Scalar's member operators
 * the Variable class allows for calculating values and automatic differentiation of matrices which are
 * products of element-wise addition, subtraction, multiplication, unary minus and matrix product. All these
 * operations are defined as Variable member functions except for matrix product which is a free template
 * function taking two arguments of type Variable of size (l x m) and (m x n) and returning a Variable object
 * sized (l x n). In order to implement additional mathematical operations to Variable, the operators
 * of the scalar class need to be expanded first.
 *
 * Author: Bartek Kozlowiec
 * Date: 2020/02/02
 */

#ifndef VARIABLE_H
#define VARIABLE_H

#include <iostream>
#include <vector>
#include <memory>
#include "scalar.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <std::size_t rowsNo, std::size_t colsNo>
class Variable;

template <std::size_t rowsNo, std::size_t colsNo>
std::ostream& operator<<( std::ostream&, const Variable<rowsNo,colsNo>& );

template <std::size_t rowsNo = 1, std::size_t colsNo = 1>
class Variable : public BaseVariable
{
    Scalar matrix[rowsNo][colsNo];

    void assignRootOp( Variable&, Variable&, Operation::OpTypes );

    void clearGrads();
    void assignMeWith( const Variable& );

public:
    Variable( const float=0.0 );
    Variable( const Variable& v ) : BaseVariable() { ++cnt; assignMeWith(v); }
    Variable& operator=( const Variable& v ) { assignMeWith(v); return *this; }
    constexpr float& get( std::size_t i, std::size_t j ) { return matrix[i][j].get(); }
    constexpr float get( std::size_t i, std::size_t j ) const { return matrix[i][j].get(); }

    Variable operator-();
    Variable operator+( Variable& );
    Variable operator+( Variable&& );
    Variable operator-( Variable& );
    Variable operator-( Variable&& );
    Variable operator*( Variable& );
    Variable operator*( Variable&& );
    float avg() const;
    float value( const std::size_t r=0, const std::size_t c=0 ) const { return matrix[r][c].value(); }
    float grad( const std::size_t r=0, const std::size_t c=0 ) const { return matrix[r][c].grad(); }
    void backprop( const float = 1.0f );
    void zero_grad();

    friend std::ostream& operator<< <>( std::ostream& ostr, const Variable& v );

    template <std::size_t rowsNo1, std::size_t colsNo1, std::size_t rowsNo2, std::size_t colsNo2,
              typename enable_if<colsNo1==rowsNo2>::type*>
    friend Variable<rowsNo1,colsNo2> matMul( Variable<rowsNo1,colsNo1>& v1, Variable<rowsNo2,colsNo2>& v2 );
};

template <std::size_t rowsNo, std::size_t colsNo>
Variable<rowsNo,colsNo>::Variable( const float a ) : BaseVariable()
{
    ++cnt;
    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            matrix[i][j] = Scalar{a};
}

template <std::size_t rowsNo, std::size_t colsNo>
void Variable<rowsNo,colsNo>::assignMeWith( const Variable& v )
{
    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            matrix[i][j] = v.matrix[i][j];

    if( v.rootOp ) {
        this->rootOp = std::make_unique<Operation>();
        this->rootOp->type = v.rootOp->type;

        if( v.rootOp->lOperand )
            this->rootOp->lOperand = v.rootOp->lOperand;
        else
            v.rootOp->lOperand = nullptr;

        if( v.rootOp->rOperand )
            this->rootOp->rOperand = v.rootOp->rOperand;
        else
            v.rootOp->rOperand = nullptr;
    }
    else this->rootOp = nullptr;
}

template <std::size_t rowsNo, std::size_t colsNo>
void Variable<rowsNo,colsNo>::assignRootOp( Variable& retV, Variable& v, Operation::OpTypes type )
{
    retV.rootOp = std::make_unique<Operation>();
    retV.rootOp->type = type;

    if( !rootOp )
        retV.rootOp->lOperand = this;
    else {
        retV.rootOp->lOperand = new Variable{*this};
        toDealloc.push_back(retV.rootOp->lOperand);
    }

    if( !v.rootOp )
        retV.rootOp->rOperand = &v;
    else {
        retV.rootOp->rOperand = new Variable{v};
        toDealloc.push_back(retV.rootOp->rOperand);
    }
}

template <std::size_t rowsNo, std::size_t colsNo>
Variable<rowsNo,colsNo> Variable<rowsNo,colsNo>::operator-()
{
    Variable retV{0.0};

    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            retV.matrix[i][j] = -matrix[i][j];

    retV.rootOp = std::make_unique<Operation>();
    retV.rootOp->type = Operation::OpTypes::unary_minus;

    if( !rootOp )
        retV.rootOp->lOperand = this;
    else {
        retV.rootOp->lOperand = new Variable{*this};
        toDealloc.push_back(retV.rootOp->lOperand);
    }

    return retV;
}

template <std::size_t rowsNo, std::size_t colsNo>
Variable<rowsNo,colsNo> Variable<rowsNo,colsNo>::operator+( Variable& v )
{
    Variable retV{0.0};

    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            retV.matrix[i][j] = matrix[i][j]+v.matrix[i][j];

    assignRootOp(retV,v,Operation::OpTypes::addition);

    return retV;
}

template <std::size_t rowsNo, std::size_t colsNo>
Variable<rowsNo,colsNo> Variable<rowsNo,colsNo>::operator+( Variable&& v )
{
    return (*this)+v;
}

template <std::size_t rowsNo, std::size_t colsNo>
Variable<rowsNo,colsNo> Variable<rowsNo,colsNo>::operator-( Variable& v )
{
    Variable retV{0.0};

    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            retV.matrix[i][j] = matrix[i][j]-v.matrix[i][j];

    assignRootOp(retV,v,Operation::OpTypes::subtraction);

    //*this = *this+(-v);
    return retV;
}

template <std::size_t rowsNo, std::size_t colsNo>
Variable<rowsNo,colsNo> Variable<rowsNo,colsNo>::operator-( Variable&& v )
{
    return (*this)-v;
}

template <std::size_t rowsNo, std::size_t colsNo>
Variable<rowsNo,colsNo> Variable<rowsNo,colsNo>::operator*( Variable& v )
{
    Variable retV{0.0};

    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            retV.matrix[i][j] = matrix[i][j]*v.matrix[i][j];

    assignRootOp(retV,v,Operation::OpTypes::multiplication);

    return retV;
}

template <std::size_t rowsNo, std::size_t colsNo>
Variable<rowsNo,colsNo> Variable<rowsNo,colsNo>::operator*( Variable&& v )
{
    return (*this)*v;
}

template <std::size_t rowsNo, std::size_t colsNo>
float Variable<rowsNo,colsNo>::avg() const
{
    float average = 0;

    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            average += matrix[i][j];

    return average / static_cast<float>(rowsNo*colsNo);
}

template <std::size_t rowsNo, std::size_t colsNo>
void Variable<rowsNo,colsNo>::backprop( const float deriv )
{
    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            matrix[i][j].backprop(deriv);
}

template <std::size_t rowsNo, std::size_t colsNo>
void Variable<rowsNo,colsNo>::clearGrads()
{
    for(std::size_t i=0; i<rowsNo; ++i)
        for(std::size_t j=0; j<colsNo; ++j)
            matrix[i][j].zero_grad();
}

template <std::size_t rowsNo, std::size_t colsNo>
void Variable<rowsNo,colsNo>::zero_grad()
{
    if( rootOp ) {
        if( rootOp->lOperand ) {
            rootOp->lOperand->clearGrads();
            rootOp->lOperand->zero_grad();
            rootOp->lOperand = nullptr;
        }

        if( rootOp->rOperand ) {
            rootOp->rOperand->clearGrads();
            rootOp->rOperand->zero_grad();
            rootOp->rOperand = nullptr;
        }

        rootOp.reset();
        rootOp = nullptr;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <std::size_t rowsNo1, std::size_t colsNo1, std::size_t rowsNo2, std::size_t colsNo2,
          typename enable_if<colsNo1==rowsNo2>::type* = nullptr>
Variable<rowsNo1,colsNo2> matMul( Variable<rowsNo1,colsNo1>& v1, Variable<rowsNo2,colsNo2>& v2 )
{
    Variable<rowsNo1,colsNo2> retV{0.0};

    for(std::size_t i=0; i<rowsNo1; ++i)
        for(std::size_t j=0; j<colsNo2; ++j)
            for(std::size_t k=0; k<colsNo1; ++k)
                retV.matrix[i][j] += v1.matrix[i][k] * v2.matrix[k][j];

    retV.rootOp = std::make_unique<Operation>();
    retV.rootOp->type = Operation::OpTypes::mtx_multiply;

    if( !v1.rootOp )
        retV.rootOp->lOperand = &v1;
    else {
        retV.rootOp->lOperand = new Variable{v1};
        BaseVariable::pushDealloc(retV.rootOp->lOperand);
    }

    if( !v2.rootOp )
        retV.rootOp->rOperand = &v2;
    else {
        retV.rootOp->rOperand = new Variable{v2};
        BaseVariable::pushDealloc(retV.rootOp->rOperand);
    }

    return retV;
}

template<std::size_t rowsNo, std::size_t colsNo>
std::ostream& operator<<( std::ostream& ostr, const Variable<rowsNo,colsNo>& v )
{
    ostr << "Variable of size: " << rowsNo << "x" << colsNo << ":\n";
    for(std::size_t i=0; i<rowsNo; ++i) {
        for(std::size_t j=0; j<colsNo; ++j)
            ostr << v.matrix[i][j].value() << " ";
        ostr << "\n";
    }

    return ostr;
}

#endif // VARIABLE_H
