// https://stackoverflow.com/questions/4766323/how-to-determine-sizeof-class-with-virtual-functions

#include <cstdio>

// The existence of member functions in a class has no influence on the object's size.

class A1
{
protected:
    int x, y, z;

public:
    A1( const int x, const int y, const int z ) : x{x}, y{y}, z{z} {}
    void print() const { printf("%d %d %d\n", x, y, z); }
};

class B1 : public A1
{
public:
    B1( const int X, const int Y, const int Z ) : A1{X, Y, Z} {}
    void rPrint() const { printf("%d %d %d\n", z, y, x); }
};

// However, if a member function is virtual, a pointer to the vtable is silently added
// to every instance of the class, increasing its size.

class A2
{
protected:
    int a, b, c;

public:
    A2( const int x, const int y, const int z ) : a{x}, b{y}, c{z} {}
    virtual void print() const { printf("%d %d %d\n", a, b, c); }
    virtual ~A2() { printf("A2 destructor called...\n"); }
};

// When you derive from a class which has virtual functions, the new virtual methods
// (if any) are gathered in the base class' vtable. Hence no increase in size comparing
// to the base class.

class B2 : public A2
{
public:
    B2( const int x, const int y, const int z ) : A2{x, y, z} {}
    void print() const override { printf("%d %d %d\n", a, b, c); }
    ~B2() override { printf("B2 destructor called...\n"); }
};

struct C
{
    virtual ~C() { printf("C destructor callled...\n"); }
};

// But when there's multiple inheritance from classes which have virtual functions,
// their vtables can't be combined. Your new virtual methods will be added to one of the
// base class's vtable, but still you'll get a size increase due to the second base
// class vpointer.

class D : public A2, public C
{
public:
    D( const int x, const int y, const int z ) : A2{x, y, z} {}
    ~D() override { printf("D destructor called...\n"); }
};

class E : public D
{
public:
    E( const int x, const int y, const int z ) : D{x, y, z} {}
    ~E() override { printf("E destructor called...\n"); }
};

// If you inherit from classes with a common ancestor, you'll get the size of your class
// increased by the possible fields which are inherited twice and by the possible two
// inherited vpointers.

class F : public A2, public B2
{
public:
    F( const int x, const int y, const int z, const int X, const int Y, const int Z ) : A2{x, y, z}, B2{X, Y, Z} {}
    ~F() override { printf("F destructor called...\n"); }
};

// But if you inherit from classes derived _virutally_ from a common ancestor, you won't
// have the possible fields doubled. You'll only possibly inherit two vpointers.

class B3 : virtual public A2
{
public:
    B3( const int x, const int y, const int z ) : A2{x, y, z} {}
    ~B3() override { printf("B3 destructor called...\n"); }
};

class B4 : virtual public A2
{
public:
    B4( const int x, const int y, const int z ) : A2{x, y, z} {}
    ~B4() override { printf("B4 destructor called...\n"); }
};

class G : public B3, public B4
{
public:
    G( const int x, const int y, const int z ) : A2{x, y, z}, B3{x, y, z}, B4{x, y, z} {}
    ~G() override { printf("G destructor called...\n"); }
};

// The main driver function below.

int main()
{
    A1 a1{0,1,2};
    B1 b1{1,2,3};
    A2 a2{2,3,4};
    B2 b2{3,4,5};
    D d{4,5,6};
    E e{5,6,7};
    F f{6,7,8,9,10,11};
    G g{12,13,14};

    printf("Size of a1 is %lu.\n", sizeof(a1));
    printf("Size of b1 is %lu.\n", sizeof(b1));
    printf("Size of a2 is %lu.\n", sizeof(a2));
    printf("Size of b2 is %lu.\n", sizeof(b2));
    printf("Size of d is %lu.\n", sizeof(d));
    printf("Size of e is %lu.\n", sizeof(e));
    printf("Size of f is %lu.\n", sizeof(f));
    printf("Size of g is %lu.\n", sizeof(g));

    return 0;
}
