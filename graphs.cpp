#include <iostream>
#include <climits>

/****************************************************** Type trait utilities ******************************************************/

template <typename ... Ts>
struct void_t_impl
{
    typedef void type;
};

template <typename ... Ts>
using void_t = typename void_t_impl<Ts...>::type;

template <typename T>
T&& declval();

template <typename T>
struct can_be_index
{
    static constexpr unsigned idx = static_cast<unsigned>(T{});
};

/***************************************** Template function for printing array contents ******************************************/

template <typename T, typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
void printArray( const T array[], const unsigned n )
{
    std::cout << "Array contents: ";
    for(unsigned i=0; i<n; ++i)
        std::cout << array[i] << " ";
    std::cout << "\n";
}

/******************************************** Some utility container template classes *********************************************/

template <typename T, unsigned maxSize>
class CircularQueue
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type storage[maxSize];
    unsigned front, rear;

    class iterator
    {
        T* ptr;
        CircularQueue<T,maxSize>& qRef;

    public:
        iterator( T* ptr, CircularQueue<T,maxSize>& q ) : ptr{ptr}, qRef{q} {}
        iterator operator++() { ptr == reinterpret_cast<T*>(qRef.storage+maxSize-1) ? ptr = reinterpret_cast<T*>(qRef.storage) : ++ptr; return *this; }
        bool operator!=( const iterator& other ) const { return ptr != other.ptr; }
        const T& operator*() const { return *ptr; }
    };

public:
    constexpr CircularQueue() : front{0}, rear{0} {}
    CircularQueue( const T[], const unsigned );
    CircularQueue( const CircularQueue& ) = default;
    ~CircularQueue() { while( !isEmpty() ) dequeue(); }

    constexpr bool isEmpty() const { return front == rear; }
    constexpr bool isFull() const { return (rear+1) % maxSize == front; }
    constexpr unsigned size() const;

    bool enqueue( const T& );
    T dequeue();
    bool find( const T& );
    iterator begin() { return iterator{reinterpret_cast<T*>(storage+front), *this}; }
    iterator end() { return iterator{reinterpret_cast<T*>(storage+rear), *this}; }
    const iterator begin() const { return iterator{reinterpret_cast<const T*>(storage+front), *this}; }
    const iterator end() const { return iterator{reinterpret_cast<const T*>(storage+rear), *this}; }

    template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr>
    void display( Str&& );
};

template <typename T, unsigned maxSize>
CircularQueue<T,maxSize>::CircularQueue( const T arr[], const unsigned n ) : front{0}, rear{0}
{
    if( n > maxSize-1 )
        std::cerr << "Array size " << n << " too large for queue's max allowed size. Creating empty queue...\n";
    else
        for(unsigned i=0; i<n; ++i)
            enqueue(arr[i]);
}

template <typename T, unsigned maxSize>
constexpr unsigned CircularQueue<T,maxSize>::size() const
{
    unsigned currSize = 0;
    rear >= front ? currSize = rear-front : currSize = maxSize-(front-rear);
    return currSize;
}

template <typename T, unsigned maxSize>
bool CircularQueue<T,maxSize>::enqueue( const T& elem )
{
    bool enqueued = false;

    if( !isFull() ) {
        T* temp = new( storage+rear ) T{elem};
        if( temp ) {
            enqueued = true;
            ++rear;
            rear %= maxSize;
        }
    }

    return enqueued;
}

template <typename T, unsigned maxSize>
T CircularQueue<T,maxSize>::dequeue()
{
    T dequeued{};

    if( !isEmpty() ) {
        dequeued = *reinterpret_cast<T*>(storage+front);
        reinterpret_cast<T*>(storage+front)->~T();
        ++front;
        front %= maxSize;
    }

    return dequeued;
}

template <typename T, unsigned maxSize>
bool CircularQueue<T,maxSize>::find( const T& elem )
{
    bool found = false;

    for(unsigned i=front; i!=rear; ++i, i%=maxSize)
        if( *reinterpret_cast<T*>(storage+i) == elem ) {
            found = true;
            break;
        }

    return found;
}

template <typename T, unsigned maxSize>
template <typename Str, void_t<decltype(declval<Str>() << declval<T>())>* >
void CircularQueue<T,maxSize>::display( Str&& ostr )
{
    ostr << "Queue contents: ";
    for(unsigned i=front; i!=rear; ++i, i%=maxSize)
        ostr << *reinterpret_cast<T*>(storage+i) << " ";
    ostr << "\n";
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T, unsigned maxSize>
class StaticStack;

template <typename T, unsigned maxSize>
std::ostream& operator<<( std::ostream&, const StaticStack<T,maxSize>& );

template <typename T, unsigned maxSize>
class StaticStack
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type storage[maxSize];
    unsigned currSize; // instead of top

public:
    constexpr StaticStack() : currSize{0} {}
    StaticStack( const T[], const unsigned );
    ~StaticStack() { while( !isEmpty() ) pop(); }

    constexpr bool isEmpty() const { return currSize == 0; }
    constexpr bool isFull() const { return currSize == maxSize; }

    bool push( const T& );
    T pop();

    friend std::ostream& operator<< <>( std::ostream&, const StaticStack& );
};

template <typename T, unsigned maxSize>
StaticStack<T,maxSize>::StaticStack( const T arr[], const unsigned n ) : currSize{0}
{
    if( n > maxSize )
        std::cerr << "Warning: size of array too big for stack size. Constructing stack of only " << n << " elements.\n";

    for(unsigned i=0; i<n; ++i)
        push(arr[i]);
}

template <typename T, unsigned maxSize>
bool StaticStack<T,maxSize>::push( const T& elem )
{
    bool pushed = false;

    if( !isFull() ) {
        T* temp = new(storage+currSize) T{elem};
        if( temp ) {
            pushed = true;
            ++currSize;
        }
    }

    return pushed;
}

template <typename T, unsigned maxSize>
T StaticStack<T,maxSize>::pop()
{
    T popped{};

    if( !isEmpty() ) {
        popped = *reinterpret_cast<T*>(storage+currSize-1);
        reinterpret_cast<T*>(storage+currSize)->~T();
        --currSize;
    }

    return popped;
}

template <typename T, unsigned maxSize>
std::ostream& operator<<( std::ostream& ostr, const StaticStack<T,maxSize>& stack )
{
    ostr << "Stack contents: ";
    for(unsigned i=0; i<stack.currSize; ++i)
        ostr << *reinterpret_cast<const T*>(stack.storage+i) << " ";
    ostr << "\n";

    return ostr;
}

/**************************************** A simple hash table template class implementation ***************************************/

template <typename Key, unsigned maxSize, void_t<decltype(can_be_index<Key>::idx)>* = nullptr>
struct HashFunction
{
    unsigned operator()( const Key& key ) const { return static_cast<unsigned>(key) % maxSize; }
};

template <typename Key, typename Val, unsigned maxSize = 100, typename HashFun = HashFunction<Key,maxSize>>
class HashTable
{
    struct Node
    {
        Key key;
        Val value;
        Node* next;
    };

    Node* storage[maxSize];
    unsigned currSize;
    HashFun hash;

public:
    constexpr HashTable() : storage{nullptr} {}
    ~HashTable();

    bool insert( const Key&, const Val& );
    bool erase( const Key& );
    Node* operator[]( const Key& ) const;
};

template <typename Key, typename Val, unsigned maxSize, typename HashFun>
bool HashTable<Key,Val,maxSize,HashFun>::insert( const Key& k, const Val& v )
{
    bool inserted = true;
    const unsigned bin = hash(k);

    if( storage[bin] ) {
        Node *prev = nullptr, *t = storage[bin];

        while( t && (t->key != k) ) {
            prev = t;
            t = t->next;
        }
        if( t ) t->value = v; else prev->next = new Node{k,v,nullptr};

        if( !prev || !prev->next ) inserted = false;
    }
    else {
        storage[bin] = new Node{k,v,nullptr};
        if( !storage[bin] ) inserted = false;
    }

    return inserted;
}

template <typename Key, typename Val, unsigned maxSize, typename HashFun>
bool HashTable<Key,Val,maxSize,HashFun>::erase( const Key& k )
{
    bool erased = true;
    const unsigned bin = hash(k);

    if( storage[bin] ) {
        Node *prev = nullptr, *t = storage[bin];

        while( t && (t->key != k) ) {
            prev = t;
            t = t->next;
        }

        if( t ) {
            delete t;
            prev->next = nullptr;
        }
        else erased = false;
    }
    else erased = false;

    return erased;
}

template <typename Key, typename Val, unsigned maxSize, typename HashFun>
typename HashTable<Key,Val,maxSize,HashFun>::Node* HashTable<Key,Val,maxSize,HashFun>::operator[]( const Key& k ) const
{
    Node* nodPtr = nullptr;
    const unsigned bin = hash(k);

    if( storage[bin] ) {
        nodPtr = storage[bin];
        while( nodPtr && (nodPtr->key != k) )
            nodPtr = nodPtr->next;
    }

    return nodPtr;
}

template <typename Key, typename Val, unsigned maxSize, typename HashFun>
HashTable<Key,Val,maxSize,HashFun>::~HashTable()
{
    for(unsigned i=0; i<maxSize; ++i) {
        if( storage[i] ) {
            Node *prev = nullptr, *t = storage[i];
            while( t ) {
                prev = t;
                t = t->next;
                delete prev;
            }
        }
    }
}

/************************* A class for implementing weighted union and collapsed find operations on sets **************************/

class Sets
{
    int* array;
    const unsigned size;

public:
    Sets( const unsigned n ) : size{n} { array = new int[size]{-1}; }
    ~Sets() { delete [] array; }

    unsigned cFind( unsigned ) const;
    void wUnionOf( const unsigned, const unsigned );
};

unsigned Sets::cFind( unsigned elem ) const
{
    unsigned parent = 0, x = elem;

    if( elem >= size )
        std::cerr << "Error: element index outside of array's size. Returning 0.\n";
    else {
        while( array[x] > 0 )
            x = array[x];
        parent = x;

        // Collapsing section below
        while( elem != parent ) {
            x = array[elem];
            array[elem] = parent;
            elem = x;
        }
    }

    return parent;
}

void Sets::wUnionOf( const unsigned elem1, const unsigned elem2 )
{
    if( elem1 >= size || elem2 >= size ) {
        std::cerr << "Error: element index outside of array's size. Returning 0.\n";
        return;
    }

    unsigned parent1 = cFind(elem1), parent2 = cFind(elem2);
    if( parent1 == parent2 && parent1 > 0 ) {
        std::cerr << "Warning: Attempted a weighted union operation on non-disjoint sets. Returning...";
        return;
    }

    if( array[parent1] < array[parent2] ) {
        array[parent1] += array[parent2];
        array[parent2] = parent1;
    }
    else {
        array[parent2] += array[parent1];
        array[parent1] = parent2;
    }
}

/******************************************* Template classes for graph representation ********************************************/

template <typename T, bool directed>
class AdjacencyMatrix
{
    struct Edge
    {
        bool filled;
        T value;
        unsigned first, second;
    };

    Edge** matrix;
    const unsigned verticesNo;

public:
    AdjacencyMatrix( const unsigned );
    ~AdjacencyMatrix();

    bool addEdge( const unsigned, const unsigned, const T& );
    bool removeEdge( const unsigned, const unsigned );
    Edge getMaxEdge() const;

    CircularQueue<unsigned,50> breadthFirstSearch( const unsigned ) const;
    CircularQueue<unsigned,50> depthFirstSearch( const unsigned ) const;
    void depthFirstSearch( const unsigned, CircularQueue<unsigned,50>& ) const;

    CircularQueue<Edge,50> minSpanningTreePrim() const;
    CircularQueue<Edge,50> minSpanningTreeKruskal() const;

    friend bool operator!=( const Edge& e1, const Edge& e2 ) { return e1.first != e2.first || e1.second != e2.second; }
};

template <typename T, bool directed>
AdjacencyMatrix<T,directed>::AdjacencyMatrix( const unsigned nodes ) : verticesNo{nodes}
{
    matrix = new Edge*[verticesNo];
    Edge* elems = new Edge[verticesNo*verticesNo];

    if( matrix && elems ) {
        for(unsigned i=0; i<verticesNo; ++i) {
            matrix[i] = elems + i*verticesNo;
            for(unsigned j=0; j<verticesNo; ++j)
                matrix[i][j] = {false, T{}, i, j};
        }
    }
}

template <typename T, bool directed>
bool AdjacencyMatrix<T,directed>::addEdge( const unsigned from, const unsigned to, const T& val )
{
    bool added = false;

    if( from < verticesNo && to < verticesNo ) {
        matrix[from][to] = { true, val, from, to };
        if( !directed ) matrix[to][from] = { true, val, from, to };
        added = true;
    }

    return added;
}

template <typename T, bool directed>
bool AdjacencyMatrix<T,directed>::removeEdge( const unsigned from, const unsigned to )
{
    bool removed = false;

    if( from < verticesNo && to < verticesNo ) {
        matrix[from][to] = { false, T{}, from, to };
        if( !directed ) matrix[to][from] = { false, T{}, to, from };
        removed = true;
    }

    return removed;
}

template <typename T, bool directed>
typename AdjacencyMatrix<T,directed>::Edge AdjacencyMatrix<T,directed>::getMaxEdge() const
{
    Edge max{};

    for(unsigned i=0; i<verticesNo; ++i)
        for(unsigned j=0; j<verticesNo; ++j)
            if( matrix[i][j].filled ) {
                max = matrix[i][j];
                break;
            }

    for(unsigned i=0; i<verticesNo; ++i)
        for(unsigned j=0; j<verticesNo; ++j)
            if( matrix[i][j].filled && matrix[i][j].value > max.value )
                max = matrix[i][j];

    return max;
}

template <typename T, bool directed>
CircularQueue<unsigned,50> AdjacencyMatrix<T,directed>::breadthFirstSearch( const unsigned startVertex ) const
{
    CircularQueue<unsigned,50> vertQueue, workQueue;
    workQueue.enqueue(startVertex);

    while( !workQueue.isEmpty() ) {
        const unsigned currVert = workQueue.dequeue();
        if( !vertQueue.find(currVert) ) vertQueue.enqueue(currVert);

        for(unsigned i=0; i<verticesNo; ++i)
            if( matrix[currVert][i].filled && !vertQueue.find(i) ) {
                workQueue.enqueue(i);
                vertQueue.enqueue(i);
            }
    }

    return vertQueue;
}

// this iterative version uses HashTable and doesn't use CircularQueue::find (unlike the BFS above)
template <typename T, bool directed>
CircularQueue<unsigned,50> AdjacencyMatrix<T,directed>::depthFirstSearch( const unsigned startVertex ) const
{
    CircularQueue<unsigned,50> vertQueue;
    StaticStack<unsigned,50> workStack;
    HashTable<unsigned,bool> hashTab;
    workStack.push(startVertex);

    while( !workStack.isEmpty() ) {
        const unsigned currVert = workStack.pop();
        if( !hashTab[currVert] ) {
            vertQueue.enqueue(currVert);
            hashTab.insert(currVert,true);
        }

        for(unsigned i=verticesNo; i-->0;)
            if( matrix[currVert][i].filled && !hashTab[i] ) {
                workStack.push(currVert);
                workStack.push(i);
            }
    }

    return vertQueue;
}

// this is the recursive DFS version
template <typename T, bool directed>
void AdjacencyMatrix<T,directed>::depthFirstSearch( const unsigned startVertex, CircularQueue<unsigned,50>& queue ) const
{
    queue.enqueue(startVertex);

    for(unsigned i=0; i<verticesNo; ++i)
        if( matrix[startVertex][i].filled && !queue.find(i) ) {
            depthFirstSearch(i,queue);
        }
}

template <typename T, bool directed>
CircularQueue<typename AdjacencyMatrix<T,directed>::Edge,50> AdjacencyMatrix<T,directed>::minSpanningTreePrim() const
{
    CircularQueue<Edge,50> edgeQueue;
    HashTable<unsigned,bool> included;
    unsigned howManyVerts = 0;

    // Find minimum valued edge
    Edge min = getMaxEdge();

    for(unsigned i=0; i<verticesNo; ++i)
        for(unsigned j=0; j<verticesNo; ++j)
            if( matrix[i][j].filled && matrix[i][j].value < min.value )
                min = matrix[i][j];

    edgeQueue.enqueue(min);
    included.insert(min.first, true);
    included.insert(min.second, true);
    howManyVerts += 2;

    // Until every vertex of the graph is included
    while( howManyVerts < verticesNo ) {
        // find the next minimum connected edge
        min = getMaxEdge();
        for( const auto& currEdge : edgeQueue )
            for(unsigned i=0; i<verticesNo; ++i)
                if( !included[i] ) {
                    if( matrix[currEdge.first][i].filled && matrix[currEdge.first][i].value <= min.value )
                        min = matrix[currEdge.first][i];

                    if( matrix[currEdge.second][i].filled && matrix[currEdge.second][i].value <= min.value )
                        min = matrix[currEdge.second][i];

                    if( matrix[i][currEdge.first].filled && matrix[i][currEdge.first].value <= min.value )
                        min = matrix[i][currEdge.first];

                    if( matrix[i][currEdge.second].filled && matrix[i][currEdge.second].value <= min.value )
                        min = matrix[i][currEdge.second];
                }

        edgeQueue.enqueue(min);
        included.insert(min.first, true);
        included.insert(min.second, true);
        ++howManyVerts;
    }

    return edgeQueue;
}

template <typename T, bool directed>
CircularQueue<typename AdjacencyMatrix<T,directed>::Edge,50> AdjacencyMatrix<T,directed>::minSpanningTreeKruskal() const
{
    CircularQueue<Edge,50> edgeQueue;

    struct EdgeHash { unsigned operator()( const Edge& key ) const { return (key.first^key.second) % 100; } };
    HashTable<Edge,bool,100,EdgeHash> included;

    Sets setsRepr{verticesNo};
    while( edgeQueue.size() < verticesNo-1 ) {
        // Find minimum valued edge not included in the solution
        Edge min = getMaxEdge();

        unsigned u, v;
        for(unsigned i=0; i<verticesNo; ++i)
            for(unsigned j=0; j<verticesNo; ++j)
                if( matrix[i][j].filled && matrix[i][j].value < min.value && !included[matrix[i][j]] ) {
                    min = matrix[i][j];
                    u = i;
                    v = j;
                }

        // Include the edge in the solution if it's not forming a cycle with those already found
        if( !(setsRepr.cFind(u) == setsRepr.cFind(v) && setsRepr.cFind(u) > 0) ) {
            edgeQueue.enqueue(min);
            setsRepr.wUnionOf(u,v);
        }

        included.insert(min, true);
    }

    return edgeQueue;
}

template <typename T, bool directed>
AdjacencyMatrix<T,directed>::~AdjacencyMatrix()
{
    delete [] matrix[0];
    delete [] matrix;
}

//////////////////////////////////////////// Adjacency Matrix specialization for T=int //////////////////////////////////////////////

template <bool directed>
class AdjacencyMatrix<int,directed>
{
    struct Edge
    {
        int value;
        unsigned first, second;
    };

    Edge** matrix;
    const unsigned verticesNo;

public:
    AdjacencyMatrix( const unsigned );
    ~AdjacencyMatrix();

    bool addEdge( const unsigned, const unsigned, const int );
    bool removeEdge( const unsigned, const unsigned );

    CircularQueue<Edge,50> minSpanningTreePrim() const;
};

template <bool directed>
AdjacencyMatrix<int,directed>::AdjacencyMatrix( const unsigned nodes ) : verticesNo{nodes}
{
    matrix = new Edge*[verticesNo];
    Edge* elems = new Edge[verticesNo*verticesNo];

    if( matrix && elems ) {
        for(unsigned i=0; i<verticesNo; ++i) {
            matrix[i] = elems + i*verticesNo;
            for(unsigned j=0; j<verticesNo; ++j)
                matrix[i][j].value = INT_MAX;
        }
    }
}

template <bool directed>
bool AdjacencyMatrix<int,directed>::addEdge( const unsigned from, const unsigned to, const int val )
{
    bool added = false;

    if( from < verticesNo && to < verticesNo ) {
        matrix[from][to].value = val;
        if( !directed ) matrix[to][from].value = val;
        added = true;
    }

    return added;
}

template <bool directed>
bool AdjacencyMatrix<int,directed>::removeEdge( const unsigned from, const unsigned to )
{
    bool removed = false;

    if( from < verticesNo && to < verticesNo ) {
        matrix[from][to].value = INT_MAX;
        if( !directed ) matrix[to][from].value = INT_MAX;
        removed = true;
    }

    return removed;
}

template <bool directed>
CircularQueue<typename AdjacencyMatrix<int,directed>::Edge,50> AdjacencyMatrix<int,directed>::minSpanningTreePrim() const
{
    CircularQueue<Edge,50> MST;
    int* near = new int[verticesNo];
    for(unsigned i=0; i<verticesNo; ++i) near[i] = INT_MAX;

    // Find the minimum edge
    int min = INT_MAX;
    unsigned u, v;
    for(unsigned i=0; i<verticesNo; ++i)
        for(unsigned j=0; j<verticesNo; ++j)
            if( matrix[i][j].value < min ) {
                min = matrix[i][j].value;
                u = i;
                v = j;
            }

    // Enqueue the min edge to the queue to be returned
    MST.enqueue(Edge{min, u, v});

    // Initialize the array of vertices from current spanning tree which are closest to the vertex given by index no.
    near[u] = -1;
    near[v] = -1;
    for(unsigned i=0; i<verticesNo; ++i) {
        if( near[i] != -1 ) {
            bool iOvU, iOvV;
            matrix[i][u].value < matrix[u][i].value ? iOvU = true : iOvU = false;
            matrix[i][v].value < matrix[v][i].value ? iOvV = true : iOvV = false;
            if( iOvU && iOvV )
                matrix[i][u].value < matrix[i][v].value ? near[i] = u : near[i] = v;
            else if( iOvU && !iOvV )
                matrix[i][u].value < matrix[v][i].value ? near[i] = u : near[i] = v;
            else if( !iOvU && iOvV )
                matrix[u][i].value < matrix[i][v].value ? near[i] = u : near[i] = v;
            else
                matrix[u][i].value < matrix[v][i].value ? near[i] = u : near[i] = v;
        }
    }

    for(unsigned i=1; i<verticesNo-1; ++i) {
        // Find the minimum edge connected to the current spanning tree
        min = INT_MAX;
        unsigned j=0, k;
        for(; j<verticesNo; ++j) {
            if( near[j] != -1 && matrix[j][near[j]].value < min ) {
                min = matrix[j][near[j]].value;
                k = u = j;
                v = near[j];
            }
            if( near[j] != -1 && matrix[near[j]][j].value < min ) {
                min = matrix[near[j]][j].value;
                u = near[j];
                k = v = j;
            }
        }
        MST.enqueue(Edge{min, u, v});
        near[k] = -1;

        // Update the near array with the new vertex just connected to the MST
        for(j=0; j<verticesNo; ++j)
            if( near[j] != -1 && (matrix[k][j].value < matrix[near[j]][j].value || matrix[j][k].value < matrix[j][near[j]].value) )
                near[j] = k;
    }
    delete [] near;

    return MST;
}

template <bool directed>
AdjacencyMatrix<int,directed>::~AdjacencyMatrix()
{
    delete [] matrix[0];
    delete [] matrix;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T, bool directed>
class AdjacencyTable
{
    struct Edge
    {
        T vertex;
        Edge* next;
    };

    Edge** vertices;
    const unsigned verticesNo;

public:
    constexpr AdjacencyTable() : vertices{nullptr}, verticesNo{0} {}
    AdjacencyTable( const unsigned v ) : verticesNo{v} { vertices = new Edge*[v]{nullptr}; }
    ~AdjacencyTable();
};

template <typename T, bool directed>
AdjacencyTable<T,directed>::~AdjacencyTable()
{
    for(unsigned i=0; i<verticesNo; ++i) {
        Edge* edges = vertices[i];
        while( edges ) {
            Edge* const te = edges;
            edges = edges->next;
            delete te;
        }
    }

    delete vertices;
}

/***************************************************** The main driver program ****************************************************/

class UserDefinedType {};

auto main() -> int
{
    float fArray[] = { 4.2f, -25.6f, 13.1f, 37.9f };
    unsigned fSize = sizeof(fArray)/sizeof(float);
    CircularQueue<float,5> cq{fArray, fSize};

    cq.dequeue();
    cq.enqueue(85.2f);
    cq.enqueue(51.9f);

    cq.display(std::cout);

    AdjacencyMatrix<unsigned,false> graph1{4};
    graph1.addEdge(0,1,1);
    graph1.addEdge(1,2,2);
    graph1.addEdge(2,3,3);
    graph1.addEdge(3,0,4);

    std::cout << "Graph 1's BFS traversal: ";
    graph1.breadthFirstSearch(0).display(std::cout);
    std::cout << "Graph 1's DFS traversal: ";
    graph1.depthFirstSearch(0).display(std::cout);
    std::cout << "Graph 1's DFS recursive traversal: ";
    CircularQueue<unsigned,50> rDFSqueue;
    graph1.depthFirstSearch(0, rDFSqueue);
    rDFSqueue.display(std::cout);

    auto MST1p = graph1.minSpanningTreePrim();
    std::cout << "Graph 1's minimum spanning tree (via Prim's method):";
    for( const auto& elem : MST1p )
        std::cout << elem.first << "->" << elem.second << " ";
    std::cout << "\n";
    auto MST1k = graph1.minSpanningTreePrim();
    std::cout << "Graph 1's minimum spanning tree (via Kruskal's method):";
    for( const auto& elem : MST1k )
        std::cout << elem.first << "->" << elem.second << " ";
    std::cout << "\n\n";

    AdjacencyMatrix<unsigned,false> graph2{7};
    graph2.addEdge(0,5,5);
    graph2.addEdge(5,4,20);
    graph2.addEdge(4,3,16);
    graph2.addEdge(4,6,18);
    graph2.addEdge(3,6,14);
    graph2.addEdge(3,2,8);
    graph2.addEdge(2,1,12);
    graph2.addEdge(1,6,10);
    graph2.addEdge(1,0,25);

    auto MST2p = graph2.minSpanningTreePrim();
    std::cout << "Graph 2's minimum spanning tree (via Prim's method):";
    for( const auto& elem : MST2p )
        std::cout << elem.first << "->" << elem.second << " ";
    std::cout << "\n";
    auto MST2k = graph2.minSpanningTreeKruskal();
    std::cout << "Graph 2's minimum spanning tree (via Kruskal's method):";
    for( const auto& elem : MST2k )
        std::cout << elem.first << "->" << elem.second << " ";
    std::cout << "\n\n";

    std::cout << "Stack: " << StaticStack<float,30>{fArray, fSize} << "\n";

    void_t<decltype(can_be_index<int>::idx)>* vptr1 = nullptr;
    //void_t<decltype(can_be_index<UserDefinedType>::idx)>* vptr2 = nullptr;

    return 0;
}
