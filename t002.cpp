#include <iostream>
#include <cstring>

using namespace std;

class K
{
    int  k;

public:
    K( int x = 0 ) : k(x) { }
};

class A
{
    int  a;
    int* pa;
    K*   pk;

public:
    A( int x = 0 ) : a(x), pa(&a), pk(new K()) { }

    A( const A& aa ) : a(aa.a), pa(&a), pk(new K())
    {
        cout << "A:cpy-ctor\n";
        memcpy(pk,aa.pk,sizeof(K));
    }

    A& operator=( const A& aa )
    {
        if(this != &aa)
        {
            //this->pa = new int;  // niekonieczne
            this->a  = aa.a;
            pa = &a;
            memcpy(pk,aa.pk,sizeof(K));
        }
        return *this;
    }

    ~A()
    {
        delete pk;
    }

    int geta() const
    {
        return *pa;
    }

    int* getpa() const
    {
        return pa;
    }

    K* getpk() const
    {
        return pk;
    }
};

class B : public A
{
    int  b;
    int* pb;

public:
    B( int x = 0 ) : A(x), b(x), pb(&b) { }

    B( const B& bb ) : A(bb), b(bb.b), pb(&b) { cout << "B:cpy-ctor\n"; }

    B& operator=( const B& bb )
    {
        if(this != &bb)
        {
            this->A::operator=(bb);
            //this->pb = new int;  // niekonieczne
            this->b  = bb.b;
            pb = &b;
        }
        return *this;
    }

    ~B() { } // niepotrzebny, ale nie przeszkadza

    int getb() const
    {
        return *pb;
    }

    int* getpb() const
    {
        return pb;
    }
};

int main()
{
    A a0, a1(1);
    B b0, b2(2);

    cout << "a1.a  = " << a1.geta()  << endl;
    cout << "a1.pa = " << a1.getpa() << endl;
    cout << "a1.pk = " << a1.getpk() << endl;
    cout << "b2.a  = " << b2.geta()  << endl;
    cout << "b2.pa = " << b2.getpa() << endl;
    cout << "b2.b  = " << b2.getb()  << endl;
    cout << "b2.pb = " << b2.getpb() << endl;
    cout << "b2.pk = " << b2.getpk() << endl;
    cout << endl;

    cout << "Przed przypisaniem:" << endl;
    cout << "a0.a  = " << a0.geta()  << endl;
    cout << "a0.pa = " << a0.getpa() << endl;
    cout << "a0.pk = " << a0.getpk() << endl;
    cout << "b0.a  = " << b0.geta()  << endl;
    cout << "b0.pa = " << b0.getpa() << endl;
    cout << "b0.b  = " << b0.getb()  << endl;
    cout << "b0.pb = " << b0.getpb() << endl;
    cout << "b0.pk = " << b0.getpk() << endl;
    cout << endl;

    a0 = a1;
    b0 = b2;

    cout << "Po przypisaniu a0 = a1, b0 = b2:" << endl;
    cout << "a0.a  = " << a0.geta()  << endl;
    cout << "a0.pa = " << a0.getpa() << endl;
    cout << "a0.pk = " << a0.getpk() << endl;
    cout << "b0.a  = " << b0.geta()  << endl;
    cout << "b0.pa = " << b0.getpa() << endl;
    cout << "b0.b  = " << b0.getb()  << endl;
    cout << "b0.pb = " << b0.getpb() << endl;
    cout << "b0.pk = " << b0.getpk() << endl;
    cout << endl;

    A a3 = a1;
    B b3 = b2;

    cout << "Uzycie konstruktora kopiujacego - nowe obiekty a3(a1), b3(b2):" << endl;
    cout << "a3.a  = " << a3.geta()  << endl;
    cout << "a3.pa = " << a3.getpa() << endl;
    cout << "a3.pk = " << a3.getpk() << endl;
    cout << "b3.a  = " << b3.geta()  << endl;
    cout << "b3.pa = " << b3.getpa() << endl;
    cout << "b3.b  = " << b3.getb()  << endl;
    cout << "b3.pb = " << b3.getpb() << endl;
    cout << "b3.pk = " << b3.getpk() << endl;

    // Kopiowanie obiektow miedzy klasami dziedziczonymi
    A a4 = b2;
    //B b4 = reinterpret_cast<B>(a1); // error

    cout << "Uzycie konstruktora kopiujacego - nowe obiekty a4(b2), b4(a1):" << endl;
    cout << "a4.a  = " << a4.geta()  << endl;
    cout << "a4.pa = " << a4.getpa() << endl;
    cout << "a4.pk = " << a4.getpk() << endl;

    return 0;
}
