#include <iostream>

///////////////////////////////// Tools to enable printing of only printable types //////////////////////////////////

template <typename ... T>
struct void_t_impl
{
    typedef void type;
};

template <typename ... T>
struct void_t
{
    using void_t_impl<T...>::type;
};

template <typename T>
T&& declval();

//////////////////////////////////////////// Printing function template /////////////////////////////////////////////

template <typename T, typename Stream, void_t<decltype(declval<Stream>() << declval<T>())>* = nullptr>
void printArray( T array[], Stream& stream, unsigned n )
{
    stream << "Array contents:\n";
    for(unsigned i=0; i<n; ++i)
        stream << array[i] << "\t";
    stream << "\n";
}

//////////////////////////////////// Circular doubly-linked list class template /////////////////////////////////////

template <typename T>
class CircularList
{
    struct Node
    {
        Node *prev, *next;
        T data;
    }* first;

public:
    constexpr CircularList() : first{nullptr} {}
    CircularList( const T arr[], const unsigned n ) : first{nullptr} { for(unsigned i=0; i<n; ++i) push(arr[i]); }
    ~CircularList() { while( !isEmpty() ) pop(); }

    constexpr bool isEmpty() const { return first == nullptr; }
    unsigned size() const;

    const T& operator[]( unsigned ) const;
    bool insert( const T&, unsigned );
    T remove( unsigned );
    bool push( const T& );
    T pop();
};

template <typename T>
unsigned CircularList<T>::size() const
{
    unsigned size = 0;

    if( !isEmpty() ) {
        Node* temp = first;
        do {
            temp = temp->next;
            ++size;
        } while( temp != first );
    }

    return size;
}

template <typename T>
const T& CircularList<T>::operator[]( unsigned idx ) const
{
    Node* p = first;
    if( idx >= size() ) std::cerr << "Warning: index " << idx << " larger or equal to the list's size.\n";

    for(unsigned i=0; i<idx; ++i)
        p = p->next;

    return p->data;
}

template <typename T>
bool CircularList<T>::insert( const T& elem, unsigned idx )
{
    bool inserted = true;

    if( isEmpty() )
        push(elem);
    else {
        Node* const temp = new(std::nothrow) Node;
        if( idx > size() ) std::cerr << "Warning: index " << idx << " larger than the list's size.\n";

        if( temp ) {
            temp->data = elem;

            Node* p = first;
            for(unsigned i=0; i<idx; ++i)
                p = p->next;

            temp->next = p;
            temp->prev = p->prev;
            p->prev->next = temp;
            p->prev = temp;

            if( p == first ) first = temp;
        }
        else inserted = false;
    }

    return inserted;
}

template <typename T>
T CircularList<T>::remove( unsigned idx )
{
    T removed{};

    if( !isEmpty() ) {
        if( idx == size()-1 )
            pop();
        else {
            if( idx >= size() ) std::cerr << "Warning: index " << idx << " larger or equal to the list's size.\n";

            Node* p = first;
            for(unsigned i=0; i<idx; ++i)
                p = p->next;

            p->prev->next = p->next;
            p->next->prev = p->prev;

            if( p == first ) first = p->next;
            delete p;
        }
    }

    return removed;
}

template <typename T>
bool CircularList<T>::push( const T& elem )
{
    bool pushed = true;
    Node* const temp = new(std::nothrow) Node;

    if( temp ) {
        temp->data = elem;

        isEmpty() ? first = temp : first->prev->next = temp;

        temp->prev = first->prev;
        temp->next = first;
        first->prev = temp;
    }
    else pushed = false;

    return pushed;
}

template <typename T>
T CircularList<T>::pop()
{
    T popped{};

    if( !isEmpty() ) {
        Node* const last = first->prev;
        popped = last->data;

        if( first->prev == first && first->next == first )
            first = nullptr;
        else {
            first->prev = last->prev;
            last->prev->next = first;
        }
        delete last;
    }

    return popped;
}

//////////////////////////////////////////////// Sorting algorithms /////////////////////////////////////////////////

template <typename T>
void insertionSort( T array[], const unsigned n )
{
    for(unsigned i=1; i<n; ++i)
    {
        for(unsigned j=0; j<i; ++j)
        {
            if( array[i] < array[j] )
            {
                T temp = array[i];
                for(unsigned k=i+1; k-->(j+1);)
                    array[k] = array[k-1];
                array[j] = temp;
            }
        }
    }
}

template <typename T>
void shellInsSort( T array[], const unsigned n )
{
    unsigned gap = n/2;

    while( gap > 0 )
    {
        for(unsigned i=0; i<=gap; ++i)
        {
            unsigned lastIdx = i;
            CircularList<unsigned> idxs;
            idxs.push(lastIdx);
            while( (lastIdx += gap) < n )
                idxs.push(lastIdx);

            T* subArray = new T[idxs.size()];
            unsigned subIdx = 0;
            for(unsigned i=0; i<idxs.size(); ++i) //for(auto idx : idxs)
                subArray[subIdx++] = array[idxs[i]];

            insertionSort(subArray,idxs.size());

            for(subIdx=0; subIdx<idxs.size(); ++subIdx)
                array[idxs[subIdx]] = subArray[subIdx];
        }

        gap /= 2;
    }
}

template <typename T, void_t<decltype(declval<T>() < declval<T>())>* = nullptr>
void shellSort( T array[], const unsigned n )
{
    int gap = n-1;

    while( gap > 0 ) {
        for(int i=n-1; i>=0; --i) {
            int j = i-gap;

            T temp = array[i];
            while( j >= 0 && array[i] < array[j] ) {
                array[j+gap] = array[j];
                j-=gap;
            }

            array[j+gap] = temp;
        }
        gap /= 2;
    }
}

//////////////////////////////////////////////// The driver function /////////////////////////////////////////////////

int main()
{
    // Input array
    int intArray[] = { 29, 17, 87, 91, 44, 32, 73, -5 };
    unsigned iSize = sizeof(intArray) / sizeof(int);
    printArray(intArray,std::cout,iSize);

    // Sorting by insertion
    insertionSort(intArray,iSize);
    printArray(intArray,std::cout,iSize);

    // Input array
    float floatArray[] = { 0.2f, -7.8f, 28.4f, 77.3f, 31.2f, 11.9f, 19.2f, 2.0f };
    unsigned fSize = sizeof(floatArray) / sizeof(float);
    printArray(floatArray,std::cout,fSize);

    // Sorting using shell
    shellInsSort(floatArray,fSize);
    printArray(floatArray,std::cout,fSize);

    // CircularList test
    double doubleArray[] = { 4.5, 6.7, 8.9 };
    CircularList<double> cList{doubleArray, 3};
    cList.insert(3.4, 0);
    cList.insert(1.2, 4);
    cList.remove(0);

    std::cout << "Lists's size: " << cList.size() << "\n";
    while(!cList.isEmpty())
        std::cout << cList.pop() << "\n";

    return 0;
}
