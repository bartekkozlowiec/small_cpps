#include <iostream>

struct AA  // user-defined identifier space
{
    void opis() { std::cout << "Jestem obiektem klasy AA." << std::endl; }
};

typedef struct
{
    void opis() { std::cout << "Jestem obiektem anonimowej klasy znanej jako BB." << std::endl; }
} BB;  // global identifier space

typedef struct C // user-defined identifier space
{
    void opis() { std::cout << "Jestem obiektem klasy C i znanej jako CC." << std::endl; }
} CC;  // global identifier space

void AA() { std::cout << "Globalna funkcja AA()." << std::endl; }

//void BB() {}

//void CC() {}

namespace test
{
void BB() { std::cout << "Funkcja test::BB()." << std::endl; }
void CC() { std::cout << "Funkcja test::AA()." << std::endl;}
void ff()
{
    struct AA a;
    a.opis();
    AA();  // == ::AA();
    BB();
    //struct BB b;
    CC();
    struct C c;
    c.opis();
}
}

int main()
{
    std::cout << "Zakres przestrzeni nazw 'test':" << std::endl;
    test::ff();

    std::cout << "Zakres globalny:" << std::endl;
    struct AA a;
    a.opis();
    BB b;
    b.opis();
    C c;
    c.opis();
    CC cc;
    cc.opis();

    return 0;
}
