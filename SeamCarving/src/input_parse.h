/* This file contains the declaration of the parsing function used by the seam-carving program
 * and the definition of the error message which may be thrown by it. The function was made
 * a part of the sc library to allow testing its outcome with the unit tests gathered inside the
 * tests/test_parsing.cpp file.
 *
 * Author: Bartek Kozlowiec
 * Date: 2021/02/12
 */

#ifndef INPUT_PARSE_H
#define INPUT_PARSE_H

#include <vector>
#include <string>

constexpr const char sizeArgMessage[] = "Invalid size argument given! Should be of format NxM, where N,M are integers. Aborting...\n";

std::vector<std::string> parseInputs( const int, const char** );

#endif // INPUT_PARSE_H
