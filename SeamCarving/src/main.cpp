/*
 * This is file contains the main seam_carving driver function.
 *
 * Author: Bartek Kozlowiec
 * Date: 2021/02/12
 */

#include <opencv2/highgui.hpp>

#include <cstdio>
#include <vector>
#include <string>

#include "seam_carving.h"
#include "input_parse.h"

// Useful links:
// https://perso.crans.org/frenoy/matlab2012/seamcarving.pdf
// https://answers.opencv.org/question/173045/glcm-texture-analysis-using-opencv-c/
// https://stackoverflow.com/questions/29696805/what-is-the-best-way-to-remove-a-row-or-col-from-a-cv-mat
// https://stackoverflow.com/questions/44794678/c-opencv-how-to-delete-specific-path-pixels-in-efficient-way
// https://answers.opencv.org/question/180503/energy-computation-of-dct-of-image/
// https://sodocumentation.net/opencv/topic/1957/pixel-access
// https://docs.opencv.org/master/d3/d63/classcv_1_1Mat.html

int main(const int argc, const char** argv) {
    if( argc < 3 || argc > 4 ) {
        fprintf(stderr, "Wrong number of input arguments given! Closing...\n");
        return EXIT_FAILURE;
    }

    std::vector<std::string> parsedStrings;
    try {
        parsedStrings = parseInputs(argc, argv);
    } catch( const std::runtime_error& e ) {
        fprintf(stderr, "Error: %s", e.what());
        return EXIT_FAILURE;
    }

    const auto in = cv::imread(parsedStrings[2]);

    if( in.empty() ) {
        fprintf(stderr, "Error opening image: %s. Closing...\n", argv[1]);
        return EXIT_FAILURE;
    }

    printf("Carving started, please wait...\n"); fflush(stdout);
    const auto out = seamCarvingDyn(in, cv::Size(std::stoi(parsedStrings[0]), std::stoi(parsedStrings[1])));
    cv::imwrite(parsedStrings[3], out);
    return 0;
}
