/* This file contains the implementation of the parsing function used by the seam-carving program.
 *
 * Author: Bartek Kozlowiec
 * Date: 2021/02/21
 */

#include "input_parse.h"
#include <cstdio>
#include <stdexcept>

std::vector<std::string> parseInputs( const int argc, const char** argv )
{
    std::vector<std::string> retStrings;

    const std::string sizeArg{argv[1]};
    if( sizeArg.find('x') == std::string::npos )
        throw std::runtime_error(sizeArgMessage);

    const std::string widthArg = sizeArg.substr(0, sizeArg.find('x'));
    const std::string heightArg = sizeArg.substr(sizeArg.find('x')+1);
    if( !widthArg.length() || !heightArg.length() )
        throw std::runtime_error(sizeArgMessage);

    printf("SIZE STRINGS: %s %s\n",widthArg.c_str(),heightArg.c_str());

    retStrings.push_back(widthArg);
    retStrings.push_back(heightArg);
    retStrings.push_back(argv[2]);

    if( argc < 4 )
        retStrings.push_back("./output.png");
    else {
        const std::string outArg{argv[3]};
        if( outArg.find('.') == std::string::npos )
            retStrings.push_back(outArg+".png");
        else
            retStrings.push_back(outArg);
    }

    return retStrings;
}
