/*
 * This file contains implementation of SeamCreator class methods and the free functions declared in
 * seam_carving.h header file.
 *
 * Author: Bartek Kozlowiec
 * Date: 2021/02/12
 */

#include "seam_carving.h"

#include <vector>
#include <thread>
#include <future>
#include <cstdio>

cv::Mat seamCarving(const cv::Mat& image, const cv::Size& out_size)
{
    //(void)out_size;

    cv::Mat carved = image;
    unsigned iter=0;

    while( carved.size().width != out_size.width && carved.size().height != out_size.height ) {
        SeamCreator sc{carved, SeamCreator::Mode::dual};
        auto hvSeam = sc.getSeam();
        printf("ITER_NO (H/V): %u\n",iter++); fflush(stdout);

        if( std::get<1>(hvSeam) == true ) // GO HORIZONTAL
            carved = expandOrShrink(carved, std::get<2>(hvSeam), carved.size().height, out_size.height, true);
        else // GO VERTICAL
            carved = expandOrShrink(carved, std::get<2>(hvSeam), carved.size().width, out_size.width, false);
    }

    while( carved.size().width != out_size.width ) {
        SeamCreator sc{carved, SeamCreator::Mode::vertical};
        std::tuple<float,bool,std::vector<int> > vertSeam = sc.getSeam();
        printf("ITER_NO (V): %u\n",iter++); fflush(stdout);
        //std::vector<int> indices(carved.rows, 0);

        carved = expandOrShrink(carved, std::get<2>(vertSeam), carved.size().width, out_size.width, false);
    }

    while( carved.size().height != out_size.height ) {
        SeamCreator sc{carved, SeamCreator::Mode::horizontal};
        std::tuple<float,bool,std::vector<int> > horSeam = sc.getSeam();
        printf("ITER_NO (H): %u\n",iter++); fflush(stdout);
        //std::vector<int> indices(carved.cols, 0);

        carved = expandOrShrink(carved, std::get<2>(horSeam), carved.size().height, out_size.height, true);
    }
    printf("FINISHED!\n");

    return carved;
}

cv::Mat seamCarvingDyn(const cv::Mat& image, const cv::Size& out_size)
{
    cv::Mat carved = image;
    unsigned iter=0;

    if( carved.size().height != out_size.height && carved.size().width != out_size.width )
    {
        std::vector<std::vector<cv::Mat> > Tmap;
        Tmap.push_back(std::vector<cv::Mat>{});
        Tmap.back().push_back(carved);

        while( Tmap.back().back().size().width != out_size.width ) {
            SeamCreator sc{Tmap.back().back(), SeamCreator::Mode::vertical};
            std::tuple<float,bool,std::vector<int> > vertSeam = sc.getSeam();
            Tmap.back().push_back(expandOrShrink(Tmap.back().back(), std::get<2>(vertSeam), Tmap.back().back().size().width, out_size.width, false));
            printf("ITER_NO: %u\n",iter++); fflush(stdout);
        }
        while( Tmap.back()[0].size().height != out_size.height ) {
            SeamCreator sc{Tmap.back()[0], SeamCreator::Mode::horizontal};
            std::tuple<float,bool,std::vector<int> > horSeam = sc.getSeam();
            Tmap.push_back(std::vector<cv::Mat>{});
            Tmap.back().push_back(expandOrShrink(Tmap.end()[-2][0], std::get<2>(horSeam), Tmap.end()[-2][0].size().height, out_size.height, true));
            printf("ITER_NO: %u\n",iter++); fflush(stdout);
        }

        const long unsigned r = Tmap.size();
        const long unsigned c = Tmap[0].size();
        for(unsigned i=1; i<r; ++i) {
            for(unsigned j=1; j<c; ++j) {
                SeamCreator scv{Tmap[i][j-1], SeamCreator::Mode::vertical};
                SeamCreator sch{Tmap[i-1][j], SeamCreator::Mode::horizontal};
                std::tuple<float,bool,std::vector<int> > vertSeam = scv.getSeam();
                std::tuple<float,bool,std::vector<int> > horSeam = sch.getSeam();

                if( std::get<0>(vertSeam) < std::get<0>(horSeam) )
                    Tmap[i].push_back(expandOrShrink(Tmap[i][j-1], std::get<2>(vertSeam), Tmap[i][j-1].size().width, out_size.width, false));
                else
                    Tmap[i].push_back(expandOrShrink(Tmap[i-1][j], std::get<2>(horSeam), Tmap[i-1][j].size().height, out_size.height, true));
                printf("ITER_NO: %u\n",iter++); fflush(stdout);
            }
        }

        carved = Tmap[r-1][c-1];
    }

    while( carved.size().width != out_size.width ) {
        SeamCreator sc{carved, SeamCreator::Mode::vertical};
        std::tuple<float,bool,std::vector<int> > vertSeam = sc.getSeam();

        carved = expandOrShrink(carved, std::get<2>(vertSeam), carved.size().width, out_size.width, false);
    }

    while( carved.size().height != out_size.height ) {
        SeamCreator sc{carved, SeamCreator::Mode::horizontal};
        std::tuple<float,bool,std::vector<int> > horSeam = sc.getSeam();

        carved = expandOrShrink(carved, std::get<2>(horSeam), carved.size().height, out_size.height, true);
    }
    printf("FINISHED!\n");

    return carved;
}

cv::Mat expandOrShrink( const cv::Mat& srcMat, const std::vector<int>& seam, const int currSize, const int outSize,
                        const bool horizontal )
{
    if( horizontal ) {
        if( currSize > outSize )
            return copyMatExceptHSeam(srcMat, seam, cv::Vec3b{});
        else
            return copyMatWithHSeam(srcMat, seam, cv::Vec3b{});
    }
    else {
        if( currSize > outSize )
            return copyMatExceptVSeam(srcMat, seam, cv::Vec3b{});
        else
            return copyMatWithVSeam(srcMat, seam, cv::Vec3b{});
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SeamCreator::SeamCreator( const cv::Mat& img, const Mode mode ) : imgRows{img.rows}, imgCols{img.cols},
    threadsNo{ std::thread::hardware_concurrency() }
{
    if( threadsNo == 0 )
        threadsNo = 2;

    cv::Mat imgGray;
    //cv::GaussianBlur(img, imgBlur, cv::Size(3, 3), 0, 0, cv::BORDER_DEFAULT);
    cv::cvtColor(img, imgGray, cv::COLOR_BGR2GRAY);

    cv::Sobel(imgGray, gradsX, CV_32FC1, 1, 0, 3);
    cv::Sobel(imgGray, gradsY, CV_32FC1, 0, 1, 3);

    //cv::convertScaleAbs(rawGradsX, gradsX);
    //cv::convertScaleAbs(rawGradsY, gradsY);

    switch( mode )
    {
    case Mode::horizontal:
        generateSeamH();
        break;
    case Mode::vertical:
        generateSeamV();
        break;
    case Mode::dual:
        generateSeamH();
        generateSeamV();
    }
}

void SeamCreator::generateSeamH()
{
    std::vector<float> partialEnergies;
    std::vector<std::vector<int> > paths;

    for(int j=0; j<imgRows; ++j) {
        partialEnergies.push_back(pixEnergy(0,j));
        paths.push_back(std::vector<int>{});
        paths.back().push_back(j);
    }

    for(int i=1; i<imgCols; ++i) {
        const auto pathsSoFar = paths;
        const auto pensSoFar = partialEnergies;
        for(int j=0; j<imgRows; ++j) {
            std::vector<std::pair<std::vector<int>,float> > prevs = { std::make_pair(pathsSoFar[j], pensSoFar[j]) };

            if( j > 0 ) prevs.push_back(std::make_pair(pathsSoFar[j-1], pensSoFar[j-1]));
            if( j < imgRows-1 ) prevs.push_back(std::make_pair(pathsSoFar[j+1], pensSoFar[j+1]));

            const auto minEl = *std::min_element(prevs.begin(), prevs.end(),
                                                 []( const std::pair<std::vector<int>,float>& p1, const std::pair<std::vector<int>,float>& p2 ){ return p1.second < p2.second; });
            partialEnergies[j] = minEl.second + pixEnergy(i,j);
            paths[j] = minEl.first;
            paths[j].push_back(j);
        }
    }

    const auto minEnergyIt = std::min_element(partialEnergies.begin(), partialEnergies.end());
    const auto minEnergyIdx = std::distance(partialEnergies.begin(), minEnergyIt);
    seams[*minEnergyIt] = std::make_pair(true, paths[minEnergyIdx]);
}

void SeamCreator::generateSeamV()
{
    std::vector<float> partialEnergies;
    std::vector<std::vector<int> > paths;

    for(int i=0; i<imgCols; ++i) {
        partialEnergies.push_back(pixEnergy(i,0));
        paths.push_back(std::vector<int>{});
        paths.back().push_back(i);
    }

    for(int j=1; j<imgRows; ++j) {
        const auto pathsSoFar = paths;
        const auto pensSoFar = partialEnergies;
        for(int i=0; i<imgCols; ++i) {
            std::vector<std::pair<std::vector<int>,float> > prevs = { std::make_pair(pathsSoFar[i], pensSoFar[i]) };

            if( i > 0 ) prevs.push_back(std::make_pair(pathsSoFar[i-1], pensSoFar[i-1]));
            if( i < imgCols-1 ) prevs.push_back(std::make_pair(pathsSoFar[i+1], pensSoFar[i+1]));

            const auto minEl = *std::min_element(prevs.begin(), prevs.end(),
                                                 []( const std::pair<std::vector<int>,float>& p1, const std::pair<std::vector<int>,float>& p2 ){ return p1.second < p2.second; });
            partialEnergies[i] = minEl.second + pixEnergy(i,j);
            paths[i] = minEl.first;
            paths[i].push_back(i);
        }
    }

    const auto minEnergyIt = std::min_element(partialEnergies.begin(), partialEnergies.end());
    const auto minEnergyIdx = std::distance(partialEnergies.begin(), minEnergyIt);
    seams[*minEnergyIt] = std::make_pair(false, paths[minEnergyIdx]);
}

void SeamCreator::generateSeamsH()
{
    // CONCURRENCY HERE!!
    spawnSeamsGeneration(&SeamCreator::calculateSeamH, imgRows, true);
}

void SeamCreator::generateSeamsV()
{
    // CONCURRENCY HERE!!
    spawnSeamsGeneration(&SeamCreator::calculateSeamV, imgCols, false);
}

void SeamCreator::spawnSeamsGeneration( std::pair<float,std::vector<int> > (SeamCreator::*f)( const int ) const,
                                        const int size, const bool horiz )
{
    std::vector<std::pair<float,std::vector<int> > > seamsVec;
    std::vector<std::future<std::pair<float,std::vector<int> > > > futuresVec;

    for(int i=0; i<size; ++i) {
        if( (i+1) % threadsNo )
            futuresVec.push_back(std::async(f, this, i));
        else {
            seamsVec.push_back((this->*f)(i));
            while( futuresVec.size() ) {
                seamsVec.push_back(futuresVec.back().get());
                futuresVec.pop_back();
            }
        }
    }
    if( futuresVec.size() ) {
        seamsVec.push_back(futuresVec.back().get());
        futuresVec.pop_back();
    }

    for( const auto& seam : seamsVec )
        seams[seam.first] = std::make_pair(horiz, seam.second);
}

std::pair<float,std::vector<int> > SeamCreator::calculateSeamH( const int j ) const
{
    float energy = 0.0f;
    std::vector<int> jPath{j};

    energy += pixEnergy(0, j);
    for(int i=0; i<(imgCols-1); ++i) {
        auto idxEnergy = findNextPixelH(i, jPath.back());
        jPath.push_back(idxEnergy.first);
        energy += idxEnergy.second;
    }

    return std::make_pair(energy, jPath);
}

std::pair<float,std::vector<int> > SeamCreator::calculateSeamV( const int i ) const
{
    float energy = 0.0f;
    std::vector<int> iPath{i};

    energy += pixEnergy(i, 0);
    for(int j=0; j<(imgRows-1); ++j) {
        auto idxEnergy = findNextPixelV(iPath.back(), j);
        iPath.push_back(idxEnergy.first);
        energy += idxEnergy.second;
    }

    return std::make_pair(energy, iPath);
}

std::pair<int,float> SeamCreator::findNextPixelH( const int i, const int j ) const
{
    int nextI = i+1, nextJ = j;

    float energy = pixEnergy(nextI, nextJ);

    std::vector<int> nextJs;
    if( j == 0 )
        nextJs.push_back(j+1);
    else if( j == (imgRows-1) )
        nextJs.push_back(j-1);
    else {
        nextJs.push_back(j+1);
        nextJs.push_back(j-1);
    }

    for( const auto& nJ : nextJs ) {
        float temp = pixEnergy(nextI,nJ);
        if( temp < energy ) {
            energy = temp;
            nextJ = nJ;
        }
    }

    return std::make_pair(nextJ, energy);
}

std::pair<int,float> SeamCreator::findNextPixelV( const int i, const int j ) const
{
    int nextI = i, nextJ = j+1;

    float energy = pixEnergy(nextI, nextJ);

    std::vector<int> nextIs;
    if( i == 0 )
        nextIs.push_back(i+1);
    else if( i == (imgCols-1) )
        nextIs.push_back(i-1);
    else {
        nextIs.push_back(i+1);
        nextIs.push_back(i-1);
    }

    for( const auto& nI : nextIs ) {
        float temp = pixEnergy(nI,nextJ);
        if( temp < energy ) {
            energy = temp;
            nextI = nI;
        }
    }

    return std::make_pair(nextI, energy);
}

std::tuple<float,bool,std::vector<int> > SeamCreator::getSeam() const
{
    auto minIter = std::min_element(seams.begin(), seams.end(), [](const auto l, const auto r){ return l.first < r.first; });
    if( minIter == seams.end() )
        fprintf(stderr, "Error: no seams calculated for the image!\n");

    return std::make_tuple(minIter->first, minIter->second.first, minIter->second.second);
}
