/*
 * This file contains unit tests which check the 'sc' library's seam creation
 * behaviour (the 'energyTest' test suite) and the boolean operations on
 * OpenCV matrices using seams (the 'resizeTests' test suite).
 *
 * Author: Bartek Kozlowiec
 * Date: 2021/02/12
 */

#include "gtest/gtest.h"
#include "../seam_carving.h"

#include <iterator>
#include <functional>

TEST(resizeTests, copyWithHSeamFirst)
{
    const int r = 5, c = 10;
    const cv::Mat srcMat(r, c, CV_8UC3);
    const std::vector<int> horSeam(c, 0);
    const cv::Mat destMat = copyMatWithHSeam(srcMat, horSeam, cv::Vec3b{});

    for(int i=0; i<c; ++i)
        EXPECT_EQ(destMat.at<cv::Vec3b>(0,i), destMat.at<cv::Vec3b>(1,i));
}

TEST(resizeTests, copyWithHSeamLast)
{
    const int r = 5, c = 10;
    const cv::Mat srcMat(r, c, CV_8UC3);
    const std::vector<int> horSeam(c, r-1);
    const cv::Mat destMat = copyMatWithHSeam(srcMat, horSeam, cv::Vec3b{});

    for(int i=0; i<c; ++i)
        EXPECT_EQ(destMat.at<cv::Vec3b>(r-1,i), destMat.at<cv::Vec3b>(r,i));
}

TEST(resizeTests, copyExceptHSeamFirst)
{
    const int r = 5, c = 10;
    const cv::Mat srcMat(r, c, CV_8UC3);
    const std::vector<int> horSeam(c, 0);
    const cv::Mat destMat = copyMatExceptHSeam(srcMat, horSeam, cv::Vec3b{});

    for(int i=0; i<c; ++i)
        EXPECT_EQ(destMat.at<cv::Vec3b>(0,i), srcMat.at<cv::Vec3b>(1,i));
}

TEST(resizeTests, copyExceptHSeamLast)
{
    const int r = 5, c = 10;
    const cv::Mat srcMat(r, c, CV_8UC3);
    const std::vector<int> horSeam(c, r-1);
    const cv::Mat destMat = copyMatExceptHSeam(srcMat, horSeam, cv::Vec3b{});

    for(int i=0; i<c; ++i)
        EXPECT_EQ(destMat.at<cv::Vec3b>(r-2,i), srcMat.at<cv::Vec3b>(r-2,i));
}

TEST(resizeTests, copyWithVSeamFirst)
{
    const int r = 5, c = 10;
    const cv::Mat srcMat(r, c, CV_8UC3);
    const std::vector<int> vertSeam(r, 0);
    const cv::Mat destMat = copyMatWithVSeam(srcMat, vertSeam, cv::Vec3b{});

    for(int j=0; j<r; ++j)
        EXPECT_EQ(destMat.at<cv::Vec3b>(j,0), destMat.at<cv::Vec3b>(j,1));
}

TEST(resizeTests, copyWithVSeamLast)
{
    const int r = 5, c = 10;
    const cv::Mat srcMat(r, c, CV_8UC3);
    const std::vector<int> vertSeam(r, c-1);
    const cv::Mat destMat = copyMatWithVSeam(srcMat, vertSeam, cv::Vec3b{});

    for(int j=0; j<r; ++j)
        EXPECT_EQ(destMat.at<cv::Vec3b>(j,c-1), destMat.at<cv::Vec3b>(j,c));
}

TEST(resizeTests, copyExceptVSeamFirst)
{
    const int r = 5, c = 10;
    const cv::Mat srcMat(r, c, CV_8UC3);
    const std::vector<int> vertSeam(r, 0);
    const cv::Mat destMat = copyMatExceptVSeam(srcMat, vertSeam, cv::Vec3b{});

    for(int j=0; j<r; ++j)
        EXPECT_EQ(destMat.at<cv::Vec3b>(j,0), srcMat.at<cv::Vec3b>(j,1));
}

TEST(resizeTests, copyExceptVSeamLast)
{
    const int r = 5, c = 10;
    const cv::Mat srcMat(r, c, CV_8UC3);
    const std::vector<int> vertSeam(r, c-1);
    const cv::Mat destMat = copyMatExceptVSeam(srcMat, vertSeam, cv::Vec3b{});

    for(int j=0; j<r; ++j)
        EXPECT_EQ(destMat.at<cv::Vec3b>(j,c-2), srcMat.at<cv::Vec3b>(j,c-2));
}

TEST(energyTests, checkHSeamEnergy)
{
    const int r = 5, c = 10;
    const cv::Mat img(r, c, CV_8UC3);

    SeamCreator sc{img, SeamCreator::Mode::horizontal};
    auto seam = sc.getSeam();
    EXPECT_EQ(std::get<2>(seam).size(), static_cast<unsigned long>(c));

    std::vector<std::pair<int,float> > energies;
    for(int j=0; j<r; ++j)
        energies.push_back(std::make_pair(j, 0.0f));

    const std::function<void(const int, const int)> updateAndSpawn = [&]( const int currIdx, const int currCol ){
        if( currCol == c )
            return;

        energies[currIdx].second += sc.pixEnergy(currCol, energies[currIdx].first);
        const auto currEnergy = energies[currIdx].second;
        updateAndSpawn(currIdx, currCol+1);

        if( energies[currIdx].first == 0 ) {
            energies.push_back(std::make_pair(energies[currIdx].first+1, currEnergy));
            updateAndSpawn(energies.size()-1, currCol+1);
        }
        else if( energies[currIdx].first == r-1 ) {
            energies.push_back(std::make_pair(energies[currIdx].first-1, currEnergy));
            updateAndSpawn(energies.size()-1, currCol+1);
        }
        else {
            energies.push_back(std::make_pair(energies[currIdx].first+1, currEnergy));
            updateAndSpawn(energies.size()-1, currCol+1);
            energies.push_back(std::make_pair(energies[currIdx].first-1, currEnergy));
            updateAndSpawn(energies.size()-1, currCol+1);
        }
    };

    for(int j=0; j<r; ++j) updateAndSpawn(j, 0);

    std::vector<unsigned> indices;
    for(unsigned j=0; j<r; ++j) indices.push_back(j);
    for(unsigned i=0; i<c; ++i) {
        for(const auto idx : indices) {
            if( idx == 0 )
                indices.push_back(1);
            else if( idx == (r-1) )
                indices.push_back(r-2);
            else {
                indices.push_back(idx-1);
                indices.push_back(idx+1);
            }
        }
    }
    EXPECT_EQ(indices.size(), energies.size());

    auto minEnergy = std::min_element(energies.begin(),
                                      energies.end(),
                                      [](const auto l, const auto r){ return l.second < r.second; });

    float seamEnergy = 0.0f;
    for(int i=0; i<c; ++i)
        seamEnergy += sc.pixEnergy(i, std::get<2>(seam)[i]);

    EXPECT_FLOAT_EQ(std::get<0>(seam), seamEnergy);
    EXPECT_FLOAT_EQ(std::get<0>(seam), minEnergy->second);
}

TEST(energyTests, checkVSeamEnergy)
{
    const int r = 5, c = 10;
    const cv::Mat img(r, c, CV_8UC3);

    SeamCreator sc{img, SeamCreator::Mode::vertical};
    auto seam = sc.getSeam();
    EXPECT_EQ(std::get<2>(seam).size(), static_cast<unsigned long>(r));

    std::vector<std::pair<int,float> > energies;
    for(int i=0; i<c; ++i)
        energies.push_back(std::make_pair(i, 0.0f));

    const std::function<void(const int, const int)> updateAndSpawn = [&]( const int currIdx, const int currRow ){
        if( currRow == r )
            return;

        energies[currIdx].second += sc.pixEnergy(energies[currIdx].first, currRow);
        const auto currEnergy = energies[currIdx].second;
        updateAndSpawn(currIdx, currRow+1);

        if( energies[currIdx].first == 0 ) {
            energies.push_back(std::make_pair(energies[currIdx].first+1, currEnergy));
            updateAndSpawn(energies.size()-1, currRow+1);
        }
        else if( energies[currIdx].first == c-1 ) {
            energies.push_back(std::make_pair(energies[currIdx].first-1, currEnergy));
            updateAndSpawn(energies.size()-1, currRow+1);
        }
        else {
            energies.push_back(std::make_pair(energies[currIdx].first+1, currEnergy));
            updateAndSpawn(energies.size()-1, currRow+1);
            energies.push_back(std::make_pair(energies[currIdx].first-1, currEnergy));
            updateAndSpawn(energies.size()-1, currRow+1);
        }
    };

    for(int i=0; i<c; ++i) updateAndSpawn(i, 0);

    std::vector<unsigned> indices;
    for(unsigned i=0; i<c; ++i) indices.push_back(i);
    for(unsigned j=0; j<r; ++j) {
        for(const auto idx : indices) {
            if( idx == 0 )
                indices.push_back(1);
            else if( idx == (c-1) )
                indices.push_back(c-2);
            else {
                indices.push_back(idx-1);
                indices.push_back(idx+1);
            }
        }
    }
    EXPECT_EQ(indices.size(), energies.size());

    auto minEnergy = std::min_element(energies.begin(),
                                      energies.end(),
                                      [](const auto l, const auto r){ return l.second < r.second; });

    float seamEnergy = 0.0f;
    for(int j=0; j<r; ++j)
        seamEnergy += sc.pixEnergy(std::get<2>(seam)[j], j);

    EXPECT_FLOAT_EQ(std::get<0>(seam), seamEnergy);
    EXPECT_FLOAT_EQ(std::get<0>(seam), minEnergy->second);
}
