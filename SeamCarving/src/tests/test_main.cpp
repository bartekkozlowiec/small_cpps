/*
 * This file contains the main driver function for the unit tests suite 'sc-tests'
 * of the 'sc' library functions and class methods.
 * The GoogleTest framework is a required dependency for the 'sc-tests' sub-project.
 *
 * Author: Bartek Kozlowiec
 * Date: 2021/02/12
 */

#include "gtest/gtest.h"

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
