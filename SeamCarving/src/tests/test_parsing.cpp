/*
 * This file contains unit tests which check the behaviour of the 'seam-carving' program's
 * argument parsing function.
 *
 * Author: Bartek Kozlowiec
 * Date: 2021/02/12
 */

#include "gtest/gtest.h"
#include "../input_parse.h"

TEST(parsingTests, inputArgs)
{
    const int argc = 3;
    const char* argv[] = {"./seam_carving", "640x480", "input.png"};
    EXPECT_EQ(parseInputs(argc,argv).size(), 4ul);
}

TEST(parsingTests, inputOutputArgs1)
{
    const int argc = 4;
    const char* argv[] = {"./seam_carving", "640x480", "input.png", "output.png"};
    EXPECT_EQ(parseInputs(argc,argv).size(), 4ul);
}

TEST(parsingTests, inputOutputArgs2)
{
    const int argc = 4;
    const char* argv[] = {"./seam_carving", "640x480", "input.png", "output"};
    EXPECT_EQ(parseInputs(argc,argv).size(), 4ul);
}

TEST(parsingTests, inputOutputArgs3)
{
    const int argc = 4;
    const char* argv[] = {"./seam_carving", "640x480", "input.png", "output"};
    auto parsedStrings = parseInputs(argc,argv);
    EXPECT_EQ(parsedStrings[3], "output.png");
}

TEST(parsingTests, wrongArgs1)
{
    const int argc = 3;
    const char* argv[] = {"./seam_carving", "640x", "input.png"};

    try {
        parseInputs(argc,argv);
        FAIL() << "Expected std::runtime_error";
    } catch( const std::runtime_error& e ) {
        EXPECT_EQ(e.what(), std::string{sizeArgMessage});
    }
    catch(...) {
        FAIL() << "Expected std::runtime_error";
    }
}

TEST(parsingTests, wrongArgs2)
{
    const int argc = 3;
    const char* argv[] = {"./seam_carving", "x480", "input.png"};

    try {
        parseInputs(argc,argv);
        FAIL() << "Expected std::runtime_error";
    } catch( const std::runtime_error& e ) {
        EXPECT_EQ(e.what(), std::string{sizeArgMessage});
    }
    catch(...) {
        FAIL() << "Expected std::runtime_error";
    }
}

TEST(parsingTests, wrongArgs3)
{
    const int argc = 3;
    const char* argv[] = {"./seam_carving", "12938", "input.png"};

    try {
        parseInputs(argc,argv);
        FAIL() << "Expected std::runtime_error";
    } catch( const std::runtime_error& e ) {
        EXPECT_EQ(e.what(), std::string{sizeArgMessage});
    }
    catch(...) {
        FAIL() << "Expected std::runtime_error";
    }
}
