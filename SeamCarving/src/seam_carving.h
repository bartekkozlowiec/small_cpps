/*
 * This file contains the definition of SeamCreator class, the declaration of seamCarving and expandOrShrink
 * free functions as well as the definition of four template functions: copyMatExceptVSeam, copyMatWithVSeam,
 * copyMatExceptHSeam, copyMatWithHSeam. These entities are part of the 'sc' seam carving library.
 * The seamCarving function is the entry point to all content-aware image resizing activities. The
 * SeamCreator class serves for creating horizontal and/or vertical seams (paths of lowest pixel energy).
 * The template functions perform boolean operations on OpenCV matrices.
 *
 * Author: Bartek Kozlowiec
 * Date: 2021/02/12
 */

#pragma once

#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>

#include <cstdio>
#include <utility>
#include <type_traits>
#include <unordered_map>
#include <cmath>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

cv::Mat seamCarving( const cv::Mat& image, const cv::Size& out_size );
cv::Mat seamCarvingDyn( const cv::Mat& image, const cv::Size& out_size );
cv::Mat expandOrShrink( const cv::Mat&, const std::vector<int>&, const int, const int, const bool );

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T, std::enable_if_t<!std::is_lvalue_reference<T>::value>* = nullptr>
cv::Mat copyMatExceptVSeam( const cv::Mat& srcMat, const std::vector<int>& seam, T&& )
{
    cv::Mat destMat(srcMat.rows, srcMat.cols-1, srcMat.type());
    int idx = 0;

    while( idx < destMat.rows ) {
        T* destRow = destMat.ptr<T>(idx);

        for(int i=0; i<destMat.cols; ++i) {
            if( seam[idx] > i )
                destRow[i] = srcMat.ptr<T>(idx)[i];
            else
                destRow[i] = srcMat.ptr<T>(idx)[i+1];
        }
        ++idx;
    }

    return destMat;
}

template <typename T, std::enable_if_t<!std::is_lvalue_reference<T>::value>* = nullptr>
cv::Mat copyMatWithVSeam( const cv::Mat& srcMat, const std::vector<int>& seam, T&& )
{
    cv::Mat destMat(srcMat.rows, srcMat.cols+1, srcMat.type());
    int idx = 0;

    while( idx < destMat.rows ) {
        T* destRow = destMat.ptr<T>(idx);

        for(int i=0; i<destMat.cols; ++i) {
            if( seam[idx] >= i )
                destRow[i] = srcMat.ptr<T>(idx)[i];
            else
                destRow[i] = srcMat.ptr<T>(idx)[i-1];
        }
        ++idx;
    }

    return destMat;
}

template <typename T, std::enable_if_t<!std::is_lvalue_reference<T>::value>* = nullptr>
cv::Mat copyMatExceptHSeam( const cv::Mat& srcMat, const std::vector<int>& seam, T&& )
{
    cv::Mat destMat(srcMat.rows-1, srcMat.cols, srcMat.type());
    int idx = 0;

    while( idx < destMat.rows ) {
        T* destRow = destMat.ptr<T>(idx);

        for(int i=0; i<srcMat.cols; ++i) {
            if( seam[i] != idx )
                destRow[i] = srcMat.ptr<T>(idx)[i];
            else {
                //if( seam[i] < srcMat.rows-1 )
                    destRow[i] = srcMat.ptr<T>(idx+1)[i];
                //else
                //    destRow[i] = srcMat.ptr<T>(idx-1)[i];
            }
        }
        ++idx;
    }

    return destMat;
}

template <typename T, std::enable_if_t<!std::is_lvalue_reference<T>::value>* = nullptr>
cv::Mat copyMatWithHSeam( const cv::Mat& srcMat, const std::vector<int>& seam, T&& )
{
    cv::Mat destMat(srcMat.rows+1, srcMat.cols, srcMat.type());
    int idx = 0;

    while( idx < destMat.rows ) {
        T* destRow = destMat.ptr<T>(idx);

        for(int i=0; i<srcMat.cols; ++i) {
            if( seam[i] >= idx )
                destRow[i] = srcMat.ptr<T>(idx)[i];
            else
                destRow[i] = srcMat.ptr<T>(idx-1)[i];
        }
        ++idx;
    }

    return destMat;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class SeamCreator
{
    const int imgRows, imgCols;
    std::size_t threadsNo;
    cv::Mat gradsX, gradsY;
    std::unordered_map<float,std::pair<bool,std::vector<int> > > seams;

    void generateSeamH();
    void generateSeamV();
    void generateSeamsH();
    void generateSeamsV();
    void spawnSeamsGeneration( std::pair<float,std::vector<int> > (SeamCreator::*f)( const int ) const,
                               const int, const bool );
    std::pair<float,std::vector<int> > calculateSeamH( const int ) const;
    std::pair<float,std::vector<int> > calculateSeamV( const int ) const;
    std::pair<int,float> findNextPixelH( const int, const int ) const;
    std::pair<int,float> findNextPixelV( const int, const int ) const;

public:
    enum class Mode
    {
        horizontal = 0,
        vertical = 1,
        dual = 2
    };

    explicit SeamCreator( const cv::Mat&, const Mode );
    std::tuple<float,bool,std::vector<int> > getSeam() const;
    float pixEnergy( const int i, const int j ) const { return std::abs(gradsX.at<float>(j,i))
                + std::abs(gradsY.at<float>(j,i)); }
};
