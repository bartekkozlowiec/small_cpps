# Project

This repository contains a working CMake based project you may use for writing your solution.

## Prerequisities

All packages necessary to build the project are defined inside the [Dockerfile](Dockerfile "Dockerfile") (Ubuntu 20.04 based).

### Docker Container

Building the docker image:
```
cd seam-carving
docker build . -t seamcarving
```

Running the docker container:
```
docker run -v $(pwd):/workdir -it seamcarving bash
```

The above command maps the current working directory from the host OS to the _/workdir_ directory inside the docker container.

## Building

Execute the following steps inside the docker container:
```
cd /workdir
cmake -S src -B build -G Ninja
cmake --build build
```

## Running

The following command runs the built _seam-carving_ binary (executed from the docker container):
```
/workdir/build/seam-carving NxM examples/castle.jpg [examples/output.jpg]
```
where NxM is the desired size of the image after resizing (in pixels). The last argument inside square brackets is optional, the default name of the output file is _output.png_.

The process requires path to the input image passed as a mandatory command line argument. The generated output image created by the seam-carving algorithm is saved down to the filesystem in the current runtime directory (directory the docker container has been run from). You may open that file from your host OS file to see the result.

## Implementation

The `seamCarving` function defined inside the [seam_carving.cpp](src/seam_carving.cpp "seam_carving.cpp") file is an entry point to the implementation.
The main driver program is implemented inside the [main.cpp](src/main.cpp "main.cpp"). All files gathered in the project contain short descriptions
of their contents written as comments on their first lines.

## Test suite

The project contains a subfolder with unit tests inside. The main unit tests' driver program is implemented inside the [test\_main.cpp](src/tests/test_main.cpp "test_main.cpp"). To build the test suite turn the BUILD\_TESTS variable in top-level CmakeLists.txt to ON. To run the _sc-tests_ binary please execute:
```
/workdir/build/tests/sc-tests
```

## Examples

Example images are located inside the [examples/](examples/ "Example input images") directory.

## 
