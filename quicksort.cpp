#include <iostream>

//////////////////////////////////////// Utilities to limit template functions' usage ////////////////////////////////////////

template <typename T>
struct void_t_impl
{
    typedef void type;
};

template <typename T>
using void_t = typename void_t_impl<T>::type;


template <bool Cond, typename T = void>
struct enable_if {};

template <typename T>
struct enable_if<true,T>
{
    typedef T type;
};

template <bool Cond, typename T = void>
using enable_if_t = typename enable_if<Cond,T>::type;

template <typename T>
T&& declval();

//////////////////////////////////// Wrapper class of an int to test declval correctness ////////////////////////////////////

class wrapperInt
{
    int content;

public:
    constexpr wrapperInt() : content{} {}
    constexpr wrapperInt( int arg ) : content{arg} {}
    constexpr int get() const { return content; }
};

//////////////////////////////////////// Template class to implement a static stack /////////////////////////////////////////

template <typename T, unsigned maxSize>
class StaticStack
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type storage[maxSize];
    unsigned size; // instead of top

public:
    constexpr StaticStack() : size{0} {}
    StaticStack( const T arr[], const unsigned n ) : size{0} { for(unsigned i=0; i<n; ++i) push(arr[i]); }
    ~StaticStack() { while( !isEmpty() ) pop(); }

    bool push( const T& );
    T pop();

    constexpr bool isEmpty() const { return size == 0; }
    constexpr bool isFull() const { return size == maxSize; }

    template <typename Str>
    void display( Str&& ostr, void_t<decltype(declval<Str>() << declval<T>())>* = nullptr );
};

template <typename T, unsigned maxSize>
bool StaticStack<T,maxSize>::push( const T& elem )
{
    bool pushed = isFull();

    if( !isFull() ) {
        T* ptr = new(storage+size) T{elem};
        ptr ? ++size : pushed = false;
    }

    return pushed;
}

template <typename T, unsigned maxSize>
T StaticStack<T,maxSize>::pop()
{
    T popped{};

    if( !isEmpty() ) {
        popped = *reinterpret_cast<T*>(storage+size-1);
        reinterpret_cast<T*>(storage+size-1)->~T();
        --size;
    }

    return popped;
}

template <typename T, unsigned maxSize>
template <typename Str>
void StaticStack<T,maxSize>::display( Str&& ostr, void_t<decltype(declval<Str>() << declval<T>())>* )
{
    for(unsigned i=0; i<size; ++i)
        ostr << *reinterpret_cast<T*>(storage+i) << " ";
    ostr << "\n";
}

//////////////////////////////////////// Template function to print fixed-size array ////////////////////////////////////////

template <typename T, typename Stream>
void printArray( const T array[], const unsigned n, Stream&& str,
                 void_t<decltype(declval<Stream>() << declval<T>())>* = nullptr )
{
    str << "Array contents: ";
    for(unsigned i=0; i<n; ++i)
        str << array[i] << " ";
    str << "\n";
}

/////////////////////////////////////////////////// Sorting algorithms //////////////////////////////////////////////////////

template <typename T>
void swap( T* const ptr1, T* const ptr2 )
{
    if( *ptr1 != *ptr2 ) {
        const T temp = std::move(*ptr1);
        *ptr1 = std::move(*ptr2);
        *ptr2 = std::move(temp);
    }
}

template <typename T>
void rQuickSort( T array[], const unsigned l, const unsigned r )
{
    if( r > l ) {
        unsigned p=l;
        unsigned i=l+1;
        unsigned j=r;

        while( i <= j ) {
            if( array[i] > array[p] && array[j] < array[p] ) {
                swap(array+i, array+j);
                ++i;
                --j;
            }
            else {
                if( array[i] < array[p] ) ++i;
                if( array[j] > array[p] ) --j;
            }
        }
        swap(array+p, array+j);

        if( p > 0 ) rQuickSort(array, l, p-1);
        rQuickSort(array, p+1, r);
    }
}

template <typename T>
void iQuickSort( T array[], const unsigned n )
{
    auto reposition = [array]( const unsigned l, const unsigned r ) -> unsigned {
        unsigned p=l, i=l+1, j=r;

        while( i <= j ) {
            if( array[i] > array[p] && array[j] < array[p] ) {
                swap(array+i, array+j);
                ++i;
                --j;
            }
            else {
                if( array[i] < array[p] ) ++i;
                if( array[j] > array[p] ) --j;
            }
        }
        swap(array+p, array+j);

        return j;
    };

    struct Interval {
        unsigned left, right;
    } first = {0, n-1};

    StaticStack<Interval,100> intervals;
    intervals.push(first);

    while( !intervals.isEmpty() ) {
        Interval currItrv = intervals.pop();
        unsigned pivot = reposition(currItrv.left, currItrv.right);
        if( pivot-currItrv.left > 1 ) intervals.push({currItrv.left, pivot-1});
        if( currItrv.right-pivot > 1 ) intervals.push({pivot+1, currItrv.right});
    }
}

//////////////////////////////////////////////// The main driver function ///////////////////////////////////////////////////

int main()
{
    // Array of floats
    float fArray[] = { 26.3f, -14.9f, 31.7f, 74.2f, -9.1f, 55.8f, 47.8f, 3.5f, 18.6f, 60.4f };
    unsigned fSize = sizeof(fArray) / sizeof(float);
    printArray(fArray, fSize, std::cout);

    iQuickSort(fArray, fSize);//rQuickSort(fArray, 0, fSize-1);
    printArray(fArray, fSize, std::cout);

    StaticStack<float,8> fStack(fArray, 8);
    fStack.display(std::cout);

    // Array of ints
    int iArray[] = { 3, 2, 1, 0 };
    unsigned iSize = sizeof(iArray) / sizeof(int);
    printArray(iArray, iSize, std::cout);

    iQuickSort(iArray, iSize);//rQuickSort(iArray, 0, iSize-1);
    printArray(iArray, iSize, std::cout);

    // Array of wrappedInts
    wrapperInt wiArray[] = { 3, 2, 1 };
    unsigned wiSize = sizeof(wiArray) / sizeof(wrapperInt);
    //printArray(wiArray,wiSize,std::cout); // WON'T COMPILE

    wrapperInt wInt{ 9 };
    wrapperInt& wIntRef1 = wInt;
    wrapperInt&& wIntRef2 = wrapperInt{ 9 };

    return 0;
}
