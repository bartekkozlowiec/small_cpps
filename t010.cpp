#include <iostream>
#include <cstring>

using namespace std;

/////////////////////////////////// Deklaracje i definicje klas ///////////////////////////////////

class numer;

class zespol
{
    float rzeczyw;
    float urojon;

public:
    //----konstruktor konwertujacy float-->zespol
    zespol( float r = 0, float i = 0 ) : rzeczyw(r), urojon(i)
    { }
    //----konstruktor konwertujacy numer-->zespol
    zespol( numer ob );

    //----dwa operatory konwersji
    operator float() { return rzeczyw; }
    operator numer();

    void pokaz()
    {
        cout << "\tLiczba zespolona: (" << rzeczyw << "," << urojon << ")\n";
    }

    //----operator odejmowania
    zespol& operator-( zespol );

    //----zaprzyjazniona funkcja globalna
    friend zespol operator+(zespol a, zespol b);
};

class numer
{
    float n;
    char* opis;

    //----deklaracja funkcji zaprzyjaznionych
    friend zespol::zespol( numer );
    friend void plakietka( numer );

public:
    //----konstruktor konwertujacy float-->numer
    numer( float k, const char* t = "opis domniemany" ) : n(k), opis(new char[strlen(t)+1])
    {
        strcpy(opis,t);
    }

    //----funkcja konwertujaca numer-->float
    operator float() { return n; }

    //----konstruktor kopiujacy
    numer( const numer& wzor ) : n(wzor.n), opis(new char[strlen(wzor.opis)+1])
    {
        strcpy(opis,wzor.opis);
    }

    //----operator przypisania
    numer& operator=( const numer& wzor )
    {
        if(this != &wzor)
        {
            delete [] this->opis;
            this->n    = wzor.n;
            this->opis = new char[strlen(wzor.opis)+1];
            strcpy(this->opis,wzor.opis);
        }
        return *this;
    }

    //----destruktor
    ~numer() { delete [] opis; }

    void wypis()
    {
        cout << "Obiekt klasy numer: n = " << n << "; opis = " << opis << endl;
    }
};

/////////////////////////////////// Definicje funkcji skladowych ///////////////////////////////////

zespol::zespol( numer ob ) : rzeczyw(ob.n), urojon(0)
{ }

zespol::operator numer()
{
    // Wywolanie konstruktora (nie-konwertujacego, bo wywolywanego z dwoma argumentami
    return numer(rzeczyw,"powstal z zespolonej");
}

zespol& zespol::operator-( zespol odjemna )
{
    this->rzeczyw -= odjemna.rzeczyw;
    this->urojon  -= odjemna.urojon;

    return *this;
}

/*///////////////////////////////// Deklaracje funkcji globalnych /////////////////////////////////*/

void   pole_kwadratu( float );
void   plakietka( numer );
zespol operator+( zespol, zespol );

/*///////////////////////////////// Definicje funkcji globalnych /////////////////////////////////*/

int main()
{
    // definicje trzech obiektow
    float  x  = 3.21;
    numer  nr(44,"a imie jego");
    zespol z(6,-2);

    // sprawdzenie poprawnosci konstr. kopiujacego, operatora przypisania i destruktora klasy numer
    numer* wsknr1 = new numer(0);
    numer* wsknr2 = new numer(nr);
    *wsknr1 = nr;
    wsknr1->wypis();
    wsknr2->wypis();
    delete wsknr1;
    delete wsknr2;

    // wywolania funkcji "pole_kwadratu"
    pole_kwadratu(x);      // niepotrzebna zadna konwersja
    pole_kwadratu(nr);     // niejawne wywolanie operatora konwersji numer-->float
    pole_kwadratu(z);      // niejawne wywolanie operatora konwersji zespol-->float
    cout << endl;

    // definicja dwoch roboczych obiektow
    zespol z2(4,5), wynik;

    // wywolanie globalnego operatora dodawania
    wynik = z + z2;        // niepotrzebna zadna konwersja
    wynik.pokaz();
    cout << endl;

    // wywolania operatora odejmowania jako funkcji skladowej klasy
    wynik = z - z2;
    wynik.pokaz();
    z.pokaz();
    cout << endl;

    // ponizsze wywolania nie sa dopasowane, ale mimo to mozliwe, bo kompilator samoczynnie zastosuje
    // nasze konwersje
    wynik = z + zespol(x); // jawne wywolanie konstr. konwertujacego float-->zespol (jawne, bo dwuznacznosc operatora+)
    wynik.pokaz();
    cout << endl;

    wynik = z + nr;        // niejawne wywolanie konstr. konwertujacego numer-->zespol
    wynik.pokaz();
    cout << endl;

    wynik = z - zespol(x); // jw.
    wynik.pokaz();
    z.pokaz();
    cout << endl;

    wynik = z - nr;        // jw.
    wynik.pokaz();
    z.pokaz();
    cout << endl;

    //------------------------------------------------------------------------------------------------

    // wywolania funkcji plakietka(numer)
    plakietka(nr);

    // ponizsze wywolania nie sa dopasowane, ale mimo to mozliwe, bo kompilator samoczynnie zastosuje
    // nasze konwersje
    plakietka(x);          // niejawne wywolanie konstr. konwertujacego float-->numer
    plakietka(z);          // niejawne wywolanie operatora konwersji zespol-->numer

    return 0;
}

zespol operator+( zespol a, zespol b )
{
    zespol chwilowy(a.rzeczyw + b.rzeczyw, a.urojon + b.urojon);
    return chwilowy;
}

void plakietka( numer nnn )
{
    cout << "******************************" << endl;
    cout << "*** ***\r"
         << "*** " << nnn.opis << endl;
    cout << "*** ***\r"
         << "*** " << nnn.n << endl;
    cout << "******************************" << endl;
}

void pole_kwadratu( float bok )
{
    cout << "Pole kwadratu o boku " << bok << " wynosi " << bok*bok << endl;
}
